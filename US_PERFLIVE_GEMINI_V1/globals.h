#ifndef _GLOBALS_H
#define _GLOBALS_H

//--------------------------------------------------------------------
// Include Files
#include "lrun.h"
#include "web_api.h"
#include "lrw_custom_body.h"

//--------------------------------------------------------------------
// Global Variables
int LINK_TT, FORM_TT, TYPE_SPEED; //Think Time
	//set global think time values for link clicks, form entry pages and type speed for typeAhead search.
	LINK_TT = 10;
	FORM_TT = 10;
	TYPE_SPEED = 1;
int authcookielen , authtokenlen; isLoggedIn ;
authcookielen = 0;	
int orderItemIdscount, onlineLogging = 0;

char  *VtsServer = "10.56.29.36";  //Server name or IP, don�t use localhost.
int   nPort = 8888;   //Port used when enabling the instance.
int   rc = 0;  // Return code, to make sure there were no errors.
PVCI2 pvci = 0;

//	long lastResponse, request, response, total = 0;
#define OPTIONSENABLED 0

int NAV_BROWSE, NAV_SEARCH, NAV_CLEARANCE, NAV_PLACE; //2nd level (T02) NAV_BY ratio
int DRILL_ONE_FACET, DRILL_TWO_FACET, DRILL_THREE_FACET, DRILL_SUB_CATEGORY; //3rd level (T03) DRILL_BY ratio
int PDP, QUICKVIEW, PRODUCT_QUICKVIEW_SERVICE, RESERVE_ONLINE, MIXED_CART_RATIO, ECOMM_CART_RATIO, NO_BROWSE_RATIO_HOME, NO_BROWSE_RATIO_CATEGORY, NO_BROWSE_RATIO_SUBCATEGORY, NO_BROWSE_RATIO_PDP, BOSS_CART_RATIO, MIXED_CART_RATIO; //Product Display vs Product QuickView ratio (T04)
int APPLY_SORT, RATIO_TCPSkuSelectionView; 	PDP_to_WL; ATC_FLAG; RATIO_ADDTOCART_LIMIT;  //Sort ratio
int RATIO_UPDATE_ITEM_TO_BOSS, APPLY_PAGINATE, RATIO_STORE_LOCATOR, RATIO_SEARCH_SUGGEST, API_SUB_TRANSACTION_SWITCH, ESPOT_FLAG, GETPOINTSSERVICE_FLAG, CACHE_PRIME_MODE; //Pagination ratio //USE_LOW_INVENTORY,
int hourMin = 0, ATC_FLAG_OVERRIDE, RATIO_USERGROUP, GETOFFERS, GETALLCOUPONS_RATIO, SUBMIT_ORDER,SFCC_CART_MERGE=0;

	CACHE_PRIME_MODE = 0; //1 - cacheprime only, 0 - full scenario mode
	GETOFFERS = 1; //1=yes/0=no
	//set 2nd level navigation ratio for T02 transaction. Sum must be 100. Valid value is 0 to 100.
	
	NAV_BROWSE = 88;  //95;
	NAV_SEARCH = 12; //5;
	NAV_CLEARANCE = 0;
	NAV_PLACE = 0;

	//set drill navigation ratio for T03 transaction. Sum must be 100. Valid value is 0 to 100.
	DRILL_ONE_FACET = 11; //35; //25;
	DRILL_TWO_FACET = 3; //10; //15;
	DRILL_THREE_FACET = 1; //10;
	DRILL_SUB_CATEGORY = 85; //50;
/*
	DRILL_ONE_FACET = 100; //25;
	DRILL_TWO_FACET = 0; //15;
	DRILL_THREE_FACET = 0; //10;
	DRILL_SUB_CATEGORY = 100;
*/
	//set the % for SORT during browse activity. Valid value is 0 to 100. Used only in Browse scripts,
	APPLY_SORT = 10;
	//set the % for PAGINATION during browse activity, this will apply pagination only if additional pages are available. Valid value is 0 to 100. Used only in Browse scripts.
	APPLY_PAGINATE = 10;    

	//set PDP vs PQV view ratio (T04). Sum must be 100. Valid value is 0 to 100.
	//82/18 as per the 11/30/2015 Omniture data
//	PDP = 72;
//	QUICKVIEW = 28;
	PDP = 72;
	QUICKVIEW = 0; //no more quickview?
	GETALLCOUPONS_RATIO = 10; //10->25k, 20->50k; 30->75k
	RATIO_STORE_LOCATOR  = 5;
	RATIO_SEARCH_SUGGEST = 0; //50; //Did You Mean? descoped on 06/15
	RATIO_TCPSkuSelectionView = 20;

	PRODUCT_QUICKVIEW_SERVICE = 15; //30; //50;
	RESERVE_ONLINE = 0; //ropis
	MIXED_CART_RATIO = 00;
	ECOMM_CART_RATIO = 100; //90;
	RATIO_UPDATE_ITEM_TO_BOSS = 00; //switch ecom items to boss order
	//To enable Bopis, we'll need to provide the catentryIDs{randCatEntryId_BOPIS} parameter datafile with catentryids from 07093 zip / storeLocId=111287
	BOPIS_CART_RATIO = 00;
	BOSS_CART_RATIO = 00;//this ratio is only between BOSS and BOPIS
	
	ATC_FLAG = 0; //0 for using datafile, 1 for using UI data
	ATC_FLAG_OVERRIDE = 0;
	RATIO_ADDTOCART_LIMIT = 50;
	RATIO_USERGROUP = 3;
		
	API_SUB_TRANSACTION_SWITCH = 1; // 1 = ON; 0 = OFF
	BROWSE_OPTIONS_FLAG = 0; // 1 = ON; 0 = OFF
	ESPOT_FLAG = 0; //1-enabled 0-disabled

	MULTI_USE_COUPON_FLAG = 0;
	GETPOINTSSERVICE_FLAG = 0;
	PDP_to_WL = 0; //20; //20;  //T23_MoveFromCartToWishlist

//  0 = NO akamai 

 	NO_BROWSE_RATIO = 0; //0; //100;//0; //100;
	NO_BROWSE_RATIO_HOME		= 0; //0;//35; //40;
	NO_BROWSE_RATIO_CATEGORY	= 0; //0; //35; //30;
	NO_BROWSE_RATIO_SUBCATEGORY	= 0; //0; //57; //50;
	NO_BROWSE_RATIO_PDP			= 0; //0; //57; //00;
//====================================================================
int CART_PAGE_DROP, LOGIN_PAGE_DROP, SHIP_PAGE_DROP, BILL_PAGE_DROP, REVIEW_PAGE_DROP, WRITE_TO_SUMO; //Checkout Funnel ratio for DropCart Scripts.
//int LINK_TT, FORM_TT, TYPE_SPEED; //Think Time
int RATIO_CHECKOUT_GUEST, RATIO_CHECKOUT_LOGIN, RATIO_CHECKOUT_LOGIN_FIRST, BRIERLEY_FLAG_CHECK; // User Type
int OFFLINE_PLUGIN, VISA, MASTER, AMEX, PLCC, GIFT, DISCOVER, OFFLINE_PLUGIN; // Payment Method
int RATIO_PROMO_APPLY, RATIO_PROMO_MULTIUSE, RATIO_PROMO_SINGLEUSE, RATIO_PROMO_COUPON_REMOVE, RATIO_REDEEM_LOYALTY_POINTS, RATIO_PROMO_APPLY_ALL; // Promo Type
int RATIO_REGISTER, RATIO_UPDATE_QUANTITY, RATIO_DELETE_PROMOCODE, RATIO_DELETE_ITEM, RATIO_SELECT_COLOR, RATIO_ORDER_HISTORY, RATIO_POINTS_HISTORY, RATIO_RESERVATION_HISTORY, RATIO_RESERVATION_WISHLIST;// Registration, Update_Quantity, Delete_Item from cart
int RATIO_CART_MERGE, RATIO_DROP_CART, RATIO_WISHLIST, cartMerge, currentCartSize; RATIO_BUILDCART_DRILLDOWN, USE_LOW_INVENTORY, RATIO_KIDS_SQUAD, RATIO_SMS_SIGNUP;
int	RATIO_WL_VIEW, RATIO_WL_CREATE,	RATIO_WL_DELETE, RATIO_WL_CHANGE, RATIO_WL_DELETE_ITEM, RATIO_WL_ADD_ITEM, RATIO_WL_ADD_TO_CART;
int MOVE_FROM_CART_TO_WISHLIST, MOVE_FROM_WISHLIST_TO_CART, RATIO_BUILDCART_LARGE, RATIO_WL_SHARE;
int RATIO_ACCOUNT_OVERVIEW, RATIO_ACCOUNT_REWARDS, RATIO_ORDER_HISTORY, RATIO_RESERVATION_HISTORY, RATIO_ADDRESSBOOK, RATIO_PAYMENT, RATIO_PROFILE, RATIO_PREFERENCES, RATIO_LOGOUT, RATIO_FORGOT_PASSWORD,ONLINE_ORDER_SUBMIT, LOGINFIRSTCHECK; RATIO_ADDBIRTHDAYSAVINGS, RATIO_RTPS, RATIO_WIC, RATIO_ADDTOCART_PLP;
int BRIERLEY_CUSTOM_TEST, PICKUP_PAGE_DROP, RATIO_VENMO, RATIO_GETREGISTERED, RATIO_SAVE_FOR_LATER, RATIO_SAVE_FOR_LATER_MOVE, UNBXD_FLAG, BUILDCARTBEFORELOGINRATION;
int RATIO_COUPON_APPLY, RATIO_COUPON_REMOVE, RATIO_EDD_NODE, ATC_MULTI_PRODUCT;

	//set drop point ratio for checkout funnel. Sum must be 100. All variables must either 0 or +ve value.
	// Reconciled with CM access log checkout funnel
/*	LOGIN_PAGE_DROP = 30; 
	SHIP_PAGE_DROP = 30; 
	BILL_PAGE_DROP = 27;
*/
	LOGIN_PAGE_DROP = 30; 
	PICKUP_PAGE_DROP = 15; 
	SHIP_PAGE_DROP = 50; //22; //30;
	BILL_PAGE_DROP = 50; //20; //27
	//REVIEW_PAGE_DROP = 100;
	RATIO_VENMO = 0; //0 - DISABLE, 1 - ENABLE
	WRITE_TO_SUMO = 0; 
	RATIO_GETREGISTERED = 40;
	//set user checkout type T10 transaction. Sum must be 100. All variables must have either 0 or +ve value.
	RATIO_CHECKOUT_LOGIN = 90; //70; 
	RATIO_CHECKOUT_GUEST = 10;
	BRIERLEY_FLAG_CHECK = 0;
	//Ratio for Drop Cart vusers
	RATIO_DROP_CART = 75; //75 on 10kvu; 60 on 55kvu
	UNBXD_FLAG = 0; //1=ON; 0=OFF;
    //set users checkout that starts with login flow
  	RATIO_CHECKOUT_LOGIN_FIRST = 50; 
	BRIERLEY_CUSTOM_TEST=0;
	BUILDCARTBEFORELOGINRATION=20;
	//  11/19/2017
	RATIO_ORDER_STATUS = 100; //Changed to 100 10082018 To test DOM

    //sets a percent of users to drilldown before selecting a product when building the cart.
  	RATIO_BUILDCART_DRILLDOWN = 100; //25; //20; //25;

	USE_LOW_INVENTORY = 5;
	RATIO_BUILDCART_LARGE = 0; //1;

	//PayMethod
	OFFLINE_PLUGIN = 100;
	VISA = 0;
	MASTER = 0;
	AMEX = 0;
	PLCC = 0;
	GIFT = 0;
	DISCOVER = 0;
	PAYPAL = 0;

	//PromoType
//	RATIO_PROMO_APPLY = 100;
//	RATIO_PROMO_MULTIUSE = 100;
//	RATIO_PROMO_SINGLEUSE = 100;
	RATIO_COUPON_APPLY = 20;
	RATIO_COUPON_REMOVE = 5;
	RATIO_PROMO_APPLY = 100;
	RATIO_PROMO_MULTIUSE = 0; //100; //95;
	RATIO_PROMO_SINGLEUSE = 0; //5;
	RATIO_PROMO_APPLY_ALL = 0; //1;
	RATIO_REDEEM_LOYALTY_POINTS = 15;

	RATIO_EDD_NODE = 100;
	ATC_MULTI_PRODUCT = 0;
	ATC_TRIPACK_PRODUCT = 8;
	ATC_QUADPACK_PRODUCT = 7;

	//Ratio for deleting a promo code
	RATIO_DELETE_PROMOCODE = 0;

	//Ratio for Cart Merge
	RATIO_CART_MERGE = 0; //50;

//% of Guest user completing checkout that also register
	RATIO_REGISTER = 100;

	//Ratio for Update Quantity
	RATIO_UPDATE_QUANTITY = 35; //100

	//Ratio for Delete an item from cart
	RATIO_DELETE_ITEM = 50; //60;

	RATIO_SAVE_FOR_LATER = 50;
	RATIO_SAVE_FOR_LATER_MOVE = 25;
	//Ratio for Selecting a Color
	RATIO_SELECT_COLOR = 50;
	///RATIO_ORDER_HISTORY + RATIO_POINTS_HISTORY + RATIO_RESERVATION_HISTORY // All this should be total 100
	//Ratio for Order history / View Order Detail / Reservation

	//RATIO_RESERVATION_HISTORY = 10;
	//  11/19/2017
//	RATIO_POINTS_HISTORY = 30; //50;
	RATIO_ORDER_HISTORY = 60; //50;

	//Ratio for Wishlist
	RATIO_WISHLIST		 = 50; //50;
	RATIO_WL_CREATE      = 34; //35;
	RATIO_WL_DELETE      = 2;
	RATIO_WL_CHANGE      = 3;
	RATIO_WL_DELETE_ITEM = 5;
	RATIO_WL_ADD_ITEM    = 1;
	RATIO_WL_SHARE    = 1; //0; //new 08282018
	RATIO_WL_ADD_TO_CART = 25;

	MOVE_FROM_CART_TO_WISHLIST = 25; //25;
	MOVE_FROM_WISHLIST_TO_CART = 30; //30;

	RATIO_ACCOUNT_OVERVIEW 		= 40; //100;

	RATIO_ACCOUNT_REWARDS 		= 5;
//	RATIO_ORDER_HISTORY			= 60; //80;
	RATIO_RESERVATION_HISTORY	= 0;
	RATIO_ADDRESSBOOK			= 5;
	RATIO_PAYMENT				= 5;
	RATIO_PROFILE				= 5;
	RATIO_PREFERENCES			= 5;

	RATIO_ADDBIRTHDAYSAVINGS = 20;
	RATIO_LOGOUT			= 10;
	RATIO_FORGOT_PASSWORD	= 10;
	RATIO_KIDS_SQUAD		= 3;
	RATIO_SMS_SIGNUP		= 10;
	ONLINE_ORDER_SUBMIT		= 0;
	LOGINFIRSTCHECK			= 0;

	RATIO_RTPS = 0;//50;
	RATIO_WIC  = 0;//50;
	RATIO_ADDTOCART_PLP = 28;


#endif // _GLOBALS_H
