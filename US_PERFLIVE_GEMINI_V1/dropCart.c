dropCart()
{
	lr_user_data_point("A_Drop Cart Rate", 1);
	lr_user_data_point("A_Checkout Rate", 0);
	lr_save_string("dropCartFlow", "currentFlow");
	ECOMM_CART_RATIO = 100; 
	BOPIS_CART_RATIO = 0;
	BOSS_CART_RATIO = 0;
	
//	RATIO_UPDATE_ITEM_TO_BOSS = 0; //switch ecom item to bopis order
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_GUEST ) {
		lr_user_data_point("B_Drop_Rate Guest", 1);
		lr_user_data_point("B_Drop_Rate Registered Home", 0);
		lr_user_data_point("B_Drop_Rate Registered Shipping", 0);
		
		lr_save_string(lr_eval_string("BITROGUE_{randomChar}{randomChar}{randomChar}{randomChar}{randomFourDigits}@MAILINATOR.COM"),"userEmail");
		dropCartGuest();
	} 
	else {

		lr_save_string( lr_eval_string("{logonid}"), "userEmail" ); 
		lr_save_string( lr_eval_string("{loginType}"), "userType" ); 
//		lr_save_string( "manny456789@gmail.com", "userEmail" ); //uatlive1		
//		lr_save_string( "mannyyamzon@yopmail.com", "userEmail" ); //NON- express perf
//		lr_save_string( "tcpperf_usd_02846402.0625@childrensplace.com", "userEmail" ); //NON- express perf
		//lr_save_string( "TCP_KEYNOTE_00002@CHILDRENSPLACE.COM", "userEmail" );
		//lr_save_string( "TCPKEYNOTE2_02131@CHILDRENSPLACE.COM", "userEmail" );
		
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_LOGIN_FIRST ) { //login first
			lr_user_data_point("B_Drop_Rate Guest", 0);
			lr_user_data_point("B_Drop_Rate Registered Home", 1);
			lr_user_data_point("B_Drop_Rate Registered Shipping", 0);
			
			lr_save_string("true", "loginFirst");
//			buildCartLoginFirst();
			dropCartLoginFirst();
 
        }
        else { //build cart first
			lr_user_data_point("B_Drop_Rate Guest", 0);
			lr_user_data_point("B_Drop_Rate Registered Home", 0);
			lr_user_data_point("B_Drop_Rate Registered Shipping", 1);
			
			lr_save_string("false", "loginFirst");
			dropCartBuildCartFirst();
            
        }
		
	} 

	return 0;
}

dropCartGuest()
{
	int WIC_Submit = 0;
	
	lr_save_string("0", "cartCount");
   	buildCartDrop(0);//

	inCartEdits();
	
	if (proceedAsGuest() == LR_PASS)
	{
		if ( strcmp( lr_eval_string("{mixCart}"), "BOPIS") == 0 || strcmp( lr_eval_string("{mixCart}"), "BOSS") == 0 || strcmp( lr_eval_string("{mixCart}"), "MIX") == 0)  
		{  
			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= PICKUP_PAGE_DROP )
			{	
				lr_user_data_point("C_PICKUP_PAGE_DROP", 1);
				lr_user_data_point("C_SHIP_PAGE_DROP", 0);
				lr_user_data_point("C_BILL_PAGE_DROP", 0);
				dropCartTransaction();
				return 0;
			}
			if (Submit_Pickup_Detail_Guest() == LR_PASS )
			{
				if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
				{	
					lr_user_data_point("C_PICKUP_PAGE_DROP", 0);
					lr_user_data_point("C_SHIP_PAGE_DROP", 1);
					lr_user_data_point("C_BILL_PAGE_DROP", 0);
					dropCartTransaction();
					return 0;
				} 
				if ( submitShipping() == LR_PASS) {
					if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
					{	
						lr_user_data_point("C_PICKUP_PAGE_DROP", 0);
						lr_user_data_point("C_SHIP_PAGE_DROP", 0);
						lr_user_data_point("C_BILL_PAGE_DROP", 1);
						dropCartTransaction();
						return 0;
					} 
					if (BillingAsGuest() == LR_PASS)
					{
						dropCartTransaction();
					}
				}
			}
		} else { //ECOM Only
			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
			{	
				lr_user_data_point("C_PICKUP_PAGE_DROP", 0);
				lr_user_data_point("C_SHIP_PAGE_DROP", 1);
				lr_user_data_point("C_BILL_PAGE_DROP", 0);
				dropCartTransaction();
				return 0;
			} else if ( submitShipping() == LR_PASS)
			{
				if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
				{	
					lr_user_data_point("C_PICKUP_PAGE_DROP", 0);
					lr_user_data_point("C_SHIP_PAGE_DROP", 0);
					lr_user_data_point("C_BILL_PAGE_DROP", 1);
					dropCartTransaction();
					return 0;
				} else if (BillingAsGuest() == LR_PASS)
				{
					dropCartTransaction();
				}
			}
		}
	}
	
	return 0;
}

buildCartLoginFirst()
{
		lr_save_string( lr_eval_string("{logonid}"), "userEmail" );
		lr_save_string( lr_eval_string("{loginType}"), "userType" ); 
	
	if (loginFromHomePage() == LR_PASS)
	{
	    viewCart();  
		 
    	buildCartDrop(1);
	}
	return 0;
}

dropCartLoginFirst()
{
	int viewHistory = atoi(lr_eval_string("{RANDOM_PERCENT}"));
 
   
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BUILDCARTBEFORELOGINRATION ) {
 			
   	buildCartCheckout(1);
   	
	}
   	if (loginFromHomePage() == LR_PASS)
	{
		
        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ACCOUNT_OVERVIEW ) 
        	viewMyAccount();
         
	    viewCart();  
		 
    	buildCartDrop(1);

        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ACCOUNT_OVERVIEW )
        	viewMyAccount();
        
		if ( strcmp(strupr(lr_eval_string("{buildCart}")),  "TRUE") != 0) 
			inCartEdits();

        if ( atoi(lr_eval_string("{RANDOM_WL_PERCENT}")) <= RATIO_WISHLIST )
            wishList();

	    if (checkout_miniBagPopUp() == LR_PASS) { //Added by Pavan Dusi to cover Minibag popup
        	
			if (proceedToCheckout_ShippingView() == LR_PASS) 
			{
				if ( (strcmp( lr_eval_string("{mixCart}"), "BOPIS") == 0 || strcmp( lr_eval_string("{mixCart}"), "BOSS") == 0 || strcmp( lr_eval_string("{mixCart}"), "MIX") == 0) && strcmp(lr_eval_string("{isExpress}"), "true") ==0 )
		    	{
					dropCartTransaction();
					
				} else if ( (strcmp( lr_eval_string("{mixCart}"), "BOPIS") == 0 || strcmp( lr_eval_string("{mixCart}"), "BOSS") == 0 || strcmp( lr_eval_string("{mixCart}"), "MIX") == 0) && strcmp(lr_eval_string("{isExpress}"), "false") ==0 )
		    	{
					if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= PICKUP_PAGE_DROP )
					{	
						lr_user_data_point("C_PICKUP_PAGE_DROP", 1);
						lr_user_data_point("C_SHIP_PAGE_DROP", 0);
						lr_user_data_point("C_BILL_PAGE_DROP", 0);
						
						dropCartTransaction();
						return 0;
					}
					if (Submit_Pickup_Detail() == LR_PASS )
					{
						if (strcmp(lr_eval_string("{mixCart}"), "BOSS") != 0) //"mixCart = BOSS"
						{
					        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
							{	
								lr_user_data_point("C_PICKUP_PAGE_DROP", 0);
								lr_user_data_point("C_SHIP_PAGE_DROP", 1);
								lr_user_data_point("C_BILL_PAGE_DROP", 0);
								
								dropCartTransaction();
									return 0;
							} else 
								submitShippingRegistered();
						}
	
			            if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
						{	
							lr_user_data_point("C_PICKUP_PAGE_DROP", 0);
							lr_user_data_point("C_SHIP_PAGE_DROP", 0);
							lr_user_data_point("C_BILL_PAGE_DROP", 1);
							
							dropCartTransaction();
							return 0;
			            } else if (submitBillingRegistered() == LR_PASS) {
								dropCartTransaction();
			            }
		    		}
				} else if ( strcmp( lr_eval_string("{mixCart}"), "ECOM") == 0 && strcmp(lr_eval_string("{isExpress}"), "true") ==0 )
		    	{
					dropCartTransaction();
					
				} else if ( strcmp( lr_eval_string("{mixCart}"), "ECOM") == 0 && strcmp(lr_eval_string("{isExpress}"), "false") ==0 )
		    	{
		            if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
					{	
						lr_user_data_point("C_PICKUP_PAGE_DROP", 0);
						lr_user_data_point("C_SHIP_PAGE_DROP", 1);
						lr_user_data_point("C_BILL_PAGE_DROP", 0);
						dropCartTransaction();
						return 0;
					} else if (submitShippingRegistered() == LR_PASS)
		    		{
			            if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
						{	
							lr_user_data_point("C_PICKUP_PAGE_DROP", 0);
							lr_user_data_point("C_SHIP_PAGE_DROP", 0);
							lr_user_data_point("C_BILL_PAGE_DROP", 1);
							dropCartTransaction();
							return 0;
			            } else if (submitBillingRegistered() == LR_PASS) {
								dropCartTransaction();
						}
		    		}
		    	}	
	        }
        }
	}
	
	return 0;
}

dropCartBuildCartFirst()
{
	int viewHistory = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	int WIC_Submit = 0;
	
    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_SMS_SIGNUP )
		smsSignUp();
	
	lr_save_string("0", "cartCount");
    	buildCartDrop(0);//

	if (CACHE_PRIME_MODE == 1)
		return 0; 

	inCartEdits();
	
    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= LOGIN_PAGE_DROP )
	{		
		lr_user_data_point("LOGIN_PAGE_DROP - Buildcart", 1);
		lr_user_data_point("LOGIN_PAGE_DROP - Home", 0);
		dropCartTransaction();
		return 0;
	}

	if (login() == LR_PASS) 
	{
		if ( (strcmp( lr_eval_string("{mixCart}"), "BOPIS") == 0 || strcmp( lr_eval_string("{mixCart}"), "BOSS") == 0 || strcmp( lr_eval_string("{mixCart}"), "MIX") == 0) && strcmp(lr_eval_string("{isExpress}"), "true") ==0 )
    	{
			dropCartTransaction();
			
		} else if ( (strcmp( lr_eval_string("{mixCart}"), "BOPIS") == 0 || strcmp( lr_eval_string("{mixCart}"), "BOSS") == 0 || strcmp( lr_eval_string("{mixCart}"), "MIX") == 0) && strcmp(lr_eval_string("{isExpress}"), "false") ==0 )
    	{
			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= PICKUP_PAGE_DROP )
			{	
				lr_user_data_point("C_PICKUP_PAGE_DROP", 1);
				lr_user_data_point("C_SHIP_PAGE_DROP", 0);
				lr_user_data_point("C_BILL_PAGE_DROP", 0);
				dropCartTransaction();
				return 0;
			}
			if (Submit_Pickup_Detail() == LR_PASS )
			{
				if (strcmp(lr_eval_string("{mixCart}"), "BOSS") != 0) //"mixCart = BOSS"
				{
			        if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
					{	
						lr_user_data_point("C_PICKUP_PAGE_DROP", 0);
						lr_user_data_point("C_SHIP_PAGE_DROP", 1);
						lr_user_data_point("C_BILL_PAGE_DROP", 0);
						dropCartTransaction();
						return 0;
					} else 
						submitShippingRegistered();
				}

	            if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
				{	
					lr_user_data_point("C_PICKUP_PAGE_DROP", 0);
					lr_user_data_point("C_SHIP_PAGE_DROP", 0);
					lr_user_data_point("C_BILL_PAGE_DROP", 1);
					dropCartTransaction();
					return 0;
	            } else if (submitBillingRegistered() == LR_PASS) {
						dropCartTransaction();
	            }
    		}
		} else if ( strcmp( lr_eval_string("{mixCart}"), "ECOM") == 0 && strcmp(lr_eval_string("{isExpress}"), "true") ==0 )
    	{
			dropCartTransaction();
			
		} else if ( strcmp( lr_eval_string("{mixCart}"), "ECOM") == 0 && strcmp(lr_eval_string("{isExpress}"), "false") ==0 )
    	{
            if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= SHIP_PAGE_DROP )
			{	
				lr_user_data_point("C_PICKUP_PAGE_DROP", 0);
				lr_user_data_point("C_SHIP_PAGE_DROP", 1);
				lr_user_data_point("C_BILL_PAGE_DROP", 0);
				dropCartTransaction();
				return 0;
			} else if (submitShippingRegistered() == LR_PASS)
    		{
	            if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BILL_PAGE_DROP )
				{	
					lr_user_data_point("C_PICKUP_PAGE_DROP", 0);
					lr_user_data_point("C_SHIP_PAGE_DROP", 0);
					lr_user_data_point("C_BILL_PAGE_DROP", 1);
					dropCartTransaction();
					return 0;
	            } else if (submitBillingRegistered() == LR_PASS) {
						dropCartTransaction();
				}
    		}
    	}	
	}
/*
    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_KIDS_SQUAD )
		kidStyleSquad();
*/
    
 	return 0;
}