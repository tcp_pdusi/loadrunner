Action()
{
	web_set_sockets_option("SSL_VERSION", "TLS1.2");
	web_set_max_html_param_len ( "3072" ) ;
	web_cleanup_cookies();
	web_add_auto_header("deviceType", "desktop");
	web_add_auto_header("Accept-Encoding", "gzip, deflate, br");
	web_add_cookie("UrCapture=bebc0dd6-318d-f144-892f-5bb458cc1059;path=/;domain=tcp-perf.childrensplace.com");
	isLoggedIn=0;
	ONLINE_ORDER_SUBMIT=0;
	API_SUB_TRANSACTION_SWITCH = 0;
// Original Random cart size 7/10
/* Script Testing Paths
	1. Guest - Ecom Only, Bopis Only, Boss Only, Mixed
	2. Registered Login First- NON EXPRESS user with Ecom Only, Bopis Only, Boss Only, Mixed
    3. Registered Login First - EXPRESS user with Ecom Only, Bopis Only, Boss Only, Mixed
	4. Registered Browse First - NON EXPRESS user with Ecom Only, Bopis Only, Boss Only, Mixed 
	5. Registered Browse First - EXPRESS user with Ecom Only, Bopis Only, Boss Only, Mixed
*/
//	Temp Debugging Parameters
	RATIO_DROP_CART = lr_get_attrib_long( "RATIO_DROP_CART");
	SFCC_CART_MERGE=0;
	lr_save_string(lr_eval_string("{SFCC_COOKIE_MERGE}"),"CARTITEMS_COOKIESTR");
//	RATIO_CHECKOUT_GUEST = 00; 
//	RATIO_CHECKOUT_LOGIN_FIRST = 000;
	ATC_FLAG=0; //1 = UI; 0 = parameter file
	ATC_FLAG_OVERRIDE = 0; //1 = UI; 0 = parameter file
	RATIO_UPDATE_ITEM_TO_BOSS = lr_get_attrib_long( "RATIO_UPDATE_ITEM_TO_BOSS");
	UNBXD_FLAG = 0; //0; //lr_get_attrib_long("UNBXD_FLAG");
	GETOFFERS = lr_get_attrib_long("GETOFFERS");
	SUBMIT_ORDER = lr_get_attrib_long("SUBMIT_ORDER");
	BRIERLEY_FLAG_CHECK = 0;
	BRIERLEY_CUSTOM_TEST = 0;
	lr_save_string("0", "navXHR");
	lr_save_string(lr_eval_string("{RANDOM_PERCENT}"), "akamaiRatio" );
	lr_save_string(lr_eval_string("{RANDOM_PERCENT}"), "getFavourite" );
	lr_save_string("FALSE","CAN_APPLY_BONUS_DAY");
	lr_save_string("", "loginFirst");
	lr_save_string( "false" , "userCouponsApplied" ) ;
	lr_save_string( "false" , "userCouponRemoved" ) ;

	lr_save_string("TRUE","EDDPERSISTSTRING");

	if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 20)
	{	if (BRIERLEY_FLAG_CHECK == 1)
			lr_save_string("true","p_refreshPoints");
		else
			lr_save_string("false","p_refreshPoints");
	} else
		lr_save_string("false","p_refreshPoints");
//	lr_save_string("0", "akamaiRatio" ); 
//	ECOMM_CART_RATIO = 00; //ecomm only is working
//	BOPIS_CART_RATIO = 100; //need to find data in uatlive4, not tested
//	BOSS_CART_RATIO = 0; //Boss Only is working
	lr_save_string("FALSE","UNQUALIFIEDLIST");
	lr_save_string("10151", "storeId");
	lr_save_string("10551", "catalogId");
	lr_save_string("us", "country");
	lr_save_string("", "searchTerm");
	lr_save_string("", "stepName");
	lr_save_string("FALSE", "addBopisToCart");
	lr_save_string("FALSE", "addBossToCart");
	lr_save_string("FALSE", "addEcommToCart");
	lr_save_string("false", "isExpress" );
	lr_save_string("0", "previousATC_userpoints");
	lr_save_string("false", "pickUpDetail");
	lr_save_string("false", "primaryAddrExist");
	lr_save_string("false", "navigateXHR");	
	lr_save_string("", "navigatePath");
	lr_save_string("", "toggleSwitch");
//	web_add_auto_header("tcp-trace-session-id","{randomSeven}c-3502-777c-bdc3-861ee6e8ade8");
	lr_save_string(lr_eval_string("uid-{randomSeven}c3502777cbdc3861ee6e8ade8"), "UID");
//	web_add_auto_header("tcp-trace-session-id","{randomSeven}c3502777cbdc3861ee6e8ade8");
	web_add_auto_header("tcp-trace-session-id",lr_eval_string("{UID}"));
//	RATIO_UPDATE_ITEM_TO_BOSS = 100;
	if (RATIO_UPDATE_ITEM_TO_BOSS>0) {	//Change Made by PDUSI on 11052019 {
        BOSS_CART_RATIO=100;		//Change Made by PDUSI on 11052019
		ECOMM_CART_RATIO=00;
	}
	//lr_output_message("PDUSI ECOM CART RATIO: %d", ECOMM_CART_RATIO); //Change Made by PDUSI on 11052019

//			lr_save_string(lr_eval_string("T10_Logon_getRegisteredUserDetailsInfo Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\" User: {userEmail}"), "errorMessageToFile");
//			writeToFile();
//	ECOMM_CART_RATIO=100;
	
//	lr_save_string("test-uat2.childrensplace.com", "host");
//	lr_save_string("test-uat2.childrensplace.com/api", "api_host");
//createAccount();
//lr_abort();
//	buildCartLoginFirst();
	viewHomePage();	
	//viewCart();
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_DROP_CART ) {
		dropCart();
	} else {
		checkOut(); 
	}

	return 0;
	
}
