checkOut()
{
	lr_user_data_point("A_Drop Cart Rate", 0);
	lr_user_data_point("A_Checkout Rate", 1);
	lr_save_string("checkoutFlow", "currentFlow");

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_GUEST ) {
		lr_user_data_point("B_Checkout_Rate Guest", 1);
		lr_user_data_point("B_Checkout_Rate Registered Home", 0);
		lr_user_data_point("B_Checkout_Rate Registered Shipping", 0);
		lr_save_string(lr_eval_string("BITROGUE_{randomChar}{randomChar}{randomChar}{randomChar}{randomFourDigits}@MAILINATOR.COM"),"userEmail");
		checkoutGuest();
	} 
	else {
		
		lr_save_string( lr_eval_string("{logonid}"), "userEmail" );
		lr_save_string( lr_eval_string("{loginType}"), "userType" ); 
//		lr_save_string( "TCP_KEYNOTE_00001@CHILDRENSPLACE.COM", "userEmail" ); //not included in the datafile
//		lr_save_string( "TCP_KEYNOTE_00002@CHILDRENSPLACE.COM", "userEmail" ); 
//		lr_save_string( "TCP_KEYNOTE_00003@CHILDRENSPLACE.COM", "userEmail" ); 
//		lr_save_string( "TCPKEYNOTE2_14326@CHILDRENSPLACE.COM", "userEmail" ); 
//		lr_save_string( "TCPPERF_USD_09429260.7506@CHILDRENSPLACE.COM", "userEmail" ); //express perf
//		lr_save_string( "TCPPERF_US1_0913.09603598@CHILDRENSPLACE.COM", "userEmail" ); //express perf
//		lr_save_string( "TCPKEYNOTE3_05987@CHILDRENSPLACE.COM", "userEmail" ); //express perf
//		lr_save_string( "NON", "userType" ); 
//		lr_save_string( "TCPKEYNOTE2_08716@CHILDRENSPLACE.COM", "userEmail" ); //express perf
//		lr_save_string( "NON", "userType" ); 
//		lr_save_string( "TCPKEYNOTE2_07361@CHILDRENSPLACE.COM", "userEmail" ); 
//		lr_save_string( "NON", "userType" ); 
//		lr_save_string( "TCP_KEYNOTE_04593@CHILDRENSPLACE.COM", "userEmail" ); 
//		lr_save_string( "VEN", "userType" ); 

// 	TCP_UATLIVE3@childrensplace.com

//		lr_save_string( "manny4567890@gmail.com", "userEmail" ); //uatlive4
//		lr_save_string( "manny456789@gmail.com", "userEmail" ); //uatlive4 & 2
//		lr_save_string( "cawolihe-0154@yopmail.com", "userEmail" ); //uatlive4 & 2
//		lr_save_string( "manny654321@gmail.com", "userEmail" );
//		lr_save_string( "TCPPERF_USD_00821671.9819@CHILDRENSPLACE.COM", "userEmail" ); //express perf
//		lr_save_string( "TCPPERF_USD_09429260.7506@CHILDRENSPLACE.COM", "userEmail" ); //express perf
//		lr_save_string( "TCPPERFV_USA_120833.068@CHILDRENSPLACE.COM", "userEmail" ); //express perf
//		lr_save_string( "TCPPERF_USD_50414156.3522@CHILDRENSPLACE.COM", "userEmail" ); //NON-express perf , no default address, no CC
//		lr_save_string( "TCPPERF_USD_50414156.3522@CHILDRENSPLACE.COM", "userEmail" ); //NON-express perf, with default address, no CC
//		lr_save_string( "tcpperfv_usa_120420.479@childrensplace.com", "userEmail" ); //express perf
//		lr_save_string( "tcpperf_us1_0000.97377186@childrensplace.com", "userEmail" ); //express prodid in perflive
//		lr_save_string( "manny456789@gmail.com", "userEmail" ); //uatlive1		
//		lr_save_string( "manny654321@gmail.com", "userEmail" ); //perflive
//		lr_save_string( "manny123456@gmail.com", "userEmail" ); //uatlive3
//		lr_save_string( "manny123456@gmail.com", "userEmail" ); //uatlive2
//		lr_save_string( "TCPPERFV_USA_120711.943@CHILDRENSPLACE.COM", "userEmail" ); //express perf
//		lr_save_string( "TCPPERF_USD_00355776.5159@CHILDRENSPLACE.COM", "userEmail" );
//		lr_save_string( "TCPPERF_USD_56102314.3819@CHILDRENSPLACE.COM", "userEmail" ); 
//		lr_save_string( "joeuser@gmail.com", "userEmail" );  //test-uat2.childrensplace.com
//		lr_save_string( "TCPPERF_USD_92478389.3859@CHILDRENSPLACE.COM", "userEmail" ); //nonExpress - Failing
//		lr_save_string( "TCPKEYNOTE2_00028@CHILDRENSPLACE.COM", "userEmail" ); //Express - Passing
//		lr_save_string( "TCPKEYNOTE2_02131@CHILDRENSPLACE.COM", "userEmail" );
//		lr_save_string( "TCPKEYNOTE2_02131@CHILDRENSPLACE.COM", "userEmail" );
		
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_LOGIN_FIRST ) { //login first
			lr_user_data_point("B_Checkout_Rate Guest", 0);
			lr_user_data_point("B_Checkout_Rate Registered Home", 1);
			lr_user_data_point("B_Checkout_Rate Registered Shipping", 0);
		
			lr_save_string("true", "loginFirst");
			checkoutLoginFirst();
        }
        else { 
			lr_user_data_point("B_Checkout_Rate Guest", 0);
			lr_user_data_point("B_Checkout_Rate Registered Home", 0);
			lr_user_data_point("B_Checkout_Rate Registered Shipping", 1);
		
			lr_save_string("false", "loginFirst");
			checkoutBuildCartFirst();
        } 
	}

	return 0;
}

checkoutGuest() 
{	//addToCartFromPLP_DF();
	//addToCartFromPLP_DF(); lr_abort();
	lr_save_string("0", "cartCount");	
   	buildCartCheckout(0);

	if (CACHE_PRIME_MODE == 1)
		return 0; 
	
//	viewCart(); //04/09/2018 enabled to verify the cart count
	
//	if (strcmp( lr_eval_string("{addBopisToCart}"), "TRUE") != 0 ) 
		inCartEdits();
		
	if (proceedAsGuest() == LR_PASS)
	{
//		if ( strcmp( lr_eval_string("{addBopisToCart}"), "TRUE") == 0 || strcmp( lr_eval_string("{addBossToCart}"), "TRUE") == 0) { //
//		if ( strcmp( lr_eval_string("{addBopisToCart}"), "TRUE") == 0 || strcmp( lr_eval_string("{addBossToCart}"), "TRUE") == 0 || strcmp( lr_eval_string("{mixCart}"), "MIX") == 0)  {
		if ( strcmp( lr_eval_string("{mixCart}"), "BOPIS") == 0 || strcmp( lr_eval_string("{mixCart}"), "BOSS") == 0 || strcmp( lr_eval_string("{mixCart}"), "MIX") == 0)  
		{
			if (Submit_Pickup_Detail_Guest() == LR_PASS )
			{
				if ( submitShipping() == LR_PASS)
				{
					if (BillingAsGuest() == LR_PASS)
					{
						if( submitOrderAsGuest() == LR_PASS)
						{

							if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_REGISTER ) {
								createAccount();
							}
							
							if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) {
								StoreLocator();
							}
							
						}
					}
				}
			}
		} else { //ECOM Only
		
			if ( submitShipping() == LR_PASS)
			{
				if (BillingAsGuest() == LR_PASS) //submitBillingAddressAsGuest()
				{
					if( submitOrderAsGuest() == LR_PASS)
					{
						if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) { //added 06/07 rhy
	            			viewOrderStatusGuest(); //enabled 11/16/2020 per Pavan for Hotfix3
	            		//	resendEmailConfirmation();
						}
						
						if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_REGISTER ) {
							createAccount(); // former //registerUser();
						}
						if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) {
							StoreLocator();
						}
						
					    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_FORGOT_PASSWORD ) 
							forgetPassword();	
					}
				}
			} 
		}
	}

	
	return 0;
}

checkoutLoginFirst()
{
	int viewHistory = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= BUILDCARTBEFORELOGINRATION ) {
 			
   	buildCartDrop(1);
   	
	}
	if (loginFromHomePage() == LR_PASS)
	{	
		lr_save_string ("logon", "stepName");
		if (viewCart() == LR_FAIL)
			return 0;

    	buildCartCheckout(1);

    	if (CACHE_PRIME_MODE == 1)
			return 0; 

//		if (strcmp( lr_eval_string("{addBopisToCart}"), "TRUE") != 0 )
			inCartEdits();
		
	    if ( atoi(lr_eval_string("{RANDOM_WL_PERCENT}")) <= RATIO_WISHLIST )
	        wishList();
	    
	    if (checkout_miniBagPopUp() == LR_PASS) { //Added by Pavan Dusi to cover Minibag popup 03262019
	    	
	    	if ( strcmp(lr_eval_string("{userType}"), "VEN") == 0 && RATIO_VENMO == 1)
		    {
		    	if (proceedVenmo() == LR_PASS)
		    	{
					if (strcmp( lr_eval_string("{primaryAddrExist}"), "true") == 0 ) 
					{
						if (submitOrderVenmo() == LR_PASS)
						{
							if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
							{
		//						viewOrderStatusRegistered();
		    					//viewOrderStatusGuest();
					   			//resendEmailConfirmation();
							}
						}
					} else {
						if (submitShippingVenmo() == LR_PASS)
						{
							if (submitOrderVenmo() == LR_PASS)
							{
								if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
								{
			//						viewOrderStatusRegistered();
			    					//viewOrderStatusGuest();
						   			//resendEmailConfirmation();
								}
							}
						}
					}
		    	}
		    } else {
			    if (proceedToCheckout_ShippingView() == LR_PASS) 
			    {	
			    	if ( (strcmp( lr_eval_string("{mixCart}"), "BOPIS") == 0 || strcmp( lr_eval_string("{mixCart}"), "BOSS") == 0 || strcmp( lr_eval_string("{mixCart}"), "MIX") == 0) && strcmp(lr_eval_string("{isExpress}"), "true") ==0 )
					{
						if (Submit_Pickup_Detail() == LR_PASS )
						{
							if (submitOrderRegistered() == LR_PASS)
							{
								if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
								{
		//							viewOrderStatusRegistered();
			    					//viewOrderStatusGuest();
						   			//resendEmailConfirmation();
								}
							}
						}
					} else if ( (strcmp( lr_eval_string("{mixCart}"), "BOPIS") == 0 || strcmp( lr_eval_string("{mixCart}"), "BOSS") == 0 || strcmp( lr_eval_string("{mixCart}"), "MIX") == 0) && strcmp(lr_eval_string("{isExpress}"), "false") ==0 )
			    	{
						if (Submit_Pickup_Detail() == LR_PASS )
						{
							if (!(strcmp(lr_eval_string("{mixCart}"), "BOSS") == 0 || strcmp(lr_eval_string("{mixCart}"), "BOPIS") == 0 ) && (strcmp(lr_eval_string("{mixCart}"), "MIX") == 0 ) ) //"mixCart = BOSS" //Modified By Pavan Dusi 03262019
							//if (strcmp(lr_eval_string("{mixCart}"), "BOSS") != 0 || strcmp(lr_eval_string("{mixCart}"), "BOPIS") != 0 ) //"mixCart = BOSS" //Modified By Pavan Dusi 03262019
								submitShippingRegistered();
		
							if (submitBillingRegistered() == LR_PASS)
							{
								if (submitOrderRegistered() == LR_PASS) 
								{
									if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 5 ) 
						        		viewMyAccount();
									
									if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
									{
										//viewOrderStatusRegistered();
							   			//resendEmailConfirmation();
									}
								}
							}
			    		}
					} else if ( strcmp( lr_eval_string("{mixCart}"), "ECOM") == 0 && strcmp(lr_eval_string("{isExpress}"), "true") ==0 )
			    	{
						if (submitOrderRegistered() == LR_PASS)
						{
							if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 5 ) 
				        		viewMyAccount();
							
							if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
							{
								viewOrderStatusRegistered(); //enabled 11/16/2020 per Pavan for Hotfix3
					   			//resendEmailConfirmation();
							}
						}
					} else if ( strcmp( lr_eval_string("{mixCart}"), "ECOM") == 0 && strcmp(lr_eval_string("{isExpress}"), "false") ==0 )
			    	{
			    		if (submitShippingRegistered() == LR_PASS)
			    		{
							if (submitBillingRegistered() == LR_PASS)
							{
								if (submitOrderRegistered() == LR_PASS) 
								{
									if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 5 ) 
						        		viewMyAccount();
									
									if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
									{
										viewOrderStatusRegistered(); //enabled 11/16/2020 per Pavan for Hotfix3
							   			//resendEmailConfirmation();
									}
								}
							}
			    		}
			    	}
				} 
		    }
		}
	    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_FORGOT_PASSWORD ) 
			forgetPassword();	
	    
	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) 
			StoreLocator();
	    
	    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 50 ) 
	   		newsLetterSignup();
	    
    	logoff();
	}

	return 0;
}

checkoutBuildCartFirst()
{
	int viewHistory = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	lr_save_string("0", "cartCount");
   	buildCartCheckout(0);

//	if (CACHE_PRIME_MODE == 1)
//		return 0; 
//viewCart();
  //  	updateItemShipping();
	inCartEdits(); 
	
	if (viewCart() == LR_FAIL)
		return 0;
	
	if (login() == LR_PASS) // login->Proceed to Checkout
	{
    	if ( (strcmp( lr_eval_string("{mixCart}"), "BOPIS") == 0 || strcmp( lr_eval_string("{mixCart}"), "BOSS") == 0 || strcmp( lr_eval_string("{mixCart}"), "MIX") == 0) && strcmp(lr_eval_string("{isExpress}"), "true") ==0 )
		{
			if (Submit_Pickup_Detail() == LR_PASS )
			{
				if (submitOrderRegistered() == LR_PASS)
				{
					if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
					{
//							viewOrderStatusRegistered();
    					//viewOrderStatusGuest();
			   			//resendEmailConfirmation();
					}
				}
			}
		} else if ( (strcmp( lr_eval_string("{mixCart}"), "BOPIS") == 0 || strcmp( lr_eval_string("{mixCart}"), "BOSS") == 0 || strcmp( lr_eval_string("{mixCart}"), "MIX") == 0) && strcmp(lr_eval_string("{isExpress}"), "false") ==0 )
    	{
			if (Submit_Pickup_Detail() == LR_PASS )
			{
				if (strcmp(lr_eval_string("{mixCart}"), "BOSS") != 0) //"mixCart = BOSS"
					submitShippingRegistered();

				if (submitBillingRegistered() == LR_PASS)
				{
					if (submitOrderRegistered() == LR_PASS) 
					{
						if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 5 ) 
			        		viewMyAccount();
						
						if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
						{
					//		viewOrderStatusRegistered();
				   			//resendEmailConfirmation();
						}
					}
				}
    		}
		} else if ( strcmp( lr_eval_string("{mixCart}"), "ECOM") == 0 && strcmp(lr_eval_string("{isExpress}"), "true") ==0 )
    	{
			if (submitOrderRegistered() == LR_PASS)
			{
				if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 5 ) 
	        		viewMyAccount();
				
				if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
				{
	//				viewOrderStatusRegistered();
		   			//resendEmailConfirmation();
				}
			}
		} else if ( strcmp( lr_eval_string("{mixCart}"), "ECOM") == 0 && strcmp(lr_eval_string("{isExpress}"), "false") ==0 )
    	{
    		if (submitShippingRegistered() == LR_PASS)
    		{
				if (submitBillingRegistered() == LR_PASS)
				{
					if (submitOrderRegistered() == LR_PASS) 
					{
						if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 5 ) 
			        		viewMyAccount();
						
						if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ORDER_STATUS ) 
						{
							//viewOrderStatusRegistered();
				   			//resendEmailConfirmation();
						}
					}
				}
    		}
    	}
	} 
	
    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_LOGOUT ) 
    	logoff();
    
    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_FORGOT_PASSWORD ) 
		forgetPassword();	
    
    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_STORE_LOCATOR ) 
		StoreLocator();
    
    if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 50 ) 
   		newsLetterSignup();

    return 0;
	
} // END - if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_CHECKOUT_LOGIN_FIRST ) { //login first		
