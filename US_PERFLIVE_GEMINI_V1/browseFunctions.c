//#include "..\..\PERFLIVE_browseWorkloadModel.c"

int index , length , i, randomPercent, isLoggedIn;
char *searchString ;
char *nav_by ; // T02 - Category Navigation selection
char *drill_by ; // T03 - Facet / SubCategory drill selection
char *sort_by; // T03 - Sort selection
char *product_by; // T04 - Product Display Method
//int form_TT, link_TT, typeSpeed_TT;
typedef long time_t;
time_t t;

browseFunctions()
{
	return 0;
}


void addHeader()
{
	web_add_header("storeId", lr_eval_string("{storeId}") );
	web_add_header("catalogId", lr_eval_string("{catalogId}") );
	web_add_header("langId", "-1");
}
void addSFCCCartHeader()
{
	if (SFCC_CART_MERGE == 0)//Only the first Call
	{
		//web_add_header("cartItems", lr_eval_string("{CARTITEMS_COOKIESTR}"));
		//web_add_header("cartItems", lr_eval_string("{CARTITEMS_COOKIESTR}"));
		web_add_cookie("cartItems={CARTITEMS_COOKIESTR};path=/;domain=.childrensplace.com");
		SFCC_CART_MERGE=1;
	}
}


void webURL(char *Url, char *pageName)
{
	lr_save_string(Url, "Url");
	lr_save_string(pageName, "pageName");
	web_url ( lr_eval_string("{pageName}") ,
    	     "URL={Url}" ,
             "Resource=0" ,
             "Mode=HTML" ,
             LAST ) ;
}//end webURL

int getDataPoint()
{
/*
	if we get the following in the response header
		lr_user_data_point(lr_eval_string("{transactionName}_cache"), 1);
	else
		lr_user_data_point(lr_eval_string("{transactionName}_cache"), 0);
	
*/	
	return 0;
}

int sendErrorToSumo()
{
	lr_start_transaction("T60_SumoLogic");

	web_convert_param("errorDetail", "SourceString={errorString}",  "SourceEncoding=HTML",  "TargetEncoding=URL",  LAST);
	web_custom_request("sendErrorToSumo", 
		"URL=https://endpoint3.collection.us2.sumologic.com/receiver/v1/http/ZaVnC4dhaV1JQKgwynoWmnB9u5LH-nxhlvnzGkeuF8XhLHRZizLjQTxVGMsT0S3ryuht04aJ2vV5di9NrJJj1UpttPdTx1GnOXfUEASDFbzvc8kou2xL-A==?{errorDetail}",
		"Method=GET",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);
	lr_end_transaction("T60_SumoLogic", LR_AUTO);
	return 0;
}


int getListofDefaultWishlist()
{	
	int i;
	if ( isLoggedIn == 1 && strcmp(lr_eval_string("{callerId}"), "home") != 0) {
		lr_save_string("", "body");
//		lr_save_string("{\"productId\":[\"279132\",\"279138\",\"604392\",\"604393\",\"604394\",\"604395\",\"604396\",\"604776\"]}", "body");

		if (atoi(lr_eval_string("{u_numberOfProducts}")) == 0)
			lr_param_sprintf("body", "%s", "{\"productId\":[]}");
		else if (atoi(lr_eval_string("{u_numberOfProducts}")) == 1)
			lr_param_sprintf("body", "%s%s%s", "{\"productId\":[[", lr_eval_string("{u_imagename}"), "]]}");
		else if (atoi(lr_eval_string("{u_numberOfProducts}")) > 1) {
			for (i = 1; i <= atoi(lr_eval_string("{u_imagename_count}")); i++) {
				lr_param_sprintf("body", "%s\"%s\"", lr_eval_string("{body}"), lr_paramarr_idx("u_imagename", i));
				if (i == atoi(lr_eval_string("{u_imagename_count}")))
					lr_param_sprintf("body", "%s%s", lr_eval_string("{body}"), "]}");
				else
					lr_param_sprintf("body", "%s", lr_eval_string("{body},"));
			}
		}
		
//		lr_save_string("{\"productId\":[]}", "body");

		lr_start_sub_transaction(lr_eval_string("{mainTransaction}_getListofDefaultWishlist"), lr_eval_string("{mainTransaction}") );
		web_add_header("Accept", "application/json");
		addHeader();
		registerErrorCodeCheck();
		
		web_custom_request("getListofDefaultWishlist",
			"URL=https://{api_host}/v2/wishlist/getListOfDefaultWishlist",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"Body={body}",
			LAST);
//		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getListofDefaultWishlist"), LR_AUTO );
		if (strcmp(lr_eval_string("{errorCode}"),"")==0 && strcmp(lr_eval_string("{errorMessage}"),"")==0  ){
				lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getListofDefaultWishlist"), LR_AUTO );
				return LR_PASS;
			} else {
				lr_fail_trans_with_error( lr_eval_string("{mainTransaction}_getListofDefaultWishlist HTTP {httpReturnCode}, API Error Code: \"{userEmail}\", errorCode: {errorCode}), errorMessage: {errorMessage}" ) );
				lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getListofDefaultWishlist"), LR_FAIL );
	if (WRITE_TO_SUMO==1) {
 				lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_getListofDefaultWishlist HTTP {httpReturnCode}, API Error Code: \"{userEmail}\", errorCode: {errorCode}), errorMessage: {errorMessage}" ), "errorString" );
				sendErrorToSumo(); 
 	}
				return LR_FAIL;			
		}	
	}

	return 0;
}

int getFavouriteStoreLocation()
{
	lr_save_string(lr_eval_string("{RANDOM_PERCENT}"), "getFavourite" );
	if ( isLoggedIn == 1) {// && atoi(lr_eval_string("{getFavourite}")) <= 30) {
/*		if (atoi(lr_eval_string("{getFavourite}")) <= 45 )
		    lr_save_string("latitude&longitude", "latlon");
		else if (atoi(lr_eval_string("{getFavourite}")) <= 35 )
		    lr_save_string("latitude=40.787823499999995&longitude=-74.04445439999999&catEntryId&itemPartNumber", "latlon"); 
		else
		    lr_save_string("latitude=&longitude=", "latlon");
*/		
		web_add_header("action", "get");
		web_add_header("Expires", "0");
//		web_add_header("latitude", "40.787823499999995");
//		web_add_header("longitude", "-74.04445439999999");
		web_add_header("Accept", "application/json");
		lr_save_string("{","CheckString");
		lr_start_sub_transaction(lr_eval_string("{mainTransaction}_getFavouriteStoreLocation"), lr_eval_string("{mainTransaction}") );
		addHeader();
		registerErrorCodeCheck();
		web_custom_request("getFavouriteStoreLocation", 
//			"URL=https://{api_host}/v2/store/getFavouriteStoreLocation",
			"URL=https://{api_host}/v2/store/getFavouriteStoreLocation?{latlon}",
			"Method=GET",
			"RecContentType=text/html",
			"Mode=HTML",
			LAST);
		if (strcmp(lr_eval_string("{httpReturnCode}"),"200")==0 && strcmp(lr_eval_string("{errorCode}"),"")==0 && strcmp(lr_eval_string("{errorMessage}"),"")==0  ){
			lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getFavouriteStoreLocation"), LR_AUTO );
			return LR_PASS;
		}else{
			lr_error_message( lr_eval_string("{mainTransaction}_getFavouriteStoreLocation - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}") ) ;
			lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getFavouriteStoreLocation"), LR_FAIL );
	if (WRITE_TO_SUMO==1) {
 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_getFavouriteStoreLocation - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
 	}
			return LR_FAIL;			
		}
	}else		
		return LR_PASS;

//return LR_PASS; //02042019 Pavan hacked this o prevent failures and errors per APPD.
}

int getRegisteredUserDetailsInfo()
{
	web_reg_save_param_json("ParamName=isExpress", "QueryString=$.x_isExpress", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=addressId", "QueryString=$.addressId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);	
	web_reg_save_param_json("ParamName=addressIdAll", "QueryString=$.addressId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);	
	web_reg_save_param_json("ParamName=userId", "QueryString=$.userId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);		
	web_reg_save_param_json("ParamName=wicAddressId", "QueryString=$.wicAddressId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);		
	web_reg_save_param_json("ParamName=x_pointsDetails", "QueryString=$.x_pointsDetails", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);		
	lr_save_string("resourceName","CheckString");
	lr_save_timestamp("epochTime", LAST );

	if ( strcmp(lr_eval_string("{stepName}"),"logon") == 0)
	{	
			if (BRIERLEY_FLAG_CHECK == 1)
				lr_save_string("true","p_refreshPoints");
			else
				lr_save_string("false","p_refreshPoints");
		
	} else {
		if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 20)
		{	if (BRIERLEY_FLAG_CHECK == 1)
				lr_save_string("true","p_refreshPoints");
			else
				lr_save_string("false","p_refreshPoints");
		} else
			lr_save_string("false","p_refreshPoints");		
	}
	lr_start_sub_transaction(lr_eval_string("{mainTransaction}_getRegisteredUserInfo_{p_refreshPoints}"), lr_eval_string("{mainTransaction}") );
	web_add_header("expires", "0");
	web_add_header("refreshPoints", lr_eval_string("{p_refreshPoints}"));
	web_add_header("Accept", "application/json");
	web_add_header("Content-Type", "application/json");
//	web_add_header("pageName", lr_eval_string("{p_pageName}"));
	addHeader();
	registerErrorCodeCheck();
	web_custom_request("getRegisteredUserInfo",
		"URL=https://{api_host}/v2/account/getRegisteredUserInfo?_={epochTime}",
		"Method=GET",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);
	
//	if (BRIERLEY_FLAG_CHECK == 1 && isLoggedIn == 1) {
	if (BRIERLEY_FLAG_CHECK == 1 ) {
		if (strcmp(lr_eval_string("{mainTransaction}"), "T10_Logon") == 0) {
			if (strlen(lr_eval_string("{x_pointsDetails}")) > 0) {
				lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"));
				lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"), LR_PASS);
			} else {
				lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"));
				lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"), LR_FAIL);
			}
				
		}
	}
	
	lr_save_string("", "stepName");

//	if (strcmp(lr_eval_string("{httpReturnCode}"),"200")==0 && strcmp(lr_eval_string("{errorCode}"),"")==0 && strcmp(lr_eval_string("{errorMessage}"),"")==0  ){
	if (strcmp(lr_eval_string("{errorCode}"),"")==0 && strcmp(lr_eval_string("{errorMessage}"),"")==0  && strlen(lr_eval_string("{userId}")) > 0 ){
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getRegisteredUserInfo_{p_refreshPoints}"), LR_AUTO );
		return LR_PASS;
	} else {
		lr_fail_trans_with_error( lr_eval_string("{mainTransaction}_getRegisteredUserInfo HTTP {httpReturnCode}, API Error Code: \"{userEmail}\", errorCode: {errorCode}), errorMessage: {errorMessage}" ) );
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getRegisteredUserInfo_{p_refreshPoints}"), LR_FAIL );
		if (WRITE_TO_SUMO==1) {
	 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_getRegisteredUserInfo - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
				sendErrorToSumo(); 
	 	}
		return LR_FAIL;			
	}	
	
}

int getESpot()
{
	if (ESPOT_FLAG == 1) {
		lr_start_sub_transaction(lr_eval_string("{mainTransaction}_getESpot{getESpot}"), lr_eval_string("{mainTransaction}") );
		web_add_header("Access-Control-Request-Headers", "catalogid,devicetype,espotname,langid,storeid");
		web_add_header("deviceType","desktop");
		addHeader();
		registerErrorCodeCheck();
		web_custom_request("getESpot",
			"URL=https://{api_host}/getESpot",
	//		"URL=https://{api_host}/v2/global-components/getESpot",
			"Method=GET",
			"RecContentType=text/html",
			"Mode=HTML",
			LAST);
		if (strcmp(lr_eval_string("{httpReturnCode}"),"200")==0 && strcmp(lr_eval_string("{errorCode}"),"")==0 && strcmp(lr_eval_string("{errorMessage}"),"")==0  ){
			lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getESpot{getESpot}"), LR_AUTO );
			return LR_PASS;
		}else{
			lr_fail_trans_with_error( lr_eval_string("{mainTransaction}_getESpot HTTP {httpReturnCode}, API Error Code: \"{userEmail}\", errorCode: {errorCode}), errorMessage: {errorMessage}" ) );
			lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getESpot{getESpot}"), LR_FAIL );
	if (WRITE_TO_SUMO==1) {
 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_getESpot - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
 	}
			return LR_FAIL;			
		}
		
	}else		//IF NO ESPOTS we will always return true
		return LR_PASS;

}

int	getPriceDetails()
{/*
	if( strcmp(lr_eval_string("{storeId}"), "10151") == 0 ) {
		lr_save_string( "https://search.unbxd.io/sites/perf-us-childrenplace-com702771527802758/products?id=2043301_142,2043378_10,2043378_IV,1124756_N3,2045419_IV,1124756_1137&fields=min_offer_price,min_list_price", "Home");
		lr_save_string( "https://search.unbxd.io/sites/perf-us-childrenplace-com702771527802758/products?id=&fields=min_offer_price,min_list_price", "Other");
	} else {
		lr_save_string( "https://search.unbxd.io/sites/perf-ca-childrenplace-com702771527815544/products?id=1124756_NJ,1069528_10,1123373_10,2075867_402,2112598_FM,2008354_766&fields=min_offer_price,min_list_price", "Home");
		lr_save_string( "https://search.unbxd.io/sites/perf-ca-childrenplace-com702771527815544/products?id=&fields=min_offer_price,min_list_price", "Other");
	}
	if ( strcmp(lr_eval_string("{callerId}"), "home") == 0) {
		//Home Only
		lr_start_sub_transaction ( "T01_Home Page_getPriceDetails_Unbxd", "T01_Home Page" ) ;
		web_url("getPriceDetails_Unbxd", 
			"URL={Home}",
			"Resource=0",
			"RecContentType=application/json", 
			"Mode=HTML", 
			LAST);			
		lr_end_sub_transaction ( "T01_Home Page_getPriceDetails_Unbxd" , LR_PASS ) ;
	} else {
		//Others
		lr_start_sub_transaction ( lr_eval_string("{mainTransaction}_getPriceDetails_Unbxd"), lr_eval_string("{mainTransaction}") ) ;
		web_url("getPriceDetails_Unbxd", 
			"URL={Other}",
			"Resource=0",
			"RecContentType=application/json", 
			"Mode=HTML", 
			LAST);			
		lr_end_sub_transaction ( lr_eval_string("{mainTransaction}_getPriceDetails_Unbxd") , LR_PASS ) ;
	}
*/
	return 0;
}

int getAllCoupons()
{
	web_reg_find("TEXT/IC=offersCount", "SaveCount=apiCheck", LAST);
	web_reg_find("TEXT/IC=\"couponCode\": \"FORTYOFF\",", "SaveCount=couponFortyOffApplied", LAST);
	lr_save_string("0", "availableCoupons_count");
//	web_reg_save_param("availableCoupons", "LB=\"couponCode\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"code": "Y032050Z799K1X"
//	web_reg_save_param("userCoupons", "LB=\"code\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"code": "Y032050Z799K1X"
//	web_reg_save_param("couponDescription", "LB=\"couponDescription\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"code": "Y032050Z799K1X"
	web_reg_save_param_json("ParamName=availableCoupons", "QueryString=$..availableCoupons.userCoupons..code", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=appliedCoupons", "QueryString=$..appliedCoupons.userCoupons..code", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

	lr_start_sub_transaction(lr_eval_string("{mainTransaction}_getAllCoupons"), lr_eval_string("{mainTransaction}") );

	web_add_header("Accept", "application/json");
	web_add_header("Expires", "0");

	if (isLoggedIn == 1) {
		web_add_header("source", "login");
	}
	registerErrorCodeCheck();
	addHeader();
	web_custom_request("getAllCoupons",
		"URL=https://{api_host}/v2/wallet/getAllCoupons",
		"Method=GET",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);

//	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheck}")) > 0)
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && strcmp(lr_eval_string("{apiCheck}") ,"0") != 0 )
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getAllCoupons"), LR_AUTO );
	else {

		lr_fail_trans_with_error( lr_eval_string("{mainTransaction}_getAllCoupons HTTP {httpReturnCode}, API Error Code: \"{userEmail}\", errorCode: {errorCode}), errorMessage: {errorMessage}" ) );
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getAllCoupons"), LR_FAIL );
	if (WRITE_TO_SUMO==1) {
 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_getAllCoupons - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
		sendErrorToSumo(); 
 	}
	}

	
	return 0;
}

int getPointsService()
{
	lr_start_sub_transaction(lr_eval_string("{mainTransaction}_getPointsService"), lr_eval_string("{mainTransaction}") );
	web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");
	addHeader();
	web_custom_request("getPointsService",
		"URL=https://{api_host}/v2/wallet/getPointsService",
		"Method=GET",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);
	lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getPointsService"), LR_AUTO );
	
	return 0;
}

webURL2(char *Url, char *pageName, char *textCheck)
{
	lr_save_string(Url, "Url");
	lr_save_string(pageName, "pageName");
	lr_save_string(textCheck, "textCheck");
	web_reg_find("SaveCount=txtCheckCount", "Text/IC={textCheck}", LAST);

	web_reg_find("SaveCount=noProducts", "Text/IC=We're sorry, there are no products available for purchase at this time. Please check back later.", LAST);

	web_url ( lr_eval_string("{pageName}") ,
    	     "URL={Url}" ,
             "Resource=0" ,
             "Mode=HTML" ,
             LAST ) ;

	if ( atoi(lr_eval_string("{txtCheckCount}")) > 0 )
		return LR_PASS;
	else
		return LR_FAIL;

}//end webURL


void endIteration()
{
	//lr_message ( "in endIteration" ) ;
}

void homePageCorrelations()
{
	int categoryIndex = 0;

	web_reg_save_param ( "categories" ,
//				 "LB=<a href=\"http://{host}{store_home_page}c/" ,  //uatlive1
				 "LB=<a href=\"{store_home_page}c/" ,  //perflive
				 "RB=\" class=\"navigation-level-one-link" ,
				 "ORD=ALL" , "Convert=HTML_TO_TEXT" , "NotFound=Warning", LAST ) ;


	if ( strcmp(lr_eval_string("{storeId}"), "10152") == 0 ) {
		index = rand () % 5 + 1 ;
	} else {
		index = rand () % 7 + 1 ;
	}

	switch(index)
	{
		case 1: web_reg_save_param_regexp ("ParamName=subcategory",	"RegExp=navigationTree.*?\"Girl\"(.*?)Toddler Girl\"",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST ); break;
		case 2: web_reg_save_param_regexp ("ParamName=subcategory", "RegExp=navigationTree.*?\"Boy\"(.*?)\"Toddler Boy\"",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST ); break;
		case 3: web_reg_save_param_regexp ("ParamName=subcategory", "RegExp=navigationTree.*?\"Toddler Boy\"(.*?)\"Baby\"", "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST ); break;
		case 4: web_reg_save_param_regexp ("ParamName=subcategory",	"RegExp=navigationTree.*?\"Baby\"(.*?)\"Shoes\"",       "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST ); break;
		case 5: web_reg_save_param_regexp ("ParamName=subcategory",	"RegExp=navigationTree.*?\"Baby\"(.*?)\"Shoes\"",	    "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST ); break;
		case 6: web_reg_save_param_regexp ("ParamName=subcategory", "RegExp=navigationTree.*?\"Shoes\"(.*?)\"Accessories\"","NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*",	LAST ); break;
		case 7: web_reg_save_param_regexp ("ParamName=subcategory", "RegExp=navigationTree.*?\"Accessories\"(.*?)}]]}]",    "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*",	LAST ); break;
		default: break;
	}
//	web_reg_save_param_regexp ("ParamName=preloadedState",	"RegExp=window.__PRELOADED_STATE__ = (.*?);\\n",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST );

}

void viewHomePage()
{
	int rc_getESPOT;
	int rc_getRegistered;
	
	lr_save_string("home", "callerId");

	homePageCorrelations();

	web_global_verification("Text/IC=The store has encountered a problem processing the last request",  "Fail=Found","ID=FindStoreProblem", LAST );

	lr_param_sprintf ( "homePageUrl" , "https://%s%shome" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}") ) ;

	lr_save_string("T01_Home Page", "mainTransaction");
	lr_start_transaction( "T01_Home Page" ) ;

	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO_HOME ) {
		registerErrorCodeCheck();
		lr_start_sub_transaction("T01_Home Page_Akamai", "T01_Home Page" ) ;
		if ( webURL2(lr_eval_string ( "{homePageUrl}" ), "T01_Home Page" , "The Children's Place" ) == LR_PASS) {
			lr_end_sub_transaction( "T01_Home Page_Akamai" , LR_AUTO ) ;
			if (ESPOT_FLAG == 1)
			{	lr_save_string("1", "getESpot");
				web_add_header("espotName", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,LOYAL_MiniBagMSpot,LOYAL_AddBagMspot,DT_SiteWide_Above_Header_nonStikcy,LOYAL_MyAccountEEPBannerMSpot,LOYAL_AccountDrawerEEPMSpot,LOYAL_MyAccountEEPActivityMSpot,LOYAL_MiniBagEarnRewardsMSpot,LOYAL_AddBagEarnRewardsMSpot,LOYAL_FullBagEarnRewardsMSpot,LOYAL_PickInStoreEarnRewardsMSpot,LOYAL_CheckoutShippingEarnRewardsMSpot,LOYAL_CheckoutBillingEarnRewardsMSpot,LOYAL_CheckoutReviewEarnRewardsMSpot,LOYAL_ConfirmationEarnRewardsMSpot,LOYAL_MiniBagPointToRewardsMSpot,LOYAL_FullBagPointToRewardsMSpot,LOYAL_PickInStorePointToRewardsMSpot,LOYAL_CheckoutShippingPointToRewardsMSpot,LOYAL_CheckoutBillingPointToRewardsMSpot,LOYAL_ConfirmationPointToRewardsMSpot,BOSS_PickupLaterCTA,BOSS_AddedToBagModal,BOSS_PDP_NoFavStore,BOSS_PDP_FavStore,primary_global_nav_sale_espot,secondary_global_nav_sale_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Shoes_espot,secondary_global_nav_Shoes_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Trending_espot,secondary_global_nav_Trending_espot");
				getESpot();
			}
			lr_save_string("homePage", "p_pageName");
			
			if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_GETREGISTERED)
				getRegisteredUserDetailsInfo();
			
			//   userGroup();
	
			lr_end_transaction( "T01_Home Page" , LR_PASS ) ;
		} else {
			lr_end_sub_transaction( "T01_Home Page_Akamai" , LR_FAIL ) ;
			lr_fail_trans_with_error( lr_eval_string("T01_Home Page Text Check Failed - {homePageUrl}") ) ;
			lr_end_transaction( "T01_Home Page" , LR_FAIL ) ;
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_getAllCoupons - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
			lr_exit(LR_EXIT_MAIN_ITERATION_AND_CONTINUE, LR_FAIL);
		}
		
	} else { 
	
			if (ESPOT_FLAG == 1)
			{	lr_save_string("1", "getESpot");
				web_add_header("espotName", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,LOYAL_MiniBagMSpot,LOYAL_AddBagMspot,DT_SiteWide_Above_Header_nonStikcy,LOYAL_MyAccountEEPBannerMSpot,LOYAL_AccountDrawerEEPMSpot,LOYAL_MyAccountEEPActivityMSpot,LOYAL_MiniBagEarnRewardsMSpot,LOYAL_AddBagEarnRewardsMSpot,LOYAL_FullBagEarnRewardsMSpot,LOYAL_PickInStoreEarnRewardsMSpot,LOYAL_CheckoutShippingEarnRewardsMSpot,LOYAL_CheckoutBillingEarnRewardsMSpot,LOYAL_CheckoutReviewEarnRewardsMSpot,LOYAL_ConfirmationEarnRewardsMSpot,LOYAL_MiniBagPointToRewardsMSpot,LOYAL_FullBagPointToRewardsMSpot,LOYAL_PickInStorePointToRewardsMSpot,LOYAL_CheckoutShippingPointToRewardsMSpot,LOYAL_CheckoutBillingPointToRewardsMSpot,LOYAL_ConfirmationPointToRewardsMSpot,BOSS_PickupLaterCTA,BOSS_AddedToBagModal,BOSS_PDP_NoFavStore,BOSS_PDP_FavStore,primary_global_nav_sale_espot,secondary_global_nav_sale_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Shoes_espot,secondary_global_nav_Shoes_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Trending_espot,secondary_global_nav_Trending_espot");
				getESpot();
			}
			lr_save_string("homePage", "p_pageName");
			if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_GETREGISTERED)
				getRegisteredUserDetailsInfo();

			//   userGroup();
			
			lr_end_transaction( "T01_Home Page" , LR_PASS ) ;
	}

	
	BROWSE_OPTIONS_FLAG = 1;
	lr_save_string("Other", "callerId");

}


void navByBrowse()
{
	int categoryIndex = 0;

	lr_think_time ( LINK_TT ) ;

	if ( strcmp(lr_eval_string("{storeId}"), "10152") == 0 )
	{
		index = rand () % 5 + 1 ;
		switch(index)
		{
			case 1: lr_save_string( "349517", "category"); break;
			case 2: lr_save_string( "349514", "category"); break;
			case 3: lr_save_string( "349515", "category"); break;
			case 4: lr_save_string( "349513", "category"); break;
			case 5: lr_save_string( "349518", "category"); break;
//			case 6: lr_save_string( "childrens-shoes-kids-shoes", "category"); break;
//			case 7: lr_save_string( "kids-accessories-canada", "category"); break;
			default: break;
		}
	}
	else
	{
			index = rand () % 5 + 1 ;
			switch(index)
			{
				case 1: lr_save_string( "47511", "category"); break;
				case 2: lr_save_string( "47502", "category"); break;
				case 3: lr_save_string( "47503", "category"); break;
				case 4: lr_save_string( "47501", "category"); break;
				case 5: lr_save_string( "47504", "category"); break;
/*				case 1: lr_save_string( "girls-clothing", "category"); break;
				case 2: lr_save_string( "toddler-girl-clothes", "category"); break;
				case 3: lr_save_string( "boys-clothing", "category"); break;
				case 4: lr_save_string( "toddler-boy-clothes", "category"); break;
				case 5: lr_save_string( "baby-clothes", "category"); break;
*/				
//				case 6: lr_save_string( "childrens-shoes-kids-shoes", "category"); break;
//				case 7: lr_save_string( "kids-accessories-us", "category"); break;
				default: break;
			}
	}
	
//	lr_save_string(lr_eval_string( "{categoryPathId}" ), "paginateCategory");
//	lr_save_string(lr_eval_string( "{categoryPathId}" ), "category");
	lr_save_string(lr_eval_string( "{category}" ), "paginateCategory");
	lr_param_sprintf ( "cateogryPageUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/") , lr_eval_string( "{category}" ) ) ;

//	categoryIndex = rand() % lr_paramarr_len("categories");
//	lr_save_string(lr_paramarr_idx( "categories", categoryIndex ), "category" ) ;
//	lr_save_int(categoryIndex, "categoryIndex");

//	lr_save_string(lr_paramarr_random( "categories" ), "category" ) ;
//	lr_param_sprintf ( "cateogryPageUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/") , lr_eval_string( "{category}" ) ) ;

	web_convert_param("cateogryPageUrl", "SourceEncoding=HTML","TargetEncoding=PLAIN", LAST );
	web_reg_save_param( "subCategories", "LB=navigation-level-two-link\" href=\"{store_home_page}c/", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //navigation-level-two-link" href="/us/c/girls-outerwear-jackets"

	lr_save_string("categoryDisplay", "callerId");
	lr_save_string("T02_Category Display", "mainTransaction");

	lr_start_transaction( "T02_Category Display" ) ;

	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO_CATEGORY ) {

		lr_start_transaction( "T02_Category Display_Akamai" ) ;
		webURL(lr_eval_string ( "{cateogryPageUrl}" ), "T02_Category Display");
		lr_end_transaction ( "T02_Category Display_Akamai" , LR_PASS ) ;

	} 
		unbxdCategoryTopNav();
		if (isLoggedIn == 1) { //turned off for akamai caching
			if (ESPOT_FLAG == 1)
			{	lr_save_string("1", "getESpot");
				web_add_header("espotName", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Gift_Shop_espot,secondary_global_nav_Gift_Shop_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot");
				getESpot();
				lr_save_string("2", "getESpot");
				web_add_header("espotName", "GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderPlccMoreInfoMdal,MobileNavHeaderLinks1,HeaderNavEspotLinks,DT_SiteWide_Above_Header_nonStikcy");
				getESpot();
			}
//			getListofDefaultWishlist(); //03132019
		
		} else {
			if (ESPOT_FLAG == 1)
			{			lr_save_string("1", "getESpot");
			web_add_header("espotName", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Gift_Shop_espot,secondary_global_nav_Gift_Shop_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot");
			getESpot();
			}
		}
			
		lr_save_string("productListing", "p_pageName");
		if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_GETREGISTERED)
			getRegisteredUserDetailsInfo();
		

		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 30 ) {
			//   userGroup();
		}
//		getFavouriteStoreLocation(); //03112019 this is not present in uatlive4 - bespin
	
//		preferences(); //03112019 this is not present in uatlive4 - bespin

//		unbxdCategoryTopNav(); 03122019 moved before getListofDefaultWishlist
		
		lr_end_transaction ( "T02_Category Display" , LR_PASS ) ;
	

	lr_save_string("", "mainTransaction");
}

void typeAheadSearch()
{	
	int Unbxd_size=0, Unbxd_tempsize=0;
	
 	if (UNBXD_FLAG == 1) {
		
	    for (i = 0; i < length; i++)
	    {
	       	if (i == 0)
	           	lr_param_sprintf ("SEARCH_STRING_PARAM", "%c", searchString[i]);
	       	else
	           	lr_param_sprintf ("SEARCH_STRING_PARAM", "%s%c", lr_eval_string("{SEARCH_STRING_PARAM}"), searchString[i]);
	
			if (i == 1) lr_save_string(lr_eval_string("{SEARCH_STRING_PARAM}"),"forDidYouMean");
	
	//     	type ahead search call after every 3rd characters
			if (i == 2 || i == 5 || i == 8 || i == 11 || i == 14 || i == 17 || i == 20 )
	       	{
	    		lr_think_time ( TYPE_SPEED );
	
				web_reg_save_param_json(
					"ParamName=autoSuggests",
					"QueryString=$..autosuggest",
					"SelectAll=Yes",
					"SEARCH_FILTERS",
					"NotFound=Warning",
				"LAST");
	    		
				lr_start_transaction ( "T02_Search_unbxd.search.autosuggest" ) ;
				web_url("getAutoSuggestions", 
					"URL=https://{UNBXD_host}/autosuggest?variants=true&q={SEARCH_STRING_PARAM}&version=V2&sourceFields=categoryLinkMap&sourceField.categoryLinkMap.count=4&keywordSuggestions.count=4&topQueries.count=4&popularProducts.count=4&popularProducts.fields=catgroup_id,product_name,imageUrl,min_list_price,min_offer_price,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,seo_token,uniqueId,product_type,low_offer_price,%20high_offer_price,%20low_list_price,%20high_list_price&uid={UID}",
					"Resource=0",
					"RecContentType=application/json", 
					"Mode=HTML", 
					LAST);			
				lr_end_transaction ( "T02_Search_unbxd.search.autosuggest" , LR_PASS ) ;
				//if there is a suggestion, capture it, exit the loop, use the suggestion for submitCompleteSearch
				if (atoi(lr_eval_string("{autoSuggests_count}")) > 0) {
					Unbxd_size=atoi(lr_eval_string("{autoSuggests_count}"));
					if((Unbxd_size)<=0)
						return;
					else {
						if((Unbxd_size)>1){
							srand(time(NULL));
							Unbxd_tempsize= rand() % Unbxd_size + 1 ;				
						} 
						
						lr_save_string( lr_paramarr_idx("autoSuggests", Unbxd_tempsize), "newSearchStr");
						
						break;
					}
				}
	       	} // end iF
	   	 } // end for
	}

} // end typeAheadSearch()

void submitCompleteSearch() // 	Search for complete string should be placed
{
	int Unbxd_size=0, Unbxd_tempsize=0;
	
 	if (UNBXD_FLAG == 1) {
		lr_think_time (LINK_TT);
		web_reg_save_param("totalProductsCount", "LB=totalProductsCount\":", "RB=,\"breadcrumbs", "NotFound=Warning", LAST);
	
	//	if (strlen(lr_eval_string("{newSearchStr}")) > 0 && atoi(lr_eval_string("{autoSuggests_count}")) > 0)
	//		lr_save_string(lr_eval_string("{newSearchStr}"), "searchTerm");
	//	else
			lr_save_string(lr_eval_string("{originalSearchString}"), "searchTerm");
	
		addHeader();
		web_add_header("count", "4");
		web_add_header("resultCount", "4");
		web_add_header("searchTerm", lr_eval_string("{searchTerm}"));
		web_add_header("timeAllowed", "15000");
	
		lr_start_transaction ( "T02_Search" );
		web_url("search",
			"URL=https://{host}{store_home_page}search/{searchTerm}",
			"Resource=0",
			"RecContentType=text/html",
			"Mode=HTTP",
			LAST);
	
		lr_start_sub_transaction("T02_Search_unbxd.search", "T02_Search");
	
		web_reg_save_param_json("ParamName=numberOfProducts", "QueryString=$.response.numberOfProducts", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json("ParamName=unbxd_PDPURL", "QueryString=$.response.products.*.seo_token", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json("ParamName=unbxd_productID", "QueryString=$.response.products.*.style_partno", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	
		web_reg_save_param_json( "ParamName=u_imagename", "QueryString=$.response.products..imagename", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json("ParamName=u_numberOfProducts", "QueryString=$.response.numberOfProducts", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	
		web_url("search",
	        "URL=https://{UNBXD_host}/search?start=0&rows=20&variants=true&variants.count=0&version=V2&facet.multiselect=true&selectedfacet=true&fields=alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPFitMessageUSSstore,TCPFit,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPSwatchesUSStore,%20%20%20%20%20%20%20%20%20%20top_rated,TCPSwatchesCanadaStore,product_name,TCPColor,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_fq,categoryPath3,categoryPath3_catMap,categoryPath2_catMap,%20%20%20%20%20%20%20%20%20%20product_short_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,seo_token,prodpartno,banner,facets,auxdescription,list_of_attributes,numberOfProducts,redirect,searchMetaData,didYouMean,%20%20%20%20%20%20%20%20%20%20TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,TcpBossCategoryDisabled,TcpBossProductDisabled,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,product_type,products,%20%20%20%20%20%20%20%20%20%20low_offer_price,%20high_offer_price,%20low_list_price,%20high_list_price&q={searchTerm}&uid={UID}",
	        "Resource=0",
			"Mode=HTTP",
			LAST);
	
		if (atoi(lr_eval_string("{numberOfProducts}")) == 0 )
			lr_end_sub_transaction("T02_Search_unbxd.search", LR_FAIL);
		else
			lr_end_sub_transaction("T02_Search_unbxd.search", LR_AUTO);
	
		lr_save_string("T02_Search", "mainTransaction");
	
		if (ESPOT_FLAG == 1)
		{	lr_save_string("1", "getESpot");
			web_add_header("espotName", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_sale_espot,secondary_global_nav_sale_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Shoes_espot,secondary_global_nav_Shoes_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Trending_espot,secondary_global_nav_Trending_espot");
			getESpot();
			if (isLoggedIn == 1) {
				lr_save_string("2", "getESpot");
				web_add_header("espotName", "GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderPlccMoreInfoMdal,MobileNavHeaderLinks1,HeaderNavEspotLinks,DT_SiteWide_Above_Header_nonStikcy");
				getESpot();
			} 
		}
	
		lr_save_string("search", "p_pageName");
		if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_GETREGISTERED)
			getRegisteredUserDetailsInfo();
		
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 30 ) {
			//   userGroup();
		}
		
	//	preferences(); //
	//	getFavouriteStoreLocation();	
	//	getListofDefaultWishlist(); //03132019
		
		lr_end_transaction("T02_Search", LR_AUTO);
	
		lr_save_string("search", "paginateBy");
		
		Unbxd_size=atoi(lr_eval_string("{unbxd_PDPURL_count}"));
		if((Unbxd_size)<=0)
		{
			return;
			
		} else  if((Unbxd_size)>1){
				
				srand(time(NULL));
				Unbxd_tempsize= rand() % Unbxd_size + 1 ;
				
			
			lr_save_string(lr_paramarr_idx("unbxd_PDPURL",Unbxd_tempsize),"unbxd_PDPURL");
			lr_save_string(lr_paramarr_idx("unbxd_productID",Unbxd_tempsize),"unbxd_productID");
		}
		else{
			lr_save_string(lr_paramarr_idx("unbxd_PDPURL",1),"unbxd_PDPURL");
			lr_save_string(lr_paramarr_idx("unbxd_productID",1),"unbxd_productID");	
		}
	}
	
	return;

} // end submitCompleteSearch

void submitCompleteSearchDidYouMean() // 	Search with "Did You Mean"
{
	//lr_message ( "In submitCompleteSearch" ) ;
 	lr_think_time (LINK_TT);

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_SEARCH_SUGGEST )
		lr_param_sprintf ("SEARCH_STRING", "%s", lr_eval_string("{forDidYouMean}"));
	else
		lr_param_sprintf ("SEARCH_STRING", "%s", searchString);

	web_reg_find("Text= 0 matches", "SaveCount=searchResults_Count", LAST );
	web_reg_find("Text=The store has encountered a problem", "SaveCount=storeProblem_Count", LAST ); //The store has encountered a problem processing the last request. Try again later. If the problem persists, contact your site administrator.
	web_reg_save_param("didYouMeanId", "LB=https://{host}/shop/SearchDisplay?searchTermScope=&searchType=1002&filterTerm=&orderBy=&maxPrice=&showResultsPage=true&langId=-1&departmentId=&sType=",
	"RB=\" class=\"result\">", "ORD=All", "NotFound=Warning", LAST);

	lr_start_transaction ( "T02_Search" ) ;

	web_url("submitSearch",
		//"URL=https://{host}/shop/SearchDisplay?storeId={storeId}&catalogId={catalogId}&langId=-1&pageSize=100&beginIndex=0&searchSource=Q&sType=SimpleSearch&resultCatEntryType=2&showResultsPage=true&pageView=image&custSrch=search&searchTerm={SEARCH_STRING}&TCPSearchSubmit=",
		"URL=https://{host}/shop/SearchDisplay?searchTerm={{SEARCH_STRING}}&storeId={storeId}&catalogId={catalogId}&langId=-1&pageSize=100&beginIndex=0&searchSource=Q&sType=SimpleSearch&resultCatEntry=2&showResultsPage=true&pageView=image&custSrch=search"
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);

		lr_save_string("T02_Search", "mainTransaction");
//		callAPI();

	if (atoi(lr_eval_string("{searchResults_Count}")) == 0 || atoi(lr_eval_string("{storeProblem_Count}")) == 0 )
		lr_end_transaction ( "T02_Search" , LR_FAIL ) ;
	else
		lr_end_transaction ( "T02_Search" , LR_PASS ) ;

} // end submitCompleteSearch

writeToFile1()
{
	char *ip;
    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LowInventory_US_v2.dat";
	long file1;
//	char * filename1 = "\\\\10.56.29.36\\e$\\Performance\\Scripts\\2016_R1\\03_PERF\\Datafiles\\TestData\\searchStrNoResult.dat";
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath, ip);
	strcat(fullpath, filename1);

    if ((file1 = fopen(fullpath, "a+")) == NULL) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }

	fprintf(file1, "%s\n", lr_eval_string("{logLowCatEntryId}"));

    fclose(file1);
	return 0;
}

writeToFile2()
{
	char *ip;
    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LowInventory_CA_v2.dat";
	long file1;
//	char * filename1 = "\\\\10.56.29.36\\e$\\Performance\\Scripts\\2016_R1\\03_PERF\\Datafiles\\TestData\\searchStrNoResult.dat";
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath, ip);
	strcat(fullpath, filename1);

    if ((file1 = fopen(fullpath, "a+")) == NULL) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }

	fprintf(file1, "%s\n", lr_eval_string("{logLowCatEntryId}"));

    fclose(file1);
	return 0;
}

void navBySearch()
{
	int searchSuggest = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	lr_think_time ( TYPE_SPEED );
//	searchString = "00889705462289"; //this is the search string from the data file
	searchString = lr_eval_string("{searchStr}"); //this is the search string from the data file
	lr_save_string( "", "newSearchStr");
	lr_save_string(searchString,"originalSearchString");
	/****************REMOVE**********/
/*	searchString="boys";
	lr_save_string("boys",	"SEARCH_STRING_PARAM");
	lr_save_string("boys",	"originalSearchString");
	lr_save_string("boys",	"searchStr");
*/
    length = (int)strlen(searchString);

    if (length >= 3) //type ahead only if search term is greater than 3 characters
    	typeAheadSearch();

//	if ( searchSuggest < RATIO_SEARCH_SUGGEST )
//		submitCompleteSearchDidYouMean();
//	else
		submitCompleteSearch();

} //end navBySearch


void sortResults()
{
 	if (UNBXD_FLAG == 1) {
	 	lr_think_time ( LINK_TT ) ;
	
		index = rand () % 5 + 1 ;
		switch(index)
		{
			case 1: lr_save_string( "min_offer_price%20desc",		"sortBy" ); break;
			case 2: lr_save_string( "min_offer_price%20asc",		"sortBy" ); break;
			case 3: lr_save_string( "newest_score%20desc",			"sortBy" ); break;
			case 4: lr_save_string( "favoritedcount%20desc",		"sortBy" ); break;
			case 5: lr_save_string( "TCPBazaarVoiceRating%20desc",	"sortBy" ); break;
			default: break;
		}
	
		lr_start_transaction("T03_Sort");

	 	if (strcmp(lr_eval_string("{searchTerm}"), "") == 0 ) {
	
				lr_start_sub_transaction("T03_Sort_unbxd.category","T03_Sort");
			
				web_reg_save_param_json("ParamName=numberOfProducts", "QueryString=$.response.numberOfProducts", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
				
				web_url("sortByCategory",
					"URL=https://{UNBXD_host}/category?start=0&rows=20&variants=true&variants.count=0&version=V2&facet.multiselect=true&selectedfacet=true&fields=alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPFitMessageUSSstore,TCPFit,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPSwatchesUSStore,%20%20%20%20%20%20%20%20%20%20top_rated,TCPSwatchesCanadaStore,product_name,TCPColor,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_fq,categoryPath3,categoryPath3_catMap,categoryPath2_catMap,%20%20%20%20%20%20%20%20%20%20product_short_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,seo_token,prodpartno,banner,facets,auxdescription,list_of_attributes,numberOfProducts,redirect,searchMetaData,didYouMean,%20%20%20%20%20%20%20%20%20%20TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,TcpBossCategoryDisabled,TcpBossProductDisabled,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,product_type,products,%20%20%20%20%20%20%20%20%20%20low_offer_price,%20high_offer_price,%20low_list_price,%20high_list_price&pagetype=boolean&p-id=categoryPathId:%22{subCategoryMain}%3E{subCategoryL1}%22&sort={sortBy}&uid={UID}",
					"Resource=0",
					"Mode=HTTP",
					LAST);
		
				if (atoi(lr_eval_string("{numberOfProducts}")) == 0 )
					lr_end_sub_transaction("T03_Sort_unbxd.category", LR_FAIL);
				else
					lr_end_sub_transaction("T03_Sort_unbxd.category", LR_AUTO);
	 	} else {
			lr_start_sub_transaction("T03_Sort_unbxd.search","T03_Sort");
	
			web_reg_save_param_json("ParamName=numberOfProducts", "QueryString=$.response.numberOfProducts", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	
			web_url("sortBySearchTerm",
				"URL=https://{UNBXD_host}/search?start=0&rows=20&variants=true&variants.count=0&version=V2&facet.multiselect=true&selectedfacet=true&fields=alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPFitMessageUSSstore,TCPFit,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPSwatchesUSStore,%20%20%20%20%20%20%20%20%20%20top_rated,TCPSwatchesCanadaStore,product_name,TCPColor,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_fq,categoryPath3,categoryPath3_catMap,categoryPath2_catMap,%20%20%20%20%20%20%20%20%20%20product_short_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,seo_token,prodpartno,banner,facets,auxdescription,list_of_attributes,numberOfProducts,redirect,searchMetaData,didYouMean,%20%20%20%20%20%20%20%20%20%20TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,TcpBossCategoryDisabled,TcpBossProductDisabled,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,product_type,products,%20%20%20%20%20%20%20%20%20%20low_offer_price,%20high_offer_price,%20low_list_price,%20high_list_price&q={searchTerm}&sort={sortBy}&uid={UID}",
				"Resource=0",
				"Mode=HTTP",
				LAST);
	
			if (atoi(lr_eval_string("{numberOfProducts}")) == 0 )
					lr_end_sub_transaction("T03_Sort_unbxd.search", LR_FAIL);
				else
					lr_end_sub_transaction("T03_Sort_unbxd.search", LR_AUTO);
	 	}
	lr_save_string("T03_Sort", "mainTransaction");
//	getListofDefaultWishlist(); 03132019
	lr_end_sub_transaction("T03_Sort", LR_AUTO);
	}

} // end sortResultsvoid 

void paginate()
{

	int itemCounter = 0;
 	if (UNBXD_FLAG == 1) {
		index = rand () % 5 + 1 ;
		switch(index)
		{
			case 1: lr_save_string( "min_offer_price%20desc",		"sortBy" ); break;
			case 2: lr_save_string( "min_offer_price%20asc",		"sortBy" ); break;
			case 3: lr_save_string( "newest_score%20desc",			"sortBy" ); break;
			case 4: lr_save_string( "favoritedcount%20desc",		"sortBy" ); break;
			case 5: lr_save_string( "TCPBazaarVoiceRating%20desc",	"sortBy" ); break;
			default: break;
		}


	//	if ( atoi(lr_eval_string("{totalProductsCount}")) > 0  ) {

			lr_start_transaction ( "T03_Paginate" ) ;

			web_reg_save_param_json("ParamName=numberOfProducts", "QueryString=$.response.numberOfProducts", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		
			if(strcmp(lr_eval_string("{paginateBy}"), "search") == 0 ) {
				lr_start_sub_transaction("T03_Paginate_unbxd.search", "T03_Paginate");

				web_url("searchBySearchTerm",
					"URL=https://{UNBXD_host}/search?start=20&rows=20&variants=true&variants.count=0&version=V2&facet.multiselect=true&selectedfacet=true&fields=alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPFitMessageUSSstore,TCPFit,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPSwatchesUSStore,%20%20%20%20%20%20%20%20%20%20top_rated,TCPSwatchesCanadaStore,product_name,TCPColor,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_fq,categoryPath3,categoryPath3_catMap,categoryPath2_catMap,%20%20%20%20%20%20%20%20%20%20product_short_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,seo_token,prodpartno,banner,facets,auxdescription,list_of_attributes,numberOfProducts,redirect,searchMetaData,didYouMean,%20%20%20%20%20%20%20%20%20%20TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,TcpBossCategoryDisabled,TcpBossProductDisabled,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,product_type,products,%20%20%20%20%20%20%20%20%20%20low_offer_price,%20high_offer_price,%20low_list_price,%20high_list_price&q={searchTerm}&sort={sortBy}", //&uid=uid-1564684761584-50208
					"Resource=0",
					"Mode=HTTP",
					LAST);

				if (atoi(lr_eval_string("{numberOfProducts}")) == 0 )
					lr_end_sub_transaction("T03_Paginate_unbxd.search", LR_FAIL);
				else
					lr_end_sub_transaction("T03_Paginate_unbxd.search", LR_AUTO);
			
			} else { //paginateBy Sub-Category
				lr_start_sub_transaction("T03_Paginate_unbxd.category", "T03_Paginate");

				web_url("searchByCategory",
					"URL=https://{UNBXD_host}/category?start=20&rows=20&variants=true&variants.count=0&version=V2&facet.multiselect=true&selectedfacet=true&fields=alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPFitMessageUSSstore,TCPFit,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPSwatchesUSStore,top_rated,TCPSwatchesCanadaStore,product_name,TCPColor,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_fq,categoryPath3,categoryPath3_catMap,categoryPath2_catMap,product_short_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,seo_token,prodpartno,banner,facets,auxdescription,list_of_attributes,numberOfProducts,redirect,searchMetaData,didYouMean,TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,TcpBossCategoryDisabled,TcpBossProductDisabled,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore&pagetype=boolean&p-id=categoryPathId:%22{subCategoryMain}%3E{subCategoryL1}%22&sort={sortBy}",
	                "Resource=0",
					"Mode=HTTP",
					LAST);

				if (atoi(lr_eval_string("{numberOfProducts}")) == 0 )
					lr_end_sub_transaction("T03_Paginate_unbxd.category", LR_FAIL);
				else
					lr_end_sub_transaction("T03_Paginate_unbxd.category", LR_AUTO);

			}
			lr_save_string("T03_Paginate", "mainTransaction");
	//		getListofDefaultWishlist(); 03132019
			
			lr_end_transaction ( "T03_Paginate" , LR_PASS ) ;
//		}
	}
} // end pagination


sortOrPaginate()
{
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= APPLY_SORT )
	{
		sortResults();
		
	} else {
		
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= APPLY_PAGINATE )
			paginate();
	}
	return 0;
} // end sortOrPaginate

void drillOneFacet()
{
//	if ( lr_paramarr_len ( "subCategories" ) > 0 ) {

		lr_think_time ( LINK_TT ) ;
//		while ( checkOutfit() == LR_FAIL) {}
		web_reg_save_param ( "sizesFacetsURL", "LB=\"id\":\"ads_f16501_ntk_cs", "RB=\"},{\"displayName", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", LAST );//value":"ads_f16501_ntk_cs%3A%22YOUTH+12%22","extendedData
		web_reg_save_param ( "colorFacetsURL", "LB=\"id\":\"ads_f12001_ntk_cs", "RB=\"}", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", LAST );//value":"ads_f12001_ntk_cs%3A%22YOUTH+12%22","extendedData
		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"generalProductId":"
		lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ( "{subCategory}" ) ) ; //from datafile for unbxd

		lr_save_string("T03_Sub-Category Display", "mainTransaction");
		lr_save_string("T03_Sub-Category Display","callerId");
		lr_start_transaction ( "T03_Sub-Category Display" ) ;

		if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO_SUBCATEGORY ) { 

			lr_start_transaction ( "T03_Sub-Category Display_Akamai" ) ;
			webURL(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display" );
			lr_end_transaction ( "T03_Sub-Category Display_Akamai" , LR_AUTO ) ;
		
		} 
		
		if (unbxdCategoryL1L2() == LR_FAIL)
		{
			lr_end_transaction ( "T03_Sub-Category Display" , LR_FAIL ) ;
			lr_save_string("categoryDisplay", "callerId");
			lr_save_string("", "mainTransaction");
			return;
		}
		if (isLoggedIn == 1) { 
			if (ESPOT_FLAG == 1)
			{	lr_save_string("1", "getESpot");
				web_add_header("espotName", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Gift_Shop_espot,secondary_global_nav_Gift_Shop_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot");
				getESpot();
				lr_save_string("2", "getESpot");
				web_add_header("espotName", "GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderPlccMoreInfoMdal,MobileNavHeaderLinks1,HeaderNavEspotLinks,DT_SiteWide_Above_Header_nonStikcy");
				getESpot();
			}
//					getListofDefaultWishlist(); 03132019
		} else {
			if (ESPOT_FLAG == 1) {
				lr_save_string("1", "getESpot");
				web_add_header("espotName", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Gift_Shop_espot,secondary_global_nav_Gift_Shop_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot");
				getESpot();
			}
		}
			
		lr_save_string("productListing", "p_pageName");
		if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_GETREGISTERED)
			getRegisteredUserDetailsInfo();
		
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 30 ) {
			//   userGroup();
		}
//				getFavouriteStoreLocation(); //03112019 - this is not present in uatlive4 - bespin
	
//				preferences();//03112019 - this is not present in uatlive4 - bespin
		
		lr_save_string("", "mainTransaction");

		lr_end_transaction ( "T03_Sub-Category Display" , LR_PASS ) ;
		

		lr_save_string("categoryDisplay", "callerId");
	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 50 )
		{
			if (( lr_paramarr_len ( "unbxd_TCPSize_uFilter" ) > 0 )) {
				lr_save_string(lr_paramarr_random("unbxd_TCPSize_uFilter"), "facet");
//				web_convert_param("facet", "SourceEncoding=HTML", "TargetEncoding=URL", LAST );
				lr_param_sprintf ( "drillUrlOneFacet" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ( "{subCategory}?v_tcpsize_uFilter={facet}" ) ) ;
				lr_save_string(lr_eval_string("v_tcpsize_uFilter:%22{facet}%22"), "facet");
			}
			else
				return;
		}
		else
		{
			if (( lr_paramarr_len ( "unbxd_TCPColor_uFilter" ) > 0 )) {
				lr_save_string(lr_paramarr_random("unbxd_TCPColor_uFilter"), "facet");
//				web_convert_param("facet", "SourceEncoding=HTML", "TargetEncoding=URL", LAST );
				lr_param_sprintf ( "drillUrlOneFacet" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ( "{subCategory}?TCPColor_uFilter={facet}" ) ) ;
				lr_save_string(lr_eval_string("TCPColor_uFilter:%22{facet}%22"), "facet");
			}
			else
				return;
		}

		lr_start_transaction ( "T03_Facet Display_1facet" ) ;

		webURL(lr_eval_string ( "{drillUrlOneFacet}" ), "T03_Facet Display_1facet" );
		lr_save_string("T03_Facet Display_1facet", "mainTransaction");
	 	if (UNBXD_FLAG == 1) {
			lr_start_sub_transaction("T03_Facet Display_1facet_unbxd.search.filter", "T03_Facet Display_1facet");
	
			web_custom_request("filter",
				"URL=https://{UNBXD_host}/category?filter={facet}&start=0&rows=20&variants=true&variants.count=0&version=V2&facet.multiselect=true&selectedfacet=true&fields=alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPFitMessageUSSstore,TCPFit,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPSwatchesUSStore,%20%20%20%20%20%20%20%20%20%20top_rated,TCPSwatchesCanadaStore,product_name,TCPColor,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_fq,categoryPath3,categoryPath3_catMap,categoryPath2_catMap,%20%20%20%20%20%20%20%20%20%20product_short_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,seo_token,prodpartno,banner,facets,auxdescription,list_of_attributes,numberOfProducts,redirect,searchMetaData,didYouMean,%20%20%20%20%20%20%20%20%20%20TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,TcpBossCategoryDisabled,TcpBossProductDisabled,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,product_type,products,%20%20%20%20%20%20%20%20%20%20low_offer_price,%20high_offer_price,%20low_list_price,%20high_list_price&pagetype=boolean&p-id=categoryPathId:%22{subCategoryMain}%3E{subCategoryL1}%3E{subCategoryL2}%22&uid=uid-1567537522986-54828",
				"Method=GET",
				"Resource=0",
				"RecContentType=text/html",
				"EncType=application/x-www-form-urlencoded",
				LAST);
	
			lr_end_sub_transaction("T03_Facet Display_1facet_unbxd.search.filter", LR_AUTO);
			
	//		getListofDefaultWishlist(); 03132019
			
	//		if (atoi(lr_eval_string("{totalProductsCount}")) == 0)
	//			lr_end_transaction ( "T03_Facet Display_1facet" , LR_FAIL ) ;
	//		else
				lr_end_transaction ( "T03_Facet Display_1facet" , LR_PASS ) ;
		} // end if
} // end drillOneFacet

void drillTwoFacets()
{
	int nCount = 0;

//	if ( lr_paramarr_len ( "subCategories" ) > 0 ) {

	 	lr_think_time ( LINK_TT ) ;
//		while ( checkOutfit() == LR_FAIL) {}
		web_reg_save_param ( "sizesFacetsURL", "LB=\"id\":\"ads_f16501_ntk_cs", "RB=\"},{\"displayName", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", LAST );//value":"ads_f16501_ntk_cs%3A%22YOUTH+12%22","extendedData
		web_reg_save_param ( "colorFacetsURL", "LB=\"id\":\"ads_f12001_ntk_cs", "RB=\"}", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", LAST );//value":"ads_f12001_ntk_cs%3A%22YOUTH+12%22","extendedData
		web_reg_save_param( "pdpURL", "LB=,\"pdpUrl\":\"{store_home_page}p/", "RB=\",\"shortDescription", "ORD=All", "NotFound=Warning", LAST); //,"pdpUrl":"/us/p/Girls-Basic-Skinny-Jeans---True-Indigo-Wash-2069116-JP","shortDescription
		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"generalProductId":"
		lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ( "{subCategory}" ) ) ; //from datafile for unbxd

		lr_save_string("T03_Sub-Category Display", "mainTransaction");
		lr_start_transaction ( "T03_Sub-Category Display" ) ;

		if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO_SUBCATEGORY ) { //04162018 - enabled

			lr_start_transaction ( "T03_Sub-Category Display_Akamai" ) ;
			webURL(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display" );
			lr_end_transaction ( "T03_Sub-Category Display_Akamai" , LR_AUTO ) ;

		}

			if (unbxdCategoryL1L2() == LR_FAIL)
			{
				lr_end_transaction ( "T03_Sub-Category Display" , LR_FAIL ) ;
				lr_save_string("categoryDisplay", "callerId");
				lr_save_string("", "mainTransaction");
				return;
			}

			if (isLoggedIn == 1) { //turned off for akamai caching
				if (ESPOT_FLAG == 1)
				{		lr_save_string("1", "getESpot");
						web_add_header("espotName", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Gift_Shop_espot,secondary_global_nav_Gift_Shop_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot");
						getESpot();
						lr_save_string("2", "getESpot");
						web_add_header("espotName", "GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderPlccMoreInfoMdal,MobileNavHeaderLinks1,HeaderNavEspotLinks,DT_SiteWide_Above_Header_nonStikcy");
						getESpot();
				}
//					getListofDefaultWishlist(); 03132019
			} else {
				if (ESPOT_FLAG == 1) {
					lr_save_string("1", "getESpot");
					web_add_header("espotName", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Gift_Shop_espot,secondary_global_nav_Gift_Shop_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot");
					getESpot();
				}
			}
				
			lr_save_string("productListing", "p_pageName");
			if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_GETREGISTERED)
				getRegisteredUserDetailsInfo();
			
			if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 30 ) {
				//   userGroup();
			}
			lr_save_string("", "mainTransaction");
//			}
		lr_end_transaction ( "T03_Sub-Category Display" , LR_PASS ) ;
		//}

		lr_save_string("categoryDisplay", "callerId");
	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 80 ) //80
		{
			if (( lr_paramarr_len ( "unbxd_TCPSize_uFilter" ) > 0 )) {
				lr_save_string(lr_paramarr_random("unbxd_TCPSize_uFilter"), "facet1");
//				web_convert_param("facet1", "SourceEncoding=HTML", "TargetEncoding=URL", LAST );
				lr_param_sprintf ( "drillUrlTwoFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?v_tcpsize_uFilter={facet1}" ) ) ;
				lr_save_string(lr_eval_string("v_tcpsize_uFilter:%22{facet1}%22"), "facet");
				nCount = lr_paramarr_len("unbxd_TCPSize_uFilter");
				while( nCount >= 2)
				{
					lr_save_string(lr_paramarr_random("unbxd_TCPSize_uFilter"), "facet2");
//					web_convert_param("facet2", "SourceEncoding=HTML", "TargetEncoding=URL", LAST );
					if (strcmp(lr_eval_string("{facet2}"), lr_eval_string("{facet1}")) != 0 )
					{	lr_param_sprintf ( "drillUrlTwoFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?v_tcpsize_uFilter={facet1}%2C{facet2}" ) ) ;
						lr_save_string(lr_eval_string("v_tcpsize_uFilter:%22{facet1}%22%20OR%20v_tcpsize_uFilter:%22{facet2}%22"), "facet");
						break;
					}
				}
			}
			else
				return;
		}
		else
		{
			if (( lr_paramarr_len ( "unbxd_TCPColor_uFilter" ) > 0 )) {
				lr_save_string(lr_paramarr_random("unbxd_TCPColor_uFilter"), "facet1");
//				web_convert_param("facet1", "SourceEncoding=HTML", "TargetEncoding=URL", LAST );
				lr_param_sprintf ( "drillUrlTwoFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?TCPColor_uFilter={facet1}" ) );
				lr_save_string(lr_eval_string("TCPColor_uFilter:%22{facet1}%22"), "facet");
				nCount = lr_paramarr_len("unbxd_TCPColor_uFilter");
				while( nCount >= 2)
				{
					lr_save_string(lr_paramarr_random("unbxd_TCPColor_uFilter"), "facet2");
//					web_convert_param("facet2", "SourceEncoding=HTML", "TargetEncoding=URL", LAST );
					if (strcmp(lr_eval_string("{facet2}"), lr_eval_string("{facet1}")) != 0 )
					{	lr_param_sprintf ( "drillUrlTwoFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?TCPColor_uFilter={facet1}%2C{facet2}" ) ) ;
						lr_save_string(lr_eval_string("TCPColor_uFilter:%22{facet1}%22%20OR%20TCPColor_uFilter:%22{facet2}%22"), "facet");
						break;
					}
				}
			}
			else
				return;
		}

		lr_start_transaction ( "T03_Facet Display_2facets" ) ;

		webURL(lr_eval_string ( "{drillUrlTwoFacets}" ), "T03_Facet Display_2facets" );

	 	if (UNBXD_FLAG == 1) {
			lr_save_string("T03_Facet Display_2facets", "mainTransaction");
			lr_start_sub_transaction("T03_Facet Display_2facets_unbxd.search.filter", "T03_Facet Display_2facets");
	
			web_custom_request("getProductviewbyCategory",
				"URL=https://{UNBXD_host}/category?filter={facet}&start=0&rows=20&variants=true&variants.count=0&version=V2&facet.multiselect=true&selectedfacet=true&fields=alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPFitMessageUSSstore,TCPFit,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPSwatchesUSStore,%20%20%20%20%20%20%20%20%20%20top_rated,TCPSwatchesCanadaStore,product_name,TCPColor,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_fq,categoryPath3,categoryPath3_catMap,categoryPath2_catMap,%20%20%20%20%20%20%20%20%20%20product_short_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,seo_token,prodpartno,banner,facets,auxdescription,list_of_attributes,numberOfProducts,redirect,searchMetaData,didYouMean,%20%20%20%20%20%20%20%20%20%20TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,TcpBossCategoryDisabled,TcpBossProductDisabled,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,product_type,products,%20%20%20%20%20%20%20%20%20%20low_offer_price,%20high_offer_price,%20low_list_price,%20high_list_price&pagetype=boolean&p-id=categoryPathId:%22{subCategoryMain}%3E{subCategoryL1}%3E{subCategoryL2}%22&uid=uid-1567537522986-54828",
				"Method=GET",
				"Resource=0",
				"RecContentType=text/html",
				"EncType=application/x-www-form-urlencoded",
				LAST);
	
			lr_end_sub_transaction("T03_Facet Display_2facets_unbxd.search.filter", LR_AUTO);

	//		getListofDefaultWishlist(); 03132019
	
	//		if (atoi(lr_eval_string("{totalProductsCount}")) == 0)
	//			lr_end_transaction ( "T03_Facet Display_2facets" , LR_FAIL ) ;
	//		else
				lr_end_transaction ( "T03_Facet Display_2facets" , LR_PASS ) ;
		}
//	} // end if
} // end drillTowFacets

void drillThreeFacets()
{
	int nCount = 0;
	//drillTwoFacets();

//	if ( lr_paramarr_len ( "subCategories" ) > 0 ) {
		lr_think_time ( LINK_TT ) ;

//		while ( checkOutfit() == LR_FAIL) {}
		web_reg_save_param ( "sizesFacetsURL", "LB=\"id\":\"ads_f16501_ntk_cs", "RB=\"},{\"displayName", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", LAST );//value":"ads_f16501_ntk_cs%3A%22YOUTH+12%22","extendedData
		web_reg_save_param ( "colorFacetsURL", "LB=\"id\":\"ads_f12001_ntk_cs", "RB=\"}", "ORD=ALL" , "Convert=HTML_TO_URL", "NotFound=warning", LAST );//value":"ads_f12001_ntk_cs%3A%22YOUTH+12%22","extendedData
		lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ( "{subCategory}" ) ) ;
		web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"generalProductId":"
		lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ( "{subCategory}" ) ) ; //from datafile for unbxd
		lr_save_string("T03_Sub-Category Display", "mainTransaction");
		lr_start_transaction ( "T03_Sub-Category Display" ) ;

		if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO_SUBCATEGORY ) { //04162018 - enabled
			lr_start_transaction ( "T03_Sub-Category Display_Akamai" ) ;
			webURL(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display" );
			lr_end_transaction ( "T03_Sub-Category Display_Akamai" , LR_AUTO ) ;
		} 

		if (unbxdCategoryL1L2() == LR_FAIL)
		{
			lr_end_transaction ( "T03_Sub-Category Display" , LR_FAIL ) ;
			lr_save_string("categoryDisplay", "callerId");
			lr_save_string("", "mainTransaction");
			return;
		}
		if (isLoggedIn == 1) { //turned off for akamai caching
			if (ESPOT_FLAG == 1)
			{	lr_save_string("1", "getESpot");
				web_add_header("espotName", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Gift_Shop_espot,secondary_global_nav_Gift_Shop_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot");
				getESpot();
				lr_save_string("2", "getESpot");
				web_add_header("espotName", "GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderPlccMoreInfoMdal,MobileNavHeaderLinks1,HeaderNavEspotLinks,DT_SiteWide_Above_Header_nonStikcy");
				getESpot();
			}
//					getListofDefaultWishlist(); 03132019
		} else {
			if (ESPOT_FLAG == 1) {
				lr_save_string("1", "getESpot");
				web_add_header("espotName", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Gift_Shop_espot,secondary_global_nav_Gift_Shop_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot");
				getESpot();
			}
		}
			
		lr_save_string("productListing", "p_pageName");
		if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_GETREGISTERED)
			getRegisteredUserDetailsInfo();
		
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 30 ) {
			//   userGroup();
		}
		
		lr_save_string("", "mainTransaction");
		lr_end_transaction ( "T03_Sub-Category Display" , LR_PASS ) ;

		lr_save_string("categoryDisplay", "callerId");
	    if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 80 ) //80
		{
			if (( lr_paramarr_len ( "unbxd_TCPSize_uFilter" ) > 0 )) {
				lr_save_string(lr_paramarr_random("unbxd_TCPSize_uFilter"), "facet1");
				lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?v_tcpsize_uFilter={facet1}" ) ) ;
				lr_save_string(lr_eval_string("v_tcpsize_uFilter:%22{facet1}%22"), "facet");

				nCount = lr_paramarr_len("unbxd_TCPSize_uFilter");
				while( nCount >= 2)
				{
					lr_save_string(lr_paramarr_random("unbxd_TCPSize_uFilter"), "facet2");
					if (strcmp(lr_eval_string("{facet2}"), lr_eval_string("{facet1}")) != 0 )
					{	lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?v_tcpsize_uFilter={facet1}%2C{facet2}" ) ) ;
						lr_save_string(lr_eval_string("v_tcpsize_uFilter:%22{facet1}%2C{facet2}"), "facet");
						lr_save_string(lr_eval_string("v_tcpsize_uFilter:%22{facet1}%22%20OR%20v_tcpsize_uFilter:%22{facet2}%22"), "facet");
						break;
					}
				}
				nCount = lr_paramarr_len("unbxd_TCPSize_uFilter");
				while( nCount >= 3)
				{
					lr_save_string(lr_paramarr_random("unbxd_TCPSize_uFilter"), "facet3");
					if (strcmp(lr_eval_string("{facet3}"), lr_eval_string("{facet1}")) != 0 && strcmp(lr_eval_string("{facet3}"), lr_eval_string("{facet2}")) != 0 )
					{	lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?v_tcpsize_uFilter={facet1}%2C{facet2}%2C{facet3}" ) ) ;
						lr_save_string(lr_eval_string("v_tcpsize_uFilter:%22{facet1}%22%20OR%20v_tcpsize_uFilter:%22{facet2}%22%20OR%20v_tcpsize_uFilter:%22{facet3}%22"), "facet");
						break;
					}
				}
			}
			else
				return;
		}
		else
		{
			if (( lr_paramarr_len ( "unbxd_TCPColor_uFilter" ) > 0 )) {
				lr_save_string(lr_paramarr_random("unbxd_TCPColor_uFilter"), "facet1");
				lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?TCPColor_uFilter={facet1}" ) ) ;
				lr_save_string(lr_eval_string("TCPColor_uFilter:%22{facet1}%22"), "facet");
				nCount = lr_paramarr_len("unbxd_TCPColor_uFilter");
				while( nCount >= 2)
				{
					lr_save_string(lr_paramarr_random("unbxd_TCPColor_uFilter"), "facet2");
					if (strcmp(lr_eval_string("{facet2}"), lr_eval_string("{facet1}")) != 0 )
					{	lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?TCPColor_uFilter={facet1}%2C{facet2}" ) ) ;
						lr_save_string(lr_eval_string("TCPColor_uFilter:%22{facet1}%2C{facet2}"), "facet");
						lr_save_string(lr_eval_string("TCPColor_uFilter:%22{facet1}%22%20OR%20TCPColor_uFilter:%22{facet2}%22"), "facet");
						break;
					}
				}
				nCount = lr_paramarr_len("unbxd_TCPColor_uFilter");
				while( nCount >= 3)
				{
					lr_save_string(lr_paramarr_random("unbxd_TCPColor_uFilter"), "facet3");
					if (strcmp(lr_eval_string("{facet3}"), lr_eval_string("{facet1}")) != 0 && strcmp(lr_eval_string("{facet3}"), lr_eval_string("{facet2}")) != 0 )
					{	lr_param_sprintf ( "drillUrlThreeFacets" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ("{subCategory}?TCPColor_uFilter={facet1}%2C{facet2}%2C{facet3}" ) ) ;
						lr_save_string(lr_eval_string("TCPColor_uFilter:%22{facet1}%22%20OR%20TCPColor_uFilter:%22{facet2}%22%20OR%20TCPColor_uFilter:%22{facet3}%22"), "facet");
						break;
					}
				}
			}
			else
				return;
		}


		lr_start_transaction ( "T03_Facet Display_3facets" ) ;

		web_custom_request("T03_Facet Display_3facets" ,
			 "URL={drillUrlThreeFacets}" ,
			"Method=GET",
			"Resource=0",
			 "Mode=HTML" ,
			LAST);

		lr_save_string("T03_Facet Display_3facets", "mainTransaction");

	 	if (UNBXD_FLAG == 1) {
			lr_save_string("", "drillUrlThreeFacets");
			lr_start_sub_transaction("T03_Facet Display_3facets_unbxd.search.filter", "T03_Facet Display_3facets");

			web_custom_request("getProductviewbyCategory",
				"URL=https://{UNBXD_host}/category?filter={facet}&start=0&rows=20&variants=true&variants.count=0&version=V2&facet.multiselect=true&selectedfacet=true&fields=alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPFitMessageUSSstore,TCPFit,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPSwatchesUSStore,%20%20%20%20%20%20%20%20%20%20top_rated,TCPSwatchesCanadaStore,product_name,TCPColor,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_fq,categoryPath3,categoryPath3_catMap,categoryPath2_catMap,%20%20%20%20%20%20%20%20%20%20product_short_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,seo_token,prodpartno,banner,facets,auxdescription,list_of_attributes,numberOfProducts,redirect,searchMetaData,didYouMean,%20%20%20%20%20%20%20%20%20%20TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,TcpBossCategoryDisabled,TcpBossProductDisabled,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,product_type,products,%20%20%20%20%20%20%20%20%20%20low_offer_price,%20high_offer_price,%20low_list_price,%20high_list_price&pagetype=boolean&p-id=categoryPathId:%22{subCategoryMain}%3E{subCategoryL1}%3E{subCategoryL2}%22&uid=uid-1567537522986-54828",
				"Method=GET",
				"Resource=0",
				"RecContentType=text/html",
				"EncType=application/x-www-form-urlencoded",
				LAST);
	
			lr_end_sub_transaction("T03_Facet Display_3facets_unbxd.search.filter", LR_AUTO);

			//getListofDefaultWishlist(); 03132019
		
	//		if (atoi(lr_eval_string("{totalProductsCount}")) == 0)
	//			lr_end_transaction ( "T03_Facet Display_3facets" , LR_FAIL ) ;
	//		else
				lr_end_transaction ( "T03_Facet Display_3facets" , LR_PASS ) ;
		}
//	}// end if
}// end drillThreeFacets

int checkOutfit()
{
    extern char * strtok(char * string, const char * delimiters );
    char path[1000] = "";
    char separators[] = "-";
    char * token;
//    char fullpath[1024];
//    int counter, index = 0;

//	lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_paramarr_random ( "subCategories" ) ) ;

	lr_save_string(lr_paramarr_random ( "subCategories" ), "subCategory");
    strcpy(path, lr_eval_string("{subCategory}") );

    // Get the first token
    token = (char *)strtok(path, separators);
    if (!token) {
        lr_output_message ("No tokens found in string!");
    }

    while (token != NULL ) {
//        lr_output_message ("%s", token );
		// Get the next token
        token = (char *)strtok(NULL, separators);

        if(token != NULL) {
	        if (strcmp(token,"outfits") == 0) {
				return LR_FAIL;
	        }
        }
    }

	return LR_PASS;

}

int getSubCategories()
{
    extern char * strtok(char * string, const char * delimiters );
    char path[9999] = "";
    char separators[] = "\"";
    char * token;
//    char fullpath[1024];
    int counterX, counterY, counterZ = 0;
    char *arr[20]; // = malloc(sizeof(int)*20);

    strcpy(path, lr_eval_string("{subcategory}") );

    // Get the first token
    token = (char *)strtok(path, separators);
    if (!token) {
        lr_output_message ("No tokens found in string!");
    }

    while (token != NULL ) {
        token = (char *)strtok(NULL, separators);
//        lr_output_message ("%s", token );
//        lr_save_string(token, "url");

		if(token != NULL) { // && strcmp(token,"url") == 0) {
	        if (strcmp(token,"url") == 0) {
				token = (char *)strtok(NULL, separators);
				token = (char *)strtok(NULL, separators);
				counterX++;

				arr[counterX] = token;
				lr_message("CounterX is %d", counterX);
				if (counterX == 1) {
					lr_param_sprintf("category", "%s", token);
				} else {
					counterZ++;
					lr_param_sprintf("subcategories", "subcategories_%d", counterZ);
					lr_param_sprintf(lr_eval_string("{subcategories}"), "%s", token );
					lr_save_int(counterZ, "subcategories_count");
				}
			}
        }
    }
    lr_message("parameter length is %d", atoi(lr_eval_string("{subcategories_count}")) );
    //lr_save_string(lr_paramarr_random("subcategories"), "subCategory");
    //while ( checkOutfit() == LR_FAIL) {}
	return LR_PASS;
}

void drillSubCategory()
{
 	lr_think_time ( LINK_TT ) ;

	lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ( "{subCategory}" ) ) ; //from datafile for unbxd
//		lr_save_string(lr_paramarr_random ( "subCategories" ), "subCategory" );
//		lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}"), lr_eval_string("{store_home_page}c/"), lr_eval_string ( "{subCategory}" ) ) ; //04192018 romano enabled 

	web_reg_save_param( "pdpURL", "LB=,\"pdpUrl\":\"{store_home_page}p/", "RB=\",\"shortDescription", "ORD=All", "NotFound=Warning", LAST); //,"pdpUrl":"/us/p/Girls-Basic-Skinny-Jeans---True-Indigo-Wash-2069116-JP","shortDescription
	web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"generalProductId":"
	web_reg_save_param("totalProductsCount", "LB=totalProductsCount\":", "RB=,\"breadcrumbs", "NotFound=Warning", LAST);

	lr_save_string(lr_eval_string("{subCategory}"), "category"); //for pagination
	lr_save_string("T03_Sub-Category Display", "mainTransaction");
	lr_start_transaction ( "T03_Sub-Category Display" ) ;

	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO_SUBCATEGORY ) {

		lr_start_transaction ( "T03_Sub-Category Display_Akamai" ) ;
		webURL(lr_eval_string ( "{drillUrl}" ), "T03_Sub-Category Display" );
		lr_end_transaction ( "T03_Sub-Category Display_Akamai" , LR_AUTO ) ;
	} 

	if (unbxdCategoryL1L2() == LR_FAIL)
	{
		lr_end_transaction ( "T03_Sub-Category Display" , LR_FAIL ) ;
		lr_save_string("categoryDisplay", "callerId");
		lr_save_string("", "mainTransaction");
		return;
	}
	
	if (isLoggedIn == 1) { //turned off for akamai caching
		if (ESPOT_FLAG == 1)
		{	lr_save_string("1", "getESpot");
			web_add_header("espotName", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Gift_Shop_espot,secondary_global_nav_Gift_Shop_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot");
			getESpot();
			lr_save_string("2", "getESpot");
			web_add_header("espotName", "GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderPlccMoreInfoMdal,MobileNavHeaderLinks1,HeaderNavEspotLinks,DT_SiteWide_Above_Header_nonStikcy");
			getESpot();
		}
//					getListofDefaultWishlist(); 03132019
	} else {
		if (ESPOT_FLAG == 1)
		{	lr_save_string("1", "getESpot");
			web_add_header("espotName", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Gift_Shop_espot,secondary_global_nav_Gift_Shop_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot");
			getESpot();
		}
	}
		
	lr_save_string("productListing", "p_pageName");
	if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_GETREGISTERED)
		getRegisteredUserDetailsInfo();
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 30 ) {
		//   userGroup();
	}
	lr_save_string("categoryDisplay", "callerId");
	lr_save_string("categoryDisplay", "paginateBy");
	lr_end_transaction ( "T03_Sub-Category Display" , LR_PASS ) ;


}// end drillSubCategory

void drill()
{
	// T03_Drill Facets or SubCategory
	randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	
	if ( randomPercent <= DRILL_ONE_FACET ) {
		drillOneFacet();
	} else if ( randomPercent <= (DRILL_ONE_FACET + DRILL_TWO_FACET) ) {
		drillTwoFacets();
	} else if ( randomPercent <= (DRILL_ONE_FACET + DRILL_TWO_FACET + DRILL_THREE_FACET)) {
		drillThreeFacets();
	} else if ( randomPercent <= (DRILL_ONE_FACET + DRILL_TWO_FACET + DRILL_THREE_FACET + DRILL_SUB_CATEGORY)) {
		drillSubCategory(); //
		
	}

} // end drill

void topNav()
{
//	nav_by = lr_eval_string("{RATIO_NAV_BY}") ;

	randomPercent = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if ( randomPercent <= NAV_BROWSE ) {
		navByBrowse();
		drill();
	}
	else
		navBySearch();
	
	sortOrPaginate();
	
} // end topNav

int getValueOf()
{
    extern char * strtok(char * string, const char * delimiters );
    char path[9999] = "";
    char separators[] = "\"";
    char * token;
    strcpy(path, lr_eval_string("{getProductId}") );

    // Get the first token
    token = (char *)strtok(path, separators);
    if (!token) {
//        lr_output_message ("No tokens found in string!");
    }

    while (token != NULL ) {
//        lr_output_message ("%s", token );
//        lr_save_string(token, "generalProductId");
        token = (char *)strtok(NULL, separators);
//        lr_output_message ("%s", token );
		if (token != NULL)
        	lr_save_string(token, "generalProductId");
		
        break;
    }
	
	return LR_PASS;

}

void productDetailViewBOPIS()
{
	int k;
 	lr_think_time ( LINK_TT ) ;

//	if (( lr_paramarr_len ( "productId" ) > 0 )) {

//		index = rand ( ) % lr_paramarr_len( "productId" ) + 1 ;
	//	lr_save_string( lr_paramarr_idx("productId", index), "pdpURL") ;
//		lr_save_string( lr_paramarr_idx("productId", index), "productId") ;
//	lr_param_sprintf ( "drillUrl" , "https://%s%s" , lr_eval_string("{host}") , lr_eval_string("/us/p/{randCatEndtryId_pdp}") ) ; //from datafile for unbxd

//		lr_save_string( lr_eval_string("{unbxd_PDPURL}") , "pdpURL") ;
//		lr_save_string( lr_eval_string("{unbxd_productID}") , "productId") ; 
		lr_save_string( lr_eval_string("{randCatEntryId}") , "pdpURL") ;  //romano 01/11/19 to test BOPIS/BOSS to see if we can get parameters for getBopisInventory
//		lr_save_string( "1124756", "productId") ; //Boys-Short-Sleeve-Solid-Pique-Polo-1124756-1027
//0722
		lr_save_string( lr_eval_string("{pdpURLData}") , "pdpURL") ; // from datafile for unbxd
		lr_save_string( lr_eval_string("{pdpProdID}") , "productId") ; // from datafile for unbxd

		lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}") , lr_eval_string("{store_home_page}p/"), lr_eval_string( "{pdpURL}" ) ) ;
//		lr_param_sprintf ( "drillUrl" , "https://%s%s" , lr_eval_string("{host}") , "/us/p/Unisex-Kids-Basic-Ankle-Socks-6-Pack-2069997-10" ) ;
		
		lr_start_transaction ( "T04_Product Display Page" ) ;

		if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO_PDP ) { 

			web_reg_save_param_regexp ("ParamName=getProductId","RegExp=navigationTree.*?generalProductId(.*?)name\"","NotFound=Warning",SEARCH_FILTERS,"RequestUrl=*",	LAST );
							
			lr_start_transaction ( "T04_Product Display Page_Akamai" ) ;
			web_reg_find("SaveCount=txtCheckCount", "Text/IC=html", LAST);
				web_url("PDP",
					"URL={drillUrl}",
					"Resource=0",
					"RecContentType=text/html",
					"Mode=HTTP",
					LAST);
			lr_end_transaction ( "T04_Product Display Page_Akamai" , LR_AUTO ) ;
			getValueOf();
		}
		
		lr_save_string("T04_Product Display Page", "mainTransaction");
		lr_save_string("productDisplay", "callerId");
		
	 	if (UNBXD_FLAG == 1) {
			lr_start_sub_transaction("T04_Product Display_unbxd.search", "T04_Product Display Page");
			web_reg_save_param_regexp("ParamName=atc_catentryIds", "RegExp=\"v_item_catentry_id\":[ ]*\"(.*?)\"", "NotFound=Warning",SEARCH_FILTERS, "RequestUrl=*", "Ordinal=ALL", LAST); //"v_item_catentry_id": "952848"
			web_reg_save_param_regexp("ParamName=atc_quantity", "RegExp=\"v_qty\":[ ]*(.*?),", "NotFound=Warning",SEARCH_FILTERS, "RequestUrl=*","Ordinal=ALL", LAST); //"v_item_catentry_id": "952848"
	
			web_reg_save_param_json("ParamName=itemPartNumber", "QueryString=$..variants..variantId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json("ParamName=variantNo", "QueryString=$..variants..v_variant", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json("ParamName=u_imagename", "QueryString=$.response.products..productid", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_add_header("Origin", "https://{host}");
	
			web_url("PDP", 
				"URL=https://{UNBXD_host}/search?variants=true&variants.count=100&version=V2&rows=20&pagetype=boolean&q={productId}&promotion=false&fields=alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPFitMessageUSSstore,TCPFit,product_name,TCPColor,top_rated,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_catMap,categoryPath2_catMap,product_short_description,style_long_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,product_long_description,seo_token,variantCount,prodpartno,variants,v_tcpfit,v_qty,v_tcpsize,style_name,v_item_catentry_id,v_listprice,v_offerprice,v_qty,variantId,auxdescription,list_of_attributes,additional_styles,TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,v_variant,%20low_offer_price,%20high_offer_price,%20low_list_price,%20high_list_price,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore",
				"Resource=0",
				"RecContentType=application/json", 
				"Mode=HTML", 
				LAST);
			
			lr_end_sub_transaction("T04_Product Display_unbxd.search", LR_AUTO);
		}
		
		if (isLoggedIn == 1) { 
			if (ESPOT_FLAG == 1)
			{	lr_save_string("1", "getESpot");
				web_add_header("espotName", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_sale_espot,secondary_global_nav_sale_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Shoes_espot,secondary_global_nav_Shoes_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Trending_espot,secondary_global_nav_Trending_espot");
				getESpot();
				lr_save_string("3", "getESpot");
				web_add_header("espotname", "GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderPlccMoreInfoMdal,MobileNavHeaderLinks1,HeaderNavEspotLinks,DT_SiteWide_Above_Header_nonStikcy");
				getESpot();
			}
		} else {
			if (ESPOT_FLAG == 1)
			{	lr_save_string("1", "getESpot");
				web_add_header("espotname", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_sale_espot,secondary_global_nav_sale_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Shoes_espot,secondary_global_nav_Shoes_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Trending_espot,secondary_global_nav_Trending_espot");
				getESpot();
			}
		}
		if (ESPOT_FLAG == 1) {
			lr_save_string("2", "getESpot");
			web_add_header("espotname", "DTPDPGlobalPromoHeader,DTPDPGlobalPromoPricing,DTPDPGlobalPromoAddtoBag,DTSizeChart,DTPDPBOPIS");
			getESpot();
		}
		lr_save_string("productDetails", "p_pageName");
		if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_GETREGISTERED)
			getRegisteredUserDetailsInfo();
		
		//   userGroup();
//		getFavouriteStoreLocation(); //03112019 - this is not present in uatlive4 - bespin
	
//		preferences(); //03112019 - this is not present in uatlive4 - bespin

//					getListofDefaultWishlist(); 03132019
		
		lr_save_string("PDP","PDP_or_PQV");
		lr_end_transaction ( "T04_Product Display Page" , LR_PASS ) ;
		
		if ( lr_paramarr_len ( "atc_catentryIds" ) > 0 )
		{
			for (k = 1; k<= atoi(lr_eval_string("{atc_catentryIds_count}")); k++ )
			{
				if (atoi( lr_paramarr_idx("atc_quantity", k) ) != 0)
				{
					lr_save_string(lr_paramarr_idx("atc_catentryIds", k), "atc_catentryId" ); 
					lr_save_string(lr_paramarr_idx("variantNo", k), "VariantNo" );
					lr_save_string(lr_paramarr_idx("itemPartNumber", k), "itemPartNumber" );
					
					break;
				}
			}
		}

//	}

}


int productDetailView()
{
	int k;
 	lr_think_time ( LINK_TT ) ;

 	if (ATC_FLAG != 1) {
		lr_save_string( lr_eval_string("{pdpURLData}") , "pdpURL") ; // from datafile for unbxd
		lr_save_string( lr_eval_string("{pdpProdID}") , "productId") ; // from datafile for unbxd
 	} else {
		lr_save_string( lr_eval_string("{unbxd_PDPURL}") , "pdpURL") ; 
		lr_save_string( lr_eval_string("{unbxd_productID}") , "productId") ; 
 	}
	lr_param_sprintf ( "drillUrl" , "https://%s%s%s" , lr_eval_string("{host}") , lr_eval_string("{store_home_page}p/"), lr_eval_string( "{pdpURL}" ) ) ;
	//lr_continue_on_error(1);	//"skuId": "790846",

	//web_reg_save_param( "productId", "LB=generalProductId\":\"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //"generalProductId":"


	lr_save_string("T04_Product Display Page", "mainTransaction");
	lr_save_string("productDisplay", "callerId");
	lr_start_transaction ( "T04_Product Display Page" ) ;

	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO_PDP ) { 
		web_reg_save_param_regexp ("ParamName=getProductId","RegExp=navigationTree.*?generalProductId(.*?)name\"","NotFound=Warning",SEARCH_FILTERS,"RequestUrl=*",	LAST );
						
		lr_start_transaction ( "T04_Product Display Page_Akamai" ) ;
		webURL(lr_eval_string ( "{drillUrl}" ), "T04_Product Display Page"); 
		lr_end_transaction ( "T04_Product Display Page_Akamai" , LR_AUTO ) ;

	}

	getValueOf();
	lr_save_string("0", "itemPartNumber_count");
	lr_save_string("productDisplay", "callerId");
 	if (UNBXD_FLAG == 1) {
		lr_start_sub_transaction("T04_Product Display_unbxd.search", "T04_Product Display Page");
		web_reg_save_param_regexp("ParamName=atc_catentryIds", "RegExp=\"v_item_catentry_id\":[ ]*\"(.*?)\"", "NotFound=Warning",SEARCH_FILTERS, "RequestUrl=*", "Ordinal=ALL", LAST); //"v_item_catentry_id": "952848"
		web_reg_save_param_regexp("ParamName=atc_quantity", "RegExp=\"v_qty\":[ ]*(.*?),", "NotFound=Warning",SEARCH_FILTERS, "RequestUrl=*","Ordinal=ALL", LAST); //"v_item_catentry_id": "952848"
		web_reg_save_param_json("ParamName=itemPartNumber", "QueryString=$..variants..variantId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json("ParamName=variantNo", "QueryString=$..variants..v_variant", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json("ParamName=productidWL", "QueryString=$..variants..productid", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json("ParamName=u_imagename", "QueryString=$.response.products..productid", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json("ParamName=u_numberOfProducts", "QueryString=$.response.numberOfProducts", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_add_header("Origin", "https://{host}");
	
		web_url("PDP", 
			"URL=https://{UNBXD_host}/search?variants=true&variants.count=100&version=V2&rows=20&pagetype=boolean&q={productId}&promotion=false&fields=alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPFitMessageUSSstore,TCPFit,product_name,TCPColor,top_rated,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_catMap,categoryPath2_catMap,product_short_description,style_long_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,product_long_description,seo_token,variantCount,prodpartno,variants,v_tcpfit,v_qty,v_tcpsize,style_name,v_item_catentry_id,v_listprice,v_offerprice,v_qty,variantId,auxdescription,list_of_attributes,additional_styles,TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,v_variant,%20low_offer_price,%20high_offer_price,%20low_list_price,%20high_list_price,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore&uid={UID}",
			"Resource=0",
			"RecContentType=application/json", 
			"Mode=HTML", 
			LAST);
		lr_end_sub_transaction("T04_Product Display_unbxd.search", LR_AUTO);
	}
	lr_save_string("T04_Product Display Page", "mainTransaction");
	lr_save_string("productDisplay", "callerId");
	if (isLoggedIn == 1) { 
		if (ESPOT_FLAG == 1)
		{		lr_save_string("1", "getESpot");
				web_add_header("espotname", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Gift_Shop_espot,secondary_global_nav_Gift_Shop_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot");
				getESpot();
				lr_save_string("3", "getESpot");
				web_add_header("espotname", "GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderPlccMoreInfoMdal,MobileNavHeaderLinks1,HeaderNavEspotLinks,DT_SiteWide_Above_Header_nonStikcy");
				getESpot();
		}
	} else {
		if (ESPOT_FLAG == 1)
		{					lr_save_string("1", "getESpot");
				web_add_header("espotname", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Gift_Shop_espot,secondary_global_nav_Gift_Shop_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot");
				getESpot();
		}
	}
	if (ESPOT_FLAG == 1) {
		lr_save_string("2", "getESpot");
		web_add_header("espotname", "DTPDPGlobalPromoHeader,DTPDPGlobalPromoPricing,DTPDPGlobalPromoAddtoBag,DTSizeChart,DTPDPBOPIS");
		getESpot();
	}
	
//					getListofDefaultWishlist(); 03132019

	lr_save_string("productDetails", "p_pageName");
	if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_GETREGISTERED)
		getRegisteredUserDetailsInfo();
	
//		getFavouriteStoreLocation(); //03112019 - this is not present in uatlive4 - bespin
//		preferences(); //03112019 - this is not present in uatlive4 - bespin

	if ( lr_paramarr_len ( "atc_catentryIds" ) > 0 )
	{
		for (k = 1; k<= atoi(lr_eval_string("{atc_catentryIds_count}")); k++ )
		{
			if (atoi( lr_paramarr_idx("atc_quantity", k) ) != 0)
			{
				lr_save_string(lr_paramarr_idx("atc_catentryIds", k), "atc_catentryId" ); 
				lr_save_string(lr_paramarr_idx("variantNo", k), "VariantNo" );
				lr_save_string(lr_paramarr_idx("itemPartNumber", k), "itemPartNumber" );
				lr_save_string(lr_paramarr_idx("productidWL", k), "productidWL" );
				
				break;
			}
		}
	}
	
	if ( isLoggedIn == 1 && atoi(lr_eval_string("{RANDOM_PERCENT}")) <= PDP_to_WL && strcmp(lr_eval_string("{itemPartNumber_count}"),"0") != 0 )
	{
		lr_start_sub_transaction("T04_Product Display Page_addOrUpdateWishlist", "T04_Product Display Page");
		addHeader();
		registerErrorCodeCheck();
		web_add_header("Content-Type","application/json");
		web_add_header("Accept","application/json");
		web_add_header("addItem", "true");
		web_custom_request("addOrUpdateWishlist",
			"URL=https://{api_host}/v2/wishlist/addOrUpdateWishlist", 
			"Method=PUT",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"Body={\"item\":[{\"productId\":\"{productidWL}\",\"quantityRequested\":\"1\",\"isProduct\":\"TRUE\"}],\"uniqueId\":\"{itemPartNumber}\"}",  
//			"Body={\"item\":[{\"productId\":\"1118160\",\"quantityRequested\":\"1\",\"isProduct\":\"TRUE\"}],\"uniqueId\":\"2102017_545\"}",  
//			"Body={\"item\":[{\"productId\":\"{productId}\",\"quantityRequested\":\"1\",\"isProduct\":\"TRUE\"}]}",  //generalProductId
//			"Body={\"item\":[{\"productId\":\"{productId}\",\"quantityRequested\":\"1\",\"isProduct\":\"TRUE\"}]}",  
			LAST);
		
		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
		{	lr_end_sub_transaction("T04_Product Display Page_addOrUpdateWishlist", LR_AUTO);
		}
		else
		{
			lr_error_message( lr_eval_string("T04_Product Display_addOrUpdateWishlist - Failed with APICode: \"{errorCode}\", ErrMssg: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, productId:{productidWL},uniqueId:{itemPartNumber}") ) ;
			lr_end_sub_transaction("T04_Product Display Page_addOrUpdateWishlist", LR_FAIL);
		if (WRITE_TO_SUMO==1) {
	 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}addOrUpdateWishlist - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
				sendErrorToSumo(); 
	 	}
		}

	}

	lr_save_string("PDP","PDP_or_PQV");
	lr_end_transaction ( "T04_Product Display Page" , LR_PASS ) ;
				
 	if (ATC_FLAG != 1) {
		lr_save_string( lr_eval_string("{pdpURLData}") , "pdpURL") ; // from datafile for unbxd
		lr_save_string( lr_eval_string("{pdpProdID}") , "productId") ; // from datafile for unbxd
 	} else {
		lr_save_string( lr_eval_string("{unbxd_PDPURL}") , "pdpURL") ; 
		lr_save_string( lr_eval_string("{unbxd_productID}") , "productId") ; 
 	}
		
		
	return 0;
} // end productDisplay


int productDisplay()
{
	if (productDetailView() == LR_FAIL)
	    return LR_FAIL;

	return 0;

	} // end ProductDisplay

void productDisplayBOPIS()
{
	productDetailViewBOPIS(); //calls the TCPGetWishListForUsersView 2x
}

void StoreLocator()
{
}

void StoreLocatorOLD()
{
 	lr_think_time ( LINK_TT ) ;

	lr_start_transaction("T22_StoreLocator");

		lr_save_string("T22_StoreLocator", "mainTransaction" );

		lr_start_sub_transaction("T22_StoreLocator_store-locator", lr_eval_string("{mainTransaction}") );

			web_url("StoreLocator",
			"URL=https://{host}/{country}/store-locator",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=text/html",
			"Mode=HTML",
			LAST);

			getRegisteredUserDetailsInfo();
			
			if (ESPOT_FLAG == 1)
			{
				lr_save_string("1", "getESpot");
				web_add_header("espotName","GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_sale_espot,secondary_global_nav_sale_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Shoes_espot,secondary_global_nav_Shoes_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Trending_espot,secondary_global_nav_Trending_espot");
				web_add_header("deviceType","desktop");
				getESpot();
			}
			
		lr_end_sub_transaction("T22_StoreLocator_store-locator", LR_AUTO);

		lr_start_sub_transaction("T22_StoreLocator_Find", "T22_StoreLocator" );
			addHeader();
			web_add_header("latitude", "40.7879061");
			web_add_header("longitude", "-74.0444976");
			web_add_header("maxItems", "5");
			web_add_header("radius", "75");
//			web_reg_save_param("uniqueId", "LB=\\t\\t\\t\\t\"uniqueId\": ", "RB=\"", "ORD=1", "NotFound=Warning", LAST); //"uniqueId": "114298"
		web_reg_save_param_json("ParamName=uniqueId", "QueryString=$.PhysicalStore..uniqueId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_find("TEXT/IC=uniqueId", "SaveCount=apiCheck", LAST);
			web_url("findStoresbyLatitudeandLongitude", ///api/v2/store/findStoresbyLatitudeandLongitude - Phase 2
//				"URL=https://{api_host}/tcpproduct/findStoresbyLatitudeandLongitude",
				"URL=https://{api_host}/v2/store/findStoresbyLatitudeandLongitude",
				"Resource=0",
				"RecContentType=application/json",
				LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T22_StoreLocator_Find", LR_FAIL);
		else
			lr_end_sub_transaction("T22_StoreLocator_Find", LR_AUTO);

		lr_save_string("T22_StoreLocator_Set_As_Favorite_Store", "mainTransaction" );
		lr_start_sub_transaction("T22_StoreLocator_Set_As_Favorite_Store", "T22_StoreLocator" );

			addHeader();
//			web_add_header("Content-Type", "application/json");
			web_add_header("fromPage", "StoreLocator");
			web_add_header("action", "add");
			web_add_header("storeLocId", lr_eval_string("{uniqueId}") );
			web_add_header("userId", lr_eval_string("{userId}") );
			web_reg_find("TEXT/IC={uniqueId}", "SaveCount=apiCheck", LAST);
			web_custom_request("addFavouriteStoreLocation",
				"URL=https://{api_host}/v2/store/addFavouriteStoreLocation",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
				"Body={}",
				LAST);
			if (atoi(lr_eval_string("{apiCheck}")) == 0 )
				lr_end_sub_transaction("T22_StoreLocator_Set_As_Favorite_Store", LR_FAIL);
			else
				lr_end_sub_transaction("T22_StoreLocator_Set_As_Favorite_Store", LR_AUTO);

		lr_save_string("T22_StoreLocator_See_StoreDetails", "mainTransaction" );
		lr_start_sub_transaction("T22_StoreLocator_See_StoreDetails", "T22_StoreLocator" );

			web_url("See Store Details",
				"URL=https://{host}/us/store/secaucusoutlet-nj-secaucus-07094-114298",
				"Resource=0",
				"RecContentType=text/html",
				"Mode=HTTP",
					LAST);

			getRegisteredUserDetailsInfo();
			
			if (ESPOT_FLAG == 1)
			{
				lr_save_string("1", "getESpot");
				web_add_header("espotName","GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_sale_espot,secondary_global_nav_sale_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Shoes_espot,secondary_global_nav_Shoes_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Trending_espot,secondary_global_nav_Trending_espot");
				web_add_header("deviceType","desktop");
				getESpot();
			}
			
//		getFavouriteStoreLocation(); //03112019 - this is not present in uatlive4 - bespin
	
//		preferences(); //03112019 - this is not present in uatlive4 - bespin

		lr_end_sub_transaction("T22_StoreLocator_See_StoreDetails", LR_AUTO);

	lr_end_transaction("T22_StoreLocator", LR_AUTO);
}

int unbxdCategoryTopNav()
{
    extern char * strtok(char * string, const char * delimiters );
    char path[1000] = "";
    char separator1[] = "|";
    char separator2[] = ">";
    char * token;
    
 	if (UNBXD_FLAG == 1) {
		web_reg_save_param_json( "ParamName=u_uniqueId", "QueryString=$..uniqueId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	
		web_reg_save_param_json( "ParamName=u_categoryPathId", "QueryString=$..categoryPath3_catMap[*]", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	
		web_reg_save_param_json( "ParamName=u_imagename", "QueryString=$.response.products..imagename", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json("ParamName=u_numberOfProducts", "QueryString=$.response.numberOfProducts", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	
		lr_start_sub_transaction("T02_Category Display_unbxd.category", "T02_Category Display");
		web_add_header("Origin", "https://{host}");
		
		web_url("category", 
			"URL=https://{UNBXD_host}/category?start=0&rows=20&variants=true&variants.count=0&version=V2&facet.multiselect=true&selectedfacet=true&fields=alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPFitMessageUSSstore,TCPFit,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPSwatchesUSStore,%20%20%20%20%20%20%20%20%20%20top_rated,TCPSwatchesCanadaStore,product_name,TCPColor,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_fq,categoryPath3,categoryPath3_catMap,categoryPath2_catMap,%20%20%20%20%20%20%20%20%20%20product_short_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,seo_token,prodpartno,banner,facets,auxdescription,list_of_attributes,numberOfProducts,redirect,searchMetaData,didYouMean,%20%20%20%20%20%20%20%20%20%20TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,TcpBossCategoryDisabled,TcpBossProductDisabled,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,product_type,products,%20%20%20%20%20%20%20%20%20%20low_offer_price,%20high_offer_price,%20low_list_price,%20high_list_price&pagetype=boolean&p-id=categoryPathId:%22{category}%22&uid=uid-1566397659278-33845", //Updated on 09042019 by Pavan Dusi
	        "Resource=0",
			"RecContentType=application/json", 
			"Mode=HTML", 
			LAST);
		lr_end_sub_transaction("T02_Category Display_unbxd.category", LR_AUTO);
	
		lr_save_string("", "subCategoryL1");
		lr_save_string("", "subCategoryL2");
		
		lr_save_string("", "subCategoryMain");
	
		while(atoi(lr_eval_string("{u_categoryPathId_count}")) != 0) {
			
			lr_save_string(lr_paramarr_random ("u_categoryPathId"), "parseThisCategoryPath");
			
		    strcpy(path, lr_eval_string("{parseThisCategoryPath}") );
		    token = (char *)strtok(path, separator1);
		    
		    if (token) {
		    	
		    	lr_save_string(token, "parseThisCategoryPath");
		    	
			    strcpy(path, lr_eval_string("{parseThisCategoryPath}") );
			    token = (char *)strtok(path, separator2);
				lr_save_string(token, "subCategoryMain");
			    
			    if (token) {
		
			        token = (char *)strtok(NULL, separator2);
			
			        if(token != NULL) {
			        	
		    			lr_save_string(token, "subCategory");
		    			lr_save_string(token, "subCategoryL1");
		    			
				        token = (char *)strtok(NULL, separator2);
				        
				        if(token != NULL) {
		    				lr_save_string(token, "subCategory");
			    			lr_save_string(token, "subCategoryL2");
				        }
				        
		    			break;
			        }
			    }
		    }
		}
    }
	return 0;	
}

int unbxdCategoryL1L2()
{
	int Unbxd_size=0, Unbxd_tempsize=0;
 	if (UNBXD_FLAG == 1) {
		lr_save_string("", "unbxd_productID");
		
		web_reg_save_param_json( "ParamName=unbxd_TCPColor_uFilter", "QueryString=$..[?(@.facetName== 'TCPColor_uFilter')].values.[0,2,4,6,8,10,12,14,16,18,20]", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=unbxd_TCPSize_uFilter", "QueryString=$..[?(@.facetName== 'v_tcpsize_uFilter')].values.[0,2,4,6,8,10,12,14,16,18,20]", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=unbxd_price_range_uFilter", "QueryString=$..[?(@.facetName== 'unbxd_price_range_uFilter')].values.[0,2,4,6,8,10,12,14,16,18,20]", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=unbxd_gender_uFilter", "QueryString=$..[?(@.facetName== 'gender_uFilter')].values.[0,2,4,6,8,10,12,14,16,18,20]", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=unbxd_age_group_uFilter", "QueryString=$..[?(@.facetName== 'age_group_uFilter')].values.[0,2,4,6,8,10,12,14,16,18,20]", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json("ParamName=unbxd_PDPURL", "QueryString=$.response.products.*.seo_token", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json("ParamName=unbxd_productID", "QueryString=$.response.products.*.style_partno", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=u_imagename", "QueryString=$.response.products..imagename", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json("ParamName=u_numberOfProducts", "QueryString=$.response.numberOfProducts", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	
		
		if (strcmp(lr_eval_string("{subCategoryL2}"), "") !=0 ) { //up to Level 2
			lr_save_string(lr_eval_string("https://{UNBXD_host}/category?start=0&rows=20&variants=true&variants.count=0&version=V2&facet.multiselect=true&selectedfacet=true&fields=alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPFitMessageUSSstore,TCPFit,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPSwatchesUSStore,%20%20%20%20%20%20%20%20%20%20top_rated,TCPSwatchesCanadaStore,product_name,TCPColor,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_fq,categoryPath3,categoryPath3_catMap,categoryPath2_catMap,%20%20%20%20%20%20%20%20%20%20product_short_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,seo_token,prodpartno,banner,facets,auxdescription,list_of_attributes,numberOfProducts,redirect,searchMetaData,didYouMean,%20%20%20%20%20%20%20%20%20%20TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,TcpBossCategoryDisabled,TcpBossProductDisabled,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,product_type,products,%20%20%20%20%20%20%20%20%20%20low_offer_price,%20high_offer_price,%20low_list_price,%20high_list_price&pagetype=boolean&p-id=categoryPathId:%22{subCategoryMain}%3E{subCategoryL1}%3E{subCategoryL2}%22&uid=uid-1567537522986-54828"), "URL1");
		} else 	if (strcmp(lr_eval_string("{subCategoryL1}"), "") !=0 ) { //up to Level 1
			lr_save_string(lr_eval_string("https://{UNBXD_host}/category?start=0&rows=20&variants=true&variants.count=0&version=V2&facet.multiselect=true&selectedfacet=true&fields=alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPFitMessageUSSstore,TCPFit,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPSwatchesUSStore,%20%20%20%20%20%20%20%20%20%20top_rated,TCPSwatchesCanadaStore,product_name,TCPColor,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_fq,categoryPath3,categoryPath3_catMap,categoryPath2_catMap,%20%20%20%20%20%20%20%20%20%20product_short_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,seo_token,prodpartno,banner,facets,auxdescription,list_of_attributes,numberOfProducts,redirect,searchMetaData,didYouMean,%20%20%20%20%20%20%20%20%20%20TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,TcpBossCategoryDisabled,TcpBossProductDisabled,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,product_type,products,%20%20%20%20%20%20%20%20%20%20low_offer_price,%20high_offer_price,%20low_list_price,%20high_list_price&pagetype=boolean&p-id=categoryPathId:%22{subCategoryMain}%3E{subCategoryL1}%22&uid=uid-1567537522986-54828"), "URL1");
	//	} else if (strcmp(lr_eval_string("{subCategoryMain}"), "") !=0 ) { 
		} else { 
			lr_save_string(lr_eval_string("https://{UNBXD_host}/category?start=0&rows=20&variants=true&variants.count=0&version=V2&facet.multiselect=true&selectedfacet=true&fields=alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPFitMessageUSSstore,TCPFit,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPSwatchesUSStore,%20%20%20%20%20%20%20%20%20%20top_rated,TCPSwatchesCanadaStore,product_name,TCPColor,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_fq,categoryPath3,categoryPath3_catMap,categoryPath2_catMap,%20%20%20%20%20%20%20%20%20%20product_short_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,seo_token,prodpartno,banner,facets,auxdescription,list_of_attributes,numberOfProducts,redirect,searchMetaData,didYouMean,%20%20%20%20%20%20%20%20%20%20TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,TcpBossCategoryDisabled,TcpBossProductDisabled,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore,product_type,products,%20%20%20%20%20%20%20%20%20%20low_offer_price,%20high_offer_price,%20low_list_price,%20high_list_price&pagetype=boolean&p-id=categoryPathId:%22{subCategoryMain}%22&uid=uid-1567537522986-54828"), "URL1");
		} 
		
		lr_start_transaction("T03_Sub-Category Display_unbxd.category.start"); 
		web_add_header("Origin", "https://{host}");
		web_url("categoryL1L2", 
			"URL={URL1}",
			"Resource=0", 
			"RecContentType=application/json", 
			"Mode=HTML", 
			LAST);
		lr_end_transaction("T03_Sub-Category Display_unbxd.category.start", LR_AUTO);
		
		Unbxd_size=atoi(lr_eval_string("{unbxd_PDPURL_count}"));
		if((Unbxd_size)<=0)
			return LR_FAIL;
		else {
				srand(time(NULL));
				Unbxd_tempsize= rand() % Unbxd_size + 1 ;
				
			lr_save_string(lr_paramarr_idx("unbxd_PDPURL",Unbxd_tempsize),"unbxd_PDPURL");
			lr_save_string(lr_paramarr_idx("unbxd_productID",Unbxd_tempsize),"unbxd_productID");
		}
	}
	return 0;	
}
