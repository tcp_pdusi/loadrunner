int index , length , i, target_itemsInCart, index_buildCart, randomPercent, isLoggedIn;
int isLoggedIn=0;
char *searchString ;
char *nav_by ; // T02 - Category Navigation selection
char *drill_by ; // T03 - Facet / SubCategory drill selection
char *sort_by; // T03 - Sort selection
char *product_by; // T04 - Product Display Method
int atc_Stat = 0; //0-Pass 1-Fail
int HttpRetCode;
int start_time, target_time;

//VTS variables
int rNum;
unsigned short updateStatus;
char **colnames = NULL;
char **rowdata = NULL;
//PVCI2 pvci = 0;

checkoutFunctions()
{
	return 0;
}

// NOTE: NEED TO USE "VISA" AS PAYMENT METHOD ID "payMethodId". USING VISA MEANS USING THE OFFLINE PLUGIN. DO NOT USE COMPASSVISA!!!

void registerErrorCodeCheck()
{
	web_reg_save_param_json("ParamName=errorCode", "QueryString=$..errorCode", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=errorMessage", "QueryString=$..errorMessage", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	
	web_reg_save_param("httpReturnCode", "LB=HTTP/1.1 ", "RB= OK", "Notfound=warning","Search=Headers", LAST);
	web_reg_save_param_json("ParamName=timestamp", "QueryString=$..timestamp", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=hostName", "QueryString=$..hostName", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	
}
int getCC()
{
	int i;
	
	for (i=1; i <= atoi(lr_eval_string("{accountNo_count}")); i++)
	{
		if (strcmp(lr_paramarr_idx("accountNo", i), "") != 0)
		{
		    lr_save_string(lr_paramarr_idx("expMonth", i), "expMonth");
		    lr_save_string(lr_paramarr_idx("expYear", i), "expYear");
		    lr_save_string(lr_paramarr_idx("ccType", i), "ccType");
		    lr_save_string(lr_paramarr_idx("accountNo", i), "accountNo");
		    lr_save_string(lr_paramarr_idx("ccBrand", i), "ccBrand");
		    lr_save_string(lr_paramarr_idx("billingAddressId", i), "billingAddressId");
		    lr_save_string(lr_paramarr_idx("creditCardId", i), "creditCardId");

			break;
		}
	}
	return 0;
}

int updateShippingMethodSelection()
{
	lr_start_sub_transaction(lr_eval_string("{mainTransaction}_updateShippingMethodSelection"), lr_eval_string("{mainTransaction}") );
	
	web_add_header("Accept", "application/json");
	web_add_header("Content-Type", "application/json");
	if (strcmp(lr_eval_string("{storeId}"), "10151") != 0)
		lr_save_string("900103", "shipmode");
	else
		lr_save_string("901101", "shipmode");

	addHeader();
	registerErrorCodeCheck();
	web_reg_find("TEXT/IC=orderId", "SaveCount=apiCheck", LAST);
	web_custom_request("updateShippingMethodSelection",
		"URL=https://{api_host}/v2/checkout/updateShippingMethodSelection", 
		"Method=PUT",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
//        "Body={\"shipModeId\":\"{shipmode}\",\"addressId\":\"{addressId}\",\"requesttype\":\"ajax\",\"prescreen\":false,\"x_calculationUsage\":\"-1,-2,-3,-4,-5,-6,-7\",\"transVibesSmsPhoneNo\":\"\",\"optimizeCalc\":true}", 
        "Body={\"shipModeId\":\"{shipmode}\",\"addressId\":\"{addressId}\",\"requesttype\":\"ajax\",\"prescreen\":false,\"x_calculationUsage\":\"-1,-2,-3,-4,-5,-6,-7\",\"transVibesSmsPhoneNo\":null,\"optimizeCalc\":true}", 
	LAST);
	
	if(atoi(lr_eval_string("{apiCheck}")) > 0 )
		lr_end_sub_transaction ( lr_eval_string("{mainTransaction}_updateShippingMethodSelection") , LR_AUTO ) ;
	else {
		lr_error_message( lr_eval_string("{mainTransaction}_updateShippingMethodSelection Browse 1st - Failed with mixCart:{mixCart},\"shipModeId\":\"{shipmode}\",\"addressId\":\"{addressId}\", API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_updateShippingMethodSelection"), LR_FAIL);
//		lr_end_transaction(lr_eval_string("{mainTransaction}"), LR_FAIL);
		if (WRITE_TO_SUMO==1) {
	 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_updateShippingMethodSelection  Browse 1st- Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
	 	}
		return LR_FAIL;
	}
	
	return 0;			
}

int updateShippingMethodSelectionVenmo()
{
	lr_start_sub_transaction(lr_eval_string("{mainTransaction}_updateShippingMethodSelection"), lr_eval_string("{mainTransaction}") );
	
	web_add_header("Accept", "application/json");
	web_add_header("Content-Type", "application/json");
	if (strcmp(lr_eval_string("{storeId}"), "10151") != 0)
		lr_save_string("900103", "shipmode");
	else
		lr_save_string("901101", "shipmode");

	addHeader();
	registerErrorCodeCheck();
	web_reg_find("TEXT/IC=orderId", "SaveCount=apiCheck", LAST);
	web_custom_request("updateShippingMethodSelection",
		"URL=https://{api_host}/v2/checkout/updateShippingMethodSelection", 
		"Method=PUT",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"Body={\"shipModeId\":\"{shipmode}\",\"addressId\":\"{addressId}\",\"requesttype\":\"ajax\",\"prescreen\":false,\"x_calculationUsage\":\"-1,-2,-3,-4,-5,-6,-7\",\"transVibesSmsPhoneNo\":\"\"}", 
//venmo	      {\"shipModeId\":\"901107\",\"addressId\":\"61941414\",\"requesttype\":\"ajax\",\"prescreen\":true,\"x_calculationUsage\":\"-1,-2,-3,-4,-5,-6,-7\"}
		LAST);
	if(atoi(lr_eval_string("{apiCheck}")) > 0 )
//		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0) //03142019
		lr_end_sub_transaction ( lr_eval_string("{mainTransaction}_updateShippingMethodSelection") , LR_AUTO ) ;
	else {
		lr_error_message( lr_eval_string("{mainTransaction}_updateShippingMethodSelection Browse 1st - Failed with mixCart:{mixCart},\"shipModeId\":\"901101\",\"addressId\":\"{addressId}\", API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_updateShippingMethodSelection"), LR_FAIL);
		if (WRITE_TO_SUMO==1) {
	 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_updateShippingMethodSelection  Browse 1st- Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
	 	}
		return LR_FAIL;
	}
	
	return 0;			
}
			
int getVenmoClientToken()
{
	if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 30 ) { 
		lr_start_sub_transaction(lr_eval_string("{mainTransaction}_getVenmoClientToken"), lr_eval_string("{mainTransaction}") );
	
		web_add_header("Accept", "application/json" );
		web_add_header("deviceType", "mobile" );
		web_add_header("orderId", lr_eval_string("{orderId}") );
		addHeader();
		web_custom_request("getVenmoClientToken",
			"URL=https://{api_host}/v2/venmo/getVenmoClientToken", 
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
	
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getVenmoClientToken"), LR_AUTO);
	}
	
	return 0;
}

int saveForLater()
{
	lr_start_sub_transaction(lr_eval_string("{mainTransaction}_saveForLater"), lr_eval_string("{mainTransaction}") );

	web_add_header("Accept", "application/json" );
	addHeader();
	
	if (strcmp(lr_eval_string("{apiMethod}"), "GET") == 0)
	{
		web_custom_request("saveForLater",
			"URL=https://{api_host}/v2/cart/saveForLater", 
			"Method={apiMethod}",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
	} else {
		web_reg_save_param_json("ParamName=itemCatentryId", "QueryString=$..sflItems..itemCatentryId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_custom_request("saveForLater",
			"URL=https://{api_host}/v2/cart/saveForLater", 
			"Method={apiMethod}",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
			"Body={\"catentryId\":\"{itemCatentryId}\",\"isRememberedUser\":false,\"isRegistered\":true}",
			LAST);
	}
	
	lr_end_sub_transaction(lr_eval_string("{mainTransaction}_saveForLater"), LR_AUTO);
	
	return 0;
}

int constructOLPS()
{
	lr_start_sub_transaction(lr_eval_string("{mainTransaction}_constructOLPS"), lr_eval_string("{mainTransaction}") );

	web_add_header("Accept", "application/json" );
	addHeader();
	
//	web_reg_save_param_json("ParamName=itemCatentryId", "QueryString=$..sflItems..itemCatentryId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_custom_request("constructOLPS",
		"URL=https://{api_host}/v2/ads_dms/constructOLPS", 
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"\"prescreen\":true}",
		LAST);

	lr_end_sub_transaction(lr_eval_string("{mainTransaction}_constructOLPS"), LR_AUTO);
	
	return 0;
}
	
int userGroup()
{
	lr_start_sub_transaction(lr_eval_string("{mainTransaction}_userGroup"), lr_eval_string("{mainTransaction}") );

	web_add_header("Accept", "application/json" );
	addHeader();
	web_custom_request("userGroup",
		"URL=https://{api_host}/v2/account/userGroup", 
		"Method=PUT",
		"Resource=0",
		"RecContentType=application/json",
		LAST);

	lr_end_sub_transaction(lr_eval_string("{mainTransaction}_userGroup"), LR_AUTO);
	
	return 0;
}

int socialNew()
{
	lr_start_sub_transaction(lr_eval_string("{mainTransaction}_socialNew"), lr_eval_string("{mainTransaction}") );
	addHeader();
	web_add_header("Expires", "0" );
	web_add_header("Accept", "application/json" );
	web_custom_request("socialNew",
		"URL=https://{api_host}/v2/account/preferences/socialNew", 
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	lr_end_sub_transaction(lr_eval_string("{mainTransaction}_socialNew"), LR_AUTO);
	
	return 0;
}

int getShipmentMethods()
{
//	lr_set_debug_message(16|8|4|2,1);
	web_reg_save_param_json("ParamName=p_shipModeID", "QueryString=$..jsonArr..shipModeId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_find("TEXT/IC=jsonArr", "SaveCount=apiCheck", LAST);
	web_add_header("accept", "application/json");
	web_add_header("state", "NJ" );
	web_add_header("zipCode", "07094" );
	web_add_header("addressfield1", "500 Plaza Dr" );
	lr_start_sub_transaction(lr_eval_string("{mainTransaction}_getShipmentMethods"), lr_eval_string("{mainTransaction}") );
	addHeader();
	registerErrorCodeCheck();
	web_custom_request("getShipmentMethods", 
		"URL=https://{api_host}/v2/checkout/getShipmentMethods",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	
//	lr_set_debug_message(16|8|4|2,0);
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
	{
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getShipmentMethods"), LR_AUTO );
	}
	else {
		lr_error_message( lr_eval_string("{mainTransaction}_getShipmentMethods - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}") ) ;
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getShipmentMethods"), LR_FAIL );
	if (WRITE_TO_SUMO==1) {
 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_getShipmentMethods - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
		sendErrorToSumo(); 
 	}
		return LR_FAIL;
	}
//lr_exit(LR_EXIT_ITERATION_AND_CONTINUE, LR_AUTO);
	return 0;
}

int preferences()
{
	if (isLoggedIn==1) {
		
		lr_start_sub_transaction(lr_eval_string("{mainTransaction}_preferences"), lr_eval_string("{mainTransaction}") );
		web_add_header("Accept", "application/json");
		
		web_reg_find("TEXT/IC=CustomerPreferences", "SaveCount=apiCheck", LAST);
		addHeader();
		registerErrorCodeCheck();
		web_custom_request("preferences", 
			"URL=https://{api_host}/v2/account/preferences",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		
		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheck}")) > 0) 
		{
			lr_end_sub_transaction(lr_eval_string("{mainTransaction}_preferences"), LR_AUTO );
		}
		else {
			lr_error_message( lr_eval_string("{mainTransaction}_preferences - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}") ) ;
			lr_end_sub_transaction(lr_eval_string("{mainTransaction}_preferences"), LR_FAIL );
	if (WRITE_TO_SUMO==1) {
 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_preferences - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
 	}
			return LR_FAIL;
		}
	}
	
	return 0;
}

int bonusDay()
{
	if (isLoggedIn==1 && BRIERLEY_FLAG_CHECK == 1) 
	{
		web_reg_save_param_json("ParamName=s_availableBonusPointDays", "QueryString=$..availableBonusPointDays", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	
		web_add_header("Accept", "application/json");
		lr_start_sub_transaction(lr_eval_string("{mainTransaction}_bonusDay"), lr_eval_string("{mainTransaction}") );
		addHeader();
		registerErrorCodeCheck();

		web_custom_request("bonusDay", 
			"URL=https://{api_host}/v2/account/bonusDay",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
		{	
//			if ( atoi( lr_eval_string("{s_availableBonusPointDays}")) > 0 )
//				lr_save_string("TRUE","CAN_APPLY_BONUS_DAY");
			
			lr_end_sub_transaction(lr_eval_string("{mainTransaction}_bonusDay"), LR_AUTO );
			return LR_PASS;
		}
		else
		{
//			lr_error_message( lr_eval_string("{mainTransaction}_bonusDay - Failed with HTTP {httpReturnCode}, API Error Code: {errorCode}, Error Message: {errorMessage}, Email: {userEmail}" )) ;
//			lr_fail_trans_with_error( lr_eval_string("{mainTransaction}_bonusDay Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_sub_transaction(lr_eval_string("{mainTransaction}_bonusDay"), LR_FAIL );
	if (WRITE_TO_SUMO==1) {
 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_bonusDay - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
 	}
			return LR_FAIL;
		}
		
	}
	
	return 0;
}

int bonusDayApply()
{
	if (isLoggedIn==1 && BRIERLEY_FLAG_CHECK == 1 && strcmp( lr_eval_string("{CAN_APPLY_BONUS_DAY}") ,"TRUE") == 0 ) 
	{
		web_reg_save_param_json("ParamName=s_availableBonusPointDays_status", "QueryString=$.status", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	
		web_add_header("Accept", "application/json");
		lr_start_sub_transaction(lr_eval_string("{mainTransaction}_bonusDayApply"), lr_eval_string("{mainTransaction}") );
		addHeader();
		registerErrorCodeCheck();

		web_custom_request("bonusDayApply",
			"URL=https://{api_host}/v2/account/bonusDay", 
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
			"Body={\"orderId\":{orderId},\"bonusDaySelected\":1}",
			LAST);
		
		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && strcmp( lr_eval_string("{s_availableBonusPointDays_status}") ,"success") == 0 ) //if no error code and message
		{	
			lr_end_sub_transaction(lr_eval_string("{mainTransaction}_bonusDayApply"), LR_AUTO );
		}
		else
		{
			lr_end_sub_transaction(lr_eval_string("{mainTransaction}_bonusDayApply"), LR_FAIL );
			return LR_FAIL;
		}

		lr_start_sub_transaction(lr_eval_string("{mainTransaction}_bonusDayApply_getOrderDetails"), lr_eval_string("{mainTransaction}") );
		web_add_header("calc", "false");
		web_add_header("recalculate", "true");
		web_add_header("pageName", "fullOrderInfo");
		web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
		getOrderDetails();
	
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction(lr_eval_string("{mainTransaction}_bonusDayApply_getOrderDetails"), LR_FAIL);
		else
			lr_end_sub_transaction(lr_eval_string("{mainTransaction}_bonusDayApply_getOrderDetails"), LR_AUTO);		
	}
	
	return 0;
}


int waysToEarn()
{
	if (isLoggedIn==1 && BRIERLEY_FLAG_CHECK == 1) 
	{
		web_add_header("Accept", "application/json");
		lr_start_sub_transaction(lr_eval_string("{mainTransaction}_waysToEarn"), lr_eval_string("{mainTransaction}") );
		addHeader();
		registerErrorCodeCheck();

		web_custom_request("waysToEarn", 
			"URL=https://{api_host}/v2/account/points/waysToEarn",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
		{	
			lr_end_sub_transaction(lr_eval_string("{mainTransaction}_waysToEarn"), LR_AUTO );
			return LR_PASS;
		}
		else
		{
			lr_fail_trans_with_error( lr_eval_string("{mainTransaction}_waysToEarn Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_sub_transaction(lr_eval_string("{mainTransaction}_waysToEarn"), LR_FAIL );
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_waysToEarn - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
			return LR_FAIL;
		}
		
	}
	
	return 0;
}

int getListofWishList()
{
	web_reg_save_param_json("ParamName=status", "QueryString=$..status", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);		
	web_reg_save_param_json("ParamName=giftListExternalIdentifier", "QueryString=$..giftListExternalIdentifier", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);		
	web_reg_save_param_json("ParamName=nameIdentifier", "QueryString=$..nameIdentifier", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);		
	web_reg_save_param_json("ParamName=itemCount", "QueryString=$..itemCount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);		

	lr_start_sub_transaction(lr_eval_string("{mainTransaction}_getListofWishList"), lr_eval_string("{mainTransaction}") );
	web_add_header("Access-Control-Request-Headers", "catalogid,langid,storeid");
	web_add_header("Accept", "application/json");
	addHeader();
	
	web_custom_request("getListofWishList",
//		"URL=https://{api_host}/tcpstore/getListofWishList",
		"URL=https://{api_host}/v2/wishlist/getListOfWishlist",  //not yet working on uatlive1 on 08282018
		"Method=GET",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);
	lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getListofWishList"), LR_AUTO );
	
	return 0;
}


int getUnqualifiedItems()
{
    web_reg_save_param_json("ParamName=getUnqualifiedListOfItems", "QueryString=$.orderItemList..orderItemId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
    web_add_header("Accept", "application/json");
    lr_start_sub_transaction(lr_eval_string("{mainTransaction}_getUnqualifiedItems"), lr_eval_string("{mainTransaction}") );
    addHeader();
    registerErrorCodeCheck();
    
    web_custom_request("getUnqualifiedItems",
        "URL=https://{api_host}/v2/cart/getUnqualifiedItems",
        "Method=GET",
        "Resource=0",
        "Mode=HTML",
        LAST);
    
    lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getUnqualifiedItems"), LR_AUTO );
    
    if (atoi(lr_eval_string("{getUnqualifiedListOfItems_count}"))>0){
        lr_save_string("TRUE", "UNQUALIFIEDLIST");
    }else{
        lr_save_string("FALSE", "UNQUALIFIEDLIST");
    }

    return 0;
}
int getOrderDetails_archive(){
	return 0;
}
int getOrderDetails()
{
	registerErrorCodeCheck();
	
	web_reg_save_param_json("ParamName=userPoints", "QueryString=$..userPoints", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=itemPoints", "QueryString=$..orderItems..itemPoints", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=pointsToNextReward", "QueryString=$..pointsToNextReward", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");

	web_reg_save_param_json("ParamName=itemPartNumbers", "QueryString=$..orderItems..itemPartNumber", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=variantNo", "QueryString=$..orderItems..variantNo", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=productidWL", "QueryString=$..orderItems..productid", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=orderItemType", "QueryString=$..orderItems..orderItemType", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=cartCount", "QueryString=$..cartCount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	
	web_reg_save_param_json("ParamName=parentOrderId", "QueryString=$..parentOrderId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=orderItemId", "QueryString=$..orderItems..orderItemId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	
	web_add_header("Accept", "application/json");

	web_reg_find("TEXT/IC=parentOrderId","SaveCount=getOrderDetailsCustomCheck", LAST);
	web_reg_save_param ( "mixCart" ,    "LB=\"mixCart\": \"" , "RB=\"" , "NotFound=Warning", LAST ) ; //"mixCart": "MIX" 
	addHeader();
	addSFCCCartHeader();
	web_custom_request("getOrderDetails",
		"URL=https://{api_host}/v2/checkout/getOrderDetails", 
		"Method=GET",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);
	
//	if (BRIERLEY_FLAG_CHECK == 1 && isLoggedIn == 1) {
	if (BRIERLEY_FLAG_CHECK == 1 ) {
		if ( atoi(lr_eval_string("{userPoints}")) <= 0 || atoi(lr_eval_string("{itemPoints}")) <= 0 || atoi(lr_eval_string("{pointsToNextReward}")) <= 0 ) 
		{
			lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"));
			lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"), LR_FAIL);
		} else 		{
			lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"));
			lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"), LR_PASS);
		}
	}
	
	if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {
		lr_start_transaction("T50_cookie is 0 alert on getOrderDetails_463");
		lr_end_transaction ("T50_cookie is 0 alert on getOrderDetails_463", LR_FAIL ) ;
	}else{
		lr_start_transaction("T50_cookie is 0 alert on getOrderDetails_463");
		lr_end_transaction ("T50_cookie is 0 alert on getOrderDetails_463", LR_PASS ) ;
	}
	
	return 0;
}

int getCartDetails()
{
	web_reg_find("TEXT/IC=cartCount", "SaveCount=apiCheckCount",LAST);
	web_reg_save_param_json("ParamName=userPoints", "QueryString=$..orderDetails..userPoints", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=itemPoints", "QueryString=$..orderDetails..itemPoints", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=pointsToNextReward", "QueryString=$..orderDetails..pointsToNextReward", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");

//	web_reg_save_param_regexp ("ParamName=cartCount",	"RegExp=[Cc]artCount\":[ ]*(.*?),",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST );
	web_reg_save_param_json("ParamName=cartCount", "QueryString=$.orderDetails.cartCount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=offersSku", "QueryString=$..orderItems..variantNo", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=offersPrice", "QueryString=$..orderItems..itemPrice", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=offersQty", "QueryString=$..orderItems..qty", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");

	web_reg_save_param_json("ParamName=expMonth", "QueryString=$..cardExpirationMonth", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=expYear", "QueryString=$..cardExpirationYear", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=addressLineId", "QueryString=$.orderDetails..shippingAddressDetails.addressId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=addressLine", "QueryString=$.orderDetails..shippingAddressDetails.address", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=addressLineZip", "QueryString=$.orderDetails..shippingAddressDetails.zipCode", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=addressLineCity", "QueryString=$.orderDetails..shippingAddressDetails.city", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=addressLinestate", "QueryString=$.orderDetails..shippingAddressDetails.state", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=addressLineFirstName", "QueryString=$.orderDetails..shippingAddressDetails.firstName", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=addressLineLastName", "QueryString=$.orderDetails..shippingAddressDetails.lastName", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=addressLineStLocId", "QueryString=$.orderDetails..shippingAddressDetails.stLocId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=expMonth", "QueryString=$..cardExpirationMonth", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=expYear", "QueryString=$..cardExpirationYear", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
//$..orderItemType
	web_reg_save_param_json("ParamName=parentOrderId", "QueryString=$..parentOrderId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=orderItemId", "QueryString=$..orderItems..orderItemId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=itemCatentryIdForPost", "QueryString=$..orderItems..itemCatentryId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=orderItemType", "QueryString=$..orderItems..orderItemType", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=creditCardId", "QueryString=$..orderItems..creditCardId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=itemCatentryId", "QueryString=$..orderItems..itemCatentryId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=itemQuantity", "QueryString=$..orderItems..qty", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=itemPartNumbers", "QueryString=$..orderItems..itemPartNumber", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=itemsCount", "QueryString=$..mixOrderDetails.data..itemsCount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

//	if ( strcmp(lr_eval_string("{userCouponsApplied}"), "false") == 0 && strcmp(lr_eval_string("{stepName}"), "logon") == 0) {
		web_reg_save_param_json("ParamName=offersCount", "QueryString=$.coupons.offersCount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json("ParamName=couponCode", "QueryString=$.coupons..couponCode", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json("ParamName=isApplied", "QueryString=$.coupons..isApplied", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
//	}
	/*	
	web_reg_save_param( "orderItemType", "LB=\"orderItemType\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST);
	web_reg_save_param( "orderItemTypeECOM", "LB=\"orderItemType\": \"", "RB=ECOM\"", "ORD=All", "NotFound=Warning", LAST); 
	web_reg_save_param( "orderItemTypeBOSS", "LB=\"orderItemType\": \"", "RB=BOSS\"", "ORD=All", "NotFound=Warning", LAST); //  "orderItemType": "BOSS",
	web_reg_save_param( "orderItemTypeBOPIS", "LB=\"orderItemType\": \"", "RB=BOPIS\"", "ORD=All", "NotFound=Warning", LAST); //  "orderItemType": "BOPIS",
	*/
	web_reg_save_param ( "mixCart" ,    "LB=\"mixCart\": \"" , "RB=\"" , "NotFound=Warning", LAST ) ; //"mixCart": "MIX" 
	web_add_header("Accept", "application/json");
	lr_start_sub_transaction(lr_eval_string("{mainTransaction}_cart"), lr_eval_string("{mainTransaction}") );
	registerErrorCodeCheck();
	addHeader();
	addSFCCCartHeader();
//	web_add_header("isRest", "true" );
	web_custom_request("cart",
		"URL=https://{api_host}/v2/checkout/cart", 
		"Method=GET",
		"Resource=0",
		//"RecContentType=application/json",
		LAST);
//	lr_end_sub_transaction(lr_eval_string("{mainTransaction}_cart"), LR_AUTO );
 	setOrderType();
 	/*
    if ( (atoi(lr_eval_string("{orderItemTypeBOPIS_count}")) + atoi(lr_eval_string("{orderItemTypeBOSS_count}"))) > 0)
    	lr_save_string("TRUE", "addBopisToCart");
    
    if ( atoi(lr_eval_string("{orderItemTypeECOM_count}"))  > 0)
    	lr_save_string("TRUE", "addEcommToCart");
	*/
//	if (BRIERLEY_FLAG_CHECK == 1 && isLoggedIn == 1) {
	if (BRIERLEY_FLAG_CHECK == 1 ) {
		if ( atoi(lr_eval_string("{userPoints}")) <= 0 || atoi(lr_eval_string("{pointsToNextReward}")) <= 0 ) 
		{	lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyPoints"));
			lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyPoints"), LR_FAIL);
		} else {
			lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyPoints"));
			lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyPoints"), LR_PASS);
		}
	}
	return 0;
}

void setOrderType(){///////REVISIT
	int i_count=0;
	lr_save_string("FALSE", "addEcommToCart");
	lr_save_string("FALSE", "addBopisToCart");
	lr_save_string("FALSE", "addBossToCart");
	for (i_count=1;i_count<=atoi(lr_eval_string("{orderItemType_count}"));i_count++){
		if (strcmp(lr_paramarr_idx ( "orderItemType" , i_count ),"ECOM")==0)
			lr_save_string("TRUE", "addEcommToCart");
		else if (strcmp(lr_paramarr_idx ( "orderItemType" , i_count ),"BOPIS")==0)
			lr_save_string("TRUE", "addBopisToCart");
		else if (strcmp(lr_paramarr_idx ( "orderItemType" , i_count ),"BOSS")==0)
			lr_save_string("TRUE", "addBossToCart");
		else{
			//This is a very dangerous situation. //Fatal error User will terminate
			lr_error_message("Aborting Vuser: Received unknown type of order that DOES NOT MATCH ECOM BOPIS BOSS : %s", lr_paramarr_idx ( "orderItemType" , i_count ));
			lr_abort();
		}
	}
	
}
int getOrderAndCouponDetails()
{

	addHeader();
	web_add_header("Accept", "application/json");
	web_custom_request("getOrderAndCouponDetails",
		"URL=https://{api_host}/v2/checkout/getOrderAndCouponDetails", 
		"Method=GET",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);
	
	return 0;
}


int giftOptionsCmd()
{
	web_add_header("Accept", "application/json");
	lr_start_sub_transaction(lr_eval_string("{mainTransaction}_giftOptionsCmd"), lr_eval_string("{mainTransaction}") );
	addHeader();
	web_custom_request("giftOptionsCmd",
		"URL=https://{api_host}/v2/checkout/giftOptionsCmd",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);

	lr_end_sub_transaction(lr_eval_string("{mainTransaction}_giftOptionsCmd"), LR_AUTO );

	return 0;
}



int getWishListbyId()
{
	lr_start_sub_transaction(lr_eval_string("{mainTransaction}_getWishListbyId"), lr_eval_string("{mainTransaction}") );
	addHeader();
	registerErrorCodeCheck();
	web_add_header("Accept", "application/json");
	
		
	web_reg_save_param_json("ParamName=giftListItemID", "QueryString=$..giftListItemID", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);// externalId 14646003		
	web_reg_save_param_json("ParamName=productId", "QueryString=$..productId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);	//catEntryId for adding to wishlist cart
	web_reg_save_param_json("ParamName=giftListItemID", "QueryString=$..giftListItemID", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);		
	web_reg_save_param_json("ParamName=productCatentryId", "QueryString=$..parentProductId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);		
	web_reg_save_param_json("ParamName=productPartNumber", "QueryString=$..productPartNumber", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);		

		web_custom_request("getWishListbyId",
			"URL=https://{api_host}/v2/wishlist/getWishListbyId",
			"Method=GET",
//			"RecContentType=text/html",
			"Mode=HTML",
			LAST);
/*	
	if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 || strcmp(lr_eval_string("{errorMessage}") ,"") != 0 ) {
		lr_fail_trans_with_error( lr_eval_string("{mainTransaction}_getWishListbyId Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}") ) ;
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getWishListbyId"), LR_FAIL );
		if (WRITE_TO_SUMO==1) {
	 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_getWishListbyId - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
	 	}
	} else */
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getWishListbyId"), LR_AUTO );

	return 0;

}
int	getPointsAndOrderHistory()
{
	return 0;
}
int	getPointsAndOrderHistory_ARCHIVE()
{
	lr_start_sub_transaction ( lr_eval_string("{mainTransaction}_getPointsAndOrderHistory"), lr_eval_string("{mainTransaction}") );
	web_reg_save_param("s_OrderLookup","LB=\"orderNumber\": \"","RB=\"","NotFound=Warning","Ord=All",LAST);
	addHeader();
	registerErrorCodeCheck();
	web_add_header("Accept", "application/json");
	web_add_header("fromRest", "true");
	web_reg_find("TEXT/IC=getOrderHistoryResponse", "SaveCount=apiCheck", LAST);
	web_custom_request("getPointsAndOrderHistory", 
		"URL=https://{api_host}/v2/wallet/getPointsAndOrderHistory",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getPointsAndOrderHistory"), LR_FAIL);
	else
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getPointsAndOrderHistory"), LR_AUTO);

	return 0;
}

void getCatEntryID() {
	lr_save_string(lr_eval_string(""), "atc_catentryId");

	if ( strcmp(lr_eval_string("{atc_catentryId}"), lr_eval_string("{lastvalue}") ) == 0 ) {
		lr_param_sprintf ( "atc_catentryId" , "%s" , lr_paramarr_random ( "atc_catentryIds" ) ) ;
		USE_LOW_INVENTORY = 0;
	}
	else {
		lr_save_string(lr_eval_string("{atc_catentryId}"), "lastvalue");
		lr_save_string(lr_eval_string("{atc_catentryId}"), "atc_comment");
		lr_start_transaction("T20_Low_QTY_Count");
		lr_end_transaction("T20_Low_QTY_Count", LR_AUTO);
	}

	return;
}

void prepLoyaltyDataCapture()
{
	if (isLoggedIn == 1 && BRIERLEY_FLAG_CHECK == 1) {
	//==== for /loyaltyPoints
		web_reg_save_param_json("ParamName=orderTotal", "QueryString=$..orderTotal", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=orderItemTotal", "QueryString=$..orderItemTotal", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=isElectiveBonus", "QueryString=$..isElectiveBonus", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=orderSubTotal", "QueryString=$..orderSubTotal", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=orderSubTotalBeforeDiscount", "QueryString=$..orderSubTotalBeforeDiscount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=orderSubTotalDiscount", "QueryString=$..orderSubTotalDiscount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=orderSubTotalWithoutGiftCard", "QueryString=$..orderSubTotalWithoutGiftCard", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=orderTotalAfterDiscount", "QueryString=$..orderTotalAfterDiscount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=orderTotalListPrice", "QueryString=$..orderTotalListPrice", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=orderTotalSaving", "QueryString=$..orderTotalSaving", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=mprId", "QueryString=$..mprId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=isPLCC", "QueryString=$..isPLCC", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");

		web_reg_save_param_json("ParamName=multiPack", "QueryString=$..multiPack", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=itemUnitPrice", "QueryString=$..itemUnitPrice", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=itemBrand", "QueryString=$..itemBrand", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=itemPrice", "QueryString=$..itemPrice", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=qtyATC", "QueryString=$..qty", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=variantNoATC", "QueryString=$..variantNo", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=itemDstPrice", "QueryString=$..itemDstPrice", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=itemUnitDstPrice", "QueryString=$..itemUnitDstPrice", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=orderItemIdATC", "QueryString=$.orderItems..orderItemId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=itemPoints", "QueryString=$..itemPoints", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	//=====
	}
}

int loyaltyPoints()
{
	if (isLoggedIn == 1 && BRIERLEY_FLAG_CHECK == 1) {
		
		if ( atoi(lr_eval_string("{orderItemIdATC_count}")) > 0 ) {
			lr_param_sprintf("loyaltyBody", "%s", lr_eval_string("{\"orderId\":\"{orderId}\",\"orderItemId\":\"{orderItemId}\",\"orderTotal\":\"{orderTotal}\",\"orderItemTotal\":\"{orderItemTotal}\",\"orderItems\":[") );
	
			for (i=1; i <= atoi(lr_eval_string("{orderItemIdATC_count}")); i++) {
				
				lr_save_string( lr_paramarr_idx("multiPack",i), "p_multiPack" );
				lr_save_string( lr_paramarr_idx("itemUnitPrice",i), "p_itemUnitPrice" );
				lr_save_string( lr_paramarr_idx("itemBrand",i), "p_itemBrand" );
				lr_save_string( lr_paramarr_idx("itemPrice",i), "p_itemPrice" );
				lr_save_string( lr_paramarr_idx("qtyATC",i), "p_qtyATC" );
				lr_save_string( lr_paramarr_idx("variantNoATC",i), "p_variantNoATC" );
				lr_save_string( lr_paramarr_idx("itemDstPrice",i), "p_itemDstPrice" );
				lr_save_string( lr_paramarr_idx("itemUnitDstPrice",i), "p_itemUnitDstPrice" );
				lr_save_string( lr_paramarr_idx("orderItemIdATC",i), "p_orderItemIdATC" );
				lr_save_string( lr_paramarr_idx("itemPoints",i), "p_itemPoints" );
	
				if (atoi(lr_eval_string("{orderItemIdATC_count}")) == 1)
					lr_param_sprintf("loyaltyBody", "%s%s", lr_eval_string("{loyaltyBody}"), lr_eval_string("{\"multiPack\":{p_multiPack},\"itemUnitPrice\":{p_itemUnitPrice},\"giftItem\":false,\"itemBrand\":\"TCP\",\"itemPrice\":{p_itemPrice},\"qty\":{p_qtyATC},\"variantNo\":\"{p_variantNoATC}\",\"itemDstPrice\":{p_itemDstPrice},\"itemUnitDstPrice\":{p_itemUnitDstPrice},\"orderItemId\":\"{p_orderItemIdATC}\",\"itemPoints\":{p_itemPoints}}") );
				else
					lr_param_sprintf("loyaltyBody", "%s%s", lr_eval_string("{loyaltyBody}"), lr_eval_string("{\"multiPack\":{p_multiPack},\"itemUnitPrice\":{p_itemUnitPrice},\"giftItem\":false,\"itemBrand\":\"TCP\",\"itemPrice\":{p_itemPrice},\"qty\":{p_qtyATC},\"variantNo\":\"{p_variantNoATC}\",\"itemDstPrice\":{p_itemDstPrice},\"itemUnitDstPrice\":{p_itemUnitDstPrice},\"orderItemId\":\"{p_orderItemIdATC}\",\"itemPoints\":{p_itemPoints}},") );
			}
	
			if (strcmp(lr_eval_string("{isPLCC}"), "") == 0)
				lr_param_sprintf("loyaltyBody", "%s%s", lr_eval_string("{loyaltyBody}"), lr_eval_string("],\"isElectiveBonus\":\"0\",\"orderSubTotal\":{orderSubTotal},\"orderSubTotalBeforeDiscount\":{orderSubTotalBeforeDiscount},\"orderSubTotalDiscount\":{orderSubTotalDiscount},\"orderSubTotalWithoutGiftCard\":{orderSubTotalWithoutGiftCard},\"orderTotalAfterDiscount\":{orderTotalAfterDiscount},\"orderTotalListPrice\":{orderTotalListPrice},\"orderTotalSaving\":{orderTotalSaving},\"mprId\":\"{mprId}\",\"isPLCC\":false}") );
			else
				lr_param_sprintf("loyaltyBody", "%s%s", lr_eval_string("{loyaltyBody}"), lr_eval_string("],\"isElectiveBonus\":\"0\",\"orderSubTotal\":{orderSubTotal},\"orderSubTotalBeforeDiscount\":{orderSubTotalBeforeDiscount},\"orderSubTotalDiscount\":{orderSubTotalDiscount},\"orderSubTotalWithoutGiftCard\":{orderSubTotalWithoutGiftCard},\"orderTotalAfterDiscount\":{orderTotalAfterDiscount},\"orderTotalListPrice\":{orderTotalListPrice},\"orderTotalSaving\":{orderTotalSaving},\"mprId\":\"{mprId}\",\"isPLCC\":{isPLCC}}") );
	
			lr_start_sub_transaction(lr_eval_string("{mainTransaction}_loyaltyPoints"), lr_eval_string("{mainTransaction}") );
			registerErrorCodeCheck();
			addHeader();
	
			web_add_header("Accept", "application/json");
			web_custom_request("loyaltyPoints",
				"URL=https://{api_host}/v2/cart/loyaltyPoints", 
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
				"Body={loyaltyBody}",
				LAST);
			
			if (strcmp(lr_eval_string("{errorCode}"),"")==0 && strcmp(lr_eval_string("{errorMessage}"),"") ==0  ) {
				lr_end_sub_transaction(lr_eval_string("{mainTransaction}_loyaltyPoints"), LR_AUTO );
				return LR_PASS;
			} else {
				lr_fail_trans_with_error( lr_eval_string("{mainTransaction}_loyaltyPoints HTTP {httpReturnCode}, API Error Code: \"{userEmail}\", errorCode: {errorCode}), errorMessage: {errorMessage}" ) );
				lr_end_sub_transaction(lr_eval_string("{mainTransaction}_loyaltyPoints"), LR_FAIL );
				if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_loyaltyPoints - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
			 	}
		
				return LR_FAIL;
			}
		}
	}
	
	return 0;
}


int addToCartFromPLP_UI()
{
	int k;
	atc_Stat = 0; //0-Pass 1-Fail

	lr_think_time ( FORM_TT ) ;

	if ( atoi( lr_eval_string("{unbxd_productID_count}" )) > 0 )
	{
		lr_save_string(lr_paramarr_random("unbxd_productID"), "productId" );//03212018
		lr_save_string( lr_eval_string("{unbxd_PDPURL}") , "pdpURL") ;
		lr_save_string( lr_eval_string("{unbxd_productID}") , "productId") ; 


	 	if (UNBXD_FLAG == 1) {
			lr_start_transaction ( "T04_Product List Page Bag" ) ;
			web_reg_save_param_regexp("ParamName=atc_catentryIds", "RegExp=\"v_item_catentry_id\":[ ]*\"(.*?)\"", "NotFound=Warning",SEARCH_FILTERS, "RequestUrl=*", "Ordinal=ALL", LAST); //"v_item_catentry_id": "952848"
			web_reg_save_param_regexp("ParamName=atc_quantity", "RegExp=\"v_qty\":[ ]*(.*?),", "NotFound=Warning",SEARCH_FILTERS, "RequestUrl=*","Ordinal=ALL", LAST); //"v_item_catentry_id": "952848"
			web_reg_save_param_json("ParamName=itemPartNumber", "QueryString=$..variants..variantId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json("ParamName=variantNo", "QueryString=$..variants..v_variant", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			lr_start_sub_transaction("T04_Product List Page Bag_unbxd.search", "T04_Product List Page Bag");
			web_add_header("Origin", "https://{host}");
	
			web_url("PLP", 
				"URL=https://{UNBXD_host}/search?variants=true&variants.count=100&version=V2&rows=20&pagetype=boolean&q={productId}&promotion=false&fields=alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPFitMessageUSSstore,TCPFit,product_name,TCPColor,top_rated,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_catMap,categoryPath2_catMap,product_short_description,style_long_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,product_long_description,seo_token,variantCount,prodpartno,variants,v_tcpfit,v_qty,v_tcpsize,style_name,v_item_catentry_id,v_listprice,v_offerprice,v_qty,variantId,auxdescription,list_of_attributes,additional_styles,TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,v_variant,%20low_offer_price,%20high_offer_price,%20low_list_price,%20high_list_price,long_product_title,TCPOutOfStockFlagUSStore",
				"Resource=0",
				"RecContentType=application/json", 
				"Mode=HTML", 
				LAST);
			lr_end_sub_transaction("T04_Product List Page Bag_unbxd.search", LR_AUTO);

			lr_end_transaction ( "T04_Product List Page Bag" , LR_AUTO ) ;
		}
		
		if ( lr_paramarr_len ( "atc_catentryIds" ) > 0 )
		{
			if (ATC_FLAG_OVERRIDE == 0) {
				lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId" );
			} else {
				for (k = 1; k<= atoi(lr_eval_string("{atc_catentryIds_count}")); k++ )
				{
					if (atoi( lr_paramarr_idx("atc_quantity", k) ) != 0)
					{
						lr_save_string(lr_paramarr_idx("atc_catentryIds", k), "atc_catentryId" ); 
						lr_save_string(lr_paramarr_idx("variantNo", k), "VariantNo" );
						lr_save_string(lr_paramarr_idx("itemPartNumbers", k), "itemPartNumber" );
						
						break;
					}
				}
			}
			lr_save_string ( "T05_Add To Cart" , "mainTransaction") ;
//			getBOPISInvetoryDetails();
	
			lr_start_transaction ( "T05_Add To Cart" ) ;
	
			lr_start_sub_transaction("T05_Add To Cart_addProductToCart" , "T05_Add To Cart");
			
			if (strcmp(lr_eval_string("{navigateXHR}"), "false") == 0) {
				web_reg_save_param_regexp ("ParamName=WC_AUTHENTICATION",	"RegExp=set-cookie: WC_AUTHENTICATION_(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
				web_reg_save_param_regexp ("ParamName=WC_ACTIVEPOINTER",	"RegExp=set-cookie: WC_ACTIVEPOINTER=(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers", "RequestUrl=*", LAST );
				web_reg_save_param_regexp ("ParamName=tcp_email_signup_modal_persistent", "RegExp=date: (.*?) GMT",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers", "RequestUrl=*", LAST ); //need to concatenate withiur spaces
			}

			web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
			web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n
			web_reg_save_param_json("ParamName=orderId", "QueryString=$.orderId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param_json("ParamName=orderItemId", "QueryString=$.orderItemId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param("cartCount", "LB=set-cookie: cartItemsCount=", "RB=; Path=/; Domain", "NotFound=Warning", LAST); //set-cookie: cartItemsCount=1; Path=/; Domain
			registerErrorCodeCheck();
			addHeader();
		//	addSFCCCartHeader();
			
			web_add_header("content-type", "application/json");
			web_add_header("Accept", "application/json");
			web_custom_request("addProductToCart",
				"URL=https://{api_host}/v2/cart/addProductToCart", //PLP UI
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
//				"Body={\"calculationUsage[]\":\"-7\",\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{atc_catentryId}\",\"quantity\":\"1\"}",
				"Body={\"catalogId\":\"{catalogId}\",\"storeId\":\"{storeId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{atc_catentryId}\",\"quantity\":\"1\",\"calculationUsage[]\":\"-7\",\"externalId\":\"\",\"multipackCatentryId\":null,\"skipLoyaltyCall\":false}",
				LAST);

			if ( strlen(lr_eval_string("{orderId}")) <= 0 ) {
				atc_Stat = 1; //0-Pass 1-Fail
				if ( isLoggedIn == 1 ) 
					lr_error_message( lr_eval_string("T05_Add To Cart_addProductToCart - Failed with HTTP {httpReturnCode}, catentryId: {atc_catentryId}, API Error Code: {errorCode}, Error Message: {errorMessage}, OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
				else
					lr_error_message( lr_eval_string("T05_Add To Cart_addProductToCart - Failed with HTTP {httpReturnCode}, catentryId: {atc_catentryId}, API Error Code: {errorCode}, Error Message: {errorMessage}, OrderId:{orderId}, Email: Guest, timestamp: {timestamp}, hostname: {hostName}") ) ;

				lr_end_sub_transaction ("T05_Add To Cart_addProductToCart", LR_FAIL) ;
				lr_end_transaction ( "T05_Add To Cart" , LR_FAIL ) ;
				if (WRITE_TO_SUMO==1) {
			 				lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_addProductToCart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
							sendErrorToSumo(); 
			 	}
				return LR_FAIL;
			} // end if
			lr_end_sub_transaction ("T05_Add To Cart_addProductToCart", LR_AUTO);

			lr_save_string("T05_Add To Cart", "mainTransaction");
//			getAllCoupons(); 03112019

			web_add_header("content-type", "application/json");
			web_add_header("Accept", "application/json");
						
			if (BRIERLEY_CUSTOM_TEST == 1 && BRIERLEY_FLAG_CHECK == 1)
				web_add_header("recalculate", "true" );
			else
				web_add_header("recalculate", "false" );

			web_add_header("pageName", "fullOrderInfo");
			web_add_header("calc", "false");
			registerErrorCodeCheck();
			
			web_reg_save_param_json("ParamName=userPoints", "QueryString=$..userPoints", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param_json("ParamName=itemPoints", "QueryString=$..orderItems..itemPoints", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param_json("ParamName=pointsToNextReward", "QueryString=$..pointsToNextReward", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		
			web_reg_save_param_json("ParamName=itemPartNumbers", "QueryString=$..orderItems..itemPartNumber", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json("ParamName=variantNo", "QueryString=$..orderItems..variantNo", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json("ParamName=productidWL", "QueryString=$..orderItems..productid", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json("ParamName=orderItemType", "QueryString=$..orderItems..orderItemType", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json("ParamName=cartCount", "QueryString=$..cartCount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

			web_reg_save_param_json("ParamName=parentOrderId", "QueryString=$..parentOrderId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param_json("ParamName=orderItemId", "QueryString=$..orderItems..orderItemId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			if (strcmp(lr_eval_string("{navigateXHR}"), "false") == 0) {
				web_reg_save_param_regexp ("ParamName=WC_PERSISTENT",	"RegExp=set-cookie: WC_PERSISTENT=(.*?);",  "NotFound=warning", SEARCH_FILTERS, "Scope=Headers", "RequestUrl=*", LAST );
				web_reg_save_param_regexp ("ParamName=WC_AUTHENTICATION",	"RegExp=set-cookie: WC_AUTHENTICATION_(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
				web_reg_save_param_regexp ("ParamName=WC_cartItemsCount",	"RegExp=set-cookie: cartItemsCount=(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
				web_reg_save_param_regexp ("ParamName=WC_USERACTIVITY",	"RegExp=set-cookie: WC_USERACTIVITY_(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
			}
			web_reg_find("TEXT/IC=parentOrderId","SaveCount=getOrderDetailsCustomCheck", LAST);
			web_reg_save_param ( "mixCart" ,    "LB=\"mixCart\": \"" , "RB=\"" , "NotFound=Warning", LAST ) ; //"mixCart": "MIX" 
			addHeader();
	//		addSFCCCartHeader();
			lr_start_sub_transaction("T05_Add To Cart_getOrderDetails" , "T05_Add To Cart");
			web_custom_request("getOrderDetails",
				"URL=https://{api_host}/v2/checkout/getOrderDetails", 
				"Method=GET",
				"Resource=0",
				"RecContentType=text/html",
				"Mode=HTML",
				LAST);
			lr_end_sub_transaction("T05_Add To Cart_getOrderDetails" , LR_AUTO);
			
//			if (BRIERLEY_FLAG_CHECK == 1 && isLoggedIn == 1) {
			if (BRIERLEY_FLAG_CHECK == 1 ) {
				if ( atoi(lr_eval_string("{userPoints}")) <= 0 || atoi(lr_eval_string("{itemPoints}")) <= 0 || atoi(lr_eval_string("{pointsToNextReward}")) <= 0 ) 
				{
					lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"));
					lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"), LR_FAIL);
				} else {
					lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"));
					lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"), LR_PASS);
					if (atoi(lr_eval_string("{userPoints}")) > atoi(lr_eval_string("{previousATC_userpoints}")) ) {
						lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints_XXX"));
						lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints_XXX"), LR_PASS);
					} else {
						lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints_XXX"));
						lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints_XXX"), LR_FAIL);
					}
					lr_save_string(lr_eval_string("{userPoints}"), "previousATC_userpoints");
				}
			}
			if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {
				lr_start_transaction("T50_cookie is 0 alert on getOrderDetails_812");
				lr_end_transaction ("T50_cookie is 0 alert on getOrderDetails_812", LR_FAIL ) ;
			}else{
				lr_start_transaction("T50_cookie is 0 alert on getOrderDetails_812");
				lr_end_transaction ("T50_cookie is 0 alert on getOrderDetails_812", LR_PASS ) ;
			}
			CheckAndCallnavigateXHR();
			lr_end_transaction ( "T05_Add To Cart" , LR_AUTO ) ;

		}
	}
	else
	{
		//lr_continue_on_error(1);
		atc_Stat = 1; //0-Pass 1-Fail
		return LR_FAIL;
	}

	return 0;
}

int addToCartFromPLP_DF()
{
	int k, randNumber;
	atc_Stat = 0; //0-Pass 1-Fail

	if (CACHE_PRIME_MODE == 1)
		return 0;

	lr_think_time ( FORM_TT ) ;
	//lr_continue_on_error(0);

//	if ( strlen( lr_eval_string("{unbxd_productID}" )) > 0 )
//	{
//		lr_save_string(lr_paramarr_random("productId"), "productId" );//03212018

//		lr_advance_param("pdpURLData");
//		lr_save_string(lr_eval_string("{pdpProdID}"), "productId" );   //romano09102018
//		web_reg_save_param_regexp ("ParamName=orderId",	"RegExp=\"orderId\":[ ]*\"(.*?)\"",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST ); //romano disabled this 04232018
		
	 	if (UNBXD_FLAG == 1) {
			lr_save_string( lr_eval_string("{unbxd_PDPURL}") , "pdpURL") ;
			lr_save_string( lr_eval_string("{unbxd_productID}") , "productId") ; 

			lr_start_transaction ( "T04_Product List Page Bag" ) ;
				web_reg_save_param_regexp("ParamName=atc_catentryIds", "RegExp=\"v_item_catentry_id\":[ ]*\"(.*?)\"", "NotFound=Warning",SEARCH_FILTERS, "RequestUrl=*", "Ordinal=ALL", LAST); //"v_item_catentry_id": "952848"
				web_reg_save_param_regexp("ParamName=atc_quantity", "RegExp=\"v_qty\":[ ]*(.*?),", "NotFound=Warning",SEARCH_FILTERS, "RequestUrl=*","Ordinal=ALL", LAST); //"v_item_catentry_id": "952848"
				lr_start_sub_transaction("T04_Product List Page Bag_unbxd.search", "T04_Product List Page Bag");
				web_add_header("Origin", "https://{host}");

				web_url("PLP", 
					"URL=https://{UNBXD_host}/search?variants=true&variants.count=100&version=V2&pagetype=boolean&q={productId}&promotion=false&fields=alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPFitMessageUSSstore,TCPFit,product_name,TCPColor,top_rated,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_catMap,product_short_description,style_long_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,product_long_description,seo_token,variantCount,prodpartno,variants,v_tcpfit,v_qty,v_tcpsize,style_name,v_item_catentry_id,v_listprice,v_offerprice,v_qty,variantId,auxdescription,list_of_attributes,TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,v_variant&rows=30",
					"Resource=0",
					"RecContentType=application/json", 
					"Mode=HTML", 
					LAST);
				lr_end_sub_transaction("T04_Product List Page Bag_unbxd.search", LR_AUTO);
				
			lr_end_transaction ( "T04_Product List Page Bag" , LR_AUTO ) ;
		}

		randNumber = atoi(lr_eval_string("{RANDOM_PERCENT}"));
		lr_save_string("", "multiProduct" );
		if (randNumber > (ATC_MULTI_PRODUCT + ATC_TRIPACK_PRODUCT + ATC_QUADPACK_PRODUCT) ) {
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId" );
			lr_save_string("", "multiProduct" );
/*		} else if (randNumber <= ATC_MULTI_PRODUCT) {
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId1" );
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId2" );
			lr_save_string(lr_eval_string("{atc_catentryId1}\",\"{atc_catentryId2}"), "atc_catentryId" );
			lr_save_string("_Multiproduct", "multiProduct" );
*/		} else if (randNumber <= ATC_MULTI_PRODUCT + ATC_TRIPACK_PRODUCT ) {
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId1" );
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId2" );
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId3" );
			lr_save_string(lr_eval_string("[\"{atc_catentryId1}\",\"{atc_catentryId2}\",\"{atc_catentryId3}\"]"), "atc_catentryId" );
			lr_save_string("_Tripack", "multiProduct" );
		} else if (randNumber <= ATC_MULTI_PRODUCT + ATC_TRIPACK_PRODUCT + ATC_QUADPACK_PRODUCT ) {
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId1" );
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId2" );
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId3" );
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId4" );
			lr_save_string(lr_eval_string("[\"{atc_catentryId1}\",\"{atc_catentryId2}\",\"{atc_catentryId3}\",\"{atc_catentryId4}\"]"), "atc_catentryId" );
			lr_save_string("_Quadpack", "multiProduct" );
		}
		lr_save_string(lr_eval_string("{multipackId}"), "atc_multipackId" );
		
		lr_save_string ( "T05_Add To Cart" , "mainTransaction") ;
//		getBOPISInvetoryDetails();

		lr_start_transaction ( "T05_Add To Cart" ) ;

		lr_start_sub_transaction(lr_eval_string("T05_Add To Cart_addProductToCart{multiProduct}") , "T05_Add To Cart");
		web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
		web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n
		web_reg_save_param_json("ParamName=orderId", "QueryString=$.orderId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=orderItemId", "QueryString=$.orderItemId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param("cartCount", "LB=set-cookie: cartItemsCount=", "RB=; Path=/; Domain", "NotFound=Warning", LAST); //set-cookie: cartItemsCount=1; Path=/; Domain

		prepLoyaltyDataCapture();

		registerErrorCodeCheck();
		addHeader();
		//addSFCCCartHeader();
		if (strcmp(lr_eval_string("{navigateXHR}"), "false") == 0) {
			web_reg_save_param_regexp ("ParamName=WC_PERSISTENT",	"RegExp=set-cookie: WC_PERSISTENT=(.*?);",  "NotFound=warning", SEARCH_FILTERS, "Scope=Headers", "RequestUrl=*", LAST );
			web_reg_save_param_regexp ("ParamName=WC_AUTHENTICATION",	"RegExp=set-cookie: WC_AUTHENTICATION_(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
			web_reg_save_param_regexp ("ParamName=WC_cartItemsCount",	"RegExp=set-cookie: cartItemsCount=(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
			web_reg_save_param_regexp ("ParamName=WC_USERACTIVITY",	"RegExp=set-cookie: WC_USERACTIVITY_(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
		}

		web_add_header("Accept", "application/json");
		web_custom_request("addProductToCart",
			"URL=https://{api_host}/v2/cart/addProductToCart", //PLP DF
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
//	         "Body={    \"catalogId\":\"{catalogId}\",    \"storeId\":\"{storeId}\",    \"langId\":\"-1\",    \"orderId\":\".\",    \"field2\":\"0\",    \"requesttype\":\"ajax\",    \"catEntryId\":[\"{atc_catentryId}\" ],    \"quantity\":\"1\",    \"calculationUsage[]\":\"-7\",    \"externalId\":\"\",    \"multipackCatentryId\":\"1575963:841923_2\",    \"skipLoyaltyCall\":false }",
//060121	"Body={\"catalogId\":\"{catalogId}\",\"storeId\":\"{storeId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{atc_catentryId}\",\"quantity\":\"1\",\"calculationUsage[]\":\"-7\",\"externalId\":\"\",\"multipackCatentryId\":null,\"skipLoyaltyCall\":false}",
//	        "Body={    \"catalogId\":\"{catalogId}\",    \"storeId\":\"{storeId}\",    \"langId\":\"-1\",    \"orderId\":\".\",    \"field2\":\"0\",    \"requesttype\":\"ajax\",    \"catEntryId\":[       \"{atc_catentryId}\"    ],    \"quantity\":\"1\",    \"calculationUsage[]\":\"-7\",    \"externalId\":\"\",    \"multipackCatentryId\":null,    \"skipLoyaltyCall\":false }",
	        "Body={    \"catalogId\":\"{catalogId}\",    \"storeId\":\"{storeId}\",    \"langId\":\"-1\",    \"orderId\":\".\",    \"field2\":\"0\",    \"requesttype\":\"ajax\",    \"catEntryId\":{atc_catentryId},    \"quantity\":\"1\",    \"calculationUsage[]\":\"-7\",    \"externalId\":\"\",    \"multipackCatentryId\":null,    \"skipLoyaltyCall\":false }",
//1	        "Body={    \"catalogId\":\"{catalogId}\",    \"storeId\":\"{storeId}\",    \"langId\":\"-1\",    \"orderId\":\".\",    \"field2\":\"0\",    \"requesttype\":\"ajax\",    \"catEntryId\":[       \"{atc_catentryId}\"    ],    \"quantity\":\"1\",    \"calculationUsage[]\":\"-7\",    \"externalId\":\"\",    \"multipackCatentryId\":\"1575963:841923_2\",    \"skipLoyaltyCall\":false }",
			LAST);
		
		if ( strlen(lr_eval_string("{orderId}")) <= 0 ) {
			atc_Stat = 1; //0-Pass 1-Fail
			if ( isLoggedIn == 1 ) 
				lr_error_message( lr_eval_string("T05_Add To Cart_addProductToCart{multiProduct} - Failed with HTTP {httpReturnCode}, catentryId: {atc_catentryId}, API Error Code: {errorCode}, Error Message: {errorMessage}, OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
			else
				lr_error_message( lr_eval_string("T05_Add To Cart_addProductToCart{multiProduct} - Failed with HTTP {httpReturnCode}, catentryId: {atc_catentryId}, API Error Code: {errorCode}, Error Message: {errorMessage}, OrderId:{orderId}, Email: Guest, timestamp: {timestamp}, hostname: {hostName}") ) ;

			lr_end_sub_transaction (lr_eval_string("T05_Add To Cart_addProductToCart{multiProduct}"), LR_FAIL) ;
			lr_end_transaction ( "T05_Add To Cart" , LR_FAIL ) ;
			if (WRITE_TO_SUMO==1) {
		 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_addProductToCart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
				sendErrorToSumo(); 
		 	}
			return LR_FAIL;
		} // end if
		lr_end_sub_transaction (lr_eval_string("T05_Add To Cart_addProductToCart{multiProduct}"), LR_AUTO);

		lr_save_string("T05_Add To Cart", "mainTransaction");

		loyaltyPoints();

		if ( BRIERLEY_FLAG_CHECK == 0) {
/*	060221 - per Pavan, Match prod UI, this doesnt exist in prod, so this is now disabled
	//		web_add_header("content-type", "application/json");
			web_add_header("Accept", "application/json");
						
			if (BRIERLEY_CUSTOM_TEST == 1 && BRIERLEY_FLAG_CHECK == 1)
				web_add_header("recalculate", "true" );
			else
				web_add_header("recalculate", "false" );
	
			web_add_header("pageName", "fullOrderInfo");
			web_add_header("calc", "false");
			registerErrorCodeCheck();
			lr_start_sub_transaction("T05_Add To Cart_getOrderDetails" , "T05_Add To Cart");
			
			web_reg_save_param_json("ParamName=userPoints", "QueryString=$..userPoints", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param_json("ParamName=itemPoints", "QueryString=$..orderItems..itemPoints", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param_json("ParamName=pointsToNextReward", "QueryString=$..pointsToNextReward", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		
			web_reg_save_param_json("ParamName=itemPartNumbers", "QueryString=$..orderItems..itemPartNumber", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json("ParamName=variantNo", "QueryString=$..orderItems..variantNo", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json("ParamName=productidWL", "QueryString=$..orderItems..productid", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json("ParamName=orderItemType", "QueryString=$..orderItems..orderItemType", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json("ParamName=cartCount", "QueryString=$..cartCount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	
			web_reg_save_param_json("ParamName=parentOrderId", "QueryString=$..parentOrderId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param_json("ParamName=orderItemId", "QueryString=$..orderItems..orderItemId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	
			if (strcmp(lr_eval_string("{navigateXHR}"), "false") == 0) {
				web_reg_save_param_regexp ("ParamName=WC_PERSISTENT",	"RegExp=set-cookie: WC_PERSISTENT=(.*?);",  "NotFound=warning", SEARCH_FILTERS, "Scope=Headers", "RequestUrl=*", LAST );
				web_reg_save_param_regexp ("ParamName=WC_AUTHENTICATION",	"RegExp=set-cookie: WC_AUTHENTICATION_(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
				web_reg_save_param_regexp ("ParamName=WC_cartItemsCount",	"RegExp=set-cookie: cartItemsCount=(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
				web_reg_save_param_regexp ("ParamName=WC_USERACTIVITY",	"RegExp=set-cookie: WC_USERACTIVITY_(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
			}
			
			web_reg_find("TEXT/IC=parentOrderId","SaveCount=getOrderDetailsCustomCheck", LAST);
			web_reg_save_param ( "mixCart" ,    "LB=\"mixCart\": \"" , "RB=\"" , "NotFound=Warning", LAST ) ; //"mixCart": "MIX" 
			addHeader();
			web_custom_request("getOrderDetails",
				"URL=https://{api_host}/v2/checkout/getOrderDetails", 
				"Method=GET",
				"Resource=0",
				"RecContentType=text/html",
				"Mode=HTML",
				LAST);
				lr_end_sub_transaction ("T05_Add To Cart_getOrderDetails", LR_AUTO);
		
	//		if (BRIERLEY_FLAG_CHECK == 1 && isLoggedIn == 1) {
			if (BRIERLEY_FLAG_CHECK == 1 ) {
				if ( atoi(lr_eval_string("{userPoints}")) <= 0 || atoi(lr_eval_string("{itemPoints}")) <= 0 || atoi(lr_eval_string("{pointsToNextReward}")) <= 0 ) 
				{
					lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"));
					lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"), LR_FAIL);
				} else {
					lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"));
					lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"), LR_PASS);
					if (atoi(lr_eval_string("{userPoints}")) > atoi(lr_eval_string("{previousATC_userpoints}")) ) {
						lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints_XXX"));
						lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints_XXX"), LR_PASS);
					} else {
						lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints_XXX"));
						lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints_XXX"), LR_FAIL);
					}
					lr_save_string(lr_eval_string("{userPoints}"), "previousATC_userpoints");
				}
			}
	
			if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {
				lr_start_transaction("T50_cookie is 0 alert on getOrderDetails_971");
				lr_end_transaction ("T50_cookie is 0 alert on getOrderDetails_971", LR_FAIL ) ;
				}else{
					lr_start_transaction("T50_cookie is 0 alert on getOrderDetails_971");
				lr_end_transaction ("T50_cookie is 0 alert on getOrderDetails_971", LR_PASS ) ;
				}
*/		
		}
	CheckAndCallnavigateXHR();	

	lr_end_transaction ( "T05_Add To Cart" , LR_AUTO ) ;

	return 0;
}

int addToCart_UI()
{
	int k = 0, newTime, RANDOM_PERCENT = 0;
	atc_Stat = 0; //0-Pass 1-Fail

	if (CACHE_PRIME_MODE == 1)
		return 0;

	lr_think_time ( FORM_TT ) ;
		
	if ( lr_paramarr_len ( "atc_catentryIds" ) > 0 ) { // at least 1 product with positive quantity check
		
		if (ATC_FLAG_OVERRIDE == 0){
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId" );
		} else {
			for (k = 1; k<= atoi(lr_eval_string("{atc_catentryIds_count}")); k++ )
			{
				if (atoi( lr_paramarr_idx("atc_quantity", k) ) != 0 )
				{
					lr_save_string(lr_paramarr_idx("atc_catentryIds", k), "atc_catentryId" ); break;
				}
			}
		}
		lr_save_string ( "T05_Add To Cart" , "mainTransaction") ;

		lr_start_transaction ( "T05_Add To Cart" ) ;
		
		web_reg_save_param_json("ParamName=orderId", "QueryString=$.orderId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=orderItemId", "QueryString=$.orderItemId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_find("Text=the products you wish to purchase are not available", "SaveCount=atcErrorFound");
		web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
		web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n
		web_reg_save_param("cartCount", "LB=set-cookie: cartItemsCount=", "RB=; Path=/; Domain", "NotFound=Warning", LAST); //set-cookie: cartItemsCount=1; Path=/; Domain
		if (strcmp(lr_eval_string("{navigateXHR}"), "false") == 0) {
			web_reg_save_param_regexp ("ParamName=WC_PERSISTENT",	"RegExp=set-cookie: WC_PERSISTENT=(.*?);",  "NotFound=warning", SEARCH_FILTERS, "Scope=Headers", "RequestUrl=*", LAST );
			web_reg_save_param_regexp ("ParamName=WC_AUTHENTICATION",	"RegExp=set-cookie: WC_AUTHENTICATION_(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
			web_reg_save_param_regexp ("ParamName=WC_cartItemsCount",	"RegExp=set-cookie: cartItemsCount=(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
			web_reg_save_param_regexp ("ParamName=WC_USERACTIVITY",	"RegExp=set-cookie: WC_USERACTIVITY_(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
		}

		prepLoyaltyDataCapture();
		lr_start_sub_transaction("T05_Add To Cart_addProductToCart" , "T05_Add To Cart");
		registerErrorCodeCheck();
		addHeader();
		//addSFCCCartHeader();
		web_add_header("content-type", "application/json");
		web_add_header("Accept", "application/json");
		web_custom_request("addProductToCart",
			"URL=https://{api_host}/v2/cart/addProductToCart",  //ATC UI
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTML",
			"EncType=application/json",
//old			"Body={\"calculationUsage[]\":\"-7\",\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{atc_catentryId}\",\"quantity\":\"1\"}",
//			"Body={\"catalogId\":\"{catalogId}\",\"storeId\":\"{storeId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{atc_catentryId}\",\"quantity\":\"1\",\"calculationUsage[]\":\"-7\",\"externalId\":\"\",\"multipackCatentryId\":\"{atc_multipackId},\"}",
			"Body={\"catalogId\":\"{catalogId}\",\"storeId\":\"{storeId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{atc_catentryId}\",\"quantity\":\"1\",\"calculationUsage[]\":\"-7\",\"externalId\":\"\",\"multipackCatentryId\":null,\"skipLoyaltyCall\":false}",
			LAST);

		if ( strlen(lr_eval_string("{orderId}")) <= 0 ) {
			atc_Stat = 1; //0-Pass 1-Fail
			if ( isLoggedIn == 1 ) 
				lr_error_message( lr_eval_string("T05_Add To Cart_addProductToCart - Failed with HTTP {httpReturnCode}, catentryId: {atc_catentryId}, API Error Code: {errorCode}, Error Message: {errorMessage}, OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
			else
				lr_error_message( lr_eval_string("T05_Add To Cart_addProductToCart - Failed with HTTP {httpReturnCode}, catentryId: {atc_catentryId}, API Error Code: {errorCode}, Error Message: {errorMessage}, OrderId:{orderId}, Email: Guest, timestamp: {timestamp}, hostname: {hostName}") ) ;

			lr_end_sub_transaction ("T05_Add To Cart_addProductToCart", LR_FAIL) ;
			lr_end_transaction ( "T05_Add To Cart" , LR_FAIL ) ;
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_addProductToCart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
			return LR_FAIL;
		} // end if

		lr_end_sub_transaction ("T05_Add To Cart_addProductToCart", LR_AUTO) ;

		lr_save_string("T05_Add To Cart", "mainTransaction");
//		getAllCoupons();03112019
		loyaltyPoints();

		web_reg_save_param_json("ParamName=userPoints", "QueryString=$..userPoints", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=itemPoints", "QueryString=$..orderItems..itemPoints", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=pointsToNextReward", "QueryString=$..pointsToNextReward", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	
		web_reg_save_param_json("ParamName=itemPartNumbers", "QueryString=$..orderItems..itemPartNumber", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json("ParamName=variantNo", "QueryString=$..orderItems..variantNo", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json("ParamName=productidWL", "QueryString=$..orderItems..productid", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json("ParamName=orderItemType", "QueryString=$..orderItems..orderItemType", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	
		web_reg_save_param_json("ParamName=parentOrderId", "QueryString=$..parentOrderId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=orderItemId", "QueryString=$..orderItems..orderItemId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");

		lr_start_sub_transaction("T05_Add To Cart_getOrderDetails" , "T05_Add To Cart");
		web_add_header("content-type", "application/json");
		web_add_header("Accept", "application/json");
		if (strcmp(lr_eval_string("{navigateXHR}"), "false") == 0) {
			web_reg_save_param_regexp ("ParamName=WC_PERSISTENT",	"RegExp=set-cookie: WC_PERSISTENT=(.*?);",  "NotFound=warning", SEARCH_FILTERS, "Scope=Headers", "RequestUrl=*", LAST );
			web_reg_save_param_regexp ("ParamName=WC_AUTHENTICATION",	"RegExp=set-cookie: WC_AUTHENTICATION_(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
			web_reg_save_param_regexp ("ParamName=WC_cartItemsCount",	"RegExp=set-cookie: cartItemsCount=(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
			web_reg_save_param_regexp ("ParamName=WC_USERACTIVITY",	"RegExp=set-cookie: WC_USERACTIVITY_(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
		}
				
		if (BRIERLEY_CUSTOM_TEST == 1 && BRIERLEY_FLAG_CHECK == 1)
			web_add_header("recalculate", "true" );
		else
			web_add_header("recalculate", "false" );

		web_add_header("pageName", "fullOrderInfo");
		web_add_header("calc", "false");
		addHeader();
		//addSFCCCartHeader();
		web_custom_request("getOrderDetails",
			"URL=https://{api_host}/v2/checkout/getOrderDetails", 
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
			LAST);
		lr_end_sub_transaction ("T05_Add To Cart_getOrderDetails", LR_AUTO);
			
//		if (BRIERLEY_FLAG_CHECK == 1 && isLoggedIn == 1) {
		if (BRIERLEY_FLAG_CHECK == 1 ) {
			if ( atoi(lr_eval_string("{userPoints}")) <= 0 || atoi(lr_eval_string("{itemPoints}")) <= 0 || atoi(lr_eval_string("{pointsToNextReward}")) <= 0 ) 
			{
				lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"));
				lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"), LR_FAIL);
			} else {
				lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"));
				lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"), LR_PASS);
				if (atoi(lr_eval_string("{userPoints}")) > atoi(lr_eval_string("{previousATC_userpoints}")) ) {
					lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints_XXX"));
					lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints_XXX"), LR_PASS);
				} else {
					lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints_XXX"));
					lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints_XXX"), LR_FAIL);
				}
				lr_save_string(lr_eval_string("{userPoints}"), "previousATC_userpoints");
			}
		}

		if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {
			lr_start_transaction("T50_cookie is 0 alert on getOrderDetails_1095");
			lr_end_transaction ("T50_cookie is 0 alert on getOrderDetails_1095", LR_FAIL ) ;
		}else{
			lr_start_transaction("T50_cookie is 0 alert on getOrderDetails_1095");
			lr_end_transaction ("T50_cookie is 0 alert on getOrderDetails_1095", LR_PASS ) ;
		}
			CheckAndCallnavigateXHR();
		lr_end_transaction ( "T05_Add To Cart" , LR_AUTO ) ;

	}
	return 0;
} // end addToCart

int addToCart_DF()
{
	int k = 0, newTime, RANDOM_PERCENT = 0, randNumber;
	atc_Stat = 0; //0-Pass 1-Fail

	if (CACHE_PRIME_MODE == 1)
		return 0;

	//lr_continue_on_error(1);
	lr_think_time ( FORM_TT ) ;

/*	if ( lr_paramarr_len ( "atc_catentryIds" ) > 0 ) { // at least 1 product with positive quantity check

		for (k = 1; k<= atoi(lr_eval_string("{atc_catentryIds_count}")); k++ )
		{
			if (atoi( lr_paramarr_idx("atc_quantity", k) ) != 0 )
			{
				lr_save_string(lr_paramarr_idx("atc_catentryIds", k), "atc_catentryId" ); break;
			}
		}
*/
//		index = rand ( ) % lr_paramarr_len( "atc_catentryIds" ) + 1 ;
//		lr_save_string ( lr_paramarr_idx( "atc_catentryIds" , index ) , "atc_catentryId" ) ;
		
		lr_save_string("", "multiProduct" );
		randNumber = atoi(lr_eval_string("{RANDOM_PERCENT}"));
		if (randNumber > (ATC_MULTI_PRODUCT + ATC_TRIPACK_PRODUCT + ATC_QUADPACK_PRODUCT) ) {
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId" );
			lr_save_string("", "multiProduct" );
/*		} else if (randNumber <= ATC_MULTI_PRODUCT) {
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId1" );
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId2" );
			lr_save_string(lr_eval_string("{atc_catentryId1}\",\"{atc_catentryId2}"), "atc_catentryId" );
			lr_save_string("_Multiproduct", "multiProduct" );
*/		} else if (randNumber <= ATC_MULTI_PRODUCT + ATC_TRIPACK_PRODUCT ) {
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId1" );
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId2" );
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId3" );
			lr_save_string(lr_eval_string("[\"{atc_catentryId1}\",\"{atc_catentryId2}\",\"{atc_catentryId3}\"]"), "atc_catentryId" );
			lr_save_string("_Tripack", "multiProduct" );
		} else if (randNumber <= ATC_MULTI_PRODUCT + ATC_TRIPACK_PRODUCT + ATC_QUADPACK_PRODUCT ) {
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId1" );
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId2" );
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId3" );
			lr_save_string(lr_eval_string("{randCatEntryId}"), "atc_catentryId4" );
			lr_save_string(lr_eval_string("[\"{atc_catentryId1}\",\"{atc_catentryId2}\",\"{atc_catentryId3}\",\"{atc_catentryId4}\"]"), "atc_catentryId" );
			lr_save_string("_Quadpack", "multiProduct" );
		}
		lr_save_string(lr_eval_string("{multipackId}"), "atc_multipackId" );
//		lr_save_string(lr_eval_string("{randCatEndtryId_pdp}"), "atc_catentryId" );
//		lr_save_string(lr_eval_string("{randCatEntryId_BOPIS}"), "atc_catentryId" );

		lr_save_string ( "T05_Add To Cart" , "mainTransaction") ;
//		getBOPISInvetoryDetails();

		lr_start_transaction ( "T05_Add To Cart" ) ;

		web_reg_save_param_json("ParamName=orderId", "QueryString=$.orderId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=orderItemId", "QueryString=$.orderItemId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_find("Text=the products you wish to purchase are not available", "SaveCount=atcErrorFound");
		web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
		web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n
		web_reg_save_param("cartCount", "LB=set-cookie: cartItemsCount=", "RB=; Path=/; Domain", "NotFound=Warning", LAST); //set-cookie: cartItemsCount=1; Path=/; Domain
		
		if (strcmp(lr_eval_string("{navigateXHR}"), "false") == 0) {
			web_reg_save_param_regexp ("ParamName=WC_PERSISTENT",	"RegExp=set-cookie: WC_PERSISTENT=(.*?);",  "NotFound=warning", SEARCH_FILTERS, "Scope=Headers", "RequestUrl=*", LAST );
			web_reg_save_param_regexp ("ParamName=WC_AUTHENTICATION",	"RegExp=set-cookie: WC_AUTHENTICATION_(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
			web_reg_save_param_regexp ("ParamName=WC_cartItemsCount",	"RegExp=set-cookie: cartItemsCount=(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
			web_reg_save_param_regexp ("ParamName=WC_USERACTIVITY",	"RegExp=set-cookie: WC_USERACTIVITY_(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
		}

		
		prepLoyaltyDataCapture();
		lr_start_sub_transaction(lr_eval_string("T05_Add To Cart_addProductToCart{multiProduct}") , "T05_Add To Cart");
		registerErrorCodeCheck();
		addHeader();
		//addSFCCCartHeader();
		web_add_header("Accept", "application/json");
		web_custom_request("addProductToCart",
			"URL=https://{api_host}/v2/cart/addProductToCart", //ATC DF
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTML",
			"EncType=application/json",
//060121	"Body={\"catalogId\":\"{catalogId}\",\"storeId\":\"{storeId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{atc_catentryId}\",\"quantity\":\"1\",\"calculationUsage[]\":\"-7\",\"externalId\":\"\",\"multipackCatentryId\":null,\"skipLoyaltyCall\":false}",
	        "Body={    \"catalogId\":\"{catalogId}\",    \"storeId\":\"{storeId}\",    \"langId\":\"-1\",    \"orderId\":\".\",    \"field2\":\"0\",    \"requesttype\":\"ajax\",    \"catEntryId\":{atc_catentryId},    \"quantity\":\"1\",    \"calculationUsage[]\":\"-7\",    \"externalId\":\"\",    \"multipackCatentryId\":null,    \"skipLoyaltyCall\":false }",
//1	        "Body={    \"catalogId\":\"{catalogId}\",    \"storeId\":\"{storeId}\",    \"langId\":\"-1\",    \"orderId\":\".\",    \"field2\":\"0\",    \"requesttype\":\"ajax\",    \"catEntryId\":[       \"{atc_catentryId}\"    ],    \"quantity\":\"1\",    \"calculationUsage[]\":\"-7\",    \"externalId\":\"\",    \"multipackCatentryId\":\"1575963:841923_2\",    \"skipLoyaltyCall\":false }",
//2         "Body={    \"catalogId\":\"{catalogId}\",    \"storeId\":\"{storeId}\",    \"langId\":\"-1\",    \"orderId\":\".\",    \"field2\":\"0\",    \"requesttype\":\"ajax\",    \"catEntryId\":[       \"{atc_catentryId1}\",       \"{atc_catentryId2}\"    ],    \"quantity\":\"1\",    \"calculationUsage[]\":\"-7\",    \"externalId\":\"\",    \"multipackCatentryId\":null,    \"skipLoyaltyCall\":false }",
            LAST);

		if ( strlen(lr_eval_string("{orderId}")) <= 0 ) {
			atc_Stat = 1; //0-Pass 1-Fail
			if ( isLoggedIn == 1 ) 
				lr_error_message( lr_eval_string("T05_Add To Cart_addProductToCart{multiProduct} - Failed with HTTP {httpReturnCode}, catentryId: {atc_catentryId}, API Error Code: {errorCode}, Error Message: {errorMessage}, OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
			else
				lr_error_message( lr_eval_string("T05_Add To Cart_addProductToCart{multiProduct} - Failed with HTTP {httpReturnCode}, catentryId: {atc_catentryId}, API Error Code: {errorCode}, Error Message: {errorMessage}, OrderId:{orderId}, Email: Guest, timestamp: {timestamp}, hostname: {hostName}") ) ;

			lr_end_sub_transaction (lr_eval_string("T05_Add To Cart_addProductToCart{multiProduct}"), LR_FAIL) ;
			lr_end_transaction ( "T05_Add To Cart" , LR_FAIL ) ;
			if (WRITE_TO_SUMO==1) {
		 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_addProductToCart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
				sendErrorToSumo(); 
		 	}
			return LR_FAIL;
		} // end if

		lr_end_sub_transaction (lr_eval_string("T05_Add To Cart_addProductToCart{multiProduct}"), LR_AUTO) ;

		lr_save_string("T05_Add To Cart", "mainTransaction");
		
		loyaltyPoints();
		
		if ( BRIERLEY_FLAG_CHECK == 0) {
/*	060221 - per Pavan, Match prod UI, this doesnt exist in prod, so this is now disabled

			web_reg_save_param_json("ParamName=userPoints", "QueryString=$..userPoints", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param_json("ParamName=itemPoints", "QueryString=$..orderItems..itemPoints", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param_json("ParamName=pointsToNextReward", "QueryString=$..pointsToNextReward", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		
			web_reg_save_param_json("ParamName=itemPartNumbers", "QueryString=$..orderItems..itemPartNumber", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json("ParamName=variantNo", "QueryString=$..orderItems..variantNo", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json("ParamName=productidWL", "QueryString=$..orderItems..productid", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json("ParamName=orderItemType", "QueryString=$..orderItems..orderItemType", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		
			web_reg_save_param_json("ParamName=parentOrderId", "QueryString=$..parentOrderId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param_json("ParamName=orderItemId", "QueryString=$..orderItems..orderItemId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			
			web_reg_save_param_json("ParamName=itemQuantity", "QueryString=$..orderItems..qty", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param_json("ParamName=itemCatentryId", "QueryString=$..orderItems..itemCatentryId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	
			lr_start_sub_transaction("T05_Add To Cart_getOrderDetails" , "T05_Add To Cart");
	//			web_add_header("content-type", "application/json");
				web_add_header("Accept", "application/json");
		
			if (strcmp(lr_eval_string("{navigateXHR}"), "false") == 0) {
				web_reg_save_param_regexp ("ParamName=WC_PERSISTENT",	"RegExp=set-cookie: WC_PERSISTENT=(.*?);",  "NotFound=warning", SEARCH_FILTERS, "Scope=Headers", "RequestUrl=*", LAST );
				web_reg_save_param_regexp ("ParamName=WC_AUTHENTICATION",	"RegExp=set-cookie: WC_AUTHENTICATION_(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
				web_reg_save_param_regexp ("ParamName=WC_cartItemsCount",	"RegExp=set-cookie: cartItemsCount=(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
				web_reg_save_param_regexp ("ParamName=WC_USERACTIVITY",	"RegExp=set-cookie: WC_USERACTIVITY_(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
			}
					
			if (BRIERLEY_CUSTOM_TEST == 1 && BRIERLEY_FLAG_CHECK == 1)
				web_add_header("recalculate", "true" );
			else
				web_add_header("recalculate", "false" );
	
				web_add_header("pageName", "fullOrderInfo");
				web_add_header("calc", "false");
				addHeader();
				web_custom_request("getOrderDetails",
					"URL=https://{api_host}/v2/checkout/getOrderDetails", 
					"Method=GET",
					"Resource=0",
					"RecContentType=application/json",
					"Mode=HTTP",
					"EncType=application/json",
					LAST);
				lr_end_sub_transaction ("T05_Add To Cart_getOrderDetails", LR_AUTO);
				
	//			if (BRIERLEY_FLAG_CHECK == 1 && isLoggedIn == 1) {
				if (BRIERLEY_FLAG_CHECK == 1 ) {
					if ( atoi(lr_eval_string("{userPoints}")) <= 0 || atoi(lr_eval_string("{itemPoints}")) <= 0 || atoi(lr_eval_string("{pointsToNextReward}")) <= 0 ) 
					{
						lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"));
						lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"), LR_FAIL);
					} else {
						lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"));
						lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints"), LR_PASS);
						if (atoi(lr_eval_string("{userPoints}")) > atoi(lr_eval_string("{previousATC_userpoints}")) ) {
							lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints_XXX"));
							lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints_XXX"), LR_PASS);
						} else {
							lr_start_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints_XXX"));
							lr_end_transaction(lr_eval_string("{mainTransaction}_BrierlyAddToCartPoints_XXX"), LR_FAIL);
						}
						lr_save_string(lr_eval_string("{userPoints}"), "previousATC_userpoints");
					}
				}
			if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {
				lr_start_transaction("T50_cookie is 0 alert on getOrderDetails_1226");
				lr_end_transaction ("T50_cookie is 0 alert on getOrderDetails_1226", LR_FAIL ) ;
				}else{
					lr_start_transaction("T50_cookie is 0 alert on getOrderDetails_1226");
				lr_end_transaction ("T50_cookie is 0 alert on getOrderDetails_1226", LR_PASS ) ;
				}
*/
	}

	CheckAndCallnavigateXHR();

	lr_end_transaction ( "T05_Add To Cart" , LR_AUTO ) ;

	return 0;
} // end addToCart


void addToCartMixed()
{
	int k = 0, newTime, RANDOM_PERCENT = 0;
	atc_Stat = 0; //0-Pass 1-Fail

//	if ( lr_paramarr_len ( "atc_catentryIds" ) > 0 ) { // at least 1 product with positive quantity check
//		lr_param_sprintf ( "atc_catentryId" , "%s" , lr_paramarr_random ( "atc_catentryIds" ) ) ; //042318
//		web_reg_save_param ( "orderId" , "LB=\"orderId\": [\"" , "RB=\"]" , "NotFound=Warning",  LAST ) ;
//		web_reg_save_param_regexp ("ParamName=orderId",	"RegExp=\"orderId\":[ ]*\"(.*?)\"",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST ); //romano disabled this 04232018
//		web_reg_save_param ( "orderItemId" , "LB=\"orderItemId\": [\"" , "RB=\"]" , "NotFound=Warning", LAST ) ;//romano disabled this 04232018
		web_reg_find("Text=the products you wish to purchase are not available", "SaveCount=atcErrorFound");

		lr_think_time ( FORM_TT ) ;

	    if ( atoi( lr_eval_string("{RANDOM_PERCENT}")) <= BOPIS_CART_RATIO )
	    	lr_save_string("bopis", "pickupType");
	    else
	    	lr_save_string("boss", "pickupType");
	    	
		pickupInStore();
				if ( addBopisToCartDF() == LR_PASS)
				{
					atc_Stat = 0; //0-Pass 1-Fail
					lr_save_string("TRUE", "addBopisToCart");
				} else {
					atc_Stat = 1; //0-Pass 1-Fail //03112020
//					lr_save_string("TRUE", "addEcommToCart");//03112020
//					productDisplay();//03112020
//					addToCart_DF();//03112020
				}
//	}

}
void addToCartMixed_OLD_asThisWillNotWorkIf_ATC_FLAG_is_0()
{
	int k = 0, newTime, RANDOM_PERCENT = 0;
	atc_Stat = 0; //0-Pass 1-Fail

//	if ( lr_paramarr_len ( "atc_catentryIds" ) > 0 ) { // at least 1 product with positive quantity check
//		lr_param_sprintf ( "atc_catentryId" , "%s" , lr_paramarr_random ( "atc_catentryIds" ) ) ; //042318
//		web_reg_save_param ( "orderId" , "LB=\"orderId\": [\"" , "RB=\"]" , "NotFound=Warning",  LAST ) ;
//		web_reg_save_param_regexp ("ParamName=orderId",	"RegExp=\"orderId\":[ ]*\"(.*?)\"",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST ); //romano disabled this 04232018
//		web_reg_save_param ( "orderItemId" , "LB=\"orderItemId\": [\"" , "RB=\"]" , "NotFound=Warning", LAST ) ;//romano disabled this 04232018
		web_reg_find("Text=the products you wish to purchase are not available", "SaveCount=atcErrorFound");

		lr_think_time ( FORM_TT ) ;

	    if ( atoi( lr_eval_string("{RANDOM_PERCENT}")) <= BOPIS_CART_RATIO )
	    	lr_save_string("bopis", "pickupType");
	    else
	    	lr_save_string("boss", "pickupType");
	    	
		if (pickupInStore() == LR_PASS)
		{
			if (ATC_FLAG == 1) {
				if ( addBopisToCartUI() == LR_PASS)
				{
					atc_Stat = 0; //0-Pass 1-Fail
					lr_save_string("TRUE", "addBopisToCart");
				} else {
//					atc_Stat = 1; //0-Pass 1-Fail
					lr_save_string("TRUE", "addEcommToCart");
					productDisplay();
					addToCart_UI();
				}
			} else {
				if ( addBopisToCartDF() == LR_PASS)
				{
					atc_Stat = 0; //0-Pass 1-Fail
					lr_save_string("TRUE", "addBopisToCart");
				} else {
					atc_Stat = 1; //0-Pass 1-Fail //03112020
//					lr_save_string("TRUE", "addEcommToCart");//03112020
//					productDisplay();//03112020
//					addToCart_DF();//03112020
				}
			}
		} else {
			atc_Stat = 1; //0-Pass 1-Fail //03112020
			lr_save_string("TRUE", "addEcommToCart");
			productDisplay();
			if (ATC_FLAG == 1) 
				addToCart_UI();
			else
				addToCart_DF();
		}
//	}

}

int updateQuantity()
{
	lr_think_time ( FORM_TT ) ;
	
	lr_save_string("T06_Update Quantity", "mainTransaction");

	if (atoi( lr_eval_string("{itemCatentryId_count}")) > 0 ) // && atoi( lr_eval_string("{orderItemIds_count}")) > 0 )
	{
//		index = rand ( ) % lr_paramarr_len( "itemCatentryId" ) + 1 ;    //04132018
		index = atoi( lr_eval_string("{itemCatentryId_count}"))	;		//04132018

		lr_save_string ( lr_paramarr_idx( "itemCatentryId" , index ) , "itemCatentryId" ) ;
		lr_save_string ( lr_paramarr_idx( "productId" , index ) , "productId" ) ;
		lr_save_string ( lr_paramarr_idx( "orderItemIds" , index ) , "orderItemId" ) ;

		lr_start_transaction ( "T06_Update Quantity" ) ;
		lr_start_sub_transaction("T06_Update Quantity_updateOrderItem", "T06_Update Quantity");

//		lr_save_string(lr_paramarr_random("atc_catentryIds"), "itemCatentryId"); //disabled 08092018 - romano
		web_add_header("Accept", "application/json");
		web_add_header("Content-Type", "application/json");
		web_add_header("storeId", lr_eval_string("{storeId}") );
		web_add_header("catalogId", lr_eval_string("{catalogId}") );
		web_add_header("langId", "-1");
		registerErrorCodeCheck();
		web_custom_request("Quantity_updateOrderItem",
			"URL=https://{api_host}/v2/cart/updateOrderItem", 
			"Method=PUT",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTML",
			//"Body={\"orderItem\":[{\"orderItemId\":\"{orderItemId}\",\"xitem_catEntryId\":\"{itemCatentryId}\",\"quantity\":\"2\"}]}",
			"Body={\"orderItem\":[{\"orderItemId\":\"{orderItemId}\",\"xitem_catEntryId\":\"{itemCatentryId}\",\"quantity\":\"2\",\"itemPartNumber\":\"{itemPartNumber}\",\"variantNo\":\"{VariantNo}\"}]}",
			LAST);
		lr_end_sub_transaction("T06_Update Quantity_updateOrderItem", LR_AUTO);

		web_reg_save_param_json("ParamName=offersSku", "QueryString=$..orderItems..variantNo", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=offersPrice", "QueryString=$..orderItems..itemPrice", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=offersQty", "QueryString=$..orderItems..qty", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");

		web_reg_save_param("piAmountonUpdateQuantity", "LB=\"piAmount\": \"", "RB=\"", "NotFound=Warning", LAST); //"piAmount": "28.85",\n
		web_add_header("pageName", "fullOrderInfo");
		web_add_header("source", "login");
		if (BRIERLEY_CUSTOM_TEST == 1 && BRIERLEY_FLAG_CHECK == 1)
			web_add_header("recalculate", "true" );
		else
			web_add_header("recalculate", "false" );
		web_add_header("calc", "true");
		getCartDetails();
		
		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheckCount}"))>0 && atoi(lr_eval_string("{cartCount}"))>0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
		{
			lr_end_sub_transaction ("T06_Update Quantity_cart", LR_AUTO) ;
		} else {
			lr_fail_trans_with_error( lr_eval_string("T06_Update Quantity Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_sub_transaction ("T06_Update Quantity_cart", LR_FAIL) ;
			lr_end_transaction ( lr_eval_string ( "T06_Update Quantity" ) , LR_FAIL ) ;
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
			return LR_FAIL;
		}
		
		if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {
			lr_start_transaction("T50_cookie is 0 alert on cart_1353");
			lr_end_transaction ("T50_cookie is 0 alert on cart_1353", LR_FAIL ) ;
		}else{
			lr_start_transaction("T50_cookie is 0 alert on cart_1353");
			lr_end_transaction ("T50_cookie is 0 alert on cart_1353", LR_PASS ) ;
		}
		
		setOrderType();
//		if ( strcmp(lr_eval_string("{addBopisToCart}"), "TRUE") == 0 && strcmp(lr_eval_string("{addBossToCart}"), "FALSE") == 0 ) {
//			getBOPISInvetoryDetails();
//		}
		
		lr_end_transaction ( "T06_Update Quantity" , LR_PASS) ;
		return LR_PASS;
	}
	return 0;
} // end updateQuantity()

int updateItemShipping()
{
	lr_think_time ( FORM_TT ) ;
	if (pickupInStoreFromBag() == LR_FAIL)
	{
		if (atoi(lr_eval_string("{itemCatentryId_count}")) <=0 ) {
			lr_start_transaction(lr_eval_string("T50_pickupInStoreFromBag_FAILED_itemCatentryId_count={itemCatentryId_count}"));
			lr_end_transaction(lr_eval_string("T50_pickupInStoreFromBag_FAILED_itemCatentryId_count={itemCatentryId_count}"), LR_FAIL);
		}
		if (atoi(lr_eval_string("{itemCatentryId_count}")) <=0 ) {
			lr_start_transaction(lr_eval_string("T50_pickupInStoreFromBag_FAILED_itemPartNumbers_count={itemPartNumbers_count}"));
			lr_end_transaction(lr_eval_string("T50_pickupInStoreFromBag_FAILED_itemPartNumbers_count={itemPartNumbers_count}"), LR_FAIL);
		}

		return LR_FAIL;
	}
	
	if (atoi( lr_eval_string("{itemCatentryId_count}")) > 0 ) // && atoi( lr_eval_string("{orderItemIds_count}")) > 0 )
	{
		lr_save_string("T06_UpdateItemShipping", "mainTransaction");
//		index = rand ( ) % lr_paramarr_len( "itemCatentryId" ) + 1 ;    //04132018
		index = atoi( lr_eval_string("{itemCatentryId_count}"))	;		//04132018

		lr_save_string ( lr_paramarr_idx( "itemCatentryId" , index ) , "itemCatentryId" ) ;
		lr_save_string ( lr_paramarr_idx( "productId" , index ) , "productId" ) ;
		lr_save_string ( lr_paramarr_idx( "orderItemIds" , index ) , "orderItemId" ) ;
		lr_save_string ( lr_paramarr_idx( "productPartNumber" , index ) , "productPartNumber" ) ;
		lr_save_string ( lr_paramarr_idx( "variantNo" , index ) , "variantNo" ) ;
		lr_save_string ( lr_paramarr_idx( "storeLocId" , 1 ) , "storeLocId" ) ;

		lr_start_transaction ( "T06_UpdateItemShipping" ) ;
		
		//   //   userGroup(); //RY added this 08052019
		
		lr_start_sub_transaction("T06_UpdateItemShipping_updateOrderItem", "T06_UpdateItemShipping");
	
//		lr_save_string(lr_paramarr_random("atc_catentryIds"), "itemCatentryId"); //disabled 08092018 - romano
		web_add_header("Accept", "application/json");
		web_add_header("Content-Type", "application/json");
		web_add_header("storeId", lr_eval_string("{storeId}") );
		web_add_header("catalogId", lr_eval_string("{catalogId}") );
		web_add_header("langId", "-1");
		registerErrorCodeCheck();
		web_custom_request("updateOrderItem",
			"URL=https://{api_host}/v2/cart/updateOrderItem", 
			"Method=PUT",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTML",
//			"Body={\"orderId\":\"{orderItemId}\",\"orderItem\":[{\"orderItemId\":\"{orderItemId}\",\"xitem_catEntryId\":\"{itemCatentryId}\",\"quantity\":\"1\",\"variantNo\":\"{VariantNo}\",\"itemPartNumber\":\"{itemPartNumber}\"}],\"x_storeLocId\":\"111287\",\"x_calculationUsage\":\"-1,-3,-5,-6,-7\",\"x_isUpdateDescription\":\"true\",\"x_orderitemtype\":\"ECOM\",\"x_updatedItemType\":\"BOPIS\"}",
			"Body={\"orderId\":\"{parentOrderId}\",\"orderItem\":[{\"orderItemId\":\"{orderItemId}\",\"xitem_catEntryId\":\"{itemCatentryId}\",\"quantity\":\"1\",\"variantNo\":\"{variantNo}\",\"itemPartNumber\":\"{itemPartNumber}\"}],\"x_storeLocId\":\"{storeLocId}\",\"x_calculationUsage\":\"-1,-3,-5,-6,-7\",\"x_isUpdateDescription\":\"true\",\"x_orderitemtype\":\"ECOM\",\"x_updatedItemType\":\"BOSS\"}",
			LAST);

		if (strcmp(lr_eval_string("{errorCode}"),"")==0 && strcmp(lr_eval_string("{errorMessage}"),"")==0  ){
			lr_end_sub_transaction("T06_UpdateItemShipping_updateOrderItem", LR_AUTO);
			lr_save_string("TRUE", "addBossToCart");
			lr_save_string("boss", "pickupType");
		} else {
			lr_end_sub_transaction("T06_UpdateItemShipping_updateOrderItem", LR_FAIL);
			lr_end_transaction ( lr_eval_string ( "T06_UpdateItemShipping" ) , LR_FAIL ) ;
			return LR_FAIL;
		}
		
		getFavouriteStoreLocation();
// 		getBOPISInvetoryDetails(); 03112019 - not present anymore in uatlive4 - bespin
 		
		web_reg_save_param_json("ParamName=offersSku", "QueryString=$..orderItems..variantNo", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=offersPrice", "QueryString=$..orderItems..itemPrice", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=offersQty", "QueryString=$..orderItems..qty", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");

		web_reg_save_param("piAmountonUpdateQuantity", "LB=\"piAmount\": \"", "RB=\"", "NotFound=Warning", LAST); //"piAmount": "28.85",\n
		web_add_header("pageName", "fullOrderInfo");
		web_add_header("source", "login");
		if (BRIERLEY_CUSTOM_TEST == 1 && BRIERLEY_FLAG_CHECK == 1)
			web_add_header("recalculate", "true" );
		else
			web_add_header("recalculate", "false" );
		web_add_header("calc", "true");
		getCartDetails();
		
		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheckCount}"))>0 && atoi(lr_eval_string("{cartCount}"))>0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
		{
			lr_end_sub_transaction ("T06_UpdateItemShipping_cart", LR_AUTO) ;
			lr_end_transaction ( "T06_UpdateItemShipping" , LR_PASS) ;
			return LR_PASS;
		} else {
			lr_fail_trans_with_error( lr_eval_string("T06_UpdateItemShipping Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_sub_transaction ("T06_UpdateItemShipping_cart", LR_FAIL) ;
			lr_end_transaction ( lr_eval_string ( "T06_UpdateItemShipping" ) , LR_FAIL ) ;
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
			return LR_FAIL;
		}
		
		if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {
			lr_start_transaction("T50_cookie is 0 alert on cart_1446");
			lr_end_transaction ("T50_cookie is 0 alert on cart_1446", LR_FAIL ) ;
		}else{
			lr_start_transaction("T50_cookie is 0 alert on cart_1446");
			lr_end_transaction ("T50_cookie is 0 alert on cart_1446", LR_PASS ) ;
		}
	}
	return 0;
} // end updateItemShipping()

int getAddressFromBook()
{
	lr_start_sub_transaction (lr_eval_string("{mainTransaction}_getAddressFromBook"), lr_eval_string("{mainTransaction}") ) ;
	addHeader();
	web_add_header("client_source", lr_eval_string("{client_source}"));
	web_add_header("Accept", "application/json");
	web_custom_request("getAddressFromBook",
		"URL=https://{api_host}/v2/account/getAddressFromBook", 
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	lr_end_sub_transaction (lr_eval_string("{mainTransaction}_getAddressFromBook"), LR_AUTO) ;
}

int viewCart()
{
	char *strLocIdString ;	
	int firstPass = 0;

	lr_think_time ( LINK_TT ) ;
	
	lr_save_string("T06_View_Cart","mainTransaction");
	
	lr_start_transaction ( "T06_View_Cart" ) ;

	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO ) {
		
		lr_start_sub_transaction ( "T06_View_Cart_myBag", "T06_View_Cart" );
		web_custom_request("bag",
			"URL=https://{host}/{country}/bag",
			"Method=GET",
			"Resource=0",
			"Mode=HTTP",
			LAST);
		lr_end_sub_transaction("T06_View_Cart_myBag", LR_AUTO);
	}
//	userGroup();
	
	web_reg_save_param("productId", "LB=\"productId\": \"", "RB=\",", "NotFound=Warning", "Ord=All", LAST); //  "productId": "426632",\n
	web_reg_save_param("productPartNumber", "LB=\"productPartNumber\": \"", "RB=\",", "NotFound=Warning", "Ord=All", LAST); //  "productId": "426632",\n
	web_reg_save_param_json("ParamName=cartCount", "QueryString=$..cartCount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
//091420	web_reg_save_param("orderItemIds", "LB=\"orderItemId\": ", "RB=,", "NotFound=Warning", "Ord=All", LAST); //\t\t\t"orderItemId": 8007970018,\n// 03/01/17
	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\",", "NotFound=Warning", LAST); //"piAmount": "7.95",\n  "piAmount": "22.30",\n
	web_reg_save_param("couponExist", "LB=\"code\": \"", "RB=\"", "NotFound=Warning", LAST); //"code": "FORTYOFF",
	web_reg_save_param_regexp ("ParamName=orderId",	"RegExp=\"orderId\":[ ]*\"(.*?)\"",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST );
	web_reg_save_param ( "mixCart" ,    "LB=\"mixCart\": \"" , "RB=\"" , "NotFound=Warning", LAST ) ; //"mixCart": "MIX" 
	web_reg_save_param( "stLocId", "LB=\"stLocId\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); 
	web_reg_find("TEXT/IC=paymentsList", "SaveCount=paymentListFound", LAST);
//	web_reg_save_param("itemPartNumbers", "LB=\"itemPartNumber\": \"", "RB=\",","Ordinal=ALL","NotFound=Warning", LAST); 
	web_reg_save_param( "itemName", "LB=\"variantNo\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); 
		lr_save_string("0", "cartCount");
	web_reg_save_param_json("ParamName=itemName", "QueryString=$..orderItems..variantNo", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=variantNo", "QueryString=$..orderItems..variantNo", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=productidWL", "QueryString=$..orderItems..productid", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=orderItemType", "QueryString=$..orderItems..orderItemType", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=orderItemIds", "QueryString=$.orderDetails..orderItemId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=orderItemIdsQty", "QueryString=$.orderDetails..qty", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

	web_add_header("pageName", "fullOrderInfo" );
	web_add_header("source", "" );
				
		if (BRIERLEY_CUSTOM_TEST == 1 && BRIERLEY_FLAG_CHECK == 1)
			web_add_header("recalculate", "true" );
		else
			web_add_header("recalculate", "false" );

	web_add_header("calc", "true" ); // 03/29
	web_add_header("coupons", "true" );	
	getCartDetails();

//	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheckCount}"))>0 && atoi(lr_eval_string("{cartCount}"))>0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheckCount}"))>0) // && atoi(lr_eval_string("{cartCount}"))>0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_cart"), LR_AUTO );
	else
	{
		lr_error_message( lr_eval_string("{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, cartCount: {cartCount},piAmount: {piAmount}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_cart"), LR_FAIL );
		lr_end_transaction ("T06_View_Cart", LR_FAIL ) ;
		if (WRITE_TO_SUMO==1) {
	 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
	 	}
		return LR_FAIL;
	}
	
	if (strcmp(lr_eval_string("{loginFirst}"), "true") == 0) {
		
//		if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {
		if ( atoi(lr_eval_string("{parentOrderId}"))<=0) { // || atoi(lr_eval_string("{itemsCount}"))<=0) { 09102019 rhy disabled the itemsCount as most loginFirst users has zero items in cart
			lr_start_transaction("T50_cookie is 0 alert on cart_1517");
			lr_end_transaction ("T50_cookie is 0 alert on cart_1517", LR_FAIL ) ;
		}else{
			lr_start_transaction("T50_cookie is 0 alert on cart_1517");
			lr_end_transaction ("T50_cookie is 0 alert on cart_1517", LR_PASS ) ;
		}
	}
	
   	//if ( strcmp(lr_eval_string("{userCouponsApplied}"), "false") == 0 && strcmp(lr_eval_string("{stepName}"), "logon") == 0 && isLoggedIn == 1)
   	if ( strcmp(lr_eval_string("{userCouponsApplied}"), "false") == 0 && atoi( lr_eval_string("{piAmount}")) > 40 && isLoggedIn == 1 && atoi( lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_COUPON_APPLY )
		applyPromoCode();

   	if ( strcmp(lr_eval_string("{userType}"), "VEN") == 0 && RATIO_VENMO == 1)
		getVenmoClientToken();
	
	//bonusDay();	
	
   	if (isLoggedIn == 1) {
		web_add_header("fromPage", "checkout");
	   	getAddressFromBook();
   	}
	//bonusDay();
	
	lr_save_string("viewbag", "stepName");
	web_reg_save_param_regexp ("ParamName=sflCount",	"RegExp=set-cookie: sflItemsCount_US=(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers", "RequestUrl=*", LAST );
	lr_save_string("pageName", "myAccount");
	lr_save_string("login", "source");
	getRegisteredUserDetailsInfo();
	
	if (atoi(lr_eval_string("{sflCount}")) > 0 ) {
		lr_save_string("GET", "apiMethod");
	    saveForLater();
	}
	
 	if (UNBXD_FLAG == 1) {
		lr_start_sub_transaction ( "T06_View_Cart_unbxd.search", "T06_View_Cart" );
		web_url("T06_View_Cart_unbxd.search", 
			"URL=https://{UNBXD_host}/search?variants=true&variants.count=100&version=V2&rows=20&pagetype=boolean&q=gift&promotion=false&fields=alt_img,style_partno,giftcard,TCPProductIndUSStore,TCPWebOnlyFlagUSStore,TCPWebOnlyFlagCanadaStore,TCPFitMessageUSSstore,TCPFit,product_name,TCPColor,top_rated,imagename,productid,uniqueId,favoritedcount,TCPBazaarVoiceReviewCount,categoryPath3_catMap,categoryPath2_catMap,product_short_description,style_long_description,min_list_price,min_offer_price,TCPBazaarVoiceRating,product_long_description,seo_token,variantCount,prodpartno,variants,v_tcpfit,v_qty,v_tcpsize,style_name,v_item_catentry_id,v_listprice,v_offerprice,v_qty,variantId,auxdescription,list_of_attributes,additional_styles,TCPLoyaltyPromotionTextUSStore,TCPLoyaltyPLCCPromotionTextUSStore,v_variant,%20low_offer_price,%20high_offer_price,%20low_list_price,%20high_list_price,long_product_title,TCPOutOfStockFlagUSStore,TCPOutOfStockFlagCanadaStore&filter=giftcard:1&sort=style_sequence%20asc&uid={UID}",
			"Resource=0",
			"RecContentType=application/json", 
			"Mode=HTML", 
			LAST);			
		
		lr_end_sub_transaction ( "T06_View_Cart_unbxd.search", LR_PASS ) ;
	}
	
	if (ESPOT_FLAG == 1)
	{
		lr_save_string("1", "getESpot");
		web_add_header("espotName", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Gift_Shop_espot,secondary_global_nav_Gift_Shop_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot");
		getESpot();
		lr_save_string("2", "getESpot");
		web_add_header("espotname", "coupon_help,bag_empty_banner,bag_ledger_promo_banner");
		getESpot();
		if (isLoggedIn == 1) {
			lr_save_string("3", "getESpot");
			web_add_header("espotname", "GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderPlccMoreInfoMdal,MobileNavHeaderLinks1,HeaderNavEspotLinks,DT_SiteWide_Above_Header_nonStikcy");
			getESpot();
		}
	}
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= GETALLCOUPONS_RATIO) {
		getAllCoupons();
	}
	lr_end_transaction ( "T06_View_Cart" , LR_PASS ) ;
	return 0;
} // end viewCart


void applyPromoCode() 
{
	int couponLoop, j = 0; 
	int maxLoop = atoi(lr_eval_string("{offersCount}"));
	
//	web_reg_save_param_json("ParamName=offersCount", "QueryString=$.coupons.offersCount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
//	web_reg_save_param_json("ParamName=couponCode", "QueryString=$.coupons..couponCode", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

	lr_think_time ( FORM_TT ) ;

	lr_save_string( "false" , "userCouponsApplied" ) ;
	for (couponLoop = 1; couponLoop <= 2; couponLoop++)
//	for (couponLoop = 1; couponLoop <= maxLoop; couponLoop++)
	{	
		lr_save_string("", "promocode");
		
		for (j = 1; j <= atoi(lr_eval_string("{offersCount}")); j++) {
			if (strcmp(lr_paramarr_idx("isApplied", j), "false") == 0)
			{	lr_save_string(lr_paramarr_idx("couponCode", j), "promocode");
				break;
			}
		}
		
		if (strcmp(lr_eval_string( "{promocode}" ), "") != 0) {

			lr_start_sub_transaction ( lr_eval_string("{mainTransaction}_coupons"), lr_eval_string("{mainTransaction}") );
			registerErrorCodeCheck();
			addHeader();
			web_reg_save_param_json("ParamName=s_couponOrderID", "QueryString=$.orderId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param_json("ParamName=s_PromoCode", "QueryString=$.promoCode", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_add_header("accept", "application/json");
			web_custom_request("coupons",
				"URL=https://{api_host}/v2/checkout/coupons",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
				"Body={\"promoCode\":\"{promocode}\"}",
				LAST);
	
			if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 || strcmp(lr_eval_string("{errorMessage}") ,"") != 0 || strlen(lr_eval_string("{s_couponOrderID}"))==0 || strlen(lr_eval_string("{s_PromoCode}"))==0)   //if no error code and message
			{
				lr_fail_trans_with_error( lr_eval_string("{mainTransaction}_coupons Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
				lr_end_sub_transaction (lr_eval_string ( "{mainTransaction}_coupons" ), LR_FAIL)	;
				writeToFileMelvin();
			}
			else {
				lr_save_string( "true" , "userCouponsApplied" ) ;
				lr_save_string( "" , "stepName" ) ;
				lr_end_sub_transaction (lr_eval_string ( "{mainTransaction}_coupons" ), LR_AUTO)	;
			}
		
			web_add_header("pageName", "fullOrderInfo" );
			web_add_header("source", "" );
			web_add_header("recalculate", "true" );
			web_add_header("calc", "true" );
			web_add_header("coupons", "true" );	
			getCartDetails();
		
			if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheckCount}"))>0) // && atoi(lr_eval_string("{cartCount}"))>0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
				lr_end_sub_transaction(lr_eval_string("{mainTransaction}_cart"), LR_AUTO );
			else
			{
				lr_error_message( lr_eval_string("{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, cartCount: {cartCount},piAmount: {piAmount}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
				lr_end_sub_transaction(lr_eval_string("{mainTransaction}_cart"), LR_FAIL );
			}			
		}
//		lr_end_transaction ( lr_eval_string ( "T07_Apply_Coupon" ) , LR_AUTO ) ;
	}
} // end applyPromoCode

int deletePromoCode()
{
	int couponLoop = 0; 
	int maxLoop = atoi( lr_eval_string("{isApplied_count}") );
	
//	web_reg_save_param_json("ParamName=offersCount", "QueryString=$.coupons.offersCount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
//	web_reg_save_param_json("ParamName=couponCode", "QueryString=$.coupons..couponCode", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

	lr_think_time ( FORM_TT ) ;

	lr_save_string( "false" , "userCouponRemoved" ) ;
//	for (couponLoop = 1; couponLoop <= maxLoop; couponLoop++)
	if (atoi(lr_eval_string("{isApplied_count}")) > 1 )
	{
//		lr_start_transaction ( "T07_Delete_Coupon" ) ;
		for (couponLoop = 1; couponLoop <= atoi(lr_eval_string("{isApplied_count}")); couponLoop++) {
			if (strcmp(lr_paramarr_idx("isApplied", couponLoop), "true") == 0)
			{	lr_save_string(lr_paramarr_idx("couponCode", couponLoop), "appliedCoupons");
				break;
			}
		}

		if (strcmp(lr_eval_string( "{appliedCoupons}" ), "") != 0) {
		
			lr_start_sub_transaction ( lr_eval_string("{mainTransaction}_removePromotionCode") , lr_eval_string("{mainTransaction}") );
			web_add_header("promoCode", lr_eval_string("{appliedCoupons}") );
			web_add_header("accept", "application/json");
			addHeader();
			registerErrorCodeCheck();
			web_reg_save_param_json("ParamName=s_DeletePromo", "QueryString=$.orderId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_custom_request("removePromotionCode",
				"URL=https://{api_host}/v2/checkout/removePromotionCode",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=",
				LAST);
						
			if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0 && strlen(lr_eval_string("{s_DeletePromo}"))==0 ) {
				lr_fail_trans_with_error( lr_eval_string("{mainTransaction}_coupons Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
				lr_end_sub_transaction ( lr_eval_string("{mainTransaction}_removePromotionCode") , LR_FAIL);
			}else{
				lr_save_string( "true" , "userCouponRemoved" ) ;
				lr_end_sub_transaction ( lr_eval_string("{mainTransaction}_removePromotionCode") , LR_AUTO );
			}
			
			web_add_header("pageName", "fullOrderInfo" );
			web_add_header("source", "" );
			web_add_header("recalculate", "true" );
			web_add_header("calc", "true" );
			web_add_header("coupons", "true" );	
			getCartDetails();
		
			if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheckCount}"))>0) // && atoi(lr_eval_string("{cartCount}"))>0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
				lr_end_sub_transaction(lr_eval_string("{mainTransaction}_cart"), LR_AUTO );
			else
			{
				lr_error_message( lr_eval_string("{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, cartCount: {cartCount},piAmount: {piAmount}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
				lr_end_sub_transaction(lr_eval_string("{mainTransaction}_cart"), LR_FAIL );
				return LR_FAIL;
			}			
		}
	}
	return 0;
	
} // end deletePromoCode

int deleteItem()   //Remove from Bag
{
	int flag=0;
	if ( lr_paramarr_len ( "orderItemIds" ) > 1 ){
		
		lr_think_time ( LINK_TT ) ;
		
		lr_save_string("T06_Cart_Item_Remove", "mainTransaction");
		
		lr_start_transaction ("T06_Cart_Item_Remove") ;

		lr_start_sub_transaction ( "T06_Cart_Item_Remove_deleteMultipleOrderItems", "T06_Cart_Item_Remove" );
		web_add_header("Accept", "application/json");
		web_add_header("Content-Type", "application/json");
		web_add_header("storeId", lr_eval_string("{storeId}") );
		web_add_header("catalogId", lr_eval_string("{catalogId}") );
		web_add_header("langId", "-1");
		registerErrorCodeCheck();
		lr_save_string ( lr_paramarr_random( "orderItemIds" ) , "orderItemId" ) ;
		web_custom_request("deleteMultipleOrderItems",
			"URL=https://{api_host}/v2/cart/deleteMultipleOrderItems",
			"Method=PUT",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"Body={\"orderItem\":[{\"orderItemId\":\"{orderItemId}\",\"quantity\":\"0\"}]}",
			LAST);
		
		//lr_end_sub_transaction("T06_Cart_Item_Remove_deleteMultipleOrderItems", LR_AUTO);
		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	        lr_end_sub_transaction("T06_Cart_Item_Remove_deleteMultipleOrderItems", LR_AUTO);
        else {
            lr_fail_trans_with_error( lr_eval_string("T06_Cart_Item_Remove_deleteMultipleOrderItems Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
            lr_end_sub_transaction("T06_Cart_Item_Remove_deleteMultipleOrderItems", LR_FAIL);
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_deleteMultipleOrderItems - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
	          
            flag=1;
        }
		 
		web_add_header("pageName", "fullOrderInfo");
		web_add_header("source", "login");
					
		if (BRIERLEY_CUSTOM_TEST == 1 && BRIERLEY_FLAG_CHECK == 1)
			web_add_header("recalculate", "true" );
		else
			web_add_header("recalculate", "false" );

		web_add_header("calc", "true");
		getCartDetails();
		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheckCount}"))>0 && atoi(lr_eval_string("{cartCount}"))>0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
		{
			lr_end_sub_transaction("T06_Cart_Item_Remove_cart", LR_AUTO);
		}
		else
		{
			lr_fail_trans_with_error( lr_eval_string("T06_Cart_Item_Remove Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_sub_transaction("T06_Cart_Item_Remove_cart", LR_FAIL);
			lr_end_transaction ( lr_eval_string ( "T06_Cart_Item_Remove" ) , LR_FAIL ) ;
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
			return LR_FAIL;
		}
		if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {
			lr_start_transaction("T50_cookie is 0 alert on cart_2051");
			lr_end_transaction ("T50_cookie is 0 alert on cart_2051", LR_FAIL ) ;
		}else{
			lr_start_transaction("T50_cookie is 0 alert on cart_2051");
			lr_end_transaction ("T50_cookie is 0 alert on cart_2051", LR_PASS) ;
		}
		
		setOrderType();
//		if ( strcmp(lr_eval_string("{addBopisToCart}"), "TRUE") == 0 && strcmp(lr_eval_string("{addBossToCart}"), "FALSE") == 0 ) {
//			getBOPISInvetoryDetails();
//		}
		lr_end_transaction ("T06_Cart_Item_Remove" , LR_PASS ) ;
		return LR_PASS;

	}
	return 0;
} // end deleteItem


int saveForLaterPost()
{
    lr_think_time ( LINK_TT ) ;
    
    lr_save_string("T07_SaveForLater", "mainTransaction");
    
//    if (atoi(lr_eval_string("{itemCatentryId_count}")) > 0)
    if (atoi(lr_eval_string("{itemCatentryIdForPost_count}")) > 0)
    {
	    lr_start_transaction ("T07_SaveForLater") ;
	    lr_save_string(lr_paramarr_random("orderItemId"), "orderItemId");
	    lr_save_string(lr_paramarr_random( "itemCatentryIdForPost"), "itemCatentryId");

//	    lr_save_string(lr_paramarr_idx( "orderItemIds", 1), "orderItemId");
//	    lr_save_string(lr_paramarr_idx( "itemCatentryIdForPost", 1), "itemCatentryId");
	    lr_start_sub_transaction ( "T07_SaveForLater_deleteMultipleOrderItems", "T07_SaveForLater" );
	    web_add_header("Accept", "application/json");
	    web_add_header("Content-Type", "application/json");
		addHeader();
	    registerErrorCodeCheck();
	
	    web_custom_request("deleteMultipleOrderItems",
	        "URL=https://{api_host}/v2/cart/deleteMultipleOrderItems",
	        "Method=PUT",
	        "Resource=0",
	        "RecContentType=application/json",
	        "Mode=HTTP",
	        "Body={\"orderItem\":[{\"orderItemId\":\"{orderItemId}\",\"quantity\":\"0\"}]}",
	        LAST);
		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	    {	
			lr_end_sub_transaction("T07_SaveForLater_deleteMultipleOrderItems", LR_AUTO);
	    } else  {
	        lr_fail_trans_with_error( lr_eval_string("T07_SaveForLater_deleteMultipleOrderItems Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", userEmail: {userEmail}, orderItemId: {orderItemId}") ) ;
	        lr_end_sub_transaction("T07_SaveForLater_deleteMultipleOrderItems", LR_FAIL);
	        lr_end_transaction("T07_SaveForLater", LR_FAIL);
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_deleteMultipleOrderItems - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
	        return LR_FAIL;
	    }
	
	    web_add_header("pageName", "fullOrderInfo");
	    			
		if (BRIERLEY_CUSTOM_TEST == 1 && BRIERLEY_FLAG_CHECK == 1)
			web_add_header("recalculate", "true" );
		else
			web_add_header("recalculate", "false" );

	    web_add_header("calc", "true");
	    getCartDetails();
	    if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0) // && atoi(lr_eval_string("{parentOrderId}"))>0 && atoi(lr_eval_string("{orderItemId_count}"))>0)
	    {
		    lr_end_sub_transaction("T07_SaveForLater_cart", LR_AUTO);
	    } else {
	        lr_fail_trans_with_error( lr_eval_string("T07_SaveForLater_cart Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Flow:{currentFlow}, userEmail: {userEmail}") ) ;
		    lr_end_sub_transaction("T07_SaveForLater_cart", LR_FAIL);
	        lr_end_transaction ( lr_eval_string ( "T07_SaveForLater" ) , LR_FAIL ) ;
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, Flow:{currentFlow}, Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
	        return LR_FAIL;
	    }
		if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {
			lr_start_transaction("T50_cookie is 0 alert on cart_2120");
			lr_end_transaction ("T50_cookie is 0 alert on cart_2120", LR_FAIL ) ;
		}else{
			lr_start_transaction("T50_cookie is 0 alert on cart_2120");
			lr_end_transaction ("T50_cookie is 0 alert on cart_2120", LR_PASS ) ;
		}
	
	    lr_save_string("POST", "apiMethod");
	    saveForLater();
	    
	    if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0) //if no error code and message
	    {
	    	lr_end_transaction ("T07_SaveForLater" , LR_PASS ) ;
	    } else {
	        lr_fail_trans_with_error( lr_eval_string("T07_SaveForLater_saveForLater Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", userEmail: {userEmail}") ) ;
	        lr_end_transaction ( lr_eval_string ( "T07_SaveForLater" ) , LR_FAIL ) ;
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_saveForLater - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
	        return LR_FAIL;
	    }
    }
    
    return 0;
    
} 

int saveForLaterMoveToBag()
{
    lr_think_time ( LINK_TT ) ;

    if (atoi(lr_eval_string("{itemCatentryId_count}")) > 0)
    {
	    lr_save_string("T07_SaveForLater", "mainTransaction");
	    
	    lr_start_transaction ("T07_SaveForLater") ;
	
	    lr_save_string(lr_paramarr_idx( "itemCatentryId", 1), "itemCatentryId");
		web_reg_save_param_json("ParamName=orderId", "QueryString=$..orderId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	    lr_start_sub_transaction ( "T07_SaveForLater_addProductToCart", "T07_SaveForLater" );
	    web_add_header("Accept", "application/json");
	    web_add_header("Content-Type", "application/json");
		addHeader();
	    registerErrorCodeCheck();
	 //   addSFCCCartHeader();
		web_custom_request("addProductToCart",
			"URL=https://{api_host}/v2/cart/addProductToCart", // SaveforLater
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
//			"Body={\"calculationUsage[]\":\"-7\",\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{itemCatentryId}\",\"quantity\":\"1\"}",
			"Body={\"catalogId\":\"{catalogId}\",\"storeId\":\"{storeId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{itemCatentryId}\",\"quantity\":\"1\",\"calculationUsage[]\":\"-7\",\"externalId\":\"\",\"multipackCatentryId\":null,\"skipLoyaltyCall\":false}",
			LAST);
	
		if ( strlen(lr_eval_string("{orderId}")) <= 0 ) {
			atc_Stat = 1; //0-Pass 1-Fail
			if ( isLoggedIn == 1 ) 
				lr_error_message( lr_eval_string("T07_SaveForLater_addProductToCart - Failed with HTTP {httpReturnCode}, catentryId: {atc_catentryId}, API Error Code: {errorCode}, Error Message: {errorMessage}, OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
			else
				lr_error_message( lr_eval_string("T07_SaveForLater_addProductToCart - Failed with HTTP {httpReturnCode}, catentryId: {atc_catentryId}, API Error Code: {errorCode}, Error Message: {errorMessage}, OrderId:{orderId}, Email: Guest, timestamp: {timestamp}, hostname: {hostName}") ) ;
	
			lr_end_sub_transaction ("T07_SaveForLater_addProductToCart", LR_FAIL) ;
			lr_end_transaction ( "T07_SaveForLater" , LR_FAIL ) ;
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_addProductToCart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
			return LR_FAIL;
	    } else {
		    lr_end_sub_transaction ( "T07_SaveForLater_addProductToCart", LR_AUTO );
		} // end if
		
	    web_add_header("pageName", "fullOrderInfo");
	    			
		if (BRIERLEY_CUSTOM_TEST == 1 && BRIERLEY_FLAG_CHECK == 1)
			web_add_header("recalculate", "true" );
		else
			web_add_header("recalculate", "false" );

	    web_add_header("calc", "true");
	    getCartDetails();
	    if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0) //if no error code and message
	    {
		    lr_end_sub_transaction("T07_SaveForLater_cart", LR_AUTO);
	    } else {
	        lr_fail_trans_with_error( lr_eval_string("T07_SaveForLater_cart Failed with Flow:{currentFlow}, Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		    lr_end_sub_transaction("T07_SaveForLater_cart", LR_FAIL);
	        lr_end_transaction ( lr_eval_string ( "T07_SaveForLater" ) , LR_FAIL ) ;
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, Flow:{currentFlow}, Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
	        return LR_FAIL;
	    }

		if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {
			lr_start_transaction("T50_cookie is 0 alert on cart_2196");
			lr_end_transaction ("T50_cookie is 0 alert on cart_2196", LR_FAIL ) ;
	    }else{
	    	lr_start_transaction("T50_cookie is 0 alert on cart_2196");
			lr_end_transaction ("T50_cookie is 0 alert on cart_2196", LR_PASS ) ;
	    }
	    
	    lr_save_string("DELETE", "apiMethod");
	    saveForLater();
	    
	    if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0) //if no error code and message
	    {
	    	lr_end_transaction ("T07_SaveForLater" , LR_PASS ) ;
	    } else {
	        lr_fail_trans_with_error( lr_eval_string("T07_SaveForLater Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
	        lr_end_transaction ( lr_eval_string ( "T07_SaveForLater" ) , LR_FAIL ) ;
	        return LR_FAIL;
	    }
    }
    return 0;
    
} 


int deleteItemUnqualifiedList()   //Remove from Bag for unqualified List added by Pavan Dusi 02052019
{
    int getunqual=0, flag=0;
    if ( lr_paramarr_len ( "getUnqualifiedListOfItems" ) > 0 ){
        getunqual=lr_paramarr_len ( "getUnqualifiedListOfItems" );
        while (getunqual>0){
        	flag=0;
	        lr_think_time ( LINK_TT ) ;
	        
	        lr_save_string("T06_Cart_Item_Remove_unqual", "mainTransaction");
	        
	        lr_start_transaction ("T06_Cart_Item_Remove_unqual") ;
	
	        lr_start_sub_transaction ( "T06_Cart_Item_Remove_unqual_deleteMultipleOrderItems", "T06_Cart_Item_Remove_unqual" );
	        web_add_header("Accept", "application/json");
	        web_add_header("Content-Type", "application/json");
	        web_add_header("storeId", lr_eval_string("{storeId}") );
	        web_add_header("catalogId", lr_eval_string("{catalogId}") );
	        web_add_header("langId", "-1");
	        registerErrorCodeCheck();
//	        lr_save_string ( lr_paramarr_random( "getUnqualifiedListOfItems" ) , "orderItemId" ) ;
	        lr_save_string ( lr_paramarr_idx( "getUnqualifiedListOfItems", getunqual ) , "orderItemId" ) ;
	        web_custom_request("deleteMultipleOrderItems",
	            "URL=https://{api_host}/v2/cart/deleteMultipleOrderItems",
	            "Method=PUT",
	            "Resource=0",
	            "RecContentType=application/json",
	            "Mode=HTTP",
	            "Body={\"orderItem\":[{\"orderItemId\":\"{orderItemId}\",\"quantity\":\"0\"}]}",
	            LAST);
	        //lr_end_sub_transaction("T06_Cart_Item_Remove_deleteMultipleOrderItems", LR_AUTO);
	 		 if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	        {     lr_end_sub_transaction("T06_Cart_Item_Remove_unqual_deleteMultipleOrderItems", LR_AUTO);
	            getunqual--;
	            //return LR_PASS;
	        }
	        else
	        {
	            lr_fail_trans_with_error( lr_eval_string("T06_Cart_Item_Remove_unqual Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
	            lr_end_sub_transaction("T06_Cart_Item_Remove_unqual_deleteMultipleOrderItems", LR_FAIL);
				if (WRITE_TO_SUMO==1) {
			 				lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_deleteMultipleOrderItems - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
							sendErrorToSumo(); 
			 	}
	            //No change in getunqual
	            getUnqualifiedItems(); //Need to revisit getUnqualifiedItems to get fresh new set of unqualified Items and re-assign appropriately
	            getunqual=lr_paramarr_len ( "getUnqualifiedListOfItems" );
	            flag=1;
	            //return LR_FAIL;
	        }


	        web_add_header("pageName", "fullOrderInfo");
	        			
		if (BRIERLEY_CUSTOM_TEST == 1 && BRIERLEY_FLAG_CHECK == 1)
			web_add_header("recalculate", "true" );
		else
			web_add_header("recalculate", "false" );

	        web_add_header("calc", "true");
	        getCartDetails();
	        lr_end_sub_transaction("T06_Cart_Item_Remove_unqual_cart", LR_AUTO);
			if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {
				lr_start_transaction("T50_cookie is 0 alert on cart_2270");
				lr_end_transaction ("T50_cookie is 0 alert on cart_2270", LR_FAIL ) ;
	        }else{
	        	lr_start_transaction("T50_cookie is 0 alert on cart_2270");
				lr_end_transaction ("T50_cookie is 0 alert on cart_2270", LR_PASS ) ;
	        }
	
	        //if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && flag==0 ) //if no error code and message
			if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{parentOrderId}"))>0 && atoi(lr_eval_string("{orderItemId_count}"))>0)
	        {    lr_end_transaction ("T06_Cart_Item_Remove_unqual" , LR_PASS ) ;
	           // getunqual--;
	            //return LR_PASS;
	        }
	        else
	        {
	            lr_fail_trans_with_error( lr_eval_string("T06_Cart_Item_Remove_unqual Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
				if (WRITE_TO_SUMO==1) {
			 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
						sendErrorToSumo(); 
			 	}
	            //No change in getunqual
//	            getUnqualifiedItems(); //Need to revisit getUnqualifiedItems to get fresh new set of unqualified Items and re-assign appropriately
//	            getunqual=lr_paramarr_len ( "getUnqualifiedListOfItems" );
	            lr_end_transaction ( lr_eval_string ( "T06_Cart_Item_Remove_unqual" ) , LR_FAIL ) ;
	            //return LR_FAIL;
	        }
           // getUnqualifiedItems(); //Need to revisit getUnqualifiedItems to get fresh new set of unqualified Items and re-assign appropriately
            //getunqual=lr_paramarr_len ( "getUnqualifiedListOfItems" );

        }//end while (getunqual>0){
    }//end if ( lr_paramarr_len ( "getUnqualifiedListOfItems" ) > 0 ){
    return 0;
} // end deleteItemUnqualifiedList


int Submit_Pickup_Detail()
{

	if (strcmp( lr_eval_string("{addBopisToCart}"), "TRUE") == 0 || strcmp( lr_eval_string("{addBossToCart}"), "TRUE") == 0) 
	{
		lr_think_time ( FORM_TT ) ;
		
		lr_save_string ("T11_Submit_Pickup_Detail", "mainTransaction" ) ;
/*
		if ( isLoggedIn != 1) {
			lr_save_string("bitrogue@mailinator.com", "userEmail");
		}
*/		
		lr_start_transaction ( "T11_Submit_Pickup_Detail") ;

		web_reg_save_param_json( "ParamName=addressId", "QueryString=$.addressId..addressId", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);//06182018
		web_reg_save_param_json( "ParamName=nickName", "QueryString=$..nickName", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);//06182018

		addHeader();
		web_add_header("Accept", "application/json");
		web_add_header("Content-Type", "application/json");

		lr_start_sub_transaction ("T11_Submit_Pickup_Detail_addAddress", "T11_Submit_Pickup_Detail" ) ;
		web_custom_request("addAddress", 
		"URL=https://{api_host}/v2/account/addAddress",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
//			"EncType=application/json", 
			"Body={\"contact\":[{\"addressType\":\"Shipping\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"phone2\":\"2014534613\",\"email1\":\"{userEmail}\",\"email2\":\"\"}]}",
			LAST);
		lr_end_sub_transaction ("T11_Submit_Pickup_Detail_addAddress", LR_AUTO) ;

		web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
//		web_reg_save_param("piId", "LB=\"piId\": \"", "RB=\",", "NotFound=Warning", LAST);
//		web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\",", "NotFound=Warning", LAST); // "piAmount": "36.84",\n
		web_reg_save_param_json("ParamName=piId", "QueryString=$.orderDetails..piId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json("ParamName=piAmount", "QueryString=$..piAmount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param( "orderItemType", "LB=\"orderType\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //orderType": "ECOM",\n  //romano added 04082019
	
		if (BRIERLEY_CUSTOM_TEST == 1 && BRIERLEY_FLAG_CHECK == 1)
			web_add_header("recalculate", "true" );
		else
			web_add_header("recalculate", "false" );

		web_add_header("pageName", "excludeCartItems" );
		web_add_header("coupons", "false" );
		web_add_header("calc", "false" );
		getCartDetails();
		
		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheckCount}"))>0 && atoi(lr_eval_string("{cartCount}"))>0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
		{	lr_end_sub_transaction(lr_eval_string("{mainTransaction}_cart"), LR_AUTO );
			lr_save_string("true","pickUpDetail");
		} else	{
			lr_error_message( lr_eval_string("{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, cartCount: {cartCount},piAmount: {piAmount}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
			lr_end_sub_transaction(lr_eval_string("{mainTransaction}_cart"), LR_FAIL );
			lr_end_transaction ("T11_Submit_Pickup_Detail", LR_FAIL ) ;
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
			lr_save_string ("", "mainTransaction" ) ;
			return LR_FAIL;
		}
		
		if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{cartCount}"))<=0) {
			lr_start_transaction("T50_cookie is 0 alert on cart_2359");
			lr_end_transaction ("T50_cookie is 0 alert on cart_2359", LR_FAIL ) ;
		}else{
			lr_start_transaction("T50_cookie is 0 alert on cart_2359");
			lr_end_transaction ("T50_cookie is 0 alert on cart_2359", LR_PASS ) ;
		}
		getShipmentMethods();

		//bonusDay();
		
		lr_end_transaction ( "T11_Submit_Pickup_Detail" , LR_PASS) ;

	}
	return 0;
}

int Submit_Pickup_Detail_Guest()
{

	lr_think_time ( FORM_TT ) ;
	
	lr_save_string ("T11_Submit_Pickup_Detail", "mainTransaction" ) ;
	lr_start_transaction ( "T11_Submit_Pickup_Detail") ;

	web_reg_save_param_json( "ParamName=addressId", "QueryString=$..addressId", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);//06182018
	web_reg_save_param_json( "ParamName=nickName", "QueryString=$..nickName", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);//06182018

	addHeader();
	web_add_header("accept", "application/json");
	web_add_header("content-Type", "application/json");

	lr_start_sub_transaction ("T11_Submit_Pickup_Detail_addAddress", "T11_Submit_Pickup_Detail" ) ;
	web_custom_request("addAddress", 
	"URL=https://{api_host}/v2/account/addAddress",
		"Method=POST",
		"Resource=0",
//		"RecContentType=application/json",
		"Mode=HTTP",
//			"EncType=application/json", 
//		"Body={\"contact\":[{\"addressType\":\"Shipping\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"phone2\":\"2014534613\",\"email1\":\"{userEmail}\",\"email2\":\"\"}]}",
		"Body={\"contact\":[{\"addressLine\":[\"{guestAdr1}\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"{guestZip}\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"{guestCity}\",\"country\":\"{guestCountry}\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"1498060089577\",\"phone1\":\"2014564545\",\"email1\":\"{userEmail}\",\"phone1Publish\":\"false\",\"primary\":\"false\",\"state\":\"{guestState}\",\"zipCode\":\"{guestZip}\",\"xcont_addressField2\":\"2\",\"xcont_addressField3\""
		":\"{guestZip}\",\"fromPage\":\"\"}]}",
		LAST);
	lr_end_sub_transaction ("T11_Submit_Pickup_Detail_addAddress", LR_AUTO) ;

	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
//	web_reg_save_param("piId", "LB=\"piId\": \"", "RB=\",", "NotFound=Warning", LAST);
//	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\",", "NotFound=Warning", LAST); // "piAmount": "36.84",\n
	web_reg_save_param_json("ParamName=piId", "QueryString=$.orderDetails..piId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=piAmount", "QueryString=$..piAmount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param( "orderItemType", "LB=\"orderType\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); //orderType": "ECOM",\n  //romano added 04082019

				
		if (BRIERLEY_CUSTOM_TEST == 1 && BRIERLEY_FLAG_CHECK == 1)
			web_add_header("recalculate", "true" );
		else
			web_add_header("recalculate", "false" );

	web_add_header("pageName", "excludeCartItems" );
	web_add_header("coupons", "false" );
	web_add_header("calc", "true" );
	getCartDetails();
	
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheckCount}"))>0 && atoi(lr_eval_string("{cartCount}"))>0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
	{	lr_end_sub_transaction(lr_eval_string("{mainTransaction}_cart"), LR_AUTO );
		lr_save_string("true","pickUpDetail");
	} else	{
		lr_error_message( lr_eval_string("{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, cartCount: {cartCount},piAmount: {piAmount}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_cart"), LR_FAIL );
		lr_end_transaction ("T11_Submit_Pickup_Detail", LR_FAIL ) ;
		if (WRITE_TO_SUMO==1) {
	 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
				sendErrorToSumo(); 
	 	}
		lr_save_string ("", "mainTransaction" ) ;
		return LR_FAIL;
	}
		
	if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{cartCount}"))<=0) {
		lr_start_transaction("T50_cookie is 0 alert on cart_2359");
		lr_end_transaction ("T50_cookie is 0 alert on cart_2359", LR_FAIL ) ;
	}else{
		lr_start_transaction("T50_cookie is 0 alert on cart_2359");
		lr_end_transaction ("T50_cookie is 0 alert on cart_2359", LR_PASS ) ;
	}
	
	if ( strcmp( lr_eval_string("{mixCart}"), "BOPIS") == 0 && strcmp( lr_eval_string("{mixCart}"), "BOSS") == 0)
	      getShipmentMethods();
	
	lr_end_transaction ( "T11_Submit_Pickup_Detail" , LR_PASS) ;

	return 0;
}

int checkout_miniBagPopUp(){  //Added by Pavan Dusi 03262019 For login First Scenario to Cover minibag before calling Proceed to Checkout
	
	int checks=0;
	lr_save_string("T06_ViewBagPopup", "mainTransaction");
	
	web_reg_save_param_json("ParamName=offersSku", "QueryString=$..orderItems..variantNo", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=offersPrice", "QueryString=$..orderItems..itemPrice", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=offersQty", "QueryString=$..orderItems..qty", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");

	
	web_add_header("pageName", "fullOrderInfo");
	web_add_header("calc", "false");
				
		if (BRIERLEY_CUSTOM_TEST == 1 && BRIERLEY_FLAG_CHECK == 1)
			web_add_header("recalculate", "true" );
		else
			web_add_header("recalculate", "false" );

	web_reg_find("TEXT/IC=userPoints", "SaveCount=apiCheck", LAST);
	lr_start_transaction(lr_eval_string("{mainTransaction}") );
	
	lr_start_sub_transaction(lr_eval_string("{mainTransaction}_getOrderDetails"), lr_eval_string("{mainTransaction}") );
	getOrderDetails();
	
	if (atoi(lr_eval_string("{apiCheck}")) == 0  || strlen(lr_eval_string("{errorMessage}"))>0 || strlen(lr_eval_string("{errorCode}"))>0){
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getOrderDetails"), LR_FAIL );
		checks=0;
	}
	else {
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_getOrderDetails"), LR_AUTO);
		checks=1;
	}
	
	setOrderType();
	
//	if ( strcmp(lr_eval_string("{addBopisToCart}"), "TRUE") == 0 && strcmp(lr_eval_string("{addBossToCart}"), "FALSE") == 0 ) {
//		getBOPISInvetoryDetails();
//	}
//		lr_set_debug_message(16|8|4|2,1);
	if (checks == 0){
		lr_end_transaction(lr_eval_string("{mainTransaction}") ,LR_FAIL);
		return LR_FAIL;
	}else{
		lr_end_transaction(lr_eval_string("{mainTransaction}") ,LR_AUTO);
		return LR_PASS;
	}
}

int proceedToCheckout_ShippingView() //Login First Scenario
{
	lr_think_time ( LINK_TT ) ;
	
	lr_save_string("T09_Proceed_To_Checkout", "mainTransaction");

	lr_start_transaction ( "T09_Proceed_To_Checkout" ) ;

	getUnqualifiedItems();
	
    if (strcmp(lr_eval_string("{UNQUALIFIEDLIST}"),"TRUE")==0){
        deleteItemUnqualifiedList(); //Hopefully we clear all the bag issues
        lr_save_string("T09_Proceed_To_Checkout", "mainTransaction"); //Fixed by Pavan Dusi 09/13/2019 Fix is needed as previous deleteItemUnqualifiedList() is overwritting "mainTransaction"
        getUnqualifiedItems();    //Lets make another attempt at checking the issue.
        if (strcmp(lr_eval_string("{UNQUALIFIEDLIST}"),"TRUE")==0){  //For some strange reason we are unable to clear unqualified items. User cannot proceed.
        lr_error_message( lr_eval_string("T09_Proceed_To_Checkout - UserID {userEmail}  has unqualified list of items and unable to delete them") ) ;
        lr_end_transaction ("T09_Proceed_To_Checkout", LR_FAIL ) ;
	if (WRITE_TO_SUMO==1) {
 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_getUnqualifiedItems - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
		sendErrorToSumo(); 
 	}
        return LR_FAIL;
        }
    }
	
	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO ) {

	//	if (strcmp( lr_eval_string("{addBopisToCart}"), "TRUE") != 0 && strcmp( lr_eval_string("{addEcommToCart}"), "FALSE") != 0 ) 
		if (strcmp( lr_eval_string("{mixCart}"), "ECOM") == 0)
			lr_save_string("shipping", "urlPath");
		else {
			if (strcmp( lr_eval_string("{isExpress}"), "TRUE") == 0 )
				lr_save_string("review", "urlPath");
			else
				lr_save_string("pickup", "urlPath");
		}
		
		lr_start_sub_transaction ("T09_Proceed_To_Checkout shipping", "T09_Proceed_To_Checkout" ) ;
		web_custom_request("checkout",
	//		"URL=https://{host}/{country}/checkout/shipping/",
			"URL=https://{host}/{country}/checkout/{urlPath}/",
			"Method=GET",
			"Resource=0",
			"Mode=HTML",
			LAST);
		lr_end_sub_transaction ("T09_Proceed_To_Checkout shipping", LR_AUTO) ;
	}

	if( strcmp(lr_eval_string("{isExpress}"), "true") == 0 ) {

		//web_reg_save_param_regexp ("ParamName=orderId",	"RegExp=\"orderId\":[ ]*\"(.*?)\"",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST );
		web_reg_save_param_json("ParamName=orderId", "QueryString=$.orderId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=Error", LAST);
		lr_start_sub_transaction ("T09_Proceed_To_Checkout expressCheckout", "T09_Proceed_To_Checkout" ) ;
		registerErrorCodeCheck();
		addHeader();
		web_add_header("prescreen", "false");
		web_custom_request("expressCheckout",
			"URL=https://{api_host}/v2/checkout/expressCheckout", 
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0  && strcmp(lr_eval_string("{orderId}"),"") !=0) //if no error code and message
		{
			lr_end_sub_transaction ("T09_Proceed_To_Checkout expressCheckout", LR_AUTO) ;
		}
		else
		{
			isLoggedIn = 0;
			lr_fail_trans_with_error( lr_eval_string("T09_Proceed_To_Checkout expressCheckout Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\" User: {userEmail}") ) ;
			lr_end_sub_transaction ("T09_Proceed_To_Checkout expressCheckout", LR_AUTO) ;
			lr_end_transaction ( lr_eval_string("{mainTransaction}") , LR_FAIL) ;
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_expressCheckout - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
			return LR_FAIL;
		}
		
		//lr_end_sub_transaction ("T09_Proceed_To_Checkout expressCheckout", LR_AUTO) ;
	}

//	preferences();03112019
	
	//bonusDay();	
//	web_add_header("accept", "application/json"); //0407 PAVAN FIXED THIS
//	web_add_header("content-type", "application/json");//0407
	web_reg_save_param_json("ParamName=cartCount", "QueryString=$..cartCount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_regexp ("ParamName=addressId",	"RegExp=addressId\":[ ]*(.*?),",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST );
	web_reg_save_param_json("ParamName=orderId", "QueryString=$.orderDetails.parentOrderId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=piId", "QueryString=$.orderDetails..piId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=piAmount", "QueryString=$..piAmount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

	if (BRIERLEY_CUSTOM_TEST == 1)
		web_add_header("recalculate", "true" );
	else
		web_add_header("recalculate", "false" );
	
	//web_add_header("calc", "false");	  //Commented Out by Romano 07/24/2019 To support expressCheckout with EXPRESS_CALCULATION_USAGE_IDS = '-1,-2,-5,-6,-7' fix in LOTHAL
	if( strcmp(lr_eval_string("{isExpress}"), "true") == 0 ) {
		web_add_header("calc", "true");	
	}else{
		web_add_header("calc", "false");	
	}

	web_add_header("pagename", "excludeCartItems" );
	web_add_header("source", "" );
	web_add_header("Expires", "0" );
	web_add_header("coupons", "false" ); //0407 - false in prod
	getCartDetails();
	
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0  && strcmp(lr_eval_string("{cartCount}"), "0") !=0 ) //&& atoi(lr_eval_string("{apiCheckCount}"))>0 ) {
		lr_end_sub_transaction("T09_Proceed_To_Checkout_cart", LR_AUTO);
	else {
		lr_error_message( lr_eval_string("T09_Proceed_To_Checkout_cart - Failed with HTTP {httpReturnCode}, CartCount: {cartCount}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, piAmount: {piAmount}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
		lr_end_sub_transaction("T09_Proceed_To_Checkout_cart", LR_FAIL);
		lr_end_transaction ("T09_Proceed_To_Checkout", LR_FAIL ) ;
		if (WRITE_TO_SUMO==1) {
	 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, CartCount: {cartCount}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, piAmount: {piAmount}, Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
	 	}
		return LR_FAIL;
	}
	
	if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {
			lr_start_transaction("T50_cookie is 0 alert on cart_2502");
			lr_end_transaction ("T50_cookie is 0 alert on cart_2502", LR_FAIL ) ;
	}else{
		lr_start_transaction("T50_cookie is 0 alert on cart_2502");
		lr_end_transaction ("T50_cookie is 0 alert on cart_2502", LR_PASS ) ;
	}
	
	web_reg_save_param_json("ParamName=isExpress", "QueryString=$.x_isExpress", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=addressId", "QueryString=$.addressId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=addressIdAll", "QueryString=$.addressId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param ( "wicAddressId" , "LB=wicAddressId\\\":\\\"", "RB=\\\"", "NotFound=Warning", LAST ) ;
	web_reg_save_param_json("ParamName=userId", "QueryString=$.userId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

//	lr_save_string("checkout", "p_pageName");
	getRegisteredUserDetailsInfo();
	
//	userGroup();
 // bonusDay();
//	getFavouriteStoreLocation();
	getShipmentMethods();
	giftOptionsCmd();

	if (ESPOT_FLAG == 1)
	{
		lr_save_string("1", "getESpot");
		web_add_header("espotName","GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Gift_Shop_espot,secondary_global_nav_Gift_Shop_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot");
		web_add_header("deviceType","desktop");
		getESpot();
		lr_save_string("2", "getESpot");
		web_add_header("espotName","CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo");
		web_add_header("deviceType","desktop");
		getESpot();
		if (isLoggedIn == 0) {
			lr_save_string("3", "getESpot");
			web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderPlccMoreInfoMdal,MobileNavHeaderLinks1,HeaderNavEspotLinks,DT_SiteWide_Above_Header_nonStikcy");
			web_add_header("deviceType","desktop");
			getESpot();
		}
	}

	if (isLoggedIn == 1) {
		web_add_header("frompage", "checkout");
		web_add_header("Accept", "application/json");
		web_add_header("Content-Type", "application/json");
		web_add_header("Expires", "0");
	
//		web_reg_save_param_json( "ParamName=addressId", "QueryString=$.contact..addressId", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
//		web_reg_save_param_json( "ParamName=addressIdAll", "QueryString=$.contact..addressId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param("savedaddressId", "LB=addressId\": \"", "RB=\"", "ORD=1","NotFound=Warning", LAST);
		web_reg_save_param("savedState", "LB=state\": \"", "RB=\"", "NotFound=Warning", LAST);
		web_reg_save_param("savedZipCode", "LB=zipCode\": \"", "RB=\"", "NotFound=Warning", LAST);
		web_reg_save_param_json( "ParamName=nickName", "QueryString=$..nickName", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

		web_reg_save_param_json( "ParamName=savedAddressIdPrimary", "QueryString=$..contact..addressLine[*]", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=savedAddressPrimary", "QueryString=$..contact..primary", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=savedCityPrimary", "QueryString=$..contact..city", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=savedStatePrimary", "QueryString=$..contact..state", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=savedZipCodePrimary", "QueryString=$..contact..zipCode", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=addressIdPrimary", "QueryString=$..contact..addressId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		//web_reg_save_param_json( "ParamName=addressId", "QueryString=$..contact..addressId", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST); //03282019 romano added

		web_reg_save_param_json( "ParamName=xcont_isBillingAddress", "QueryString=$..contact..xcont_isBillingAddress", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);//"xcont_isBillingAddress": "false",
//		web_reg_save_param_json( "ParamName=xcont_isDefaultBilling", "QueryString=$..contact..xcont_isDefaultBilling", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=xcont_isShippingAddress", "QueryString=$..contact..xcont_isShippingAddress", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);//"xcont_isShippingAddress": "true",
     	
//		web_reg_save_param("xcont_isBillingAddressX", "LB=xcont_isBillingAddress\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST);

		lr_start_sub_transaction ("T09_Proceed_To_Checkout_getAddressFromBook", "T09_Proceed_To_Checkout") ;
		addHeader();
		web_custom_request("getAddressFromBook",
			"URL=https://{api_host}/v2/account/getAddressFromBook", 
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getAddressFromBook", LR_AUTO) ;
		

		for (i=1; i <= atoi(lr_eval_string("{savedAddressPrimary_count}")); i++)
		{
			if (strcmp(lr_paramarr_idx("savedAddressPrimary", i), "true") == 0)
			{
			    lr_save_string(lr_paramarr_idx("addressIdPrimary", i), "addressIdPrimary");
			    lr_save_string(lr_paramarr_idx("addressIdPrimary", i), "addressId");
			    lr_save_string(lr_paramarr_idx("savedCityPrimary", i), "savedCityPrimary");
			    lr_save_string(lr_paramarr_idx("savedStatePrimary", i), "savedStatePrimary");
			    lr_save_string(lr_paramarr_idx("savedZipCodePrimary", i), "savedZipCodePrimary");
		    	lr_save_string(lr_paramarr_idx("savedAddressIdPrimary", i), "savedAddressIdPrimary"); 
		    	
			    if (i > 1) {
			    	lr_save_string(lr_paramarr_idx("savedAddressIdPrimary", i+1), "savedAddressIdPrimary");
			    	break;
			    }
			}
		}

	}
	
	if (strcmp( lr_eval_string("{mixCart}"), "ECOM") == 0)
    	lr_save_string("TRUE", "addEcommToCart");
	else if (strcmp( lr_eval_string("{mixCart}"), "BOSS") == 0)
    	lr_save_string("TRUE", "addBossToCart");
	else
    	lr_save_string("TRUE", "addBopisToCart");
		
	lr_start_sub_transaction ( "T09_Proceed_To_Checkout_getCreditCardDetails", "T09_Proceed_To_Checkout" );
/*
	web_reg_save_param("expMonth", "LB=expMonth\": \"", "RB=\"", "NotFound=Warning", LAST);
	web_reg_save_param("expYear", "LB=expYear\": \"", "RB=\"", "NotFound=Warning", LAST);
	web_reg_save_param("ccType", "LB=\"ccType\": \"", "RB=\",", "NotFound=Warning", LAST);
	web_reg_save_param("accountNo", "LB=\"accountNo\": \"", "RB=\",", "NotFound=Warning", LAST);
	web_reg_save_param("ccBrand", "LB=\"ccBrand\": \"", "RB=\",", "NotFound=Warning", LAST);
*/
	web_reg_save_param_json( "ParamName=expMonth", "QueryString=$.creditCardListJson..expMonth", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=expYear", "QueryString=$.creditCardListJson..expYear", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=ccType", "QueryString=$.creditCardListJson..ccType", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=accountNo", "QueryString=$.creditCardListJson..accountNo", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=ccBrand", "QueryString=$.creditCardListJson..ccBrand", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=billingAddressId", "QueryString=$.creditCardListJson..billingAddressId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=creditCardId", "QueryString=$.creditCardListJson..creditCardId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

	addHeader();
	web_add_header("isRest", "true" );
	web_custom_request("getCreditCardDetails",
		"URL=https://{api_host}/v2/account/getCreditCardDetails", 
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	lr_end_sub_transaction("T09_Proceed_To_Checkout_getCreditCardDetails", LR_AUTO);
	getCC();
	
	if (strcmp(lr_eval_string("{EDDPERSISTSTRING}"),"TRUE")==0 && strcmp(lr_eval_string("{isExpress}"), "true") == 0){
 		sendEddPersistNodeString();
 	}

	lr_end_sub_transaction("T09_Proceed_To_Checkout", LR_AUTO);
	return LR_PASS;

} // end proceedToCheckout_ShippingView

int proceedVenmo() //Login First Scenario
{
	lr_think_time ( LINK_TT ) ;
	
	lr_save_string("T09_Proceed_To_Checkout", "mainTransaction");

	lr_start_transaction ( "T09_Proceed_To_Checkout" ) ;
/*
	getUnqualifiedItems();
	
    if (strcmp(lr_eval_string("{UNQUALIFIEDLIST}"),"TRUE")==0){
        deleteItemUnqualifiedList(); //Hopefully we clear all the bag issues
        getUnqualifiedItems();    //Lets make another attempt at checking the issue.
        if (strcmp(lr_eval_string("{UNQUALIFIEDLIST}"),"TRUE")==0){  //For some strange reason we are unable to clear unqualified items. User cannot proceed.
        lr_error_message( lr_eval_string("T09_Proceed_To_Checkout - UserID {userEmail}  has unqualified list of items and unable to delete them") ) ;
        lr_end_transaction ("T09_Proceed_To_Checkout", LR_FAIL ) ;
        return LR_FAIL;
        }
    }
*/
//	if (strcmp( lr_eval_string("{addBopisToCart}"), "TRUE") != 0 && strcmp( lr_eval_string("{addEcommToCart}"), "FALSE") != 0 ) 
	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO ) {

		if (strcmp( lr_eval_string("{mixCart}"), "ECOM") == 0)
			lr_save_string("review", "urlPath"); //05162019 from shipping to review
		else {
			if (strcmp( lr_eval_string("{isExpress}"), "TRUE") == 0 )
				lr_save_string("review", "urlPath");
			else
				lr_save_string("pickup", "urlPath");
		}
		
		lr_start_sub_transaction ("T09_Proceed_To_Checkout shipping", "T09_Proceed_To_Checkout" ) ;
		web_custom_request("checkout",
	//		"URL=https://{host}/{country}/checkout/shipping/",
			"URL=https://{host}/{country}/checkout/{urlPath}/",
			"Method=GET",
			"Resource=0",
			"Mode=HTML",
			LAST);
		lr_end_sub_transaction ("T09_Proceed_To_Checkout shipping", LR_AUTO) ;
	}
	//   //   userGroup();
	giftOptionsCmd();
	//bonusDay();	
	
//	web_add_header("source", "");
	web_add_header("Accept", "application/json");
	web_add_header("content-type", "application/json");
//	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\"", "NotFound=Warning", LAST);
//	web_reg_save_param("piId", "LB=\"piId\": \"", "RB=\",", "NotFound=Warning", LAST);
	web_reg_save_param_json("ParamName=piId", "QueryString=$.orderDetails..piId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=piAmount", "QueryString=$..piAmount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
//	web_reg_save_param_regexp ("ParamName=cartCount",	"RegExp=[Cc]artCount\":[ ]*(.*?),",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST );
		web_reg_save_param_json("ParamName=cartCount", "QueryString=$..cartCount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_regexp ("ParamName=addressId",	"RegExp=addressId\":[ ]*(.*?),",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST );
	web_reg_save_param_json("ParamName=orderId", "QueryString=$.orderDetails.parentOrderId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=piId", "QueryString=$.orderDetails..piId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

	web_add_header("pageName", "fullOrderInfo" );
	if (BRIERLEY_CUSTOM_TEST == 1)
		web_add_header("recalculate", "true" );
	else
		web_add_header("recalculate", "false" );
	web_add_header("calc", "false" );
	web_add_header("coupons", "false" );
//	web_add_header("source", "" );
	web_add_header("Expires", "0" );
	getCartDetails();
	
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0  && strcmp(lr_eval_string("{cartCount}"), "0") !=0 && atoi(lr_eval_string("{apiCheckCount}"))>0 ) {
		lr_end_sub_transaction("T09_Proceed_To_Checkout_cart", LR_AUTO);
	} else {
		lr_error_message( lr_eval_string("T09_Proceed_To_Checkout_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, piAmount: {piAmount}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
		lr_end_sub_transaction("T09_Proceed_To_Checkout_cart", LR_FAIL);
		lr_end_transaction ("T09_Proceed_To_Checkout", LR_FAIL ) ;
		if (WRITE_TO_SUMO==1) {
	 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
	 	}
		return LR_FAIL;
	}
	
	if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {		
		lr_start_transaction("T50_cookie is 0 alert on cart_2707");
		lr_end_transaction ("T50_cookie is 0 alert on cart_2707", LR_FAIL ) ;
	}else{
		lr_start_transaction("T50_cookie is 0 alert on cart_2707");
		lr_end_transaction ("T50_cookie is 0 alert on cart_2707", LR_PASS ) ;
	}
	
	web_reg_save_param_json("ParamName=isExpress", "QueryString=$.x_isExpress", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=addressId", "QueryString=$.addressId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=addressIdAll", "QueryString=$.addressId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param ( "wicAddressId" , "LB=wicAddressId\\\":\\\"", "RB=\\\"", "NotFound=Warning", LAST ) ;
	web_reg_save_param_json("ParamName=userId", "QueryString=$.userId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

//	lr_save_string("checkout", "p_pageName");
	getRegisteredUserDetailsInfo();
	
	getShipmentMethods();

	updateShippingMethodSelectionVenmo();

	getVenmoClientToken();
	
	if (ESPOT_FLAG == 1)
	{
		lr_save_string("1", "getESpot");
		web_add_header("espotName","GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Gift_Shop_espot,secondary_global_nav_Gift_Shop_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot");
		web_add_header("deviceType","desktop");
		getESpot();
		lr_save_string("2", "getESpot");
		web_add_header("espotName","CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo");
		web_add_header("deviceType","desktop");
		getESpot();
		if (isLoggedIn == 0) {
			lr_save_string("3", "getESpot");
			web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderPlccMoreInfoMdal,MobileNavHeaderLinks1,HeaderNavEspotLinks,DT_SiteWide_Above_Header_nonStikcy");
			web_add_header("deviceType","desktop");
			getESpot();
		}
	}

	if (isLoggedIn == 1) {
		web_add_header("frompage", "checkout");
		web_add_header("Accept", "application/json");
		web_add_header("Content-Type", "application/json");
		web_add_header("Expires", "0");
	
//		web_reg_save_param_json( "ParamName=addressId", "QueryString=$.contact..addressId", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
//		web_reg_save_param_json( "ParamName=addressIdAll", "QueryString=$.contact..addressId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param("savedaddressId", "LB=addressId\": \"", "RB=\"", "ORD=1","NotFound=Warning", LAST);
		web_reg_save_param("savedState", "LB=state\": \"", "RB=\"", "NotFound=Warning", LAST);
		web_reg_save_param("savedZipCode", "LB=zipCode\": \"", "RB=\"", "NotFound=Warning", LAST);
		web_reg_save_param_json( "ParamName=nickName", "QueryString=$..nickName", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

		web_reg_save_param_json( "ParamName=savedAddressIdPrimary", "QueryString=$..contact..addressLine[*]", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=savedAddressPrimary", "QueryString=$..contact..primary", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=savedCityPrimary", "QueryString=$..contact..city", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=savedStatePrimary", "QueryString=$..contact..state", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=savedZipCodePrimary", "QueryString=$..contact..zipCode", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=addressIdPrimary", "QueryString=$..contact..addressId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		//web_reg_save_param_json( "ParamName=addressId", "QueryString=$..contact..addressId", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST); //03282019 romano added

		web_reg_save_param_json( "ParamName=xcont_isBillingAddress", "QueryString=$..contact..xcont_isBillingAddress", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);//"xcont_isBillingAddress": "false",
//		web_reg_save_param_json( "ParamName=xcont_isDefaultBilling", "QueryString=$..contact..xcont_isDefaultBilling", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=xcont_isShippingAddress", "QueryString=$..contact..xcont_isShippingAddress", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);//"xcont_isShippingAddress": "true",
     	
//		web_reg_save_param("xcont_isBillingAddressX", "LB=xcont_isBillingAddress\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST);

		lr_start_sub_transaction ("T09_Proceed_To_Checkout_getAddressFromBook", "T09_Proceed_To_Checkout") ;
		addHeader();
		web_custom_request("getAddressFromBook",
			"URL=https://{api_host}/v2/account/getAddressFromBook", 
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		lr_end_sub_transaction ("T09_Proceed_To_Checkout_getAddressFromBook", LR_AUTO) ;
		

		for (i=1; i <= atoi(lr_eval_string("{savedAddressPrimary_count}")); i++)
		{
			if (strcmp(lr_paramarr_idx("savedAddressPrimary", i), "true") == 0)
			{	lr_save_string("true", "primaryAddrExist");
			    lr_save_string(lr_paramarr_idx("addressIdPrimary", i), "addressIdPrimary");
			    lr_save_string(lr_paramarr_idx("addressIdPrimary", i), "addressId");
			    lr_save_string(lr_paramarr_idx("savedCityPrimary", i), "savedCityPrimary");
			    lr_save_string(lr_paramarr_idx("savedStatePrimary", i), "savedStatePrimary");
			    lr_save_string(lr_paramarr_idx("savedZipCodePrimary", i), "savedZipCodePrimary");
		    	lr_save_string(lr_paramarr_idx("savedAddressIdPrimary", i), "savedAddressIdPrimary"); 
		    	
			    if (i > 1) {
			    	lr_save_string(lr_paramarr_idx("savedAddressIdPrimary", i+1), "savedAddressIdPrimary");
			    	break;
			    }
			}
		}

	}
	
	if (strcmp( lr_eval_string("{mixCart}"), "ECOM") == 0)
    	lr_save_string("TRUE", "addEcommToCart");
	else if (strcmp( lr_eval_string("{mixCart}"), "BOSS") == 0)
    	lr_save_string("TRUE", "addBossToCart");
	else
    	lr_save_string("TRUE", "addBopisToCart");
		
	lr_start_sub_transaction ( "T09_Proceed_To_Checkout_getCreditCardDetails", "T09_Proceed_To_Checkout" );
/*
	web_reg_save_param("expMonth", "LB=expMonth\": \"", "RB=\"", "NotFound=Warning", LAST);
	web_reg_save_param("expYear", "LB=expYear\": \"", "RB=\"", "NotFound=Warning", LAST);
	web_reg_save_param("ccType", "LB=\"ccType\": \"", "RB=\",", "NotFound=Warning", LAST);
	web_reg_save_param("accountNo", "LB=\"accountNo\": \"", "RB=\",", "NotFound=Warning", LAST);
	web_reg_save_param("ccBrand", "LB=\"ccBrand\": \"", "RB=\",", "NotFound=Warning", LAST);
*/	
	web_reg_save_param_json( "ParamName=expMonth", "QueryString=$.creditCardListJson..expMonth", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=expYear", "QueryString=$.creditCardListJson..expYear", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=ccType", "QueryString=$.creditCardListJson..ccType", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=accountNo", "QueryString=$.creditCardListJson..accountNo", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=ccBrand", "QueryString=$.creditCardListJson..ccBrand", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=creditCardId", "QueryString=$.creditCardListJson..creditCardId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	
	addHeader();
	web_add_header("isRest", "true" );
	web_custom_request("getCreditCardDetails",
		"URL=https://{api_host}/v2/account/getCreditCardDetails", 
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	lr_end_sub_transaction("T09_Proceed_To_Checkout_getCreditCardDetails", LR_AUTO);
	getCC();

	lr_end_sub_transaction("T09_Proceed_To_Checkout", LR_AUTO);
	
	return LR_PASS;

} // end proceedVenmo


int proceedAsGuest()
{
	lr_think_time ( LINK_TT ) ;
	
	lr_save_string ( "T09_Proceed_To_Checkout", "mainTransaction" ) ;

	lr_start_transaction ( "T09_Proceed_To_Checkout" ) ;

	getUnqualifiedItems();

	if (strcmp(lr_eval_string("{UNQUALIFIEDLIST}"),"TRUE")==0){
        deleteItemUnqualifiedList(); //Hopefully we clear all the bag issues
        lr_save_string ( "T09_Proceed_To_Checkout", "mainTransaction" ) ; //Hack by Pavan Dusi on 09/13/2019 to fix issues in test.
        getUnqualifiedItems();    //Lets make another attempt at checking the issue.
      //  if (strcmp(lr_eval_string("{UNQUALIFIEDLIST}"),"TRUE")==0){  //For some strange reason we are unable to clear unqualified items. User cannot proceed.
      if (strcmp(lr_eval_string("{UNQUALIFIEDLIST}"),"TRUE")==0 || atoi(lr_eval_string("{orderItemType_count}")) == 0 ){  //For some strange reason we are unable to clear unqualified items. User cannot proceed. // && atoi(lr_eval_string("{orderItemType_count}")>0
        lr_error_message( lr_eval_string("T09_Proceed_To_Checkout - Guest user has unqualified list of items and unable to delete them") ) ;
        lr_end_transaction ("T09_Proceed_To_Checkout", LR_FAIL ) ;
        return LR_FAIL;
        }
    }
	
	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO ) {
	//	if (strcmp( lr_eval_string("{addBopisToCart}"), "TRUE") != 0 && strcmp( lr_eval_string("{addEcommToCart}"), "FALSE") != 0 ) 
		if (strcmp( lr_eval_string("{mixCart}"), "ECOM") == 0)
			lr_save_string("shipping", "urlPath");
		else 
			lr_save_string("pickup", "urlPath");
		
		lr_start_sub_transaction ("T09_Proceed_To_Checkout shipping", "T09_Proceed_To_Checkout" ) ;
		web_custom_request("checkout",
	//		"URL=https://{host}/{country}/checkout/shipping",
			"URL=https://{host}/{country}/checkout/{urlPath}/",
			"Method=GET",
			"Resource=0",
			"Mode=HTML",
			LAST);
		lr_end_sub_transaction ("T09_Proceed_To_Checkout shipping", LR_AUTO) ;
	}
	
	web_add_header("pageName", "fullOrderInfo");
	if (BRIERLEY_CUSTOM_TEST == 1)
		web_add_header("recalculate", "true" );
	else
		web_add_header("recalculate", "false" );
	web_add_header("calc", "false" );
	web_add_header("Expires", "0" );
	web_add_header("source", "" );
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
	getCartDetails();
//	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheckCount}"))>0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheckCount}"))>0 && atoi(lr_eval_string("{cartCount}"))>0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_cart"), LR_AUTO );
	else
	{
		lr_error_message( lr_eval_string("{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, cartCount: {cartCount},piAmount: {piAmount}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_cart"), LR_FAIL );
		lr_end_transaction ("T09_Proceed_To_Checkout", LR_FAIL ) ;
	if (WRITE_TO_SUMO==1) {
 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
		sendErrorToSumo(); 
 	}
		lr_save_string ("", "mainTransaction" ) ;
		return LR_FAIL;
	}
		if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {
			lr_start_transaction("T50_cookie is 0 alert on cart_2896");
			lr_end_transaction ("T50_cookie is 0 alert on cart_2896", LR_FAIL ) ;
	}else{
			lr_start_transaction("T50_cookie is 0 alert on cart_2896");
			lr_end_transaction ("T50_cookie is 0 alert on cart_2896", LR_PASS ) ;
	}
	
//	lr_save_string("checkout", "p_pageName");
	getRegisteredUserDetailsInfo();
	
	web_reg_save_param("savedState", "LB=state\": \"", "RB=\"", "NotFound=Warning", LAST);
	web_reg_save_param("savedZipCode", "LB=zipCode\": \"", "RB=\"", "NotFound=Warning", LAST);
	web_reg_save_param_json( "ParamName=savedAddressLine", "QueryString=$.contact[0]...addressLine[0].", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
//	web_add_header("state",lr_eval_string("{guestState}"));
//	web_add_header("addressField1",lr_eval_string("{guestAdr1}"));
	
	getShipmentMethods();
	
	giftOptionsCmd();

	if (ESPOT_FLAG == 1)
	{
		lr_save_string("1", "getESpot");
		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_add_header("deviceType","desktop");
		getESpot();

	}

	lr_end_transaction ( "T09_Proceed_To_Checkout" , LR_PASS) ;

//	Submit_Pickup_Detail(); moved to checkout

	return 0;

} // end proceedAsGuest

int forgetPassword(){
	return 0;
}
int forgetPassword1()
{
	lr_think_time ( FORM_TT ) ;
	web_cleanup_cookies ( ) ;
	web_cache_cleanup();

	//lr_continue_on_error(1);
	lr_start_transaction ( "T29_ForgotPassword_resetpassword" ) ;
	registerErrorCodeCheck();
	addHeader();
	web_add_header("Content-Type", "application/json");
	web_add_header("Accept", "application/json");
	web_add_header("deviceType", "desktop");
	web_add_header("Expires", "0");
	lr_save_string( lr_eval_string("{passwordReset}"), "userEmail" ); //select logonid from userreg where logonid like 'BITROGUE%' AND PASSWORDEXPIRED = 0 
	//need to get chunks of 5k from db if getting lots of resetPassword failures OR run a Set passwordexpired=0 where logonid like 'BITROGUE%' AND PASSWORDEXPIRED = 1 

	web_custom_request("resetpassword", ///api/v2/account/resetPassword - Phase 2
//		"URL=https://{api_host}/resetpassword",
		"URL=https://{api_host}/v2/account/resetPassword",
		"Method=PUT",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"Referer=https://{host}/us/home",
		"Mode=HTML",
		"Body={\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"isPasswordReset\":\"true\",\"logonId\":\"{userEmail}\",\"reLogonURL\":\"ChangePassword\",\"formFlag\":\"true\"}",
		LAST);

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	{
		lr_end_transaction ( "T29_ForgotPassword_resetpassword" , LR_PASS) ;
		return LR_PASS;
	}
	else
	{
		lr_fail_trans_with_error( lr_eval_string("T29_ForgotPassword_resetpassword Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\" User: {userEmail}; need to get new id's from db") ) ;
		lr_end_transaction ( "T29_ForgotPassword_resetpassword" , LR_FAIL) ;
		if (WRITE_TO_SUMO==1) {
	 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_resetpassword - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
	 	}
		return LR_FAIL;
	}

}

int login() //Browse First - Logon then proceed to checkout - 
{
	lr_think_time ( FORM_TT ) ;

	lr_save_string ( "T10_Logon_Checkout", "mainTransaction" ) ;

	lr_start_transaction ( lr_eval_string("{mainTransaction}") ) ;
	
	getUnqualifiedItems();
	
	lr_start_sub_transaction ( "T10_Logon_Checkout logon",lr_eval_string("{mainTransaction}") ) ;
	web_reg_save_param ( "addressId" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=1", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
	web_reg_save_param ( "addressIdAll" , "LB=\"addressId\": \"" , "RB=\",\n" , "ORD=ALL", "NotFound=Warning", LAST ) ; //"addressId": "1398789",\n
	web_reg_save_param ( "responseCode" , "LB=\"responseCode\": \"" , "RB=\"" , "NotFound=Warning", LAST ) ; //"responseCode": "LoginSuccess"\n
	web_add_header("Accept", "application/json");
	web_add_header("Content-Type", "application/json");
	web_add_header("Expires", "0");
	web_reg_save_param ( "responseCode" , "LB=\"responseCode\": \"" , "RB=\"" , "NotFound=Warning", LAST ) ; //"responseCode": "LoginSuccess"\n
	registerErrorCodeCheck();

	addHeader();
	web_custom_request("logon",
		"URL=https://{api_host}/v2/account/logon",
		"Method=POST",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json",	
//		"Body={\"storeId\":\"{storeId}\",\"logonId1\":\"{userEmail}\",\"logonPassword1\":\"Asdf!234\",\"rememberCheck\":true,\"rememberMe\":true,\"requesttype\":\"ajax\",\"reLogonURL\":\"TCPAjaxLogonErrorView\",\"URL\":\"TCPAjaxLogonSuccessView\",\"registryAccessPreference\":\"Public\",\"calculationUsageId\":-1,\"createIfEmpty\":1,\"deleteIfEmpty\":\"*\",\"fromOrderId\":\"*\",\"toOrderId\":\".\",\"updatePrices\":0,\"xCreditCardId\":\"\"}",
		"Body={\"storeId\":\"{storeId}\",\"logonId1\":\"{userEmail}\",\"logonPassword1\":\"Asdf!234\",\"rememberCheck\":true,\"rememberMe\":true,\"requesttype\":\"ajax\",\"reLogonURL\":\"TCPAjaxLogonErrorView\",\"URL\":\"TCPAjaxLogonSuccessView\",\"registryAccessPreference\":\"Public\",\"calculationUsageId\":-1,\"createIfEmpty\":1,\"deleteIfEmpty\":\"*\",\"fromOrderId\":\"*\",\"toOrderId\":\".\",\"updatePrices\":0,\"xCreditCardId\":\"\",\"mergeCart\":false,\"userId\":\"-1002\"}",
		LAST);

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
	{   isLoggedIn = 1;
		lr_end_sub_transaction("T10_Logon_Checkout logon", LR_AUTO);
	} else {
			isLoggedIn = 0;
			lr_error_message( lr_eval_string("T10_LogonHome logon Browse First- Failed with API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}, HTTP {httpReturnCode}") ) ;
			lr_end_sub_transaction("T10_Logon_Checkout logon", LR_FAIL);
			lr_end_transaction (lr_eval_string("{mainTransaction}"), LR_FAIL ) ;
	if (WRITE_TO_SUMO==1) {
 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_logon - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
 	}
			return LR_FAIL;
	}
	
    if (strcmp(lr_eval_string("UNQUALIFIEDLIST"),"TRUE")==0){
        deleteItemUnqualifiedList(); //Hopefully we clear all the bag issues
        lr_save_string ( "T10_Logon_Checkout", "mainTransaction" ) ; //Hack by Pavan Dusi 09/13/2019 to fix transaction names.
        getUnqualifiedItems();    //Lets make another attempt at checking the issue.
       // if (strcmp(lr_eval_string("{UNQUALIFIEDLIST}"),"TRUE")==0){  //For some strange reason we are unable to clear unqualified items. User cannot proceed.
       if (strcmp(lr_eval_string("{UNQUALIFIEDLIST}"),"TRUE")==0 || atoi(lr_eval_string("{orderItemType_count}")) == 0 ){  //For some strange reason we are unable to clear unqualified items. User cannot proceed. // && atoi(lr_eval_string("{orderItemType_count}")>0
        lr_error_message( lr_eval_string("T10_Logon_Checkout - Guest user who just  logon as {userEmail}  has unqualified list of items and unable to delete them.") ) ;
        lr_end_transaction (lr_eval_string("{mainTransaction}"), LR_FAIL ) ;
        return LR_FAIL;
        }
    }
	
	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO ) {
	
	//	if (strcmp( lr_eval_string("{addBopisToCart}"), "TRUE") != 0 && strcmp( lr_eval_string("{addEcommToCart}"), "FALSE") != 0 ) 
		if (strcmp( lr_eval_string("{mixCart}"), "ECOM") == 0)
			lr_save_string("shipping", "urlPath");
		else {
			if (strcmp( lr_eval_string("{isExpress}"), "TRUE") == 0 )
				lr_save_string("review", "urlPath");
			else
				lr_save_string("pickup", "urlPath");
		}
		
		lr_start_sub_transaction ( lr_eval_string("T10_Logon_Checkout {urlPath}"), lr_eval_string("{mainTransaction}") ) ;
			web_url("checkout",
			"URL=https://{host}/{country}/checkout/{urlPath}/",
			"TargetFrame=",
			"Resource=0",
			"Mode=HTML",
			LAST);
		lr_end_sub_transaction ( lr_eval_string("T10_Logon_Checkout {urlPath}"), LR_AUTO ) ;
	}
	
//	lr_save_string("checkout", "p_pageName");
	lr_save_string("logon", "stepName");
	web_add_header("source", "login");
	getRegisteredUserDetailsInfo();
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && strlen ( lr_eval_string ( "{userId}" ) ) > 0) //if no error code and message
	{
		isLoggedIn = 1;
	} else	{
		isLoggedIn = 0;
		lr_fail_trans_with_error( lr_eval_string("T10_Logon_Checkout getRegisteredUserDetailsInfo Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\" User: {userEmail}") ) ;
		lr_end_transaction ( lr_eval_string("{mainTransaction}") , LR_FAIL) ;
		lr_save_string("","errorMessageToFile");
		lr_save_string(lr_eval_string("T10_Logon_Checkout getRegisteredUserDetailsInfo Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\" User: {userEmail}"), "errorMessageToFile");
		if (WRITE_TO_SUMO==1) {
	 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_getRegisteredUserDetailsInfo - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
				sendErrorToSumo(); 
	 	}
		writeToFile();

		return LR_FAIL;
	}

	getAllCoupons();

	if( strcmp(lr_eval_string("{isExpress}"), "true") == 0 ) {
		web_reg_save_param_json("ParamName=orderId", "QueryString=$.orderId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=Error", LAST);
		//web_reg_save_param_regexp ("ParamName=orderId",	"RegExp=\"orderId\":[ ]*\"(.*?)\"",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST );
		lr_start_sub_transaction ("T10_Logon_Checkout_expressCheckout", lr_eval_string("{mainTransaction}") ) ;
		addHeader();
		registerErrorCodeCheck();
		web_add_header("prescreen", "false");
		web_custom_request("expressCheckout",
			"URL=https://{api_host}/v2/checkout/expressCheckout", 
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		//if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0  && strcmp(lr_eval_string("{orderId}"),"") !=0) //if no error code and message
		{
			lr_end_sub_transaction ("T10_Logon_Checkout_expressCheckout", LR_AUTO) ;
		}
		else
		{
			isLoggedIn = 0;
			lr_fail_trans_with_error( lr_eval_string("T10_Logon_Checkout_expressCheckout Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\" User: {userEmail}") ) ;
			lr_end_sub_transaction ("T10_Logon_Checkout_expressCheckout", LR_AUTO) ;
			lr_end_transaction ( lr_eval_string("{mainTransaction}") , LR_FAIL) ;
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_expressCheckout - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
			return LR_FAIL;
		}
		
		
		lr_start_sub_transaction ("T10_Logon_Checkout_getCreditCardDetails", lr_eval_string("{mainTransaction}"));
	/*	web_reg_save_param("expMonth", "LB=expMonth\": \"", "RB=\"", "NotFound=Warning", LAST);
		web_reg_save_param("expYear", "LB=expYear\": \"", "RB=\"", "NotFound=Warning", LAST);
		web_reg_save_param("ccType", "LB=\"ccType\": \"", "RB=\",", "NotFound=Warning", LAST);
		web_reg_save_param("accountNo", "LB=\"accountNo\": \"", "RB=\",", "NotFound=Warning", LAST);
		web_reg_save_param("ccBrand", "LB=\"ccBrand\": \"", "RB=\",", "NotFound=Warning", LAST);
	*/
		web_reg_save_param_json( "ParamName=expMonth", "QueryString=$.creditCardListJson..expMonth", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=expYear", "QueryString=$.creditCardListJson..expYear", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=ccType", "QueryString=$.creditCardListJson..ccType", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=accountNo", "QueryString=$.creditCardListJson..accountNo", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=ccBrand", "QueryString=$.creditCardListJson..ccBrand", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=creditCardId", "QueryString=$.creditCardListJson..creditCardId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		
		addHeader();
		web_add_header("isRest", "true" );
		web_add_header("Accept", "application/json" );
		web_url("getCreditCardDetails",
			"URL=https://{api_host}/v2/account/getCreditCardDetails", // 
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		lr_end_sub_transaction ("T10_Logon_Checkout_getCreditCardDetails",LR_AUTO);
		getCC();
	}


//	web_reg_save_param_regexp ("ParamName=cartCount",	"RegExp=[Cc]artCount\":[ ]*(.*?),",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST );
	web_reg_save_param_json("ParamName=cartCount", "QueryString=$..cartCount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
//	web_reg_save_param_regexp ("ParamName=addressId",	"RegExp=addressId\":[ ]*(.*?),",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST ); //Hack 03182019 Source of Error
	web_reg_save_param_json("ParamName=orderId", "QueryString=$.orderDetails.parentOrderId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=piId", "QueryString=$.orderDetails..piId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=piAmount", "QueryString=$..piAmount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

	//web_add_header("content-type", "application/json");
	web_add_header("Accept-Language", "en-US,en;q=0.9");
	web_add_header("pageName", "fullOrderInfo");
	//web_add_header("recalculate", "true" );
	if (BRIERLEY_CUSTOM_TEST == 1)
		web_add_header("recalculate", "true" );
	else
		web_add_header("recalculate", "false" );
	
	web_add_header("coupons", "false");
	if( strcmp(lr_eval_string("{isExpress}"), "true") == 0 ) {
		web_add_header("calc", "true");	
	}else{
		web_add_header("calc", "false");	
	}
	
	getCartDetails();
	
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && strcmp(lr_eval_string("{cartCount}"), "0") !=0 && atoi(lr_eval_string("{parentOrderId}"))>0 && atoi(lr_eval_string("{orderItemId_count}"))>0)
	{	isLoggedIn = 1;
		lr_end_sub_transaction("T10_Logon_Checkout_cart", LR_AUTO);
	}
	else {
		lr_error_message( lr_eval_string("T10_Logon_Checkout_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, piAmount: {piAmount}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
		lr_end_sub_transaction("T10_Logon_Checkout_cart", LR_FAIL);
		lr_end_transaction (lr_eval_string("{mainTransaction}"), LR_FAIL ) ;
		if (WRITE_TO_SUMO==1) {
	 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
	 	}
		return LR_FAIL;
	}
		if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {
			lr_start_transaction("T50_cookie is 0 alert on cart_3137");
			lr_end_transaction ("T50_cookie is 0 alert on cart_3137", LR_FAIL ) ;
	}else{
		lr_start_transaction("T50_cookie is 0 alert on cart_3137");
			lr_end_transaction ("T50_cookie is 0 alert on cart_3137", LR_PASS ) ;
	}
	
	lr_start_sub_transaction ("T10_Logon_Checkout_getAddressFromBook",lr_eval_string("{mainTransaction}"));
		addHeader();	
		web_add_header("frompage", "checkout");
		web_add_header("Accept", "application/json");
		web_add_header("Content-Type", "application/json");
		web_add_header("expires", "0");
	
//		web_reg_save_param_json( "ParamName=addressId", "QueryString=$.contact..addressId", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
//		web_reg_save_param_json( "ParamName=addressIdAll", "QueryString=$.contact..addressId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param("savedaddressId", "LB=addressId\": \"", "RB=\"", "ORD=1","NotFound=Warning", LAST);
		web_reg_save_param("savedState", "LB=state\": \"", "RB=\"", "NotFound=Warning", LAST);
		web_reg_save_param("savedZipCode", "LB=zipCode\": \"", "RB=\"", "NotFound=Warning", LAST);
		web_reg_save_param_json( "ParamName=nickName", "QueryString=$..nickName", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

		web_reg_save_param_json( "ParamName=savedAddressIdPrimary", "QueryString=$..contact..addressLine[*]", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=savedAddressPrimary", "QueryString=$..contact..primary", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=savedCityPrimary", "QueryString=$..contact..city", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=savedStatePrimary", "QueryString=$..contact..state", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=savedZipCodePrimary", "QueryString=$..contact..zipCode", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=addressIdPrimary", "QueryString=$..contact..addressId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

		web_reg_save_param_json( "ParamName=xcont_isBillingAddress", "QueryString=$..contact..xcont_isBillingAddress", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
//		web_reg_save_param_json( "ParamName=xcont_isDefaultBilling", "QueryString=$..contact..xcont_isDefaultBilling", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_reg_save_param_json( "ParamName=xcont_isShippingAddress", "QueryString=$..contact..xcont_isShippingAddress", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	
//		web_reg_save_param("xcont_isBillingAddressX", "LB=xcont_isBillingAddress\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST);
	web_url("getAddressFromBook",
		"URL=https://{api_host}/v2/account/getAddressFromBook", 
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	lr_end_sub_transaction ("T10_Logon_Checkout_getAddressFromBook",LR_AUTO);

	//getPrimaryAddress
	for (i=1; i <= atoi(lr_eval_string("{savedAddressPrimary_count}")); i++)
	{
		if (strcmp(lr_paramarr_idx("savedAddressPrimary", i), "true") == 0)
		{
		    lr_save_string(lr_paramarr_idx("addressIdPrimary", i), "addressIdPrimary");
		    lr_save_string(lr_paramarr_idx("addressIdPrimary", i), "addressId");
		    lr_save_string(lr_paramarr_idx("savedCityPrimary", i), "savedCityPrimary");
		    lr_save_string(lr_paramarr_idx("savedStatePrimary", i), "savedStatePrimary");
		    lr_save_string(lr_paramarr_idx("savedZipCodePrimary", i), "savedZipCodePrimary");
	    	lr_save_string(lr_paramarr_idx("savedAddressIdPrimary", i), "savedAddressIdPrimary"); 
	    //	lr_save_string(lr_eval_string("{addressIdPrimary}"), "addressId"); //Hack added on 03182019
		    if (i > 1) {
		    	lr_save_string(lr_paramarr_idx("savedAddressIdPrimary", i+1), "savedAddressIdPrimary");
		    	break;
		    }
		}
	}
/*	
	if (atoi(lr_eval_string("{savedZipCodePrimary_count}")) > 0 ) {
		web_add_header("state", lr_eval_string("{savedStatePrimary_1}") );
		web_add_header("zipCode", lr_eval_string("{savedZipCodePrimary_1}") );
		web_add_header("addressField1", lr_eval_string("{savedAddressIdPrimary_1}") );
	} else { // 
		//if (isLoggedIn = 0)
		lr_save_string(lr_eval_string("{guestState}"),"savedStatePrimary");
		lr_save_string(lr_eval_string("{guestZip}"),"savedZipCodePrimary");
		lr_save_string(lr_eval_string("{guestAdr1}"),"savedAddressIdPrimary");
		lr_save_string(lr_eval_string("{guestCity}"),"savedCityPrimary");
		
		web_add_header("state", "{savedStatePrimary}");
		web_add_header("zipcode", "{savedZipCodePrimary}");
		web_add_header("addressfield1", "{savedAddressIdPrimary}");
	}

	getShipmentMethods();
*/	
	if (ESPOT_FLAG == 1 ) {
		lr_save_string("1", "getESpot");
		web_add_header("espotname", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Gift_Shop_espot,secondary_global_nav_Gift_Shop_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espo");
		getESpot();
		lr_save_string("2", "getESpot");
		web_add_header("espotname", "GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderPlccMoreInfoMdal,MobileNavHeaderLinks1,HeaderNavEspotLinks,DT_SiteWide_Above_Header_nonStikcy");
		getESpot();
		lr_save_string("3", "getESpot");
		web_add_header("espotname", "CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo");
		getESpot();
	}
		

	lr_save_string("latitude&longitude&catEntryId&itemPartNumber", "latlon");
	getFavouriteStoreLocation();
	giftOptionsCmd(); //04012019
//	preferences(); //03112019 - not present in the uatlive4 - bespin
	getShipmentMethods();
 // bonusDay(); 
 	if (strcmp(lr_eval_string("{EDDPERSISTSTRING}"),"TRUE")==0 && strcmp(lr_eval_string("{isExpress}"), "true") == 0){
 	
 		sendEddPersistNodeString();
 	}

	lr_end_transaction ( lr_eval_string("{mainTransaction}"), LR_PASS) ;
	return LR_PASS;

}


int loginFromHomePage()  //loginFirst
{
	lr_think_time ( FORM_TT ) ;

	lr_save_string ( "T10_Logon_Home_MergeCart", "mainTransaction" ) ;

	web_reg_save_param_regexp ("ParamName=WC_AUTHENTICATION_resend",	"RegExp=set-cookie: WC_AUTHENTICATION_(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers",  "RequestUrl=*", LAST );
	web_reg_save_param_regexp ("ParamName=WC_ACTIVEPOINTER_resend",	"RegExp=set-cookie: WC_ACTIVEPOINTER=(.*?);",  "NotFound=warning",  SEARCH_FILTERS, "Scope=Headers", "RequestUrl=*", LAST );
	web_reg_save_param_regexp ("ParamName=WC_PERSISTENT_resend",	"RegExp=set-cookie: WC_PERSISTENT=(.*?);",  "NotFound=warning", SEARCH_FILTERS, "Scope=Headers", "RequestUrl=*", LAST );

	lr_start_transaction ( lr_eval_string("{mainTransaction}") ) ;

	lr_start_sub_transaction ("T10_Logon_Home_MergeCart logon", lr_eval_string("{mainTransaction}"));
	web_reg_save_param ( "responseCode" , "LB=\"responseCode\": \"" , "RB=\"" , "NotFound=Warning", LAST ) ; //"responseCode": "LoginSuccess"\n
	registerErrorCodeCheck();
	addHeader();
	web_add_header("Accept", "application/json");
	web_add_header("Content-Type", "application/json");
	web_add_header("Expires", "0");
	web_custom_request("logon",
		"URL=https://{api_host}/v2/account/logon",
		"Method=POST",
		"Resource=0",
//		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json",
//		"Body={\"storeId\":\"{storeId}\",\"logonId1\":\"{userEmail}\",\"logonPassword1\":\"Asdf!234\",\"rememberCheck\":true,\"rememberMe\":true,\"requesttype\":\"ajax\",\"reLogonURL\":\"TCPAjaxLogonErrorView\",\"URL\":\"TCPAjaxLogonSuccessView\",\"registryAccessPreference\":\"Public\",\"calculationUsageId\":-1,\"createIfEmpty\":1,\"deleteIfEmpty\":\"*\",\"fromOrderId\":\"*\",\"toOrderId\":\".\",\"updatePrices\":0,\"xCreditCardId\":\"\"}",
		"Body={\"storeId\":\"{storeId}\",\"logonId1\":\"{userEmail}\",\"logonPassword1\":\"Asdf!234\",\"rememberCheck\":true,\"rememberMe\":true,\"requesttype\":\"ajax\",\"reLogonURL\":\"TCPAjaxLogonErrorView\",\"URL\":\"TCPAjaxLogonSuccessView\",\"registryAccessPreference\":\"Public\",\"calculationUsageId\":-1,\"createIfEmpty\":1,\"deleteIfEmpty\":\"*\",\"fromOrderId\":\"*\",\"toOrderId\":\".\",\"updatePrices\":0,\"xCreditCardId\":\"\",\"mergeCart\":true,\"userId\":\"-1002\"}",
		LAST);
	
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
	{   isLoggedIn = 1;
		lr_end_sub_transaction("T10_Logon_Home_MergeCart logon", LR_AUTO);
	} else {
		isLoggedIn = 0;
		lr_error_message( lr_eval_string("T10_Logon_Home_MergeCart logon First- Failed with API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}, HTTP {httpReturnCode}") ) ;
		lr_end_sub_transaction("T10_Logon_Home_MergeCart logon", LR_FAIL);
		lr_end_transaction (lr_eval_string("{mainTransaction}"), LR_FAIL ) ;
		if (WRITE_TO_SUMO==1) {
	 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_logon - Failed with HTTP {httpReturnCode}, OrderId:{orderId}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
	 	}
		return LR_FAIL;
	}

//	lr_save_string("", "p_pageName");
	lr_save_string("logon", "stepName");
	web_add_header("source", "login");

	getRegisteredUserDetailsInfo();
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && strlen ( lr_eval_string ( "{userId}" ) ) > 0) //if no error code and message
	{
		isLoggedIn = 1;
	} else {
		isLoggedIn = 0;
//		lr_fail_trans_with_error( lr_eval_string("T10_Logon_getRegisteredUserDetailsInfo Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\" User: {userEmail}") ) ;
		lr_save_string("","errorMessageToFile");
		lr_save_string(lr_eval_string("T10_Logon_getRegisteredUserDetailsInfo Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\" User: {userEmail}"), "errorMessageToFile");
		lr_end_transaction ( lr_eval_string("{mainTransaction}") , LR_FAIL) ;
		writeToFile();
		if (WRITE_TO_SUMO==1) {
 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_getRegisteredUserDetailsInfo - Failed with HTTP {httpReturnCode}, OrderId:{orderId}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
	 	}
		return LR_FAIL;
	}

//	lr_save_string("latitude&longitude&catEntryId&itemPartNumber", "latlon");
//	getFavouriteStoreLocation();

	if (ESPOT_FLAG == 1)
	{	lr_save_string("2", "getESpot");
		web_add_header("espotname", "GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderPlccMoreInfoMdal,MobileNavHeaderLinks1,HeaderNavEspotLinks,DT_SiteWide_Above_Header_nonStikcy");
		getESpot();
	}
	
//	preferences(); //03112019 - not present in uatlive4 anymore
	getPointsAndOrderHistory(); //080321
	
	getAllCoupons();
	
	
	isLoggedIn = 1;
	lr_end_transaction ( lr_eval_string("{mainTransaction}"), LR_PASS) ;
	return LR_PASS;

}


void convertAndApplyPoints()
{
//	if (pointsBalance > 5000) and (converPoints random selector is positive)
//	{
	lr_think_time ( FORM_TT ) ;

	web_reg_save_param ( "myPlaceId" , "LB=<input type=\"hidden\" value=\"" , "RB=\" name=\"myPlaceId\" />", "Notfound=warning", LAST ) ;
	web_reg_save_param ( "myAccountId" , "LB=<input type=\"hidden\" value=\"" , "RB=\" name=\"accountId\" />", "Notfound=warning", LAST ) ;

	web_reg_save_param ( "addressIds" , "LB=<option id=\"" , "RB=\" value=" , "Ord=ALL" , "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "TCPMyAddressBook" , "LB=edit shipping address\" id=\"TCPMyAddressBook_" , "RB=\"" , "Ord=ALL", "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "emailAddress1" , "LB=<input type=\"hidden\" name=\"email1\" value=\"" , "RB=\" id=\"email1\" />" , "NotFound=Warning", LAST ) ;
	web_reg_save_param ( "authToken" , "LB=<input type=\"hidden\" name=\"authToken\" value=\"" , "RB=\"", "NotFound=Warning", LAST ) ;

	lr_start_transaction ( "T11_Convert Points To Coupon" ) ;

		lr_start_sub_transaction( "T11_Convert Points_Click MyPlace Rewards", "T11_Convert Points To Coupon" );

		web_url("AjaxLogonForm",
			"URL=https://{host}/shop/AjaxLogonForm?catalogId={catalogId}&myAcctMain=1&langId=-1&storeId={storeId}",
			"Mode=HTTP",
			LAST);

		lr_end_sub_transaction( "T11_Convert Points_Click MyPlace Rewards", LR_AUTO );

		web_reg_save_param ( "promocodeRewards" , "LB=\"javascript:applyLoyaltyCode('" , "RB=');", "NotFound=Warning", LAST ) ;

		lr_start_sub_transaction("T11_Convert Points_TCPRedeemLoyaltyPoints", "T11_Convert Points To Coupon" );

		web_submit_data("TCPRedeemLoyaltyPoints",
				"Action=https://{host}/shop/TCPRedeemLoyaltyPoints",    //rhy 06/02/2016
				"Method=POST",
				"Mode=HTTP",
				ITEMDATA,
				"Name=storeId", "Value={storeId}", ENDITEM,
				"Name=langId", "Value=-1", ENDITEM,
				"Name=catalogId", "Value={catalogId}", ENDITEM,
				"Name=myPlaceId", "Value={myPlaceId}", ENDITEM,
				"Name=accountId", "Value={myAccountId}", ENDITEM,
				"Name=visitorId", "Value=[CS]v1|2B2AE4E305078968-600001048004DD05[CE]", ENDITEM,
				"Name=amountToRedeem", "Value=5", ENDITEM,
				"Name=cpnWalletValue", "Value=0", ENDITEM, //this was zero in the recording
				LAST);

		lr_end_transaction("T11_Convert Points_TCPRedeemLoyaltyPoints", LR_AUTO);

	lr_end_transaction ( "T11_Convert Points To Coupon" , LR_PASS) ;

// 	promocodeRewards - Y022053295A08021
	if ( strlen(lr_eval_string("{promocodeRewards}")) == 16 ) {

		viewCart();

	    applyPromoCode(1);
	}

//	else if
//	{
//		//lr_message("No points convereted");
//		return;
//	}
} //end convertAndApplyPoints

int submitShippingBrowseFirst() //buildcart first
{
 	if (strcmp(lr_eval_string("{mixCart}"), "BOSS") != 0) //"mixCart = BOSS"
 	{
 		lr_think_time ( FORM_TT ) ;
	
		lr_save_string ( "T13_Submit_Shipping", "mainTransaction" );
	
		lr_start_transaction ( "T13_Submit_Shipping" );

		bonusDayApply();
	
		if (atoi(lr_eval_string("{addressIdPrimary_count}")) > 0 ) {
			lr_save_string(lr_eval_string("{addressIdPrimary_1}"), "addressId");
		} else {
			registerErrorCodeCheck();
			addHeader();
			web_reg_save_param("addressId", "LB=addressId\": \"", "RB=\"", "NotFound=Warning", LAST);
			web_reg_save_param_json( "ParamName=nickName", "QueryString=$..nickName", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);//06182018
		
			lr_start_sub_transaction ( "T13_Submit_Shipping_addAddress", "T13_Submit_Shipping" ) ;
			web_custom_request("addAddress", 
				"URL=https://{api_host}/v2/account/addAddress", // 
				"Method=POST", 
				"Resource=0", 
				"RecContentType=application/json", 
				"Mode=HTML", 
				"EncType=application/json", 
//				"Body={\"contact\":[{\"addressLine\":[\"{savedAddressIdPrimary}\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"{savedZipCodePrimary}\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"{savedCityPrimary}\",\"country\":\"{guestCountry}\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"1542040228203\",\"phone1\":\"2014531346\",\"email1\":\"{userEmail}\",\"phone1Publish\":\"true\",\"primary\":\"false\",\"state\":\"{savedStatePrimary}\",\"zipCode\":\"{savedZipCodePrimary}\",\"xcont_addressField2\":\"2\",\"xcont_addressField3\":\"{savedZipCodePrimary}\",\""
//				"fromPage\":\"\"}]}", 
				"Body={\"contact\":[{\"addressLine\":[\"500 Plaza Drive\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"07094\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"Secaucus\",\"country\":\"US\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"1542040228203\",\"phone1\":\"2014531346\",\"email1\":\"{userEmail}\",\"phone1Publish\":\"true\",\"primary\":\"false\",\"state\":\"NJ\",\"zipCode\":\"07094\",\"xcont_addressField2\":\"2\",\"xcont_addressField3\":\"07094\",\""
				"fromPage\":\"\"}]}", 
				LAST);
			
			if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0) 
			{
				lr_end_sub_transaction("T13_Submit_Shipping_addAddress", LR_AUTO);
			}
			else {
				lr_error_message( lr_eval_string("T13_Submit_Shipping_addAddress BuildCart 1st- Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
				lr_end_sub_transaction("T13_Submit_Shipping_addAddress", LR_FAIL);
				lr_end_transaction ("T13_Submit_Shipping", LR_FAIL ) ;
				if (WRITE_TO_SUMO==1) {
			 				lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_addAddress - Failed with HTTP {httpReturnCode}, OrderId:{orderId}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
							sendErrorToSumo(); 
			 	}
				return LR_FAIL;
			}
		}
		
		lr_save_string("T13_Submit_Shipping", "mainTransaction");
		if (updateShippingMethodSelection() == LR_FAIL) {
			lr_end_transaction ("T13_Submit_Shipping", LR_FAIL ) ;
			return LR_FAIL;
		}
		
		web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\"", "NotFound=Warning", LAST);
		web_reg_find("TEXT/IC=paymentsList", "SaveCount=paymentListFound", LAST);
		
		web_add_header("pageName", "excludeCartItems");
		if (BRIERLEY_CUSTOM_TEST == 1)
			web_add_header("recalculate", "true" );
		else
			web_add_header("recalculate", "false" );
		web_add_header("calc", "false");
		getCartDetails();
	//	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheckCount}"))>0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheckCount}"))>0 && atoi(lr_eval_string("{cartCount}"))>0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
		{
			lr_end_sub_transaction("T13_Submit_Shipping_cart", LR_AUTO);
		}
		else {
			lr_error_message( lr_eval_string("T13_Submit_Shipping_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, piAmount: {piAmount}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
			lr_end_sub_transaction("T13_Submit_Shipping_cart", LR_FAIL);
			lr_end_transaction ("T13_Submit_Shipping", LR_FAIL ) ;
			if (WRITE_TO_SUMO==1) {
		 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_cart Browse 1st - Failed with HTTP {httpReturnCode}, OrderId:{orderId}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
				sendErrorToSumo(); 
		 	}
			return LR_FAIL;
		}
	//		if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {
			if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{itemsCount}"))<=0) {
				lr_start_transaction("T50_cookie is 0 alert on cart_3473");
				lr_end_transaction ("T50_cookie is 0 alert on cart_3473", LR_FAIL ) ;
		}else{
				lr_start_transaction("T50_cookie is 0 alert on cart_3473");
				lr_end_transaction ("T50_cookie is 0 alert on cart_3473", LR_PASS ) ;
		}

	 // bonusDay();
		getShipmentMethods();
	//	constructOLPS();
		lr_start_sub_transaction ( "T13_Submit_Shipping_addSignUpEmail", "T13_Submit_Shipping" ) ;
		addHeader();
		web_custom_request("addSignUpEmail", ///api/v2/store/addSignUpEmail - Phase 2
	//		"URL=https://{api_host}/addSignUpEmail",
			"URL=https://{api_host}/v2/store/addSignUpEmail",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
			"Body={\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"emailaddr\":\"{userEmail}\",\"URL\":\"email-confirmation\",\"response\":\"accept_all::true:false\"}",
			LAST);
		lr_end_sub_transaction ( "T13_Submit_Shipping_addSignUpEmail" , LR_AUTO ) ;

		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= GETALLCOUPONS_RATIO) {
			getAllCoupons();
		}
		
		lr_end_transaction ( "T13_Submit_Shipping" , LR_PASS) ;
		
	 }
	return LR_PASS;
}


int submitShippingRegistered()  //login first
{
	if (strcmp(lr_eval_string("{mixCart}"), "BOSS") != 0) //"mixCart = BOSS"
	{	lr_think_time ( FORM_TT ) ;
		lr_save_string ( "T13_Submit_Shipping", "mainTransaction" );
	
		lr_start_transaction ( "T13_Submit_Shipping" );
	
		bonusDayApply();
//    if (strcmp(lr_eval_string("{mixCart}"), "ECOM") == 0) 
    	if (strcmp(lr_eval_string("{mixCart}"), "MIX") == 0 || strcmp(lr_eval_string("{mixCart}"), "ECOM") == 0) //07152020 - Romano -
//    	if (strcmp(lr_eval_string("{mixCart}"), "MIX") == 0) 
    	{	if (atoi(lr_eval_string("{addressIdPrimary_count}")) > 0 ) {
				lr_save_string(lr_eval_string("{addressIdPrimary_1}"), "addressId");
			} else if (strcmp(lr_eval_string("{pickUpDetail}"),"true") == 0 ) {
					lr_save_string(lr_eval_string("{addressLineId}"), "addressId");
			} else {
				registerErrorCodeCheck();
				addHeader();
				web_reg_save_param("addressId", "LB=addressId\": \"", "RB=\"", "NotFound=Warning", LAST);
				web_reg_save_param_json( "ParamName=nickName", "QueryString=$..nickName", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);//06182018
				lr_save_timestamp("epochTime", LAST );
			
				lr_start_sub_transaction ( "T13_Submit_Shipping_addAddress", "T13_Submit_Shipping" ) ;
				web_custom_request("addAddress", 
					"URL=https://{api_host}/v2/account/addAddress", 
					"Method=POST", 
					"Resource=0", 
					"RecContentType=application/json", 
					"Mode=HTML", 
					"EncType=application/json", 
					//"Body={\"contact\":[{\"addressLine\":[\"{savedAddressIdPrimary}\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"{savedZipCodePrimary}\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"{savedCityPrimary}\",\"country\":\"{guestCountry}\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"1542040228203\",\"phone1\":\"2014531346\",\"email1\":\"{userEmail}\",\"phone1Publish\":\"true\",\"primary\":\"false\",\"state\":\"{savedStatePrimary}\",\"zipCode\":\"{savedZipCodePrimary}\",\"xcont_addressField2\":\"2\",\"xcont_addressField3\":\"{savedZipCodePrimary}\",\"fromPage\":\"\"}]}", 
					"Body={\"contact\":[{\"addressLine\":[\"500 Plaza Dr\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"07094\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"Secaucus\",\"country\":\"US\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"{epochTime}\",\"phone1\":\"2014537613\",\"email1\":\"{userEmail}\",\"phone1Publish\":\"true\",\"primary\":\"false\",\"state\":\"NJ\",\"zipCode\":\"07094\",\"xcont_addressField2\":\"2\",\"xcont_addressField3\":\"07094\",\"fromPage\":\"\"}]}",
					LAST);
		
				if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
				{
					lr_end_sub_transaction("T13_Submit_Shipping_addAddress", LR_AUTO);
				} else {
					lr_error_message( lr_eval_string("T13_Submit_Shipping_addAddress - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
					lr_end_sub_transaction("T13_Submit_Shipping_addAddress", LR_FAIL);
					lr_end_transaction ("T13_Submit_Shipping", LR_FAIL ) ;
					if (WRITE_TO_SUMO==1) {
				 				lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_addAddress - Failed with HTTP {httpReturnCode}, OrderId:{orderId}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
								sendErrorToSumo(); 
				 	}
					return LR_FAIL;
				}
			}
	    }

		lr_save_string("T13_Submit_Shipping", "mainTransaction");
		if (updateShippingMethodSelection() == LR_FAIL) {
			lr_end_transaction ("T13_Submit_Shipping", LR_FAIL ) ;
			return LR_FAIL;
		}

		web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\"", "NotFound=Warning", LAST);
		web_reg_find("TEXT/IC=paymentsList", "SaveCount=paymentListFound", LAST);

		if (BRIERLEY_CUSTOM_TEST == 1)
			web_add_header("recalculate", "true" );
		else
			web_add_header("recalculate", "false" );

		web_add_header("pageName", "excludeCartItems");
		web_add_header("coupons", "false");
		web_add_header("calc", "false");
		getCartDetails();
		
//		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheckCount}"))>0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheckCount}"))>0 && atoi(lr_eval_string("{cartCount}"))>0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
			lr_end_sub_transaction(lr_eval_string("{mainTransaction}_cart"), LR_AUTO );
		else
		{
			lr_error_message( lr_eval_string("{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, cartCount: {cartCount},piAmount: {piAmount}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
			lr_end_sub_transaction(lr_eval_string("{mainTransaction}_cart"), LR_FAIL );
			lr_end_transaction ("T13_Submit_Shipping", LR_FAIL ) ;
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, OrderId:{orderId}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
			return LR_FAIL;
		}

		if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{itemsCount}"))<=0) {
			lr_start_transaction("T50_cookie is 0 alert on cart_3637");
			lr_end_transaction ("T50_cookie is 0 alert on cart_3637", LR_FAIL ) ;
		}else{
			lr_start_transaction("T50_cookie is 0 alert on cart_3637");
			lr_end_transaction ("T50_cookie is 0 alert on cart_3637", LR_PASS ) ;
		}
		
//		if (strcmp(lr_eval_string("{userCouponRemoved}"), "true") != 0)
//			deletePromoCode();
			
		getShipmentMethods();
	//	constructOLPS();
	 // bonusDay();
	//	05092019, back on JAKKU
		lr_start_sub_transaction ( "T13_Submit_Shipping_addSignUpEmail", "T13_Submit_Shipping" );
		addHeader();
		web_custom_request("addSignUpEmail", ///api/v2/store/addSignUpEmail - Phase 2
	//		"URL=https://{api_host}/addSignUpEmail",
			"URL=https://{api_host}/v2/store/addSignUpEmail",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
			"Body={\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"emailaddr\":\"{userEmail}\",\"URL\":\"email-confirmation\",\"response\":\"accept_all::false:false\"}",
			LAST);
		lr_end_sub_transaction("T13_Submit_Shipping_addSignUpEmail", LR_AUTO);

		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= GETALLCOUPONS_RATIO) {
			getAllCoupons();
		}
		
		lr_end_transaction ("T13_Submit_Shipping", LR_AUTO ) ; //14/12/2018
	}
	
	return 0;

}

int submitShippingVenmo()  //login first
{
//	if (strcmp(lr_eval_string("{mixCart}"), "BOSS") != 0) { //"mixCart = BOSS"

  	lr_think_time ( FORM_TT ) ;
	lr_save_string ( "T13_Submit_Shipping_Venmo", "mainTransaction" );
	
	lr_start_transaction ( "T13_Submit_Shipping_Venmo" );
	
//	bonusDayApply(); ??? not there in venmo
	
//    if (strcmp(lr_eval_string("{mixCart}"), "MIX") == 0) 
//    {	
//    	if (atoi(lr_eval_string("{addressIdPrimary_count}")) > 0 ) {
//			lr_save_string(lr_eval_string("{addressIdPrimary_1}"), "addressId");
//		} else if (strcmp(lr_eval_string("{pickUpDetail}"),"true") == 0 ) {
//			lr_save_string(lr_eval_string("{addressIdPrimary_1}"), "addressId");
//		} else {
			web_add_header("accept", "application/json");
			web_add_header("Content-Type", "application/json");
			registerErrorCodeCheck();
			addHeader();
			web_reg_save_param("addressId", "LB=addressId\": \"", "RB=\"", "NotFound=Warning", LAST);
			web_reg_save_param_json( "ParamName=nickName", "QueryString=$..nickName", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);//06182018
			lr_save_timestamp("epochTime", LAST );
		
			lr_start_sub_transaction ( "T13_Submit_Shipping_Venmo_addAddress", "T13_Submit_Shipping_Venmo" ) ;
			web_custom_request("addAddress", 
				"URL=https://{api_host}/v2/account/addAddress", 
				"Method=POST", 
				"Resource=0", 
				"RecContentType=application/json", 
				"Mode=HTML", 
				"EncType=application/json", 
				//"Body={\"contact\":[{\"addressLine\":[\"{savedAddressIdPrimary}\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"{savedZipCodePrimary}\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"{savedCityPrimary}\",\"country\":\"{guestCountry}\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"1542040228203\",\"phone1\":\"2014531346\",\"email1\":\"{userEmail}\",\"phone1Publish\":\"true\",\"primary\":\"false\",\"state\":\"{savedStatePrimary}\",\"zipCode\":\"{savedZipCodePrimary}\",\"xcont_addressField2\":\"2\",\"xcont_addressField3\":\"{savedZipCodePrimary}\",\"fromPage\":\"\"}]}", 
				"Body={\"contact\":[{\"addressLine\":[\"500 Plaza Dr\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"07094\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"Secaucus\",\"country\":\"US\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"{epochTime}\",\"phone1\":\"2014537613\",\"email1\":\"{userEmail}\",\"phone1Publish\":\"true\",\"primary\":\"false\",\"state\":\"NJ\",\"zipCode\":\"07094\",\"xcont_addressField2\":\"2\",\"xcont_addressField3\":\"07094\",\"fromPage\":\"\"}]}",
				LAST);
	
			if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
			{
				lr_end_sub_transaction("T13_Submit_Shipping_Venmo_addAddress", LR_AUTO);
			}
			else {
				lr_error_message( lr_eval_string("T13_Submit_Shipping_Venmo_addAddress - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
				lr_end_sub_transaction("T13_Submit_Shipping_Venmo_addAddress", LR_FAIL);
				lr_end_transaction ("T13_Submit_Shipping_Venmo", LR_FAIL ) ;
				if (WRITE_TO_SUMO==1) {
			 				lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_addAddress - Failed with HTTP {httpReturnCode}, OrderId:{orderId}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
							sendErrorToSumo(); 
			 	}
				return LR_FAIL;
			}
//		}
//    }
 
   lr_start_sub_transaction ( "T13_Submit_Shipping_Venmo_updateShippingMethodSelection", "T13_Submit_Shipping_Venmo" ) ;
	addHeader();	
	registerErrorCodeCheck();
	web_add_header("accept", "application/json");
	web_add_header("Content-Type", "application/json");

	web_reg_find("TEXT/IC=orderId", "SaveCount=apiCheck", LAST);
	web_custom_request("updateShippingMethodSelection", 
		"URL=https://{api_host}/v2/checkout/updateShippingMethodSelection", 
		"Method=PUT", 
		"Resource=0", 
		"Mode=HTML", 
		"Body={\"shipModeId\":\"901107\",\"addressId\":\"{addressId}\",\"requesttype\":\"ajax\",\"prescreen\":false,\"x_calculationUsage\":\"-1,-2,-3,-4,-5,-6,-7\"}", // TODO - need to check how shipmode varies
		LAST);
//   lr_set_debug_message(16|8|4|2,0);

//		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0) // 03142019
	if(atoi(lr_eval_string("{apiCheck}")) > 0 )
	{
		lr_end_sub_transaction("T13_Submit_Shipping_Venmo_updateShippingMethodSelection", LR_AUTO);
	}
	else {
		lr_error_message( lr_eval_string("T13_Submit_Shipping_Venmo_updateShippingMethodSelection Login 1st - Failed with mixCart:{mixCart},pickup={pickUpDetail},\"shipModeId\":\"901101\",\"addressId\":\"{addressId}\", API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
		lr_end_sub_transaction("T13_Submit_Shipping_Venmo_updateShippingMethodSelection", LR_FAIL);
		lr_end_transaction ("T13_Submit_Shipping_Venmo", LR_FAIL ) ;
	if (WRITE_TO_SUMO==1) {
 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_updateShippingMethodSelection Login 1st - Failed with HTTP {httpReturnCode}, OrderId:{orderId}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
		sendErrorToSumo(); 
 	}
		return LR_FAIL;
	}
    
		web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\"", "NotFound=Warning", LAST);
		web_reg_find("TEXT/IC=paymentsList", "SaveCount=paymentListFound", LAST);
		web_add_header("pageName", "excludeCartItems");
		if (BRIERLEY_CUSTOM_TEST == 1)
			web_add_header("recalculate", "true" );
		else
			web_add_header("recalculate", "false" );
		web_add_header("calc", "false");
		
		getCartDetails();
		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheckCount}"))>0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
//		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheckCount}"))>0 && atoi(lr_eval_string("{cartCount}"))>0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
			lr_end_sub_transaction("T13_Submit_Shipping_Venmo_cart", LR_AUTO );
		else
		{
			lr_error_message( lr_eval_string("{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, cartCount: {cartCount},piAmount: {piAmount}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
			lr_end_sub_transaction("T13_Submit_Shipping_Venmo_cart", LR_FAIL );
			lr_end_transaction ("T13_Submit_Shipping_Venmo", LR_FAIL ) ;
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_cart Login 1st - Failed with HTTP {httpReturnCode}, OrderId:{orderId}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
			return LR_FAIL;
		}

		if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {
			lr_start_transaction("T50_cookie is 0 alert on cart");
			lr_end_transaction ("T50_cookie is 0 alert on cart", LR_FAIL ) ;
		}
	//bonusDay();

	lr_start_sub_transaction ( "T13_Submit_Shipping_Venmo_addSignUpEmail", "T13_Submit_Shipping_Venmo" );
	addHeader();
	web_custom_request("addSignUpEmail", ///api/v2/store/addSignUpEmail - Phase 2
//		"URL=https://{api_host}/addSignUpEmail",
		"URL=https://{api_host}/v2/store/addSignUpEmail",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"emailaddr\":\"{userEmail}\",\"URL\":\"email-confirmation\",\"response\":\"accept_all::false:false\"}",
		LAST);
	lr_end_sub_transaction("T13_Submit_Shipping_Venmo_addSignUpEmail", LR_AUTO);
	
		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= GETALLCOUPONS_RATIO) {
			getAllCoupons();
		}

	lr_end_transaction ("T13_Submit_Shipping_Venmo", LR_AUTO ) ; //14/12/2018
//  }
	return 0;

}


int submitShipping() //former submitShippingAddressAsGuest()
{
// if (strcmp(lr_eval_string("{mixCart}"), "BOSS") != 0) //"mixCart = BOSS"
 if ( strcmp( lr_eval_string("{mixCart}"), "BOPIS") != 0 && strcmp( lr_eval_string("{mixCart}"), "BOSS") != 0)
 {	lr_think_time ( FORM_TT ) ;
	
	lr_save_string("T13_Submit_Shipping", "mainTransaction");
//	lr_save_string("cawolihe-0154@yopmail.com", "userEmail"); //07312019 - disabled

	lr_start_transaction ( "T13_Submit_Shipping" ) ;

	if (strcmp(lr_eval_string("{mixCart}"), "ECOM") != 0) {
		lr_start_sub_transaction ( "T13_Submit_Shipping_updateAddress", "T13_Submit_Shipping" ) ;
		registerErrorCodeCheck();
		addHeader();
		web_add_header( "accept", "application/json" );
		web_add_header( "nickName", lr_eval_string("{nickName}") );
		web_add_header( "Expires", "0" );
		web_add_header( "profileUpdate", "false" );
		web_custom_request("updateAddress",
			"URL=https://{api_host}/v2/wallet/updateAddress", 
			"Method=PUT",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
			//"Body={\"firstName\":\"Joe\",\"lastName\":\"User\",\"addressLine\":[\"{addressIdPrimary}\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"{savedZipCodePrimary}\"}],\"addressType\":\"ShippingAndBilling\",\"zipCode\":\"07094\",\"city\":\"{savedCityPrimary}\",\"state\":\"{savedStatePrimary}\",\"country\":\"US\",\"email1\":\"{userEmail}\",\"phone1\":\"877752{randomFourDigits}\",\"xcont_addressField3\":\"{savedZipCodePrimary}\",\"phone1Publish\":false,\"xcont_addressField2\":\"2\",\"xcont_pageName\":\"myAccount\"}",
			"Body={\"firstName\":\"Joe\",\"lastName\":\"User\",\"addressLine\":[\"500 Plaza Dr\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"07094\"}],\"addressType\":\"ShippingAndBilling\",\"zipCode\":\"07094\",\"city\":\"Secaucus\",\"state\":\"NJ\",\"country\":\"US\",\"email1\":\"{userEmail}\",\"phone1\":\"2014537613\",\"xcont_addressField3\":\"07094\",\"phone1Publish\":false}",
			LAST);
//		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && strlen(lr_eval_string("{addressId}") ,"") != 0 ) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0) // && strlen(lr_eval_string("{addressId}") ,"") != 0 ) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
		{
			lr_end_sub_transaction("T13_Submit_Shipping_updateAddress", LR_AUTO);
		}
		else {
			lr_error_message( lr_eval_string("T13_Submit_Shipping_updateAddress Guest - Failed with mixCart:{mixCart}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, timestamp: {timestamp}, hostname: {hostName}") ) ;
			lr_end_sub_transaction("T13_Submit_Shipping_updateAddress", LR_FAIL);
			lr_end_transaction ("T13_Submit_Shipping", LR_FAIL ) ;
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_updateAddress Guest- Failed with HTTP {httpReturnCode}, OrderId:{orderId}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
			return LR_FAIL;
		}
	} else {
		lr_start_sub_transaction ( "T13_Submit_Shipping_addAddress", "T13_Submit_Shipping" ) ;
		registerErrorCodeCheck();
		addHeader();
		web_reg_save_param_json( "ParamName=addressId", "QueryString=$..addressId", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);//06182018
		web_reg_save_param_json( "ParamName=nickName", "QueryString=$..nickName", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);//06182018
		web_add_header("Accept", "application/json");
		web_add_header("Expires", "0");
		lr_save_timestamp("epochTime", LAST );
		web_custom_request("addAddress", 
			"URL=https://{api_host}/v2/account/addAddress", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=application/json", 
			"Mode=HTML", 
			"EncType=application/json", 
			"Body={\"contact\":[{\"addressLine\":[\"{guestAdr1}\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"{guestZip}\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"{guestCity}\",\"country\":\"{guestCountry}\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"{epochTime}\",\"phone1\":\"2014531346\",\"email1\":\"{userEmail}\",\"phone1Publish\":\"true\",\"primary\":\"false\",\"state\":\"{guestState}\",\"zipCode\":\"{guestZip}\",\"xcont_addressField2\":\"2\",\"xcont_addressField3\":\"{guestZip}\",\"fromPage\":\"\"}]}", 
	//               {"contact":[{"addressLine":["500 Plaza Dr","",""],"attributes":[{"key":"addressField3","value":"07094"}],"addressType":"ShippingAndBilling","city":"Secaucus","country":"US","firstName":"joe","lastName":"user","nickName":"1545839262781","phone1":"2014537616","email1":"tcpperf@gmail.com","phone1Publish":"true","primary":"false","state":"NJ","zipCode":"07094","xcont_addressField2":"2","xcont_addressField3":"07094","fromPage":""}]}
			//"Body={\"contact\":[{\"addressType\":\"Shipping\",\"firstName\":\"sri","lastName":"vas","phone2":"15123456789","email1":"test4321@test.com","email2":""}]}
			LAST);
	
		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && strlen(lr_eval_string("{addressId}") ,"") != 0 ) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
		{
			lr_end_sub_transaction("T13_Submit_Shipping_addAddress", LR_AUTO);
		}
		else {
			lr_error_message( lr_eval_string("T13_Submit_Shipping_addAddress Guest - Failed with mixCart:{mixCart}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, timestamp: {timestamp}, hostname: {hostName}") ) ;
			lr_end_sub_transaction("T13_Submit_Shipping_addAddress", LR_FAIL);
			lr_end_transaction ("T13_Submit_Shipping", LR_FAIL ) ;
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_addAddress Guest- Failed with HTTP {httpReturnCode}, OrderId:{orderId}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
			return LR_FAIL;
		}
	}
	
	lr_save_string ( "T13_Submit_Shipping", "mainTransaction" ) ;
	if (updateShippingMethodSelection() == LR_FAIL) {
		lr_end_transaction ("T13_Submit_Shipping", LR_FAIL ) ;
    	return LR_FAIL;
 	}
//	constructOLPS();
		
	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\"", "NotFound=Warning", LAST); //"piAmount": "28.85",\n
	web_reg_save_param ( "nickName" , "LB=\"addressNickName\": \"" , "RB=\",\n" , "NotFound=Warning", LAST ) ; //"addressNickName": "sb_2018-06-25 14:49:23.543",\n
	
	if (BRIERLEY_CUSTOM_TEST == 1)
		web_add_header("recalculate", "true" );
	else
		web_add_header("recalculate", "false" );
	
	web_add_header("pageName", "excludeCartItems" );
	web_add_header("coupons", "false" );
	web_add_header("calc", "false");
	getCartDetails();

	if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 || strcmp(lr_eval_string("{errorMessage}") ,"") != 0  || strcmp(lr_eval_string("{cartCount}"), "0") ==0 ||  atoi(lr_eval_string("{apiCheckCount}"))<=0) 
	{	lr_error_message( lr_eval_string("T13_Submit_Shipping_cart Guest - Failed with mixCart:{mixCart}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, piAmount: {piAmount}, timestamp: {timestamp}, hostname: {hostName}") ) ;
		
		lr_end_sub_transaction("T13_Submit_Shipping_cart", LR_FAIL);			
		lr_end_transaction ("T13_Submit_Shipping", LR_FAIL ) ;
		if (WRITE_TO_SUMO==1) {
	 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_cart Guest - Failed with HTTP {httpReturnCode}, OrderId:{orderId}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
	 	}

		return LR_FAIL;
	} 
//		if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {
	if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{itemsCount}"))<=0) {
			lr_start_transaction("T50_cookie is 0 alert on cart_3928");
			lr_end_transaction ("T50_cookie is 0 alert on cart_3928", LR_FAIL ) ;
	}else{
		lr_start_transaction("T50_cookie is 0 alert on cart_3928");
		lr_end_transaction ("T50_cookie is 0 alert on cart_3928", LR_PASS ) ;
	}
	
	lr_end_sub_transaction("T13_Submit_Shipping_cart", LR_AUTO);
	
	lr_end_transaction ("T13_Submit_Shipping", LR_AUTO ) ;
  }
	
	return LR_PASS;

}

int BillingAsGuest() //former submitBillingAddressAsGuest()
{
	lr_think_time ( FORM_TT ) ;
	
	lr_save_string("T15_Submit_Billing", "mainTransaction");

	lr_start_transaction ( "T15_Submit_Billing" ) ;

	lr_start_sub_transaction("T15_Submit_Billing_BillingInformation", "T15_Submit_Billing");
	web_reg_save_param_json("ParamName=piId", "QueryString=$..piId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_add_header( "Accept", "application/json" );
	web_add_header( "Content-Type", "application/json" );
	web_add_header( "nickname", lr_eval_string("{nickName}") );
	web_add_header( "isrest", "true" );
	web_add_header( "savepayment", "false" );
	web_add_header( "expires", "0" ); 
	web_add_header( "identifier", "true" ); 
	
	addHeader();
	web_custom_request("billingInformation",
	"URL=https://{api_host}/v2/checkout/billingInformation", 
	"Method=PUT",
	"Resource=0",
	"RecContentType=application/json",
	"Mode=HTTP",
	"EncType=application/json",
	"Body={\"paymentInstruction\":[{\"piAmount\":\"{piAmount}\",\"cc_brand\":\"VISA\",\"payMethodId\":\"VISA\",\"cc_cvc\":\"111\",\"expire_month\":\"7\",\"expire_year\":\"2026\",\"account\":\"2818526303813893\",\"isDefault\":\"false\"}],\"addressId\":\"{addressId}\",\"fromPage\":\"checkout\"}",
	LAST);

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
	{
		lr_end_sub_transaction("T15_Submit_Billing_BillingInformation", LR_AUTO);
	}
	else {
		lr_error_message( lr_eval_string("T15_Submit_Billing_BillingInformation Guest - Failed with \"addressId\":\"{addressId}\", \"mixCart\":\"{mixCart}\", API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, piAmount: {piAmount}, timestamp: {timestamp}, hostname: {hostName}") ) ;
		lr_end_sub_transaction("T15_Submit_Billing_BillingInformation", LR_FAIL);
		lr_end_transaction ("T15_Submit_Billing", LR_FAIL ) ;
		if (WRITE_TO_SUMO==1) {
	 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_BillingInformation Guest - Failed with HTTP {httpReturnCode}, addressId:{addressId}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
	 	}
		return LR_FAIL;
	}

	web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"piAmount": "7.95",\n
	if (BRIERLEY_CUSTOM_TEST == 1)
		web_add_header("recalculate", "true" );
	else
		web_add_header("recalculate", "false" );

	web_add_header("pageName", "excludeCartItems");
	web_add_header("coupons", "false");
	web_add_header("calc", "true");	
	web_add_header("source", "");	
	getCartDetails();
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheckCount}"))>0 && atoi(lr_eval_string("{cartCount}"))>0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
	{
		lr_end_sub_transaction("T15_Submit_Billing_cart", LR_AUTO);
		if (strcmp(lr_eval_string("{EDDPERSISTSTRING}"),"TRUE")==0 ){
			sendEddPersistNodeString();
	 	}

		lr_end_transaction("T15_Submit_Billing", LR_AUTO);
	}
	else {
		lr_error_message( lr_eval_string("T15_Submit_Billing_cart - Failed with mixCart:{mixCart}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, piAmount: {piAmount}, timestamp: {timestamp}, hostname: {hostName}") ) ;
		lr_end_sub_transaction("T15_Submit_Billing_cart", LR_FAIL);
		lr_end_transaction ("T15_Submit_Billing", LR_FAIL ) ;
		if (WRITE_TO_SUMO==1) {
	 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_cart Guest - Failed with HTTP {httpReturnCode}, OrderId:{orderId}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
	 	}
		return LR_FAIL;
	}
	
//	if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {
	if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{itemsCount}"))<=0) {
		lr_start_transaction("T50_cookie is 0 alert on cart_4379");
		lr_end_transaction ("T50_cookie is 0 alert on cart_4379", LR_FAIL ) ;
	}else{
		lr_start_transaction("T50_cookie is 0 alert on cart_4379");
		lr_end_transaction ("T50_cookie is 0 alert on cart_4379", LR_PASS ) ;
	}
	
	return LR_PASS;
	
}
	
	

int submitBillingRegistered()
{
	lr_think_time ( FORM_TT ) ;
	
	lr_save_string ( "T15_Submit_Billing", "mainTransaction" );

	lr_start_transaction ( "T15_Submit_Billing" ) ;
	
	lr_start_sub_transaction("T15_Submit_Billing_BillingInformation", "T15_Submit_Billing");
	registerErrorCodeCheck();
	//web_reg_save_param_regexp ("ParamName=orderId",	"RegExp=\"orderId\":[ ]*\"(.*?)\"",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST );

	web_reg_save_param_json("ParamName=piId", "QueryString=$..piId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_add_header( "Accept", "application/json" );
	web_add_header( "Content-Type", "application/json" );
	web_add_header( "nickname", lr_eval_string("{nickName}") );
	web_add_header( "isrest", "true" );
	web_add_header( "savepayment", "false" );
	web_add_header( "expires", "0" ); 
	web_add_header( "identifier", "true" ); 
	
	addHeader();
	web_custom_request("billingInformation",
	"URL=https://{api_host}/v2/checkout/billingInformation", 
	"Method=PUT",
	"Resource=0",
	"RecContentType=application/json",
	"Mode=HTTP",
	"EncType=application/json",
	"Body={\"paymentInstruction\":[{\"piAmount\":\"{piAmount}\",\"cc_brand\":\"VISA\",\"payMethodId\":\"VISA\",\"cc_cvc\":\"111\",\"expire_month\":\"12\",\"expire_year\":\"2026\",\"account\":\"2818526303813893\",\"isDefault\":\"false\"}],\"addressId\":\"{addressId}\",\"fromPage\":\"checkout\"}",
	LAST);

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0  && atoi(lr_eval_string("{piId}")) > 0) //if no error code and message
	{
		lr_end_sub_transaction("T15_Submit_Billing_BillingInformation", LR_AUTO);
	} else {
		lr_error_message( lr_eval_string("T15_Submit_Billing_BillingInformation - Failed with mixCart:{mixCart}, {userEmail}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", \"billing_address_id\":\"{addressId}\",\"piAmount\":\"{piAmount}\",\"cc_cvc\":\"111\"") ) ;
		lr_end_sub_transaction("T15_Submit_Billing_BillingInformation", LR_FAIL);
		lr_end_transaction ("T15_Submit_Billing", LR_FAIL ) ;
		if (WRITE_TO_SUMO==1) {
	 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_BillingInformation - Failed with HTTP {httpReturnCode}, OrderId:{orderId}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
				sendErrorToSumo(); 
	 	}
		return LR_FAIL;
	}
	
	getPointsService();
	//bonusDay();

	if (BRIERLEY_CUSTOM_TEST == 1)
		web_add_header("recalculate", "true" );
	else
		web_add_header("recalculate", "false" );

	web_add_header("pageName", "fullOrderInfo");
	web_add_header("coupons", "true");
	web_add_header("calc", "true");
	web_add_header("source", "");	
	getCartDetails();
//		if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0) // && atoi(lr_eval_string("{apiCheckCount}"))>0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheckCount}"))>0 && atoi(lr_eval_string("{cartCount}"))>0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_cart"), LR_AUTO );
	
	else
	{
		lr_error_message( lr_eval_string("{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, cartCount: {cartCount},piAmount: {piAmount}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
		lr_end_sub_transaction(lr_eval_string("{mainTransaction}_cart"), LR_FAIL );
		lr_end_transaction ("T15_Submit_Billing", LR_FAIL ) ;
		if (WRITE_TO_SUMO==1) {
	 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_cart - Failed with HTTP {httpReturnCode}, OrderId:{orderId}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
				sendErrorToSumo(); 
	 	}
		return LR_FAIL;
	}
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= GETALLCOUPONS_RATIO) {
		getAllCoupons();
	}
	
	if (strcmp(lr_eval_string("{EDDPERSISTSTRING}"),"TRUE")==0 ){
		 sendEddPersistNodeString();
	 }

//	if (strcmp(lr_eval_string("{userCouponRemoved}"), "true") != 0)
//		deletePromoCode();


	lr_end_transaction ("T15_Submit_Billing", LR_PASS ) ;

//	if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{orderItemId_count}"))<=0) {
	if ( atoi(lr_eval_string("{parentOrderId}"))<=0 || atoi(lr_eval_string("{itemsCount}"))<=0) {
			lr_start_transaction("T50_cookie is 0 alert on cart_4504");
			lr_end_transaction ("T50_cookie is 0 alert on cart_4504", LR_FAIL ) ;
	}	else{
		lr_start_transaction("T50_cookie is 0 alert on cart_4504");
			lr_end_transaction ("T50_cookie is 0 alert on cart_4504", LR_PASS ) ;
	}
	return 0;

}

int submitOrderAsGuestaaa()
{
	return 0;
}

int submitOrderAsGuest()
{
	int rc;
	if (SUBMIT_ORDER == 0) {
		return 0;
	}
	lr_think_time ( FORM_TT ) ;
	
//	lr_set_debug_message(16|8|4|2,1);
	lr_save_string("T16_Submit_Order","mainTransaction");

	lr_start_transaction ( "T16_Submit_Order" ) ;
	lr_save_string(lr_eval_string("T16_Submit_Order_{mixCart}"), "T16_Submit_Order_Sub");

	lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_addCheckout"), "T16_Submit_Order" ) ;

	registerErrorCodeCheck();
	addHeader();
	web_add_header("Accept", "application/json");
	web_reg_save_param("encryptedEmail1", "LB=encryptedEmail1\":\"", "Rb=\",\"firstName", "NotFound=Warning", LAST);
	web_reg_save_param_json("ParamName=x_parentOrderId", "QueryString=$..parentOrderId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=x_parentOrderStatus", "QueryString=$..orderStatus", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=orderItemId", "QueryString=$.orderSummaryJson.orderItems..orderItemId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=itemUnitPrice", "QueryString=$.orderSummaryJson.orderItems..itemUnitPrice", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
//	web_reg_save_param_json("ParamName=authorizedAmount", "QueryString=$.orderSummaryJson..authorizedAmount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=authorizedAmount", "QueryString=$.orderSummaryJson..grandTotal", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=paymentMethod", "QueryString=$.orderSummaryJson..mixOrderPaymentDetails..paymentList..paymentMethod", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

	web_custom_request("addCheckout", 
		"URL=https://{api_host}/v2/checkout/addCheckout", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Mode=HTML", 
		"EncType=application/json", 
//		"Body={\"orderId\":{orderId},\"isRest\":\"true\",\"transVibesSmsPhoneNo\":null,\"chosenLocale\":\"en\"}", 
		"Body={\"orderId\":{parentOrderId},\"isRest\":\"true\",\"transVibesSmsPhoneNo\":null,\"chosenLocale\":\"en\"}", //09232019
		LAST);
	lr_save_string(lr_eval_string("{parentOrderId}"), "orderId");
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{x_parentOrderId}")) > 0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
	{
		lr_end_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_addCheckout"), LR_AUTO ) ;
	}
	else {
		lr_error_message( lr_eval_string("{T16_Submit_Order_Sub}_addCheckout Guest - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
		lr_end_sub_transaction (lr_eval_string("{T16_Submit_Order_Sub}_addCheckout"), LR_AUTO ) ;
		lr_end_transaction ("T16_Submit_Order", LR_FAIL ) ;
		if (WRITE_TO_SUMO==1) {
	 		lr_save_string( lr_eval_string("{runTime}-{T16_Submit_Order_Sub}_addCheckout - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, hostname: {hostName}"), "errorString" ) ;
			sendErrorToSumo(); 
	 	}
		return LR_FAIL;
	}

	if (GETOFFERS == 1) {
		callGetOffers();
		web_reg_save_param("couponCode", "LB=couponCode\":\"", "RB=\"", "Notfound=Warning", LAST);
	 	
		lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_getOffers"), "T16_Submit_Order" ) ;
		web_add_header("Accept", "application/json");
		web_add_header("Content-Type", "application/json");
		registerErrorCodeCheck();
		addHeader();
		web_custom_request("getOffers",
			"URL=https://{api_host}/v2/coupons/getOffers",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
			"Body={body}", 
			LAST);
	
		if (strlen(lr_eval_string("{couponCode}")) == 0)
		{	lr_end_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_getOffers"), LR_FAIL ) ;
	//		lr_error_message( lr_eval_string("{T16_Submit_Order_Sub}_getOffers - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
		}
		else {
			lr_end_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_getOffers"), LR_AUTO ) ;
		}
		writeOrderIdToFile();
	}
//	getFavouriteStoreLocation();
	
//	getAllCoupons();
		
	lr_save_string("", "p_pageName");
	getRegisteredUserDetailsInfo();

	lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_addSignUpEmail_Guest"), "T16_Submit_Order");

	addHeader();
	registerErrorCodeCheck();
	web_add_header("Accept", "application/json");

	web_custom_request("addSignUpEmail", ///api/v2/store/addSignUpEmail - Phase 2
//		"URL=https://{api_host}/addSignUpEmail",
		"URL=https://{api_host}/v2/store/addSignUpEmail",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"emailaddr\":\"{userEmail}\",\"URL\":\"email-confirmation\",\"response\":\"valid::false:false\",\"field1\":\"us_guest_checkout\"}",
		LAST);
	lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_addSignUpEmail_Guest"), LR_AUTO);

	lr_end_transaction ("T16_Submit_Order", LR_PASS ) ;//until getoffers are fixed, leave this to LR_PASS, else LR_AUTO
	
	return 0;
}

int truncSpace()
{
    extern char * strtok(char * string, const char * delimiters );
    char path[1000] = "";
    char separators[] = " ";
    char * token;
    strcpy(path, lr_eval_string("{expMonth}"));
    token = (char *)strtok(path, separators);
    if (!token) {
		return 0;
//        lr_output_message ("No tokens found in string!");
    }
    else {
    	lr_save_string(token, lr_eval_string("expMonth") );
    }

	return 0;
}

int submitOrderRegisteredaaa()
{
	return 0;
}

int submitOrderRegistered()
{
	int rc;
	if (SUBMIT_ORDER == 0) {
		return 0;
	}
	lr_think_time ( FORM_TT ) ;
//	lr_set_debug_message(16|8|4|2,1);
	//lr_continue_on_error(1);

	lr_start_transaction ( "T16_Submit_Order" ) ;
	lr_save_string(lr_eval_string("T16_Submit_Order_{mixCart}"), "T16_Submit_Order_Sub");
	lr_save_string(lr_eval_string("T16_Submit_Order"), "mainTransaction");

   	//if ( strcmp(lr_eval_string("{userCouponsApplied}"), "false") == 0 && strcmp(lr_eval_string("{stepName}"), "logon") == 0 && isLoggedIn == 1)
   	if ( strcmp(lr_eval_string("{userCouponsApplied}"), "false") == 0 && atoi( lr_eval_string("{piAmount}")) > 40 && atoi( lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_COUPON_APPLY )
		applyPromoCode();

	if (strcmp(lr_eval_string("{userCouponRemoved}"), "true") != 0 && atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_COUPON_REMOVE)
		deletePromoCode();

	if( strcmp(lr_eval_string("{isExpress}"), "true") == 0 ) { //&& strcmp( lr_eval_string("{addBopisToCart}"), "TRUE") != 0 ) {
/*
		if (strlen(lr_eval_string("{wicAddressId}")) == 0 )
			lr_save_string(lr_eval_string("{addressIdAll_1}"), "addressId");
		else
			lr_save_string(lr_eval_string("{wicAddressId}"), "addressId");
*/	
		truncSpace(); //truncate space from expMonth
		web_reg_find("TEXT/IC=\"piId\": \"", "SaveCount=apiCheck", LAST);
//		web_reg_save_param_regexp ("ParamName=orderId",	"RegExp=\"orderId\":[ ]*\"(.*?)\"",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST );
		lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_updatePaymentInstruction"), "T16_Submit_Order" ) ;
		registerErrorCodeCheck();
		addHeader();
		web_add_header( "savePayment", "false" );
		web_add_header( "Accept", "application/json" );
//		web_add_header( "Content-Type", "application/json" );
		web_custom_request("updatePaymentInstruction",
			"URL=https://{api_host}/v2/checkout/updatePaymentInstruction",
			"Method=PUT",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTML",
			"EncType=application/json",
			"Body={\"prescreen\":false,\"paymentInstruction\":[{\"expire_month\":\"{expMonth}\",\"piAmount\":\"{piAmount}\",\"payMethodId\":\"{ccType}\",\"cc_brand\":\"{ccBrand}\",\"expire_year\":\"{expYear}\",\"account\":\"{accountNo}\",\"piId\":\"{piId}\",\"cc_cvc\":\"111\",\"billing_address_id\":\"{addressId}\",\"isDefault\":\"false\"}]}",
			LAST);
		if(atoi(lr_eval_string("{apiCheck}")) == 0) {
			lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_updatePaymentInstruction"),  LR_FAIL);
//			lr_error_message( lr_eval_string("T16_Submit_Order_UpdatePaymentInstruction - Failed with  Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}, \"expire_month\":\"{expMonth}\",\"piAmount\":\"{piAmount}\",\"expire_year\":\"{expYear}\",\"piId\":\"{piId}\",\"billing_address_id\":\"{addressId}\"")   ) ;
			lr_fail_trans_with_error( lr_eval_string ("T16_Submit_Order_UpdatePaymentInstruction Failed with api Error Code:  \"{errorCode}\", Error Message: {errorMessage}, Email: {userEmail}, OrderId:{orderId}, \"expire_month\":\"{expMonth}\",\"piAmount\":\"{piAmount}\",\"expire_year\":\"{expYear}\",\"piId\":\"{piId}\",\"billing_address_id\":\"{addressId}\"") ) ;
			if (WRITE_TO_SUMO==1) {
		 		lr_save_string( lr_eval_string("{runTime}-{T16_Submit_Order_Sub}_updatePaymentInstruction - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, hostname: {hostName}"), "errorString" ) ;
				sendErrorToSumo(); 
		 	}
		} else {
			lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_updatePaymentInstruction"),  LR_AUTO);
		}
	}
	
	lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_addCheckout"), "T16_Submit_Order" ) ;

	registerErrorCodeCheck();
	addHeader();
	web_reg_save_param_json("ParamName=x_parentOrderId", "QueryString=$..parentOrderId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=x_parentOrderStatus", "QueryString=$..orderStatus", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=orderItemId", "QueryString=$.orderSummaryJson.orderItems..orderItemId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=itemUnitPrice", "QueryString=$.orderSummaryJson.orderItems..itemUnitPrice", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
//	web_reg_save_param_json("ParamName=authorizedAmount", "QueryString=$.orderSummaryJson..authorizedAmount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=authorizedAmount", "QueryString=$.orderSummaryJson..grandTotal", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=paymentMethod", "QueryString=$.orderSummaryJson..mixOrderPaymentDetails..paymentList..paymentMethod", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

	web_add_header( "Accept", "application/json" );
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
	web_custom_request("addCheckout",
		"URL=https://{api_host}/v2/checkout/addCheckout",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"chosenLocale\":\"en\",\"orderId\":{orderId},\"isRest\":\"true\",\"transVibesSmsPhoneNo\":null}", //transVibesSmsPhoneNo		
		LAST);

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
	{
		lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_addCheckout"),  LR_AUTO );
	}
	else {
		lr_error_message( lr_eval_string("{T16_Submit_Order_Sub}_addCheckout - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
		lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_addCheckout"),  LR_FAIL );
		lr_end_transaction ("T16_Submit_Order", LR_FAIL ) ;
		if (WRITE_TO_SUMO==1) {
	 		lr_save_string( lr_eval_string("{runTime}-{T16_Submit_Order_Sub}_addCheckout - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, hostname: {hostName}"), "errorString" ) ;
			sendErrorToSumo(); 
	 	}
		return LR_FAIL;
	}

	if (GETOFFERS == 1) {
		callGetOffers();
		web_reg_save_param("couponCode", "LB=couponCode\":\"", "RB=\"", "Notfound=Warning", LAST);
	 	
		lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_getOffers"), "T16_Submit_Order" ) ;
		web_add_header("Accept", "application/json");
		web_add_header("Content-Type", "application/json");
		registerErrorCodeCheck();
		addHeader();
		web_custom_request("getOffers",
			"URL=https://{api_host}/v2/coupons/getOffers",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
			"Body={body}", 
			LAST);
	
		if (strlen(lr_eval_string("{couponCode}")) == 0)
		{	lr_end_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_getOffers"), LR_FAIL ) ;
	//		lr_error_message( lr_eval_string("{T16_Submit_Order_Sub}_getOffers - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
		}
		else {
			lr_end_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_getOffers"), LR_AUTO ) ;
		}
		writeOrderIdToFile();
	}

	lr_save_string("T16_Submit_Order","mainTransaction");
	
//	getFavouriteStoreLocation();
//	getAllCoupons();

	lr_save_string("", "p_pageName");
	getRegisteredUserDetailsInfo();
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	{
		lr_end_transaction ( "T16_Submit_Order", LR_PASS ) ;
		ONLINE_ORDER_SUBMIT=1;
		return LR_PASS;
	}
	else {
		lr_fail_trans_with_error( lr_eval_string ("{T16_Submit_Order_Sub} Failed with api Error Code:  \"{errorCode}\", Error Message: {errorMessage}, Email: {userEmail}, OrderId:{orderId}") ) ;
		lr_end_transaction ( "T16_Submit_Order", LR_FAIL ) ;
		if (WRITE_TO_SUMO==1) {
	 		lr_save_string( lr_eval_string("{runTime}-{T16_Submit_Order_Sub}_getRegisteredUserDetailsInfo - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, hostname: {hostName}"), "errorString" ) ;
			sendErrorToSumo(); 
	 	}

		ONLINE_ORDER_SUBMIT=0;

		lr_start_transaction ( "T16_Submit_Order" ) ;
/*
		if (strcmp( lr_eval_string("{largeCart}"), "true") == 0 )
			lr_save_string("T16_Submit_Order_Large_Cart", "T16_Submit_Order_Sub");
		else if (strcmp( lr_eval_string("{addBopisToCart}"), "TRUE") == 0 )
			lr_save_string(lr_eval_string("T16_Submit_Order_Bopis"), "T16_Submit_Order_Sub");
		else if (strcmp( lr_eval_string("{addBossToCart}"), "TRUE") == 0 )
			lr_save_string(lr_eval_string("T16_Submit_Order_Boss}"), "T16_Submit_Order_Sub");
		else
			lr_save_string("T16_Submit_Order_Ecom", "T16_Submit_Order_Sub");
*/
		lr_save_string(lr_eval_string("T16_Submit_Order_{mixCart}"), "T16_Submit_Order_Sub");

		if( strcmp(lr_eval_string("{isExpress}"), "true") == 0 && strcmp( lr_eval_string("{addBopisToCart}"), "TRUE") != 0 ) {
			truncSpace(); //truncate space from expMonth
/*
			if (strlen(lr_eval_string("{wicAddressId}")) == 0 )
				lr_save_string(lr_eval_string("{addressIdAll_2}"), "addressId");
			else
				lr_save_string(lr_eval_string("{wicAddressId}"), "addressId");
*/			
			web_reg_find("TEXT/IC=paymentSummary", "SaveCount=apiCheck", LAST);
			registerErrorCodeCheck();
			lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_updatePaymentInstruction"), "T16_Submit_Order" ) ;
			addHeader();
			web_add_header( "savePayment", "false" );
			web_add_header( "Accept", "application/json" );
			web_add_header( "Content-Type", "application/json" );
			web_custom_request("updatePaymentInstruction",
				"URL=https://{api_host}/v2/checkout/updatePaymentInstruction",
				"Method=PUT",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTML",
				"EncType=application/json",
				"Body={\"prescreen\":false,\"paymentInstruction\":[{\"expire_month\":\"{expMonth}\",\"piAmount\":\"{piAmount}\",\"payMethodId\":\"VISA\",\"cc_brand\":\"VISA\",\"expire_year\":\"{expYear}\",\"account\":\"************1111\",\"piId\":\"{piId}\",\"cc_cvc\":\"111\",\"billing_address_id\":\"{addressId}\",\"isDefault\":\"false\"}]}",
				LAST);
			if(atoi(lr_eval_string("{apiCheck}")) == 0) {
				lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_updatePaymentInstruction"),  LR_FAIL);
				lr_error_message( lr_eval_string("T16_Submit_Order_UpdatePaymentInstruction - Failed with  Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}, \"expire_month\":\"{expMonth}\",\"piAmount\":\"{piAmount}\",\"expire_year\":\"{expYear}\",\"piId\":\"{piId}\",\"billing_address_id\":\"{addressId}\"")   ) ;
//				lr_fail_trans_with_error( lr_eval_string ("T16_Submit_Order_UpdatePaymentInstruction Failed with api Error Code:  \"{errorCode}\", Error Message: {errorMessage}, Email: {userEmail}, OrderId:{orderId}, \"expire_month\":\"{expMonth}\",\"piAmount\":\"{piAmount}\",\"expire_year\":\"{expYear}\",\"piId\":\"{piId}\",\"billing_address_id\":\"{addressId}\"") ) ;
				if (WRITE_TO_SUMO==1) {
			 		lr_save_string( lr_eval_string("{runTime}-{T16_Submit_Order_Sub}_UpdatePaymentInstruction - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, hostname: {hostName}"), "errorString" ) ;
					sendErrorToSumo(); 
			 	}
			} else {
				lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_updatePaymentInstruction"),  LR_AUTO);
			}
		}

		lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_addCheckout"), "T16_Submit_Order" ) ;
		registerErrorCodeCheck();
		addHeader();
		web_add_header( "Accept", "application/json" );
		web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);

		web_custom_request("addCheckout",
			"URL=https://{api_host}/v2/checkout/addCheckout",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
			//"Body={chosenLocale:\"en\",orderId:{orderId},isRest:\"true\",transVibesSmsPhoneNo:null}", //transVibesSmsPhoneNo		
//			"Body={\"chosenLocale\":\"en\",\"orderId\":{orderId},\"isRest\":\"true\",\"transVibesSmsPhoneNo\":null}", //transVibesSmsPhoneNo		
			"Body={chosenLocale:\"en\",orderId:{orderId},isRest:\"true\",transVibesSmsPhoneNo:null}", //010419	- romano	
			LAST);
//		lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_addCheckout"),  LR_AUTO );
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
	{
		lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_addCheckout"),  LR_AUTO );
	}
	else {
		lr_error_message( lr_eval_string("T11_Order_addCheckout - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
		lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_addCheckout"),  LR_FAIL );
		lr_end_transaction ("T16_Submit_Order", LR_FAIL ) ;
		if (WRITE_TO_SUMO==1) {
	 		lr_save_string( lr_eval_string("{runTime}-{T16_Submit_Order_Sub}_addCheckout - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}"), "errorString" ) ;
			sendErrorToSumo(); 
	 	}
		if (onlineLogging == 1) {
			web_custom_request("OnlineLogging",
				"URL=https://testproj-5d1d6.firebaseio.com/orders.json",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
				"Body={\"FailorderId\":\"{orderId}\"} ", 
				LAST);		
		}
		return LR_FAIL;
	}
	
	lr_save_string("", "p_pageName");
	getRegisteredUserDetailsInfo();

		if (GETOFFERS == 1) {
			callGetOffers();
			web_reg_save_param("couponCode", "LB=couponCode\":\"", "RB=\"", "Notfound=Warning", LAST);
		 	
			lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_getOffers"), "T16_Submit_Order" ) ;
			web_add_header("Accept", "application/json");
			web_add_header("Content-Type", "application/json");
			registerErrorCodeCheck();
			addHeader();
			web_custom_request("getOffers",
				"URL=https://{api_host}/v2/coupons/getOffers",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
				"Body={body}", 
				LAST);
		
			if (strlen(lr_eval_string("{couponCode}")) == 0)
			{	lr_end_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_getOffers"), LR_FAIL ) ;
		//		lr_error_message( lr_eval_string("{T16_Submit_Order_Sub}_getOffers - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
			}
			else {
				lr_end_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_getOffers"), LR_AUTO ) ;
			}
			writeOrderIdToFile();
		}

	lr_end_transaction ("T16_Submit_Order", LR_PASS ) ;//until getoffers are fixed, leave this to LR_PASS, else LR_AUTO
	}

	return 0;
}


int submitOrderVenmo()
{
	int rc;
	lr_think_time ( FORM_TT ) ;
	lr_start_transaction ( "T16_Submit_Order" ) ;
	lr_save_string(lr_eval_string("T16_Submit_Order_{mixCart}_venmo"), "T16_Submit_Order_Sub");
	
	lr_start_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_updateAddress"), "T16_Submit_Order");
	web_reg_save_param_json( "ParamName=addressId", "QueryString=$..addressId", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);//06182018
	web_reg_save_param_json( "ParamName=nickName", "QueryString=$..nickName", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);//06182018
	registerErrorCodeCheck();
	addHeader();
	web_add_header( "Accept", "application/json" );
	web_add_header( "nickName", lr_eval_string("{nickNameGuest}") );
	web_add_header( "Expires", "0" );
	
	web_custom_request("updateAddress",
		"URL=https://{api_host}/v2/wallet/updateAddress", 
		"Method=PUT",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"addressId\":\"{addressId}\",\"fromPage\":\"checkout\"}",
		LAST);

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
	{
		lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_updateAddress"), LR_AUTO);
	}
	else {
		lr_error_message( lr_eval_string("{T16_Submit_Order_Sub}_updateAddress - Failed with \"addressId\":\"{addressId}\", \"mixCart\":\"{mixCart}\", API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, piAmount: {piAmount}, timestamp: {timestamp}, hostname: {hostName}") ) ;
		lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_updateAddress"), LR_FAIL);
		lr_end_transaction ("T16_Submit_Order", LR_FAIL ) ;
	if (WRITE_TO_SUMO==1) {
 		lr_save_string( lr_eval_string("{runTime}-{T16_Submit_Order_Sub}_updateAddress - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
		sendErrorToSumo(); 
 	}
		return LR_FAIL;
	}
		
/*
		if (strlen(lr_eval_string("{wicAddressId}")) == 0 )
			lr_save_string(lr_eval_string("{addressIdAll_1}"), "addressId");
		else
			lr_save_string(lr_eval_string("{wicAddressId}"), "addressId");
*/	
	truncSpace(); //truncate space from expMonth
                                    
	web_reg_find("TEXT/IC=\"piId\": \"", "SaveCount=apiCheck", LAST);
	lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_addPaymentInstruction"), "T16_Submit_Order" ) ;
	registerErrorCodeCheck();
	addHeader();
	web_add_header( "identifier", "true" );
	web_add_header( "isRest", "true" );
	web_add_header( "savePayment", "false" );
	web_add_header( "nickName", lr_eval_string("Billing_{storeId}_{epochTime}" ));
	web_add_header( "Accept", "application/json" );
	web_custom_request("addPaymentInstruction",
		"URL=https://{api_host}/v2/checkout/addPaymentInstruction",
		"Method=POST",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json",
		"Body={\"paymentInstruction\":[{\"billing_address_id\":\"{addressId}\",\"piAmount\":\"{piAmount}\",\"payMethodId\":\"VENMO\",\"cc_brand\":\"VENMO\",\"account\":\"\",\"expire_month\":\"\",\"expire_year\":\"\",\"isDefault\":\"false\"}]}",
		LAST);
	
	if(atoi(lr_eval_string("{apiCheck}")) == 0) {
		lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_addPaymentInstruction"),  LR_FAIL);
		lr_fail_trans_with_error( lr_eval_string ("{T16_Submit_Order_Sub}_addPaymentInstruction Failed with api Error Code:  \"{errorCode}\", Error Message: {errorMessage}, Email: {userEmail}, OrderId:{orderId}, \"expire_month\":\"{expMonth}\",\"piAmount\":\"{piAmount}\",\"expire_year\":\"{expYear}\",\"piId\":\"{piId}\",\"billing_address_id\":\"{addressId}\"") ) ;
	if (WRITE_TO_SUMO==1) {
 		lr_save_string( lr_eval_string("{runTime}-{T16_Submit_Order_Sub}_addPaymentInstruction - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
		sendErrorToSumo(); 
 	}
	} else {
		lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_addPaymentInstruction"),  LR_AUTO);
	}
	
	lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_addCheckout"), "T16_Submit_Order" ) ;

	registerErrorCodeCheck();
	addHeader();
	web_reg_save_param_json("ParamName=x_parentOrderId", "QueryString=$..parentOrderId", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=x_parentOrderStatus", "QueryString=$..orderStatus", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=orderItemId", "QueryString=$.orderSummaryJson.orderItems..orderItemId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=itemUnitPrice", "QueryString=$.orderSummaryJson.orderItems..itemUnitPrice", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
//	web_reg_save_param_json("ParamName=authorizedAmount", "QueryString=$.orderSummaryJson..authorizedAmount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=authorizedAmount", "QueryString=$.orderSummaryJson..grandTotal", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	
	web_reg_save_param_json("ParamName=paymentMethod", "QueryString=$.orderSummaryJson..mixOrderPaymentDetails..paymentList..paymentMethod", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

	web_add_header( "Accept", "application/json" );
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
	web_custom_request("addCheckout",
		"URL=https://{api_host}/v2/checkout/addCheckout",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"orderId\":{orderId},\"isRest\":\"true\",\"transVibesSmsPhoneNo\":null,\"chosenLocale\":\"en\",\"venmoNonce\":\"\",\"venmo_device_data\":\"\",\"email\":\"{userEmail}\"}",
		LAST);

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
	{
		lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_addCheckout"),  LR_AUTO );
		if (onlineLogging == 1) {
		web_custom_request("OnlineLogging",
			"URL=https://testproj-5d1d6.firebaseio.com/orders.json",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
			"Body={\"PassorderId\":\"{orderId}\"} ", 
			LAST);
		}
	}
	else {
		lr_error_message( lr_eval_string("{T16_Submit_Order_Sub}_addCheckout - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;
		lr_end_sub_transaction(lr_eval_string("{T16_Submit_Order_Sub}_addCheckout"),  LR_FAIL );
		lr_end_transaction ("T16_Submit_Order", LR_FAIL ) ;
		if (onlineLogging == 1) {
			web_custom_request("OnlineLogging",
				"URL=https://testproj-5d1d6.firebaseio.com/orders.json",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
				"Body={\"FailorderId\":\"{orderId}\"} ", 
				LAST);		
		}
		return LR_FAIL;
	}

	callGetOffers();
	web_reg_save_param("couponCode", "LB=couponCode\":\"", "RB=\"", "Notfound=Warning", LAST);
 	
	lr_start_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_getOffers"), "T16_Submit_Order" ) ;
	web_add_header("Accept", "application/json");
	registerErrorCodeCheck();
	addHeader();
	web_custom_request("getOffers",
		"URL=https://{api_host}/v2/coupons/getOffers",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={body}", 
//		{\"personalizedOffersRequest\":{\"externalTransactionId\":274379422,\"scenario\":\"1006\",\"order\":{\"isElectiveBonus\":\"0\",\"currencyCode\":\"USD\",\"store\":{\"location\":\"0180\"},\"customer\":{\"customerLookupType\":\"1003\",\"customerLookupId\":\"TCPPERF_USD_00001031.0229@CHILDRENSPLACE.COM\"},\"coupon\":[],\"lineItem\":[{\"price\":14.98,\"orderItemId\":1073507920,\"itemUnitPrice\":14.98,\"transactionType\":\"PURCHASE\",\"quantity\":1,\"product\":{\"skuNumber\":\"2115580008\"}}]},\"paymentsList\":[{\"authorizedAmount\":15.98,\"paymentMethod\":\"VENMO\"}]}}
		LAST);

	if (strlen(lr_eval_string("{couponCode}")) == 0)
		lr_end_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_getOffers"), LR_FAIL ) ;
	else
		lr_end_sub_transaction ( lr_eval_string("{T16_Submit_Order_Sub}_getOffers"), LR_AUTO ) ;

	lr_save_string("T16_Submit_Order","mainTransaction");
	
	lr_save_string("", "p_pageName");
	getRegisteredUserDetailsInfo();
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	{
		lr_end_transaction ( "T16_Submit_Order", LR_PASS ) ;
		ONLINE_ORDER_SUBMIT=1;
		return LR_PASS;
	}
	else {
		lr_fail_trans_with_error( lr_eval_string ("{T16_Submit_Order_Sub} Failed with api Error Code:  \"{errorCode}\", Error Message: {errorMessage}, Email: {userEmail}, OrderId:{orderId}") ) ;
		lr_end_transaction ( "T16_Submit_Order", LR_FAIL ) ;
	if (WRITE_TO_SUMO==1) {
 		lr_save_string( lr_eval_string("{runTime}-{T16_Submit_Order_Sub}_getRegisteredUserDetailsInfo - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
		sendErrorToSumo(); 
 	}
		ONLINE_ORDER_SUBMIT=0;
		return LR_FAIL;
	}
	
}

void viewReservationHistory()
{
	lr_think_time ( LINK_TT );
	//web_reg_save_param("reservationHistory", "LB=Re", "RB=ation History", "NotFound=Warning", LAST);
	//web_reg_find("Text=Reservation History", "SaveCount=reservationHistory");

	lr_start_transaction("T18_ViewReservationHistory");

		lr_start_sub_transaction("T18_ViewReservationHistory_getReservationHistory", "T18_ViewReservationHistory" );
	addHeader();
	web_add_header("fromRest", "true");
	web_custom_request("getReservationHistory",
		"URL=https://{api_host}/payment/getReservationHistory",
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		"EncType=application/json",
		LAST);

	lr_end_sub_transaction("T18_ViewReservationHistory_getReservationHistory",LR_AUTO);

	lr_end_transaction("T18_ViewReservationHistory",LR_AUTO);

}

void viewPointsHistory()
{
	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO ) {
		lr_think_time ( LINK_TT );

		lr_start_transaction("T18_ViewPointsHistory");
	
		web_custom_request("TCPMyPointsHistoryView",
			"URL=https://{host}/shop/TCPMyPointsHistoryView?catalogId={catalogId}&langId=-1&storeId={storeId}",
			"Method=POST",
			"Resource=0",
			"RecContentType=text/html",
			"Mode=HTML",
			"EncType=",
			LAST);
	
		lr_end_transaction("T18_ViewPointsHistory",LR_AUTO);
	}
}


void viewOrderStatusGuest()
{
	
	//lr_continue_on_error(1);
	lr_think_time ( LINK_TT );
//	lr_save_string("bitrogue@mailinator.com", "guestEmail");
	lr_save_string("T08_ViewMyAccount_Orders_Details", "mainTransaction");

	web_convert_param("encryptedEmail1", "SourceEncoding=PLAIN", "TargetEncoding=URL", LAST );
	lr_start_transaction("T08_ViewMyAccount_Orders_Details");
	
	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO ) {
	
		lr_start_sub_transaction("T08_ViewMyAccount_Orders_Details_track-order", "T08_ViewMyAccount_Orders_Details" );
	//	web_reg_find("TEXT/IC={orderId}", "SaveCount=apiCheck", LAST);
		web_url("T18_ViewOrderStatus",
			"URL=https://{host}/{country}/track-order/{orderId}/{encryptedEmail1}",
			"TargetFrame=_self",
			"Resource=0",
			"RecContentType=text/html",
			"Mode=HTML",
			LAST);
	
	//	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
	//		lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_track-order", LR_FAIL);
	//	else
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_track-order", LR_AUTO);
	}
	if (ESPOT_FLAG == 1)
	{
		
		if (ESPOT_FLAG == 1) {
			if (isLoggedIn == 1)
			{
				lr_save_string("1", "getESpot");
				web_add_header("espotName", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_Place_Shops_espot,secondary_global_nav_Place_Shops_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot,primary_global_nav_sale_espot,secondary_global_nav_sale_espot,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Shoes_espot,secondary_global_nav_Shoes_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot");
				getESpot();
				lr_save_string("2", "getESpot");
				web_add_header("espotName", "GlobalHeaderBannerAboveHeader,MobileNavHeaderLinks1");
				getESpot();
				lr_save_string("3", "getESpot");
				web_add_header("espotName", "MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1,MyAccountMPRCCLanding,MyAccountMPRCCLandingMOB");
				getESpot();
			} else {
				lr_save_string("1", "getESpot");
				web_add_header("espotname", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_Place_Shops_espot,secondary_global_nav_Place_Shops_espot,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot,primary_global_nav_sale_espot,secondary_global_nav_sale_espot,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Shoes_espot,secondary_global_nav_Shoes_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot");
				getESpot();
				
			}
		}
	}
		lr_save_string("myAccount", "p_pageName");
		getRegisteredUserDetailsInfo();

//		getPointsService();
		
		web_reg_save_param_json("ParamName=offersSku", "QueryString=$..orderItems..variantNo", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=offersPrice", "QueryString=$..orderItems..itemPrice", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=offersQty", "QueryString=$..orderItems..qty", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");

		lr_start_sub_transaction("T08_ViewMyAccount_Orders_Details_getOrderDetails", "T08_ViewMyAccount_Orders_Details" );
		web_add_header("pageName", "fullOrderInfo");
		web_add_header("calc", "true");
		web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
		getOrderDetails();

		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_getOrderDetails", LR_FAIL);
		else
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_getOrderDetails", LR_AUTO);

/* Suprresed by PDUSI 08192021 as DOM calls are timing out due to external circuit issues
		lr_start_sub_transaction("T08_ViewMyAccount_Orders_Details_orderLookUp", "T08_ViewMyAccount_Orders_Details" );
		addHeader();
		web_add_header("orderId", lr_eval_string("{orderId}"));
		web_add_header("emailId", lr_eval_string("{guestEmail}"));
		web_reg_find("TEXT/IC=orderLookupResponse", "SaveCount=apiCheck", LAST);
		web_custom_request("tcporder/orderLookUp",
			"URL=https://{api_host}/tcporder/orderLookUp",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_orderLookUp", LR_FAIL);
		else
			lr_end_sub_transaction("T08_ViewMyAccount_Orders_Details_orderLookUp", LR_AUTO);
*/
		lr_end_transaction("T08_ViewMyAccount_Orders_Details", LR_AUTO);

}// end viewOrderStatusGuest()

void logoff()
{
	lr_save_string("T21_Logoff", "mainTransaction");

	addHeader();
	web_add_header( "Content-Type", "application/json" );
	lr_start_transaction("T21_Logoff");
	web_custom_request("logout",
		"URL=https://{api_host}/v2/account/logout",
		"Method=DELETE",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		LAST);

	if (ESPOT_FLAG == 1)
	{
		lr_save_string("1", "getESpot");
		web_add_header("espotName","GlobalHeaderBannerAboveHeader,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,coupon_help,bag_empty_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,bag_ledger_promo_banner,gift_services_espot,CheckoutConfirmationBanner,CheckoutConfirmationMPRPromo,MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1");
		web_add_header("deviceType","desktop");
		getESpot();
	}
	lr_save_string("homepage", "p_pageName");
	getRegisteredUserDetailsInfo();

	lr_end_transaction("T21_Logoff", LR_AUTO);

}

int createAccount() //former registerUser()
{
	lr_think_time ( FORM_TT ) ;

	//lr_continue_on_error(1);

	lr_start_transaction ( "T17_Register_User" ) ;

	//web_reg_save_param("userId", "LB=\"userId\": \"", "RB=\"", "NotFound=Warning", LAST); //"userId": "202607181"\n // when it goes thru
	lr_save_string(lr_eval_string("bitrougue{randomChar}{randomChar}{randomChar}{randomNineDigits}{randomFourDigits}@mailinator.com"), "userEmail");
//	lr_save_string(lr_eval_string("pumpdata@manh.com"), "userEmail");
	
	addHeader();
	registerErrorCodeCheck();
	web_add_header("Accept", "application/json");
	web_add_header("Expires", "0");
	web_reg_find("TEXT/IC=registrationSuccess", "SaveCount=apiCheck", LAST);
	lr_start_sub_transaction ( "T17_Register_User_addCustomerRegistration", "T17_Register_User" );
	web_custom_request("addCustomerRegistration",
		"URL=https://{api_host}/v2/wallet/addCustomerRegistration",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
		"Body={\"firstName\":\"Joe\",\"lastName\":\"User\",\"zipCode\":\"07094\",\"logonId\":\"{userEmail}\",\"logonPassword\":\"Asdf!234\",\"phone1\":\"2014531513\",\"rememberCheck\":true,\"rememberMe\":true,\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"storeId\":\"{storeId}\"}",
		LAST);
//	lr_end_sub_transaction("T17_Register_User_addCustomerRegistration", LR_AUTO);
//		"Body={\"firstName\":\"Joe\",\"lastName\":\"User\",\"zipCode\":\"07094\",\"logonId\":\"TCPPERF_{storeId}_Guest_{emailVerification}@gmail.com\",\"logonPassword\":\"Asdf!234\",\"phone1\":\"2014531513\",\"rememberCheck\":true,\"rememberMe\":true,\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"storeId\":\"{storeId}\"}",

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 && atoi(lr_eval_string("{apiCheck}")) == 1) //if no error code and message
		lr_end_sub_transaction("T17_Register_User_addCustomerRegistration", LR_AUTO);
	else {
		lr_end_sub_transaction("T17_Register_User_addCustomerRegistration", LR_FAIL);
		lr_end_transaction ( "T17_Register_User", LR_AUTO ) ;
		return LR_FAIL;
	}
	
	lr_save_string("T17_Register_User", "mainTransaction");

	getRegisteredUserDetailsInfo();

//	preferences(); 03112019
	
	if (ESPOT_FLAG == 1)
	{
		lr_save_string("1", "getESpot");
		web_add_header("espotName", "espotName:GlobalHeaderBannerAboveHeader,MobileNavHeaderLinks1");
		getESpot();
//		getESpot();
	}

	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	{
		lr_end_transaction ( "T17_Register_User", LR_AUTO ) ;
		return LR_PASS;
	}
	else {
		lr_fail_trans_with_error( lr_eval_string ("T17_Register_User Failed with Error Code:  \"{errorCode}\", Error Message: {errorMessage}") ) ;
		lr_end_transaction ( "T17_Register_User", LR_FAIL ) ;
	}

	//lr_continue_on_error(0);

	return 0;
}


void buildCartDrop(int userProfile)
{
	int cartTypeRatio = 0;
	lr_think_time ( FORM_TT ) ;
	lr_save_string("false", "largeCart");

	target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_DROP_CART_SIZE}"));

	lr_start_transaction("T20_New_Cart");
	lr_end_transaction("T20_New_Cart", LR_PASS);

	if ( userProfile == 0 && atoi(lr_eval_string("{cartCount}")) == 1) { //	userProfile == 0 is buildCartFirst before login, userProfile == 1 is loginFirst before building the cart
		orderItemIdscount = 0;
	}
	else {
		if (atoi(lr_eval_string("{cartCount}")) == 1) {
			orderItemIdscount = 0;
		}
		else
			orderItemIdscount = atoi(lr_eval_string("{cartCount}"));//cartCount
	}

	if (  orderItemIdscount < target_itemsInCart ) {

	    target_itemsInCart = target_itemsInCart - orderItemIdscount ;

//		cartTypeRatio =  atoi(lr_eval_string("{RANDOM_PERCENT}")); ///determine the type of cart to build

		for(index_buildCart=0; index_buildCart < target_itemsInCart ; index_buildCart++)
		{
			topNav();
/*
			if ( atoi( lr_eval_string( "{productId_count}" )) ==0 )
			{
				index_buildCart--;
				continue;

			} else {
*/
			cartTypeRatio =  atoi(lr_eval_string("{RANDOM_PERCENT}"));
			
				if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ADDTOCART_PLP )
				{
					if (ATC_FLAG == 1) {
						if (addToCartFromPLP_UI() == LR_FAIL) {
							atc_Stat = 1;
						}
					} else {
						if (addToCartFromPLP_DF() == LR_FAIL) {
							atc_Stat = 1;
						}
					}

				} else {
//					if (productDisplay() != LR_FAIL) {
					
						if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ADDTOCART_LIMIT ) { //01/25/2019 romano - this is to reduce the addtocart by 30%
							if (ATC_FLAG == 1) {
								if (productDisplay() != LR_FAIL) {
									if (addToCart_UI() == LR_FAIL) {
										atc_Stat = 1;
									}
								} else {
									atc_Stat = 1;
								}
							} else {
								productDisplay();
								if (addToCart_DF() == LR_FAIL) {
									atc_Stat = 1;
								}
							}
						} else {
							atc_Stat = 0;
						}
//					} else {
//						atc_Stat = 1;
//					}
				}

				if (atc_Stat == 1) { //0-Pass 1-Fail //if the addtocart failed, set the index_buildCart to original number
					index_buildCart--;
					lr_start_transaction("T40_Failed_Add_Cart");
					lr_end_transaction("T40_Failed_Add_Cart", LR_PASS);
					continue; //04092018
				} else {

					if ( atoi( lr_eval_string( "{cartCount}" )) >= target_itemsInCart ) // without this == (11.4, 14,298), with this == 5.7
						break;
				}
//			}
		} // end for loop to add o cart

	}

} // end buildCartDrop


void buildCartCheckout(int userProfile)
{
	int cartTypeRatio = 0;
	lr_think_time ( FORM_TT ) ;
	lr_save_string("false", "largeCart");
	
	target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_CART_SIZE}"));
	
	lr_start_transaction("T20_New_Cart");
	lr_end_transaction("T20_New_Cart", LR_PASS);

	if ( userProfile == 0 && atoi(lr_eval_string("{cartCount}")) == 1) { //	userProfile == 0 is buildCartFirst before login, userProfile == 1 is loginFirst before building the cart
		orderItemIdscount = 0;
	}
	else {
		if (atoi(lr_eval_string("{cartCount}")) == 1) {
			orderItemIdscount = 0;
		}
		else
			orderItemIdscount = atoi(lr_eval_string("{cartCount}"));//cartCount
	}
	
	if (  orderItemIdscount < target_itemsInCart ) {

	    target_itemsInCart = target_itemsInCart - orderItemIdscount ;

//		cartTypeRatio =  atoi(lr_eval_string("{RANDOM_PERCENT}")); ///determine the type of cart to build  //Used to be like this Pavan Dusi changed it on 03/26/2019

		for(index_buildCart=0; index_buildCart < target_itemsInCart ; index_buildCart++)
		{
			topNav();
/*
			if ( atoi( lr_eval_string( "{productId_count}" )) ==0 )
			{
				index_buildCart--;
				continue;

			} else {
*/
			cartTypeRatio =  atoi(lr_eval_string("{RANDOM_PERCENT}"));
				if ( cartTypeRatio <= ECOMM_CART_RATIO ) {
					if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ADDTOCART_PLP )
					{
						if (ATC_FLAG == 1) {
							if (addToCartFromPLP_UI() == LR_FAIL) {
								atc_Stat = 1;
							}
						} else {
							if (addToCartFromPLP_DF() == LR_FAIL) {
								atc_Stat = 1;
							}
						}

					} else {
//						if (productDisplay() != LR_FAIL) {
						
							if (ATC_FLAG == 1) {
								if (productDisplay() != LR_FAIL) {
									if (addToCart_UI() == LR_FAIL) {
										atc_Stat = 1;
									}
								} else {
									atc_Stat = 1;
								}
							} else {
								productDisplay();							
								if (addToCart_DF() == LR_FAIL) {
									atc_Stat = 1;
								}
							}
//						} else {
//							atc_Stat = 1;
//						}
					}
				}
				else { 
//					if (strcmp( lr_eval_string("{storeId}") , "10151") == 0) {
						productDisplayBOPIS();
						addToCartMixed();
/*					}
					else {
						if (productDisplay() != LR_FAIL) {
							if (addToCart() == LR_FAIL) {
								atc_Stat = 1;
							}
						} else {
							atc_Stat = 1;
						}
					}
*/				}

				if (atc_Stat == 1) { //0-Pass 1-Fail //if the addtocart failed, set the index_buildCart to original number
					index_buildCart--;
					lr_start_transaction("T40_Failed_Add_Cart");
					lr_end_transaction("T40_Failed_Add_Cart", LR_PASS);
					continue; //04092018
				} else {

					if ( atoi( lr_eval_string( "{cartCount}" )) >= target_itemsInCart ) // without this == (11.4, 14,298), with this == 5.7
						break;
				}
//			}
		} // end for loop to add o cart

		//viewCart(); //0425 disabled
	}

} // end buildCartCheckout


int pickupInStore()
{
	lr_think_time ( FORM_TT ) ;
//	if (atoi(lr_eval_string("{atc_catentryId}")) <=0 )
//		return LR_FAIL;
	
	if (atoi(lr_eval_string("{itemPartNumbers_count}")) > 0 )
		lr_save_string(lr_paramarr_random("itemPartNumbers"), "itemPartNumber");
		
	lr_start_transaction ( "T05_PickupInStore" ) ;
//	lr_save_string(lr_eval_string("{randCatEntryId_BOPIS}"), "BOPIS");
	lr_save_string(lr_eval_string("{atc_catentryId}"), "BOPIS"); //romano 12/27
	lr_save_string("T05_PickupInStore", "mainTransaction");
	web_add_header("itemPartNumber", lr_eval_string("{itemPartNumber}")); 
	web_add_header("catentryId", lr_eval_string("{atc_catentryId}")); // romano 12/27/2018
	web_add_header("Accept", "application/json");

	lr_start_sub_transaction("T05_PickupInStore_getUserBopisStores", lr_eval_string("{mainTransaction}"));
	addHeader();
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
	web_url("getUserBopisStores",
//		"URL=https://{api_host}/tcporder/getUserBopisStores",
		"URL=https://{api_host}/v2/bopis/getUserBopisStores",
		"Resource=0",
		"RecContentType=application/json",
		LAST);

	if (atoi(lr_eval_string("{apiCheck}")) == 0 ) {
		lr_end_sub_transaction("T05_PickupInStore_getUserBopisStores", LR_FAIL);
		lr_end_transaction("T05_PickupInStore", LR_FAIL);
		return LR_FAIL;
	} else {
		lr_end_transaction("T05_PickupInStore_getUserBopisStores", LR_AUTO);

//		lr_save_string("40.7860123", "latitude");
//		lr_save_string("-74.009447", "longitude");
		//randCatEntryId_BOPIS
		

/*		02142019 - disabled by romano
		web_add_header("action", "get");
		web_add_header("Accept", "application/json");
		web_add_header("itemPartNumber", lr_eval_string("{itemPartNumber}")); 
		lr_save_string("{","CheckString");
		lr_start_sub_transaction("T05_PickupInStore_getFavouriteStoreLocation", lr_eval_string("{mainTransaction}"));
		addHeader();
		web_custom_request("getFavouriteStoreLocation", 
			//"URL=https://{api_host}/v2/store/getFavouriteStoreLocation",
			"URL=https://{api_host}/v2/store/getFavouriteStoreLocation?latitude&longitude&catEntryId={BOPIS}&itemPartNumber={itemPartNumber}",
			"Method=GET",
			"RecContentType=text/html",
			"Mode=HTML",
			LAST);
		
		lr_end_sub_transaction(("T05_PickupInStore_getFavouriteStoreLocation"), LR_AUTO );
	// end of getFavouriteStoreLocation API
*/		
		lr_save_string ( lr_eval_string ("{randCatEntryId_BOPIS}"), "BOPIS");
//0702		
		addHeader();
/*02/09/21		web_add_header("sType","BOPIS");
		web_add_header("country",lr_eval_string("{country}"));
//		web_add_header("catentryId", lr_eval_string("{randCatEndtryId_pdp}") );
//		web_add_header("latitude", lr_eval_string("{randCatEntryId_Lat}") );
//		web_add_header("longitude", lr_eval_string("{randCatEntryId_Lon}"));
		web_add_header("catentryId", lr_eval_string("{BOPIS}") );
		web_add_header("itemPartNumber", lr_eval_string("{itemPartNumber}")); // 
		web_add_header("latitude", "40.792110"); //07093
		web_add_header("longitude", "-74.194946");
		web_add_header("Accept", "application/json");
		
		lr_save_string("0", "storeLocId_count");
		web_reg_save_param("numStoresHavingItem","LB=numStoresHavingItem\": ", "RB=,", "Notfound=Warning", LAST);
		web_reg_save_param("storeLocId","LB=storeUniqueID\": \"", "RB=\"", "Notfound=Warning", "ORD=All", LAST);
		web_reg_save_param("qty","LB=qty\": \"", "RB=\"", "Notfound=Warning", "ORD=All", LAST);
		web_reg_save_param("p_longitude","LB=longitude\": \"", "RB=\"", "Notfound=Warning", "ORD=All", LAST);
		web_reg_save_param("p_latitude","LB=latitude\": \"", "RB=\"", "Notfound=Warning", "ORD=All", LAST);
		
				//	
		//isStoreBOSSEligible": "1",
			web_reg_save_param("BOSSEligible","LB=isStoreBOSSEligible\": \"", "RB=\"", "Notfound=Warning", "ORD=All", LAST);
//02/09/21 - need to replace with getBOPISInvetoryDetails
//		if (strcmp(lr_eval_string("{pickupType}"), "bopis") == 0)
//			lr_save_string(lr_eval_string("https://{api_host}/v2/vendor/getStoreAndProductInventoryInfo?latitude=40.792110&longitude=-74.194946&catentryId={BOPIS}&distance=75&country=US&sType=BOPIS&itemPartNumber={itemPartNumber}"), "URL");
//		else
//			lr_save_string(lr_eval_string("https://{api_host}/v2/vendor/getStoreAndProductInventoryInfo?latitude=40.7322535&longitude=-73.987410&catentryId={BOPIS}&distance=75&country=US&sType=BOPIS&itemPartNumber={itemPartNumber}"), "URL");
//
//		lr_start_sub_transaction("T05_PickupInStore_getStoreandProductInventoryInfo", lr_eval_string("{mainTransaction}"));
//		
//		web_url("getBOPISInvetoryDetails",
//            "URL={URL}",
//			"Resource=0",
//			"RecContentType=application/json",
//			LAST);
*/
		lr_save_string(lr_eval_string("https://{api_host}/v2/vendor/getBOPISInvetoryDetails"), "URL");
			
		lr_start_sub_transaction("T05_PickupInStore_getBOPISInvetoryDetails", lr_eval_string("{mainTransaction}"));
		web_add_header("Accept", "application/json");
		web_add_header("Content-Type", "application/json");
		
		web_custom_request("getBOPISInvetoryDetails",
			"URL={URL}", //
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"Body={\"availabilityRequest\":{\"viewName\":\"US BOPIS\",\"availabilityCriteria\":{\"facilityNames\":{\"facilityName\":[\"0735\"]},\"itemNames\":{\"itemName\":[\"3018236008\"]},\"attributeMapping\":[{\"variantNo\":\"3018236008\",\"itemPartNumber\":\"00194936294463\"}]}}}",
			LAST);
		

//		if (atoi(lr_eval_string("{numStoresHavingItem}")) <= 0 ) { //03052018 - Romano
/*		if (atoi(lr_eval_string("{storeLocId_count}")) <= 0 ) {
			lr_end_sub_transaction("T05_PickupInStore_getStoreandProductInventoryInfo", LR_FAIL);
			lr_end_transaction("T05_PickupInStore", LR_FAIL);// LR_AUTO
			return LR_FAIL;
		} else {
			lr_end_sub_transaction("T05_PickupInStore_getStoreandProductInventoryInfo", LR_AUTO);
			lr_end_transaction("T05_PickupInStore", LR_AUTO);
		}
*/			lr_end_sub_transaction("T05_PickupInStore_getBOPISInvetoryDetails", LR_AUTO);
			lr_end_transaction("T05_PickupInStore", LR_AUTO);

	}

	return LR_PASS;

}

int pickupInStoreFromBag()
{
	int index;
	lr_think_time ( FORM_TT ) ;
	if (atoi(lr_eval_string("{itemCatentryId_count}")) <=0 )
		return LR_FAIL;
	
	if (atoi(lr_eval_string("{itemPartNumbers_count}")) <=0 )
		return LR_FAIL;
	
	index = rand ( ) % lr_paramarr_len( "itemPartNumbers" ) + 1 ;	
	
/*	itemPartNumbers
	variantNo_1
	mixCart = ECOM	
	orderItemIds_1	
	itemCatentryId_	
 */	
	lr_start_transaction ( "T05_PickupInStore" ) ;
	lr_save_string(lr_paramarr_idx("itemCatentryId", index), "BOPIS");
	lr_save_string(lr_paramarr_idx("itemPartNumbers", index), "itemPartNumber");
	lr_save_string(lr_paramarr_idx("orderItemIds", index), "orderItemId");
	lr_save_string(lr_paramarr_idx("variantNo", index), "variantNo");
	
	lr_save_string("T05_PickupInStore", "mainTransaction");
	
	web_add_header("itemPartNumber", lr_eval_string("{itemPartNumber}"));
	web_add_header("catentryId", lr_eval_string("{BOPIS}"));
	web_add_header("Accept", "application/json");

	lr_start_sub_transaction("T05_PickupInStore_getUserBopisStores", lr_eval_string("{mainTransaction}"));
	addHeader();
	web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
	web_url("getUserBopisStores",
//		"URL=https://{api_host}/tcporder/getUserBopisStores",
		"URL=https://{api_host}/v2/bopis/getUserBopisStores",
		"Resource=0",
		"RecContentType=application/json",
		LAST);

	if (atoi(lr_eval_string("{apiCheck}")) == 0 ) {
		lr_end_sub_transaction("T05_PickupInStore_getUserBopisStores", LR_FAIL);
		lr_end_transaction("T05_PickupInStore", LR_FAIL);
		return LR_FAIL;
	} else {
		lr_end_sub_transaction("T05_PickupInStore_getUserBopisStores", LR_AUTO);

//		lr_save_string ( lr_eval_string ("{randCatEntryId_BOPIS}"), "BOPIS");
//		getBOPISInvetoryDetails();		
//0702		
		addHeader();
		web_add_header("sType","BOPIS");
		web_add_header("country",lr_eval_string("{country}"));
		web_add_header("catentryId", lr_eval_string("{BOPIS}") );
		web_add_header("itemPartNumber", lr_eval_string("{itemPartNumber}"));
		web_add_header("latitude", "40.792110"); //07093
		web_add_header("longitude", "-74.194946");
		web_add_header("Accept", "application/json");
		
		lr_save_string("0", "storeLocId_count");
		web_reg_save_param("numStoresHavingItem","LB=numStoresHavingItem\": ", "RB=,", "Notfound=Warning", LAST);
		web_reg_save_param("storeLocId","LB=storeUniqueID\": \"", "RB=\"", "Notfound=Warning", "ORD=All", LAST);
		web_reg_save_param("qty","LB=qty\": \"", "RB=\"", "Notfound=Warning", "ORD=All", LAST);
		web_reg_save_param("p_longitude","LB=longitude\": \"", "RB=\"", "Notfound=Warning", "ORD=All", LAST);
		web_reg_save_param("p_latitude","LB=latitude\": \"", "RB=\"", "Notfound=Warning", "ORD=All", LAST);
		
		lr_save_string(lr_eval_string("https://{api_host}/v2/vendor/getStoreAndProductInventoryInfo?latitude=40.792110&longitude=-74.194946&catentryId={BOPIS}&distance=75&country=US&sType=BOPIS&itemPartNumber={itemPartNumber}"), "URL");

		lr_start_sub_transaction("T05_PickupInStore_getStoreandProductInventoryInfo", lr_eval_string("{mainTransaction}"));
		
		web_url("getStoreandProductInventoryInfo",
            "URL={URL}",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
/*			
//		if (atoi(lr_eval_string("{numStoresHavingItem}")) <= 0 ) { //03052018 - Romano 
		if (atoi(lr_eval_string("{storeLocId_count}")) <= 0 ) {
			lr_end_sub_transaction("T05_PickupInStore_getStoreandProductInventoryInfo", LR_FAIL);
			lr_end_transaction("T05_PickupInStore", LR_FAIL);// LR_AUTO
			return LR_FAIL;
		} else {
*/			lr_end_sub_transaction("T05_PickupInStore_getStoreandProductInventoryInfo", LR_AUTO);
			lr_end_transaction("T05_PickupInStore", LR_AUTO);
//		}

	}

	return LR_PASS;

}


int addBopisToCartUI()
{
	char *strLocIdString ;	
	int iCount = 0;
	
	if (atoi(lr_eval_string("{storeLocId_count}")) == 0)
		return LR_FAIL;
	
	for (iCount = 0; iCount <= atoi(lr_eval_string("{storeLocId_count}")); iCount++)
	{
		if (atoi( lr_paramarr_idx ("qty", iCount)) > 0)
		    break;
	}
	if (iCount > atoi(lr_eval_string("{storeLocId_count}")))
		lr_save_string( lr_paramarr_idx ("storeLocId", 1), "storeLocId");
	else
		lr_save_string( lr_paramarr_idx ("storeLocId", iCount), "storeLocId");
/*	
	lr_save_string( "110961", "storeLocId");
	lr_save_string( "826742", "catEntryId");
*/
//	lr_save_string( "2063538002", "variantNo");
//	lr_save_string( "00889705465747", "itemPartNumber");
	lr_save_string("","mainTransaction");
	lr_save_string(lr_eval_string("T05_Add To Cart"), "mainTransaction");
	lr_think_time ( LINK_TT ) ;
	lr_start_transaction(lr_eval_string("{mainTransaction}"));

	web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
	web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n
//	web_reg_save_param("orderId", "LB=\"orderId\": \"", "RB=\"", "NotFound=Warning", LAST); //"orderId": "8006336024"
//	web_reg_save_param("orderId" , "LB=\"orderId\": [\"" , "RB=\"]" , "NotFound=Warning",  LAST ) ;
	web_reg_save_param_regexp ("ParamName=orderId",	"RegExp=\"orderId\":[ ]*\"(.*?)\"",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST );
	web_reg_save_param("orderItemDescription", "LB=\"orderItemDescription\": \"", "RB=\"", "NotFound=Warning", LAST); //"orderItemDescription": "8006336025"
	web_reg_save_param("cartCount", "LB=set-cookie: cartItemsCount=", "RB=; Path=/; Domain", "NotFound=Warning", LAST); //set-cookie: cartItemsCount=1; Path=/; Domain

	lr_start_sub_transaction (lr_eval_string("{mainTransaction}_createBopisOrder"), lr_eval_string("{mainTransaction}") ) ;
	addHeader();
	registerErrorCodeCheck();
	web_custom_request("createBopisOrder",
		"URL=https://{api_host}/v2/bopis/createBopisOrder", //
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
//		"Body={\"storeLocId\":\"{storeLocId}\",\"quantity\":\"1\",\"catEntryId\":\"{BOPIS}\",\"isRest\":\"false\",\"pickupType\":\"{pickupType}\",\"variantNo\":\"{VariantNo}\",\"itemPartNumber\":\"{itemPartNumber}\"}",
		"Body={{createBopisOrder}}",
		LAST);
	
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
	{	
		lr_end_sub_transaction (lr_eval_string("{mainTransaction}_createBopisOrder"), LR_AUTO) ;
	}
	else
	{
		lr_fail_trans_with_error(lr_eval_string("{mainTransaction}_createBopisOrder Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		lr_end_sub_transaction (lr_eval_string("{mainTransaction}_createBopisOrder"), LR_AUTO) ;
		lr_end_transaction(lr_eval_string("{mainTransaction}"), LR_FAIL);
	if (WRITE_TO_SUMO==1) {
 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_createBopisOrder - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
		sendErrorToSumo(); 
 	}
		return LR_FAIL;
	}

	web_reg_save_param_json("ParamName=offersSku", "QueryString=$..orderItems..variantNo", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=offersPrice", "QueryString=$..orderItems..itemPrice", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=offersQty", "QueryString=$..orderItems..qty", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
 
	web_reg_save_param_json("ParamName=itemPartNumber", "QueryString=$.orderItems..itemPartNumber", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=variantNo", "QueryString=$..orderItems..variantNo", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=productidWL", "QueryString=$..orderItems..productid", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json("ParamName=orderItemType", "QueryString=$..orderItems..orderItemType", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	
	lr_start_sub_transaction (lr_eval_string("{mainTransaction}_getOrderDetails"), lr_eval_string("{mainTransaction}") ) ;
	web_add_header("pageName", "fullOrderInfo");
	//web_add_header("recalculate", "true");
	if (BRIERLEY_CUSTOM_TEST == 1)
		web_add_header("recalculate", "true" );
	else
		web_add_header("recalculate", "false" );
	web_add_header("calc", "true");
	web_reg_save_param("itemName", "LB=\"variantNo\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); 
	web_reg_save_param("stLocId", "LB=\"stLocId\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); 
	web_reg_save_param("piAmountonUpdateQuantity", "LB=\"piAmount\": \"", "RB=\"", "NotFound=Warning", LAST); //"piAmount": "28.85",\n
//	web_reg_save_param_regexp ("ParamName=cartCount",	"RegExp=[Cc]artCount\":[ ]*(.*?),",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST );
		web_reg_save_param_json("ParamName=cartCount", "QueryString=$..cartCount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		getOrderDetails();

		lr_end_sub_transaction (lr_eval_string("{mainTransaction}_getOrderDetails"), LR_AUTO) ;

	if (strcmp(lr_eval_string("{storeId}"), "10151") == 0 ) 
	{	lr_save_string("0006", "newStrLocId");
//		strLocIdString = lr_eval_string("{storeLocId}"); // get rid of the first 2 characters/numbers
//		lr_param_sprintf ("newStrLocId", "%c%c%c%c", strLocIdString[3],strLocIdString[4],strLocIdString[5],strLocIdString[6]);
		
		if(atoi(lr_eval_string("{itemName_count}")) > 0) {
			lr_save_string(lr_eval_string("{\"availabilityRequest\":{\"viewName\":\"US BOPIS\",\"availabilityCriteria\":{\"facilityNames\":{\"facilityName\":[\"{newStrLocId}\"]},\"itemNames\":{\"itemName\":["), "body");
			for (i=1; i<=atoi(lr_eval_string("{itemName_count}")); i++) 
			{
				lr_save_string(lr_paramarr_idx("itemName",i), "itemNamei");
				if (i == atoi(lr_eval_string("{itemName_count}")))
					lr_param_sprintf("body", "%s%s", lr_eval_string("{body}"), lr_eval_string("\"{itemNamei}\"") );
				else
					lr_param_sprintf("body", "%s%s", lr_eval_string("{body}"), lr_eval_string("\"{itemNamei}\",") );
			}
			
			//
				lr_param_sprintf("body", "%s%s", lr_eval_string("{body}"), "]},\"attributeMapping\":[");
			if(atoi(lr_eval_string("{itemPartNumber_count}")) > 0) {
//				lr_save_string(lr_eval_string("{\"availabilityRequest\":{\"viewName\":\"US BOPIS\",\"availabilityCriteria\":{\"facilityNames\":{\"facilityName\":[\"{newStrLocId}\"]},\"itemNames\":{\"itemName\":["), "body");
				for (i=1; i<=atoi(lr_eval_string("{itemPartNumber_count}")); i++) 
				{
					lr_save_string(lr_paramarr_idx("itemName",i), "itemNamei");
					lr_save_string(lr_paramarr_idx("itemPartNumber",i), "itemPartNumberi");
					if (strcmp(lr_paramarr_idx("orderItemType",i), "BOPIS") == 0 || strcmp(lr_paramarr_idx("orderItemType",i), "BOSS") == 0) 
					{
					lr_param_sprintf("body", "%s%s", lr_eval_string("{body}"), lr_eval_string("{\"variantNo\":\"{itemNamei}\",\"itemPartNumber\":\"{itemPartNumberi}\"}") );
						if (atoi(lr_eval_string("{itemPartNumber_count}"))!=i)
						{
							lr_param_sprintf("body", "%s%s", lr_eval_string("{body}"), ",") ;
						}		
					}
				}
				}
				
				lr_param_sprintf("body", "%s%s", lr_eval_string("{body}"), "]}}}");		
				//end of code 
				
//				if (strcmp(lr_eval_string("{pickupType}"), "bopis") == 0)// 
//				{
/*				
				lr_start_sub_transaction (lr_eval_string("{mainTransaction}_getBOPISInvetoryDetails"), lr_eval_string("{mainTransaction}") ) ; //added 06212018
				addHeader();
				web_add_header("Content-Type", "application/json");
				web_add_header("Accept", "application/json");
				web_custom_request("getBOPISInvetoryDetails",
	//					"URL=https://{api_host}/tcpproduct/getBOPISInvetoryDetails",
					"URL=https://{api_host}/v2/vendor/getBOPISInvetoryDetails",
					"Method=POST",
					"Resource=0",
					"Mode=HTML",
					"Body={getBOPISInvetoryDetails}",
			//		"Body={\"availabilityRequest\":{\"viewName\":\"US BOPIS\",\"availabilityCriteria\":{\"facilityNames\":{\"facilityName\":[\"{newStrLocId}\"]},\"itemNames\":{\"itemName\":[\"{itemName}\"]}}}}",
					LAST);
				lr_end_sub_transaction (lr_eval_string("{mainTransaction}_getBOPISInvetoryDetails"), LR_AUTO) ;
*/				
//					getBOPISInvetoryDetails();
				
//				}
		}
	}
	
	lr_end_transaction (lr_eval_string("{mainTransaction}"), LR_AUTO) ;

	return LR_PASS;
}

int addBopisToCartDF() //from Data File
{
	char *strLocIdString ;	
	int iCount = 0;
/*	
	if (atoi(lr_eval_string("{storeLocId_count}")) == 0)
		return LR_FAIL;
	
	for (iCount = 0; iCount <= atoi(lr_eval_string("{storeLocId_count}")); iCount++)
	{
		if (atoi( lr_paramarr_idx ("qty", iCount)) > 0)
		    break;
	}
//	lr_save_string( lr_paramarr_idx ("storeLocId", iCount), "storeLocId");
	
//	lr_save_string( "110961", "storeLocId");
//	lr_save_string( "814065", "catEntryId");
*/	
//	lr_save_string( "2063538002", "variantNo");
//	lr_save_string( "00889705465747", "itemPartNumber");
	lr_save_string("","mainTransaction");
	lr_save_string(lr_eval_string("T05_Add To Cart"), "mainTransaction");
	lr_think_time ( LINK_TT ) ;
	lr_start_transaction(lr_eval_string("{mainTransaction}"));
	
	web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
	web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n
//	web_reg_save_param("orderId", "LB=\"orderId\": \"", "RB=\"", "NotFound=Warning", LAST); //"orderId": "8006336024"
//	web_reg_save_param("orderId" , "LB=\"orderId\": [\"" , "RB=\"]" , "NotFound=Warning",  LAST ) ;
	web_reg_save_param_regexp ("ParamName=orderId",	"RegExp=\"orderId\":[ ]*\"(.*?)\"",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST );
	web_reg_save_param("orderItemDescription", "LB=\"orderItemDescription\": \"", "RB=\"", "NotFound=Warning", LAST); //"orderItemDescription": "8006336025"
	web_reg_save_param("cartCount", "LB=set-cookie: cartItemsCount=", "RB=; Path=/; Domain", "NotFound=Warning", LAST); //set-cookie: cartItemsCount=1; Path=/; Domain
	lr_save_string(lr_eval_string("{RANDOM_PERCENT}"), "BossOrBopis");
	
	if ( atoi(lr_eval_string("{BossOrBopis}")) <= BOSS_CART_RATIO ) 
		lr_save_string(lr_eval_string("{createBossOrder}"), "body");
	else //if ( atoi(lr_eval_string("{BossOrBopis}")) <= (BOSS_CART_RATIO + BOPIS_CART_RATIO) ) {
		lr_save_string(lr_eval_string("{createBopisOrder}"), "body");
	
	web_reg_find("TEXT/IC=Error occur while doing processing", "SaveCount=createBopisOrderCheck", LAST);		
	lr_start_sub_transaction (lr_eval_string("{mainTransaction}_createBopisOrder"), lr_eval_string("{mainTransaction}") ) ;
	web_add_header("Accept", "application/json");
	web_add_header("Expires", "0");
	addHeader();
	registerErrorCodeCheck();
	web_custom_request("createBopisOrder",
		"URL=https://{api_host}/v2/bopis/createBopisOrder",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
//		"Body={\"storeLocId\":\"{storeLocId}\",\"quantity\":\"1\",\"catEntryId\":\"{BOPIS}\",\"isRest\":\"false\"}", //TIGER - Old BOPIS
//		"Body={\"storeLocId\":\"{storeLocId}\",\"quantity\":\"1\",\"catEntryId\":\"{BOPIS}\",\"isRest\":\"false\",\"pickupType\":\"{pickupType}\",\"variantNo\":\"2036314014\",\"itemPartNumber\":\"00889705479584\"}",
//		"Body={\"storeLocId\":\"{storeLocId}\",\"quantity\":\"1\",\"catEntryId\":\"{catEntryId}\",\"isRest\":\"false\",\"pickupType\":\"{pickupType}\",\"variantNo\":\"{VariantNo}\",\"itemPartNumber\":\"{itemPartNumber}\"}",
//		"Body={\"storeLocId\":\"111616\",\"quantity\":\"1\",\"catEntryId\":\"853010\",\"isRest\":\"false\",\"pickupType\":\"{pickupType}\",\"variantNo\":\"2061439003\",\"itemPartNumber\":\"00889705256291\"}",
//		"Body={{createBopisOrder}}",
		"Body={{body}}",
//		"Body={\"storeLocId\":\"114030\",\"quantity\":\"1\",\"catEntryId\":\"837982\",\"isRest\":\"false\",\"pickupType\":\"bopis\",\"variantNo\":\"2044764001\",\"itemPartNumber\":\"00889705122978\"}",
		LAST);
	
	if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0  && strcmp(lr_eval_string("{orderId}") ,"") != 0  && atoi(lr_eval_string("{createBopisOrderCheck}")) == 0) //if no error code and message
	{	
		lr_end_sub_transaction (lr_eval_string("{mainTransaction}_createBopisOrder"), LR_AUTO) ;
	}
	else
	{
		if (atoi(lr_eval_string("{createBopisOrderCheck}")) > 0 )
			lr_fail_trans_with_error(lr_eval_string("{mainTransaction}_createBopisOrder Failed with \"availability={messages={message={severity=ERROR, code=28300004, description=Error occur while doing processing.\"") ) ;
		else
			lr_fail_trans_with_error(lr_eval_string("{mainTransaction}_createBopisOrder Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
		
		lr_end_sub_transaction (lr_eval_string("{mainTransaction}_createBopisOrder"), LR_AUTO) ;
		lr_end_transaction(lr_eval_string("{mainTransaction}"), LR_FAIL);
	if (WRITE_TO_SUMO==1) {
 		lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_createBopisOrder - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
		sendErrorToSumo(); 
 	}
		return LR_FAIL;
	}

	web_reg_save_param_json("ParamName=offersSku", "QueryString=$..orderItems..variantNo", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=offersPrice", "QueryString=$..orderItems..itemPrice", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
	web_reg_save_param_json("ParamName=offersQty", "QueryString=$..orderItems..qty", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
 
	lr_start_sub_transaction (lr_eval_string("{mainTransaction}_getOrderDetails"), lr_eval_string("{mainTransaction}") ) ;
	web_add_header("pageName", "fullOrderInfo");
	//web_add_header("recalculate", "true");
	if (BRIERLEY_CUSTOM_TEST == 1)
		web_add_header("recalculate", "true" );
	else
		web_add_header("recalculate", "false" );
	web_add_header("calc", "true");
	web_reg_save_param("itemName", "LB=\"variantNo\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); 
	web_reg_save_param("stLocId", "LB=\"stLocId\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); 
	web_reg_save_param("piAmountonUpdateQuantity", "LB=\"piAmount\": \"", "RB=\"", "NotFound=Warning", LAST); //"piAmount": "28.85",\n
//	web_reg_save_param_regexp ("ParamName=cartCount",	"RegExp=[Cc]artCount\":[ ]*(.*?),",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST );
		web_reg_save_param_json("ParamName=cartCount", "QueryString=$..cartCount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	getOrderDetails();

	lr_end_sub_transaction (lr_eval_string("{mainTransaction}_getOrderDetails"), LR_AUTO) ;

//	getBOPISInvetoryDetails();

	lr_end_transaction (lr_eval_string("{mainTransaction}"), LR_AUTO) ;

	return LR_PASS;
}


void inCartEdits()
{
	viewCart();

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_SAVE_FOR_LATER) //Save for later
		saveForLaterPost();	

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_SAVE_FOR_LATER_MOVE) //Move to Bag
		saveForLaterMoveToBag();	

//	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_PROMO_APPLY )
//			applyPromoCode(2);	   //Change made by Pavan on 03/31/2020
/*	
//	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_UPDATE_ITEM_TO_BOSS) 
//	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_UPDATE_ITEM_TO_BOSS && atoi(lr_eval_string("{hour_minute}")) >= hourMin && strcmp(lr_eval_string("{currentFlow}"),"dropCartFlow") != 0) //04.16.2019 - romano added, to only execute below at steady state
	if ( RATIO_UPDATE_ITEM_TO_BOSS > 0) 
		updateItemShipping();
	
	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_UPDATE_QUANTITY )
		updateQuantity();  //if ( atoi( lr_eval_string("{orderItemIds_count}") ) != 0 ) {

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_DELETE_ITEM && strcmp(lr_eval_string("{currentFlow}"),"dropCartFlow") == 0)
			deleteItem(); //delete item from cart
	

//	if ( strcmp( lr_eval_string("{couponExist}"), "FORTYOFF") != 0 && strcmp( lr_eval_string("{couponExist}"), "TESTCODE20") != 0 )
//	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_PROMO_APPLY && (atoi( lr_eval_string("{couponFortyOffApplied}")) == 0 ) )
//	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_PROMO_APPLY && (strcmp( lr_eval_string("{couponExist}"), "FORTYOFF") != 0 && strcmp( lr_eval_string("{couponExist}"), "TESTCODE20") != 0) ) //09142018
//	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_PROMO_APPLY )
			//applyPromoCode(2);

			
//	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_DELETE_PROMOCODE )
		deletePromoCode(2);

	if (isLoggedIn == 1 && atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_WISHLIST) {

		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= MOVE_FROM_CART_TO_WISHLIST && strcmp(lr_eval_string("{currentFlow}"),"dropCartFlow") == 0 ) {
			moveFromCartToWishlist();
		}
		//Sravanthi 
//		if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= MOVE_FROM_WISHLIST_TO_CART  ) { //02142019 disabled
//			wlAddToCart();
//		}
	}

	viewCart();
*/
	return;
}


void wlGetProduct()
{
	drill();

	productDisplay();
}

void wlFavorites()
{
	int j;
 	lr_think_time ( LINK_TT ) ;
	
	lr_save_string("T19_Favorites", "mainTransaction");

	lr_start_transaction("T19_Favorites");

	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO ) {
		web_url("favorites",
			"URL=https://{host}{store_home_page}favorites",
			"Resource=0",
			"RecContentType=text/html",
			"Mode=HTML",
			LAST);
	}
	lr_save_string("T19_Favorites", "mainTransaction");

	if (ESPOT_FLAG == 1)
	{
		lr_save_string("1", "getESpot");
		web_add_header("espotName", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_Gift_Cards_espot,secondary_global_nav_Gift_Cards_espot,primary_global_nav_sale_espot,secondary_global_nav_sale_espot,primary_global_nav_Place_Shops_espot,secondary_global_nav_Place_Shops_espot,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Shoes_espot,secondary_global_nav_Shoes_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot");
		getESpot();
		lr_save_string("2", "getESpot");
		web_add_header("espotName", "DTEmptyFavorites");
		getESpot();
		lr_save_string("3", "getESpot");
		web_add_header("espotName", "GlobalHeaderBannerAboveHeader,MobileNavHeaderLinks1");
		getESpot();
		
	}		

//	getFavouriteStoreLocation();
	
	lr_save_string("favorites", "p_pageName");
	getRegisteredUserDetailsInfo();
	
	getListofWishList();

//	getPriceDetails();
	
//	if ( atoi( lr_eval_string("{itemCount}") ) != 0 ) {
		lr_save_string("T19_Favorites", "mainTransaction");
		
		web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier", 1) );
	
		getWishListbyId();
//	}
//		preferences();

	lr_end_transaction("T19_Favorites", LR_AUTO);

}

void wlGetAll()
{
	if (lr_paramarr_len("WishlistIDs") == 0)  {//no need to call this again if it already has values
	//  giftListExternalIdentifier\": 2001,
		web_reg_save_param("WishlistIDs", "LB=\"giftListExternalIdentifier\": ", "RB=,", "ORD=All", "NotFound=Warning", LAST);
	//	web_reg_save_param("WishlistName", "LB=\"nameIdentifier\": \"", "RB=,", "ORD=All", LAST);
	//	web_reg_save_param("WishlistItemCount", "LB=\"itemCount\": ", "RB=,", "ORD=All", LAST);
	// 	lr_think_time (1) ;

		//================================================
		//Get all wish-lists for users
		//================================================
		lr_start_transaction("T19_Wishlist Get List");

			TCPGetWishListForUsersView();

			if (lr_paramarr_len("WishlistIDs") == 0)  {//confirmed NOT login, giftListExternalIdentifier is NOT found in the response

			web_reg_save_param("WishlistIDs", "LB=\"giftListId\": ", "RB=,", "ORD=All", "NotFound=Warning", LAST);
		//http://tcp-perf.childrensplace.com/webapp/wcs/stores/servlet/AjaxGiftListServiceCreate?tcpCallBack=jQuery111309054115856997669_1446156866892&registryAccessPreference=Public&requesttype=ajax&storeId=10151&catalogId=10551&langId=-1&name=Joe%27s+Wishlist&_=1446156866894
			web_url("AjaxGiftListServiceCreate",
				"URL=https://{host}/webapp/wcs/stores/servlet/AjaxGiftListServiceCreate?tcpCallBack=jQuery111309054115856997669_1446156866892&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&name=Joe%27s+Wishlist&_=1446156866894",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=text/html",
				"Referer=",
				"Snapshot=t21.inf",
				"Mode=HTML",
				LAST);

			if (lr_paramarr_len("WishlistIDs") == 0)  //confirmed NOT login, giftListExternalIdentifier is NOT found in the response
				lr_end_transaction("T19_Wishlist Get List", LR_FAIL);
			else
				lr_end_transaction("T19_Wishlist Get List", LR_PASS);
		}
		else
			lr_end_transaction("T19_Wishlist Get List", LR_PASS);

		if (lr_paramarr_len("WishlistIDs") > 0)
			lr_save_string(lr_paramarr_random("WishlistIDs"), "WishlistID");
	}

}//wlGetAll()


void wlCreate()
{
	wlFavorites();

 	lr_think_time ( LINK_TT ) ;

	if (lr_paramarr_len("giftListExternalIdentifier") < 5)  {
	//================================================
	//Create a wish-list -
	//===============================================
		lr_save_string("T19_Wishlist Create", "mainTransaction" );
		lr_start_transaction("T19_Wishlist Create");

			lr_start_sub_transaction("T19_Wishlist Create_createWishListForUser", "T19_Wishlist Create");
			addHeader();
			web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
			web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n
			web_reg_save_param("uniqueID", "LB=\"uniqueID\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
			web_reg_find("TEXT/IC=descriptionName", "SaveCount=Mycount",LAST);
			web_add_header("deviceType", "desktop" );
			web_add_header("Expires", "0" );
			web_add_header("Accept", "application/json" );
			web_add_header("Content-Type", "application/json" );
			web_custom_request("createWishListForUser",
//				"URL=https://{api_host}/tcpproduct/createWishListForUser",
				"URL=https://{api_host}/v2/wishlist/createWishListForUser",
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
				"Body={\"descriptionName\":\"Perf_{WL_Random_Number}_WL\",\"state\":\"Active\"}",  //take nameIdentifier as the descriptionName and status as state from getListofWishList api
				LAST);
			lr_end_sub_transaction("T19_Wishlist Create_createWishListForUser", LR_AUTO);
			
			lr_save_string("T19_Wishlist Create", "mainTransaction");
			web_add_header("externalId", lr_eval_string("{uniqueID}") );
			getWishListbyId();

			getListofWishList();
			
		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0  && (atoi(lr_eval_string("{Mycount}"))==0) )
		{
			lr_fail_trans_with_error( lr_eval_string("T19_Wishlist Create Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction("T19_Wishlist Create", LR_FAIL);
	if (WRITE_TO_SUMO==1) {
 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_createWishListForUser - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
 	}
		}
		else
		{
			lr_end_transaction("T19_Wishlist Create", LR_AUTO);
		}
	}

}

void wlDelete()
{
	wlFavorites();

 	lr_think_time ( LINK_TT ) ;
	if (lr_paramarr_len("giftListExternalIdentifier") > 1)  {
 		
	//================================================
	//Delete a wish-list
	//================================================
		lr_start_transaction("T19_Wishlist Delete");
		
		lr_save_string("T19_Wishlist Delete", "mainTransaction");

		addHeader();
		web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
		web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n
		web_reg_find("TEXT/IC=uniqueID", "SaveCount=Mycount",LAST);
//			web_reg_find("TEXT/IC=uniqueID","Fail=NotFound", "SaveCount=Mycount",LAST);
		web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier", atoi(lr_eval_string("{giftListExternalIdentifier_count}")) ));
																										 
		web_custom_request("deleteWishListForUser",
//				"URL=https://{api_host}/tcpproduct/deleteWishListForUser",
			"URL=https://{api_host}/v2/wishlist/deleteWishListForUser",
			"Method=DELETE",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			LAST);

		lr_save_string("T19_Wishlist Delete", "mainTransaction");
		getListofWishList();
		
		lr_save_string("T19_Wishlist Delete", "mainTransaction");
		web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier",2) );
		getWishListbyId();

		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0  && (atoi(lr_eval_string("{Mycount}"))==0) )
		{
			lr_fail_trans_with_error( lr_eval_string("T19_Wishlist Delete Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction("T19_Wishlist Delete", LR_FAIL);
	if (WRITE_TO_SUMO==1) {
 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_getWishListbyId - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
 	}
		}
		else
		{
			lr_end_transaction("T19_Wishlist Delete", LR_AUTO);
		}
	}
}

void wlChange()
{
	wlFavorites();

 	lr_think_time ( LINK_TT ) ;
	//================================================
	//Change wish-list name
	//================================================
	if (lr_paramarr_len("giftListExternalIdentifier") > 0)  {

		lr_save_string("T19_Wishlist Change Name", "mainTransaction");
		lr_start_transaction("T19_Wishlist Change Name");

		web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
		web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n

		addHeader();
		web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier", atoi(lr_eval_string("{giftListExternalIdentifier_count}")) ));
		//web_add_header("deviceType", "desktop" );
		web_add_header("Content-Type", "application/json" );
		lr_save_string( lr_paramarr_idx("status", atoi(lr_eval_string("{giftListExternalIdentifier_count}")) ), "Status" );
		web_reg_find("TEXT/IC=descriptionName", "SaveCount=Mycount",LAST);

		web_custom_request("updateWishListForUser",
			"URL=https://{api_host}/v2/wishlist/updateWishListForUser",
			"Method=PUT",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
			"Body={\"descriptionName\":\"Perf_{WL_Random_Number}_Change\",\"state\":\"{Status}\"}",  //take nameIdentifier as the descriptionName and status as state from getListofWishList api
			LAST);

		getListofWishList();
/*			addHeader();
		web_url("getListofWishList",
			"URL=https://{api_host}/tcpstore/getListofWishList",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
*/
		lr_save_string("T19_Wishlist Change Name", "mainTransaction");
		web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier",1) );
		getWishListbyId();

//		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0  && (atoi(lr_eval_string("{Mycount}"))==0) )
		if ( atoi(lr_eval_string("{Mycount}")) == 0 )
		{
			lr_fail_trans_with_error( lr_eval_string("T19_Wishlist Change Name Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction("T19_Wishlist Change Name", LR_FAIL);
	if (WRITE_TO_SUMO==1) {
 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_getWishListbyId - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
 	}
		}
		else
		{
			lr_end_transaction("T19_Wishlist Change Name", LR_AUTO);
		}
	}
}



void wlAddItem()  // This is already covered by PDP_to_WL
{
	lr_think_time ( LINK_TT ) ;
	wlGetAll(); //Get all wish-lists for users

	target_itemsInCart = atoi(lr_eval_string("{RATIO_AVG_CART_SIZE}"));

	//lr_message("target_itemsInCart = %d", target_itemsInCart );

//	for(index_buildCart=0; index_buildCart < target_itemsInCart ; index_buildCart++)
//	{
		wlGetProduct();

		if (lr_paramarr_len("atc_catentryIds") > 0) {

			lr_save_string( lr_paramarr_random( "atc_catentryIds" ), "atc_catentryId");

			//================================================
			//Add items to a wish-list
			//================================================

			lr_start_transaction("T19_Wishlist Add Item");

			web_url("AjaxGiftListServiceAddItem", //Add item to wish-lists
			"URL=https://{host}/webapp/wcs/stores/servlet/AjaxGiftListServiceAddItem?&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&tcpCallBack=jQuery111301397454545367509_1440779623033&giftListId={WishlistID}&catEntryId_1={atc_catentryId}&quantity_1=1&_=1440779623035",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=text/html",
				"Referer=",
				"Snapshot=t21.inf",
				"Mode=HTML",
				LAST);

			lr_end_transaction("T19_Wishlist Add Item", LR_AUTO);

		} // end for loop to add o cart

//	}
//Request URL:https://{api_host}/addOrUpdateWishlist
//Request Method:PUT
}

void wlGetItems()
{
//	wlFavorites();

 	lr_think_time ( LINK_TT ) ;

	if (atoi( lr_eval_string("{WishlistIDs_count}")) > 0 )
	{
		if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO ) {
			web_reg_save_param("catEntryIds", "LB=\"itemId\": \"", "RB=\"", "NotFound=Warning", "Ord=All", LAST);
			web_reg_save_param("wishListItemIds", "LB=\"wishListItemId\": \"", "RB=\"", "NotFound=Warning", "Ord=All", LAST);
			lr_save_string ( lr_paramarr_random( "WishlistIDs") , "WishlistID" ) ;
			//================================================
			//Get wish-list items for a selected wish-list
			//================================================
	
			lr_start_transaction("T19_Wishlist Get Items");
	
			web_url("TCPGetWishListItemsForSelectedListView", //Get all wish-list Items - This works for changing WL, jquery value does not matter, need to give the ListId
				"URL=https://{host}/webapp/wcs/stores/servlet/TCPGetWishListItemsForSelectedListView?&registryAccessPreference=Public&requesttype=ajax&storeId={storeId}&catalogId={catalogId}&langId=-1&wishListId={WishlistID}&tcpCallBack=jQuery111308157584751024842_1440519437271&_=1440519437287",
				"TargetFrame=",
				"Resource=0",
				"RecContentType=text/html",
				"Referer=",
				"Snapshot=t21.inf",
				"Mode=HTML",
				LAST);
	
			lr_end_transaction("T19_Wishlist Get Items", LR_AUTO);
		}
	}
}

int wlAddToCart()
{
	int k, productCount = 0;
	wlFavorites();

 	lr_think_time ( LINK_TT ) ;
	
	lr_save_string("T05_Add To Cart", "mainTransaction");
	web_reg_save_param_json("ParamName=giftListItemID", "QueryString=$..giftListItemID", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);// externalId 14646003		
	web_reg_save_param_json("ParamName=productId", "QueryString=$..productId", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);	//catEntryId for adding to wishlist cart
			
	if ( lr_paramarr_len ( "productId" ) > 0 )
	{
		if ( lr_paramarr_len ( "productId" ) > 1 )
		{	productCount = atoi( lr_eval_string("{productId_count}"))-1;
			index = rand ( ) % productCount + 1 ;
			lr_save_string(lr_paramarr_idx("productId", index), "productCatentryId");
			lr_save_string(lr_paramarr_idx("giftListItemID", index), "giftListItemID");
		}
		else {
			lr_save_string(lr_paramarr_random("productId"), "productCatentryId");
			lr_save_string(lr_paramarr_random("giftListItemID"), "giftListItemID");
		}
		lr_start_transaction("T05_Add To Cart");
	
		lr_start_sub_transaction("T05_Add To Cart_addProductToCart_WL" , "T05_Add To Cart");
		web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
		web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n
		web_reg_save_param_json("ParamName=orderId", "QueryString=$.orderId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param_json("ParamName=orderItemId", "QueryString=$.orderItemId.*", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		web_reg_save_param("cartCount", "LB=set-cookie: cartItemsCount=", "RB=; Path=/; Domain", "NotFound=Warning", LAST); //set-cookie: cartItemsCount=1; Path=/; Domain
		web_add_header("Accept", "application/json");
		prepLoyaltyDataCapture();
		registerErrorCodeCheck();
		addHeader();
		//addSFCCCartHeader();
		web_custom_request("addProductToCart",
			"URL=https://{api_host}/v2/cart/addProductToCart", 
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
//old		"Body={\"calculationUsage[]\":\"-7\",\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{productCatentryId}\",\"quantity\":\"1\",\"externalId\":\"{giftListItemID}\"}",
//			"Body={\"catalogId\":\"{catalogId}\",\"storeId\":\"{storeId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{productCatentryId}\",\"quantity\":\"1\",\"calculationUsage[]\":\"-7\",\"externalId\":\"\",\"multipackCatentryId\":null}",
			"Body={\"catalogId\":\"{catalogId}\",\"storeId\":\"{storeId}\",\"langId\":\"-1\",\"orderId\":\".\",\"field2\":\"0\",\"requesttype\":\"ajax\",\"catEntryId\":\"{productCatentryId}\",\"quantity\":\"1\",\"calculationUsage[]\":\"-7\",\"externalId\":\"\",\"multipackCatentryId\":null,\"skipLoyaltyCall\":false}",
			LAST);

		if ( strlen(lr_eval_string("{orderId}")) <= 0 ) {
			atc_Stat = 1; //0-Pass 1-Fail
			lr_error_message( lr_eval_string("T05_Add To Cart_addProductToCart_WL - Failed with HTTP {httpReturnCode}, orderId: {orderId}, catentryId: {atc_catentryId}, API Error Code: {errorCode}, Error Message: {errorMessage}, OrderId:{orderId}, Email: {userEmail}, timestamp: {timestamp}, hostname: {hostName}") ) ;

			lr_end_sub_transaction ("T05_Add To Cart_addProductToCart_WL", LR_FAIL) ;
			lr_end_transaction ( "T05_Add To Cart" , LR_FAIL ) ;
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_addProductToCart_WL - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
			return LR_FAIL;
		} // end if
		loyaltyPoints();
		lr_end_sub_transaction ("T05_Add To Cart_addProductToCart_WL", LR_AUTO);
		if ( BRIERLEY_FLAG_CHECK == 0) {
			web_reg_save_param_json("ParamName=offersSku", "QueryString=$..orderItems..variantNo", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param_json("ParamName=offersPrice", "QueryString=$..orderItems..itemPrice", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param_json("ParamName=offersQty", "QueryString=$..orderItems..qty", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
		 
			lr_start_sub_transaction (lr_eval_string("{mainTransaction}_getOrderDetails"), lr_eval_string("{mainTransaction}") ) ;
			web_add_header("pageName", "fullOrderInfo");
			//web_add_header("recalculate", "true");
			if (BRIERLEY_CUSTOM_TEST == 1)
				web_add_header("recalculate", "true" );
			else
				web_add_header("recalculate", "false" );
	
			web_add_header("calc", "false");
			web_reg_save_param("itemName", "LB=\"variantNo\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); 
			web_reg_save_param("stLocId", "LB=\"stLocId\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); 
			web_reg_save_param("piAmountonUpdateQuantity", "LB=\"piAmount\": \"", "RB=\"", "NotFound=Warning", LAST); //"piAmount": "28.85",\n
	//		web_reg_save_param_regexp ("ParamName=cartCount",	"RegExp=[Cc]artCount\":[ ]*(.*?),",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST );
			web_reg_save_param_json("ParamName=cartCount", "QueryString=$..cartCount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			getOrderDetails();
	
			lr_end_sub_transaction (lr_eval_string("{mainTransaction}_getOrderDetails"), LR_AUTO) ;
		}
//		getAllCoupons();

//		preferences();
//		getPointsService();
		
//		getFavouriteStoreLocation();

		lr_end_transaction ( "T05_Add To Cart" , LR_AUTO ) ;
	}
	  return 0;
}

void wlDeleteItem()
{
	wlFavorites();

 	lr_think_time ( LINK_TT ) ;

	//================================================
	//Delete items from wish-list
	//================================================
//	if ( lr_paramarr_len("giftListItemID") > 0 ) { //if there is any item to delete
	if ( atoi( lr_eval_string("{itemCount}") ) != 0 ) { //if there is any item to delete

		lr_save_string("T19_Wishlist Delete Item", "mainTransaction");

		lr_start_transaction("T19_Wishlist Delete Item");

			lr_save_string("T19_Wishlist Delete Item", "mainTransaction");
			web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier",1) );
			getWishListbyId();

			getListofWishList();
			
			addHeader();
			web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
			web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n
			web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier",1) );
			web_add_header("itemId", lr_paramarr_idx("giftListItemID",1) );
			web_custom_request("deleteItemFromWishList",
//				"URL=https://{api_host}/tcpproduct/deleteItemFromWishList",
				"URL=https://{api_host}/v2/wishlist/deleteItemFromWishList",
				"Method=DELETE",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				LAST);

		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0  && (atoi(lr_eval_string("{Mycount}"))==0) )
		{
			lr_fail_trans_with_error( lr_eval_string("T19_Wishlist Delete Item - Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction("T19_Wishlist Delete Item", LR_FAIL);
	if (WRITE_TO_SUMO==1) {
 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_deleteItemFromWishList - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
			sendErrorToSumo(); 
 	}
		}
		else
		{
			lr_end_transaction("T19_Wishlist Delete Item", LR_AUTO);
		}
	}

}

void wlShare()
{
	wlFavorites();

 	lr_think_time ( LINK_TT ) ;

	//================================================
	//Delete items from wish-list
	//================================================
//	if ( lr_paramarr_len("giftListItemID") > 0 ) { //if there is any item to delete
//	if ( atoi( lr_eval_string("{giftListExternalIdentifier}") ) != 0 ) { //if there is any item to delete

//		lr_save_string(lr_paramarr_random("wishListItemIds"), "wishListItemId");

		lr_save_string("T19_Wishlist shareWishListForUser", "mainTransaction");
		lr_start_transaction("T19_Wishlist shareWishListForUser");
/*
			web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier",1) );
			getWishListbyId();
*/
		addHeader();
		web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
		web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n
		//web_reg_find("TEXT/IC=uniqueID","Fail=NotFound", "SaveCount=Mycount",LAST);
		web_add_header("externalId", lr_paramarr_idx("giftListExternalIdentifier",1) );
		web_add_header("Accept", "application/json" );
		web_add_header("Content-Type", "application/json" );
		web_add_header("type", "email" );
//			web_add_header("itemId", lr_paramarr_idx("giftListItemID",1) );
		web_custom_request("shareWishListForUser",
			"URL=https://{api_host}/v2/wishlist/shareWishListForUser",
			"Method=POST",
			"Resource=0",
			"Mode=HTTP",
			"Body={\"recipientEmail\":[{\"email\":\"{userEmail}\"}],\"senderEmail\":\"{userEmail}\",\"subject\":\"Check this out!\",\"message\":\"I think U wil like this\"}",
			LAST);

		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0 )
		{
			lr_fail_trans_with_error( lr_eval_string("T19_Wishlist shareWishListForUser - Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction("T19_Wishlist shareWishListForUser", LR_FAIL);
			if (WRITE_TO_SUMO==1) {
		 			lr_save_string( lr_eval_string("{runTime}-{mainTransaction} shareWishListForUser - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
					sendErrorToSumo(); 
		 	}
		}
		else
		{
			lr_end_transaction("T19_Wishlist shareWishListForUser", LR_AUTO);
		}
//	}

}

void wlGetCatEntryId()
{
	wlFavorites();
	wlGetAll(); //Get all wish-lists for users
	wlGetItems();

 	lr_think_time ( LINK_TT ) ;
}

void wishList()
{
	int iRandomTask = 0;

	iRandomTask = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if (iRandomTask <= RATIO_WL_CREATE)
		wlCreate();
	else if (iRandomTask <= RATIO_WL_DELETE + RATIO_WL_CREATE)
		wlDelete();
	else if (iRandomTask <= RATIO_WL_CHANGE + RATIO_WL_CREATE + RATIO_WL_DELETE)
		wlChange();
	else if (iRandomTask <= RATIO_WL_DELETE_ITEM + RATIO_WL_CREATE + RATIO_WL_DELETE + RATIO_WL_CHANGE)
		wlDeleteItem();
	else if (iRandomTask <= RATIO_WL_SHARE + RATIO_WL_CREATE + RATIO_WL_DELETE + RATIO_WL_CHANGE + RATIO_WL_DELETE_ITEM)
		wlShare();
	else if (iRandomTask <= RATIO_WL_ADD_ITEM + RATIO_WL_CREATE + RATIO_WL_DELETE + RATIO_WL_CHANGE + RATIO_WL_DELETE_ITEM + RATIO_WL_ADD_TO_CART)
		wlAddToCart();

}


void dropCartTransaction()
{
	lr_start_transaction("T20_Abandon_Cart");
	lr_end_transaction("T20_Abandon_Cart", LR_PASS);

//	if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 60)
	if (atoi(lr_eval_string("{RANDOM_PERCENT}")) < 0)
		viewCart(); //06/15/17 rhy, to add extra 20k viewcart count
	//return LR_PASS;
}


int  moveFromCartToWishlist()
{
	if ( isLoggedIn == 1 ) {
		lr_think_time ( LINK_TT );

		viewCart();
	//
	//	if (atoi( lr_eval_string("{orderItemIds_count}")) > 0 )
		if (atoi( lr_eval_string("{itemCatentryId_count}")) > 0 )
		{
			lr_save_string ( "T23_MoveFromCartToWishlist", "mainTransaction" );

			index = rand ( ) % lr_paramarr_len( "orderItemIds" ) + 1 ;

			lr_save_string ( lr_paramarr_idx( "orderItemIds" , index ) , "orderItemId" ) ;
			lr_save_string ( lr_paramarr_idx( "itemCatentryId" , index ) , "productId" ) ;
			lr_save_string ( lr_paramarr_idx( "itemQuantity" , index ) , "quantity" ) ;
			lr_save_string ( lr_paramarr_idx( "productPartNumber" , index ) , "uniqueId" ) ;
/*	if ( lr_paramarr_len ( "atc_catentryIds" ) > 0 )
	{
		for (k = 1; k<= atoi(lr_eval_string("{atc_catentryIds_count}")); k++ )
		{
			if (atoi( lr_paramarr_idx("atc_quantity", k) ) != 0)
			{
				lr_save_string(lr_paramarr_idx("atc_catentryIds", k), "atc_catentryId" ); 
				lr_save_string(lr_paramarr_idx("variantNo", k), "VariantNo" );
				lr_save_string(lr_paramarr_idx("itemPartNumber", k), "itemPartNumber" );
				lr_save_string(lr_paramarr_idx("productidWL", k), "productidWL" );
				
				break;
			}
		}
	}
*/
			addHeader();
			web_add_header("Content-Type", "application/json");
			web_add_header("Accept", "application/json");

			registerErrorCodeCheck();
			//lr_continue_on_error(1);

			lr_start_transaction("T23_MoveFromCartToWishlist");

			lr_start_sub_transaction("T23_MoveFromCartToWishlist_addOrUpdateWishlist", "T23_MoveFromCartToWishlist");
			web_reg_find("TEXT/IC=item", "SaveCount=apiCheck", LAST);
			web_custom_request("addOrUpdateWishlist",
				"URL=https://{api_host}/v2/wishlist/addOrUpdateWishlist",
				"Method=PUT",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTML",
				"Body={\"item\":[{\"productId\":\"{productId}\",\"quantityRequested\":\"1\"}],\"uniqueId\":\"{uniqueId}\"}",
				LAST);
			
			if (atoi(lr_eval_string("{apiCheck}")) == 0 )
				lr_end_sub_transaction("T23_MoveFromCartToWishlist_addOrUpdateWishlist", LR_FAIL);
			else
				lr_end_sub_transaction("T23_MoveFromCartToWishlist_addOrUpdateWishlist", LR_AUTO);

			lr_start_sub_transaction("T23_MoveFromCartToWishlist_deleteMultipleOrderItems", "T23_MoveFromCartToWishlist");
			addHeader();
			web_add_header("Content-Type", "application/json");
			web_reg_find("TEXT/IC=orderId", "SaveCount=apiCheck", LAST);
			web_custom_request("deleteMultipleOrderItems",
				"URL=https://{api_host}/v2/cart/deleteMultipleOrderItems",
				"Method=PUT",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTML",
				"Body={\"orderItem\":[{\"orderItemId\":\"{orderItemId}\",\"quantity\":\"0\"}]}",
			LAST);
			if (atoi(lr_eval_string("{apiCheck}")) == 0 )
				lr_end_sub_transaction("T23_MoveFromCartToWishlist_deleteMultipleOrderItems", LR_FAIL);
			else
				lr_end_sub_transaction("T23_MoveFromCartToWishlist_deleteMultipleOrderItems", LR_AUTO);

			lr_start_sub_transaction("T23_MoveFromCartToWishlist_getOrderDetails", "T23_MoveFromCartToWishlist");
//			web_reg_save_param("itemQuantity", "LB=\"qty\": ", "RB=,", "NotFound=Warning", "Ord=All", LAST); //\t\t\t"qty": 2,\n
//			web_reg_save_param("itemCatentryId", "LB=\"itemCatentryId\": ", "RB=,", "NotFound=Warning", "Ord=All", LAST); 
			web_reg_save_param("productId", "LB=\"productId\": \"", "RB=\",", "NotFound=Warning", "Ord=All", LAST); //  "productId": "426632",\n
			web_reg_save_param("productPartNumber", "LB=\"productPartNumber\": \"", "RB=\",", "NotFound=Warning", "Ord=All", LAST); //  "productId": "426632",\n
//			web_reg_save_param_regexp ("ParamName=cartCount",	"RegExp=[Cc]artCount\":[ ]*(.*?),",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST );
		web_reg_save_param_json("ParamName=cartCount", "QueryString=$..cartCount", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param("orderItemIds", "LB=\"orderItemId\": ", "RB=,", "NotFound=Warning", "Ord=All", LAST); //\t\t\t"orderItemId": 8007970018,\n// 03/01/17
			web_reg_save_param("piAmount", "LB=\"piAmount\": \"", "RB=\",", "NotFound=Warning", LAST); //"piAmount": "7.95",\n  "piAmount": "22.30",\n
			web_reg_save_param("couponExist", "LB=\"code\": \"", "RB=\"", "NotFound=Warning", LAST); //"code": "FORTYOFF",
			web_reg_save_param_regexp ("ParamName=orderId",	"RegExp=\"orderId\":[ ]*\"(.*?)\"",  "NotFound=Warning", SEARCH_FILTERS, "RequestUrl=*", LAST );
			web_reg_save_param ( "mixCart" ,    "LB=\"mixCart\": \"", "RB=\"" , "NotFound=Warning", LAST ) ; //"mixCart": "MIX"    "mixCart": "ECOM"\n
			web_reg_save_param( "stLocId", "LB=\"stLocId\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); 
			web_reg_save_param( "itemName", "LB=\"variantNo\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); 
			web_reg_save_param( "orderItemType", "LB=\"orderItemType\": \"", "RB=\"", "ORD=All", "NotFound=Warning", LAST); 

			web_reg_save_param_json("ParamName=offersSku", "QueryString=$..orderItems..variantNo", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param_json("ParamName=offersPrice", "QueryString=$..orderItems..itemPrice", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
			web_reg_save_param_json("ParamName=offersQty", "QueryString=$..orderItems..qty", "SelectAll=Yes", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", "LAST");
 
			web_add_header("pageName", "fullOrderInfo");
			//web_add_header("recalculate", "true");
			if (BRIERLEY_CUSTOM_TEST == 1)
		web_add_header("recalculate", "true" );
	else
		web_add_header("recalculate", "false" );
			web_add_header("calc", "true");
			web_reg_find("TEXT/IC={", "SaveCount=apiCheck", LAST);
		getOrderDetails();

			if (atoi(lr_eval_string("{apiCheck}")) == 0 )
				lr_end_transaction("T23_MoveFromCartToWishlist_getOrderDetails", LR_FAIL);
			else
				lr_end_transaction("T23_MoveFromCartToWishlist_getOrderDetails", LR_AUTO);
/*
			getAllCoupons();

			getPointsService();

			getBOPISInvetoryDetails();
			
			getPriceDetails();
*/
			if ( strcmp( lr_eval_string("{errorCode}") ,"") == 0 && strcmp(lr_eval_string("{errorMessage}") ,"") == 0 ) //if no error code and message
			{
				lr_end_transaction ( "T23_MoveFromCartToWishlist", LR_AUTO ) ;
				return LR_PASS;
			}
			else {
				lr_fail_trans_with_error( lr_eval_string ("T23_MoveFromCartToWishlist Failed with Error Code:  \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
				lr_end_transaction ( "T23_MoveFromCartToWishlist", LR_FAIL ) ;
	if (WRITE_TO_SUMO==1) {
 				lr_save_string( lr_eval_string("{runTime}-{mainTransaction}_getOrderDetails - Failed with HTTP {httpReturnCode}, API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", Email: {userEmail}"), "errorString" ) ;
				sendErrorToSumo(); 
 	}
				return LR_FAIL;
			}
		}
	}
	return 0;
}


int getPromoCode() { //action is either "get" or "delete"
int rNum;
unsigned short updateStatus;
char **colnames = NULL;
char **rowdata = NULL;
//PVCI2 pvci = 0;

    int row, rc, loopCtr = 0;
    char FieldValue[50];
    int totalCoupon = lr_get_attrib_long("SinglePromoCodeCount");
//	char  *VtsServer = "10.56.29.36";   // moved to global.h
//	int   nPort = 8888;

	lr_save_string("", "promocode");
	lr_save_string("checkoutFlow", "currentFlow");
	
	while (pvci != 0) {

      rNum = rand() % totalCoupon + 1;
		lr_output_message("rNum : %d", rNum);
        loopCtr++;
        vtc_query_row(pvci, rNum, &colnames, &rowdata);
		lr_output_message("Query Row Names : %s", colnames[0]);
		lr_output_message("Query Row Data  : %s", rowdata[0]);

        if ( strcmp(rowdata[0], "") != 0) { //if row is not blank, save it to a parameter then delete it from the table

        	lr_save_string(rowdata[0], "promocode"); //save the code
//        	lr_output_message("Promo Code  : %s", lr_eval_string("{promocode}") );
        	//vtc_clear_row(pvci, rNum, &updateStatus); //delete the code from the datafile
			if (strcmp (lr_eval_string("{currentFlow}"), "checkoutFlow") == 0 )
				vtc_update_row1(pvci,"COUPON_CODE", rNum, "",",", &updateStatus);

            break;
        }
		if (loopCtr == 5)
			break;
	}

    if ( strcmp(lr_eval_string("{promocode}"), "") == 0) {
        lr_error_message("Single-Use promo code out of data.");
//        return LR_FAIL;
    }

    vtc_free_list(colnames);
    vtc_free_list(rowdata);

    if ( strcmp(lr_eval_string("{promocode}"), "") == 0) {
        lr_error_message("Single-Use promo code out of data.");
//        return LR_FAIL;
    }

    vtc_free_list(colnames);
    vtc_free_list(rowdata);

//    rc = vtc_disconnect( pvci ); // moved to vuser_end

    return LR_PASS;

}

void accountRewards()
{
	lr_save_string("T08_ViewMyAccount_PlaceRewards", "mainTransaction");
	lr_start_transaction("T08_ViewMyAccount_PlaceRewards");
	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO ) {

	web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=rewardsCheck", LAST);
	//web_reg_save_param("xxx", "LB=You can redeem", "RB= online at checkout.", "NotFound=Warning", LAST);
	web_url("account/rewards/",
		"URL=https://{host}/{country}/account/place-rewards/", //place-rewards
		"Resource=0",
		"RecContentType=text/html",
		"Referer=",
		"Mode=HTML",
		LAST);
	}
	
	lr_start_sub_transaction ( "T08_ViewMyAccount_PlaceRewards_getMyPointHistory", "T08_ViewMyAccount_PlaceRewards" );
	addHeader();
	web_add_header("Accept", "application/json");
	web_reg_find("TEXT/IC=pointsHistoryList", "SaveCount=apiCheck", LAST);
	web_custom_request("getMyPointHistory",
		"URL=https://{api_host}/v2/wallet/getMyPointHistory", 
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T08_ViewMyAccount_PlaceRewards_getMyPointHistory", LR_FAIL);
	else
		lr_end_sub_transaction("T08_ViewMyAccount_PlaceRewards_getMyPointHistory", LR_AUTO);

	//bonusDay();
	waysToEarn();	
	socialNew();
	
//	if (atoi(lr_eval_string("{rewardsCheck}")) == 0 )
//		lr_end_transaction("T08_ViewMyAccount_PlaceRewards", LR_FAIL);
//	else
		lr_end_transaction("T08_ViewMyAccount_PlaceRewards", LR_AUTO);

//=================================================================================================================================
	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO ) {

		lr_start_transaction("T08_ViewMyAccount_Rewards_PointsHistory");
		web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=rewardsCheck1", LAST);
		web_url("account/rewards/points-history",
			"URL=https://{host}/{country}/account/place-rewards/points-history/",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Mode=HTML",
			LAST);
		if (atoi(lr_eval_string("{rewardsCheck1}")) == 0 )
			lr_end_transaction("T08_ViewMyAccount_Rewards_PointsHistory", LR_FAIL);
		else
			lr_end_transaction("T08_ViewMyAccount_Rewards_PointsHistory", LR_AUTO);
	}

// --romano 01/03/19 - this may not be needed anymore
	if (MULTI_USE_COUPON_FLAG == 1)
	{
		lr_save_string ( "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag", "mainTransaction" ) ;
		lr_start_transaction ( "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag" ) ;

			lr_start_sub_transaction ( "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag_Coupons" , "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag") ;
			registerErrorCodeCheck();
			addHeader();
			web_custom_request("coupons",
				"URL=https://{api_host}/v2/checkout/coupons", //
				"Method=POST",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				"EncType=application/json",
				"Body={\"storeId\":\"{storeId}\",\"langId\":\"-1\",\"catalogId\":\"{catalogId}\",\"URL\"g:\"\",\"promoCode\":\"FORTYOFF\",\"requesttype\":\"ajax\",\"fromPage\":\"shoppingCartDisplay\",\"taskType\":\"A\"}",
				LAST);
			lr_end_sub_transaction ("T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag_Coupons", LR_AUTO)	;

			lr_start_sub_transaction ( "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag_getOrderDetails", "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag" );
			web_add_header("pageName", "fullOrderInfo");
			//web_add_header("recalculate", "true");
			if (BRIERLEY_CUSTOM_TEST == 1)
				web_add_header("recalculate", "true" );
			else
				web_add_header("recalculate", "false" );
			
			web_add_header("calc", "true");
			getOrderDetails();

			lr_end_sub_transaction ( "T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag_getOrderDetails", LR_AUTO );

			getPointsService();

		lr_end_transaction ("T08_ViewMyAccount_Rewards_Offers&Coupons_ApplyToBag", LR_AUTO)	;
	}
}

void viewOrderStatus()
{
	lr_think_time ( LINK_TT );
	//lr_continue_on_error(1);

	if ( atoi( lr_eval_string("{s_OrderLookup_count}")) > 0 ) {  //Modified by Pavan 06/20/2017

		if (ONLINE_ORDER_SUBMIT==0){
			lr_save_string( lr_paramarr_random("s_OrderLookup"),"orderId");
					lr_save_string("T08_ViewMyAccount_Orders_Details", "orderStatusTransaction");
		}else if (ONLINE_ORDER_SUBMIT==1){
			//////PAVAN DUSI FILLED LOGIC 0626
			//OrderId is already populated
				if (strcmp( lr_eval_string("{addBopisToCart}"), "TRUE") == 0 ){
					lr_save_string("T08_ViewMyAccount_Orders_Details_BOPIS", "orderStatusTransaction");
				}else{
					lr_save_string("T08_ViewMyAccount_Orders_Details_ECOM", "orderStatusTransaction");
				}
		}

		lr_start_transaction(lr_eval_string("{orderStatusTransaction}"));

		if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO ) {

			web_custom_request("T08_ViewMyAccount_OrdersDetails",
			"URL=https://{host}/{country}/account/orders/order-details/{orderId}",
			"Method=GET",
			"TargetFrame=",
			"Resource=0",
			"RecContentType=text/html",
			"Mode=HTML",
			"EncType=",
			LAST);
		}
		web_reg_find("TEXT/IC=\"orderId\": \"{orderId}\"", "SaveCount=apiCheck", LAST);
		addHeader();
		web_add_header("Origin", "https://{host}");
		web_add_header("orderId", "{orderId}");
		web_add_header("emailId", "{userEmail}");

		web_custom_request("orderLookup",
			"URL=https://{api_host}/tcporder/orderLookUp",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_transaction(lr_eval_string("{orderStatusTransaction}"), LR_FAIL);
		else
			lr_end_transaction(lr_eval_string("{orderStatusTransaction}"), LR_AUTO);


	}

	//lr_continue_on_error(0);

}// end viewOrderStatus()
viewOrderStatusRegistered(){
	
}

void viewOrderStatusRegistered_ARCHIVE()
{
	lr_think_time ( LINK_TT );
	//lr_continue_on_error(1);

//	if ( atoi( lr_eval_string("{s_OrderLookup_count}")) > 0 ) {  //Modified by Pavan 06/20/2017
/* 04182018 -- this is called after submitting oorder, should already got orderId
		if (ONLINE_ORDER_SUBMIT==0){
			lr_save_string( lr_paramarr_random("s_OrderLookup"),"orderId");
					lr_save_string("T08_ViewMyAccount_Orders_Details", "orderStatusTransaction");
		}else if (ONLINE_ORDER_SUBMIT==1){
			//////PAVAN DUSI FILLED LOGIC 0626
			//OrderId is already populated
				if (strcmp( lr_eval_string("{addBopisToCart}"), "TRUE") == 0 ){
					lr_save_string("T08_ViewMyAccount_Orders_Details_BOPIS", "orderStatusTransaction");
				}else{
					lr_save_string("T08_ViewMyAccount_Orders_Details_ECOM", "orderStatusTransaction");
				}
		}
*/
		lr_save_string("T08_ViewMyAccount_Orders_Details", "orderStatusTransaction");
		lr_start_transaction(lr_eval_string("{orderStatusTransaction}"));

		web_custom_request("T08_ViewMyAccount_OrdersDetails",
		"URL=https://{host}/{country}/account/orders/order-details/{orderId}",
		"Method=GET",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=",
		LAST);

		web_reg_find("TEXT/IC=\"orderId\": \"{orderId}\"", "SaveCount=apiCheck", LAST);
		addHeader();
		web_add_header("Origin", "https://{host}");
		web_add_header("orderId", "{orderId}");
		web_add_header("emailId", "{userEmail}");
		web_add_header("fromPage", "orderHistory");
		web_add_header("Accept", "application/json");

		web_custom_request("orderLookup",
//04012019	"URL=https://{api_host}/tcporder/orderLookUp",
			"URL=https://{api_host}/v2/account/orderLookUp",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		if (atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_transaction(lr_eval_string("{orderStatusTransaction}"), LR_FAIL);
		else
			lr_end_transaction(lr_eval_string("{orderStatusTransaction}"), LR_AUTO);


//	}

	//lr_continue_on_error(0);

}// end viewOrderStatusRegistered()


void viewOrderHistory()
{
	lr_think_time ( LINK_TT );

	web_reg_find("Text/IC=My Place Rewards", "SaveCount=checkOrderhistoryHistory");
	lr_save_string("us", "country");
	lr_start_transaction ( "T08_ViewMyAccount_Orders" ) ;
	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO ) {

	web_custom_request("T08_ViewMyAccount_Orders",
	"URL=https://{host}/{country}/account/orders", //R3 Change
		"Method=GET",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		"EncType=",
		LAST);
	}
		lr_start_sub_transaction ( "T08_ViewMyAccount_Orders_getPointsAndOrderHistory", "T08_ViewMyAccount_Orders" ) ;

		addHeader();
		web_add_header("Accept", "application/json");
		web_add_header("fromRest", "true");
		web_custom_request("getPointsAndOrderHistory",
			"URL=https://{api_host}/v2/wallet/getPointsAndOrderHistory",
			"Method=GET",
			"Resource=0",
			"RecContentType=application/json",
			LAST);
		lr_end_sub_transaction ( "T08_ViewMyAccount_Orders_getPointsAndOrderHistory" , LR_AUTO) ;
		
	if ( atoi ( lr_eval_string ( "{checkOrderhistoryHistory}" ) ) > 0 )
		lr_end_transaction ( "T08_ViewMyAccount_Orders" , LR_PASS) ;
	else
		lr_end_transaction ( "T08_ViewMyAccount_Orders" , LR_FAIL) ;

	if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 50 )
		viewOrderStatus();

}// end viewOrderHistory()



void reservationHistory() //gone???
{
	lr_think_time ( LINK_TT );
	lr_start_transaction("T08_ViewMyAccount_ReservationHistory");
		web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=reservationsCheck", LAST);
		web_url("Reservations",
			"URL=https://{host}/{country}/account/reservations/",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Mode=HTML",
		LAST);

	lr_end_transaction("T08_ViewMyAccount_ReservationHistory", LR_AUTO);
}

void addressBook()
{
	lr_think_time ( LINK_TT );
	lr_start_transaction("T08_ViewMyAccount_AddressBook");
/*	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO ) {

	web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=addressBookCheck", LAST);
	web_url("Address-book",
		"URL=https://{host}/{country}/account/address-book/",
		"Resource=0",
		"RecContentType=text/html",
		"Referer=",
		"Mode=HTML",
		LAST);
	}*/
	if (atoi(lr_eval_string("{RANDOM_PERCENT}")) <= 50 )
	{
		if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO ) {

			lr_start_sub_transaction("T08_ViewMyAccount_AddressBook_add-new-address", "T08_ViewMyAccount_AddressBook");
			web_url("Address-book/add-new-address",
				"URL=https://{host}/{country}/account/address-book/add-new-address",
				"Resource=0",
				"RecContentType=text/html",
				"Referer=",
				"Mode=HTML",
				LAST);
			lr_end_sub_transaction("T08_ViewMyAccount_AddressBook_add-new-address", LR_AUTO);
		}
		lr_start_sub_transaction ("T08_ViewMyAccount_AddressBook_addAddress","T08_ViewMyAccount_AddressBook");
		web_reg_save_param("newNickName", "LB=nickName\": \"", "RB=\"", "NotFound=Warning", LAST);
		addHeader();
		web_custom_request("addAddress", ////api/v2/account/addAddress - Phase 2
//		"URL=https://{api_host}/payment/addAddress",
		"URL=https://{api_host}/v2/account/addAddress",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTML",
			"EncType=application/json",
			"Body={\"contact\":[{\"addressLine\":[\"{guestAdr1}\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"{guestZip}\"}],\"addressType\":\"ShippingAndBilling\",\"city\":\"{guestCity}\",\"country\":\"{guestCountry}\",\"firstName\":\"Joe\",\"lastName\":\"User\",\"nickName\":\"1498060089577\",\"phone1\":\"2014564545\",\"email1\":\"{userEmail}\",\"phone1Publish\":\"false\",\"primary\":\"false\",\"state\":\"{guestState}\",\"zipCode\":\"{guestZip}\",\"xcont_addressField2\":\"1\",\"xcont_addressField3\""
			":\"{guestZip}\",\"fromPage\":\"\"}]}",
			LAST);

		lr_end_sub_transaction ("T08_ViewMyAccount_AddressBook_addAddress", LR_AUTO) ;

		if (strlen(lr_eval_string("{newNickName}")) != 0 )
		{
			web_reg_save_param_json( "ParamName=expMonth", "QueryString=$.creditCardListJson..expMonth", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json( "ParamName=expYear", "QueryString=$.creditCardListJson..expYear", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json( "ParamName=ccType", "QueryString=$.creditCardListJson..ccType", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json( "ParamName=accountNo", "QueryString=$.creditCardListJson..accountNo", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json( "ParamName=ccBrand", "QueryString=$.creditCardListJson..ccBrand", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			web_reg_save_param_json( "ParamName=creditCardId", "QueryString=$.creditCardListJson..creditCardId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
			
			lr_start_sub_transaction ("T08_ViewMyAccount_AddressBook_deleteAddressDetails","T08_ViewMyAccount_AddressBook");
			addHeader();
			web_add_header("nickname", lr_eval_string("{newNickName}"));
			web_add_header("Content-Type", "application/json");
			web_custom_request("deleteAddressDetails", // /api/v2/account/deleteAddressDetails - Phase 2
//				"URL=https://{api_host}/payment/deleteAddressDetails",
				"URL=https://{api_host}/v2/account/deleteAddressDetails",
				"Method=DELETE",
				"Resource=0",
				"RecContentType=application/json",
				"Mode=HTTP",
				LAST);
			lr_end_sub_transaction ("T08_ViewMyAccount_AddressBook_deleteAddressDetails", LR_AUTO) ;
			getCC();

			lr_start_sub_transaction ( "T08_ViewMyAccount_AddressBook_getCreditCardDetails", "T08_ViewMyAccount_AddressBook");
			addHeader();
			web_add_header("isRest", "true" );
			web_custom_request("getCreditCardDetails",
				"URL=https://{api_host}/v2/account/getCreditCardDetails", // 
				"Method=GET",
				"Resource=0",
				"RecContentType=application/json",
				LAST);
			lr_end_sub_transaction("T08_ViewMyAccount_AddressBook_getCreditCardDetails", LR_AUTO);

		}

	}
//	if (atoi(lr_eval_string("{addressBookCheck}")) == 0 )
//		lr_end_transaction("T08_ViewMyAccount_AddressBook", LR_FAIL);
//	else
		lr_end_transaction("T08_ViewMyAccount_AddressBook", LR_AUTO);

}

void payment()
{
	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO ) {
		lr_think_time ( LINK_TT );
		lr_start_transaction("T08_ViewMyAccount_Payment");
		web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=paymentCheck", LAST);
		web_url("Payment",
			"URL=https://{host}/{country}/account/payment/",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Mode=HTML",
			LAST);
	
		if (atoi(lr_eval_string("{paymentCheck}")) == 0 )
			lr_end_transaction("T08_ViewMyAccount_Payment", LR_FAIL);
		else
			lr_end_transaction("T08_ViewMyAccount_Payment", LR_AUTO);
	}
}

void addDeleteBirthdaySavings()
{
	lr_think_time ( LINK_TT );

	lr_start_transaction("T08_ViewMyAccount_Profile_addBirthdaySavings");
	
	web_reg_save_param("timeStamp", "LB=timeStamp\": \"", "RB=\"", "NotFound=Warning", LAST);
	web_reg_save_param("childId", "LB=childId\": \"", "RB=\"", "NotFound=Warning", LAST);
	web_add_header("Accept", "application/json");
	addHeader();
    web_custom_request("addBirthdaySavings", 
        "URL=https://{api_host}/v2/account/addBirthdaySavings",
        "Method=POST",
        "Resource=0",
        "RecContentType=application/json",
        "Mode=HTML",
        "EncType=application/json",
        "Body={\"firstName\":\"Joey\",\"lastName\":\"User\",\"timestamp\":\"{tsBirthdaySavings}\",\"childDetails\":[{\"childName\":\"Joey\",\"birthYear\":\"2016\",\"birthMonth\":\"3\",\"gender\":\"01\"}]}",
        LAST);

	lr_end_transaction("T08_ViewMyAccount_Profile_addBirthdaySavings", LR_AUTO);

	lr_think_time ( LINK_TT );

	lr_start_transaction("T08_ViewMyAccount_Profile_deleteBirthdaySavings");
	addHeader();
	web_add_header("Accept", "application/json");
	web_custom_request("deleteBirthdaySavings", ///api/v2/account/deleteBirthdaySavings - Phase 2
//		"URL=https://{api_host}/tcporder/deleteBirthdaySavings",
		"URL=https://{api_host}/v2/account/deleteBirthdaySavings",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		"EncType=application/json",
//		"Body={\"timestamp\":\"{timeStamp}\",\"childDetails\":[{\"childId\":\"{childId}\"}]}",
//		"Body={\"timestamp\":\"2017-6-13 14:58:46.466\",\"childDetails\":[{\"childId\":\"306510\"}]}",
		"Body={\"timestamp\":\"{timeStamp}\",\"childDetails\":[{\"childId\":\"{childId}\",\"childName\":\"Joey\",\"birthMonth\":\"3\",\"birthYear\":\"2016\",\"gender\":\"01\"}]}",
		LAST);
	lr_end_transaction("T08_ViewMyAccount_Profile_deleteBirthdaySavings", LR_AUTO);
	
}


void profile()
{
	lr_think_time ( LINK_TT );

	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO ) {
	
		lr_start_transaction("T08_ViewMyAccount_Profile");
		web_reg_find("TEXT/IC=My Place Rewards", "SaveCount=profileCheck", LAST);
		web_url("Profile",
			"URL=https://{host}/{country}/account/profile/",
			"Resource=0",
			"RecContentType=text/html",
			"Referer=",
			"Mode=HTML",
			LAST);
	
		if (atoi(lr_eval_string("{profileCheck}")) == 0 )
			lr_end_transaction("T08_ViewMyAccount_Profile", LR_FAIL);
		else
			lr_end_transaction("T08_ViewMyAccount_Profile", LR_AUTO);
	}
	
	lr_start_transaction ( "T08_ViewMyAccount_Profile_getBirthdaySavings" );
	addHeader();
	web_custom_request("tcporder/getBirthdaySavings",  ///api/v2/account/getBirthdaySavings - Phase 2
		"URL=https://{api_host}/v2/account/getBirthdaySavings", 
//		"URL=https://{api_host}/tcporder/getBirthdaySavings", 
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);

	lr_end_transaction("T08_ViewMyAccount_Profile_getBirthdaySavings", LR_AUTO);

	lr_save_string("T08_ViewMyAccount_Profile_Edit", "mainTransaction");
	
	lr_start_transaction("T08_ViewMyAccount_Profile_Edit");
	lr_start_sub_transaction("T08_ViewMyAccount_Profile_Edit_updatesAccountDataForRegisteredUser", "T08_ViewMyAccount_Profile_Edit");
	addHeader();
	web_add_header("Accept", "application/json");
	web_custom_request("updatesAccountDataForRegisteredUser",
		"URL=https://{api_host}/v2/account/updatesAccountDataForRegisteredUser",
		"Method=PUT",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTML",
		//"Body={\"firstName\":\"Joe{randomChar}\",\"lastName\":\"User\",\"associateId\":\"\,\"email1\":\"{userEmail}\",\"phone1\":\"2014531236\",\"status\":\"accept_all::false:false\",\"operation\":\"\"}",
		"Body={\"firstName\":\"Joe{randomChar}\",\"lastName\":\"User\",\"associateId\":null,\"email1\":\"{userEmail}\",\"phone1\":\"2014531313\",\"operation\":\"\",\"userBirthday\":\"1|1995\"}",
		LAST);
	lr_end_sub_transaction("T08_ViewMyAccount_Profile_Edit_updatesAccountDataForRegisteredUser", LR_AUTO);

	if ( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_ADDBIRTHDAYSAVINGS )
		addDeleteBirthdaySavings();
	
	web_reg_save_param_json( "ParamName=expMonth", "QueryString=$.creditCardListJson..expMonth", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=expYear", "QueryString=$.creditCardListJson..expYear", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=ccType", "QueryString=$.creditCardListJson..ccType", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=accountNo", "QueryString=$.creditCardListJson..accountNo", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=ccBrand", "QueryString=$.creditCardListJson..ccBrand", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=creditCardId", "QueryString=$.creditCardListJson..creditCardId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

	lr_start_sub_transaction ("T08_ViewMyAccount_Profile_Edit_getCreditCardDetails","T08_ViewMyAccount_Profile_Edit");
	addHeader();
	web_add_header("isRest", "true" );
	web_add_header("Accept", "application/json" );
	web_url("getCreditCardDetails",
		"URL=https://{api_host}/v2/account/getCreditCardDetails", 
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	lr_end_sub_transaction ("T08_ViewMyAccount_Profile_Edit_getCreditCardDetails",LR_AUTO);
	getCC();
/*
	lr_start_sub_transaction("T08_ViewMyAccount_Profile_Edit_updateAddress", "T08_ViewMyAccount_Profile_Edit");
	registerErrorCodeCheck();
	addHeader();
	web_add_header( "Accept", "application/json" );
	web_add_header( "nickName", lr_eval_string("{userEmail}") );
	web_add_header( "Expires", "0" );
	web_add_header( "profileUpdate", "true" );
	web_custom_request("updateAddress",
		"URL=https://{api_host}/v2/wallet/updateAddress", 
		"Method=PUT",
		"Resource=0",
		"RecContentType=application/json",
		"Mode=HTTP",
		"EncType=application/json",
//		"Body={\"firstName\":\"Joe\",\"lastName\":\"User\",\"addressLine\":[\"{addressIdPrimary}\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"{savedZipCodePrimary}\"}],\"addressType\":\"ShippingAndBilling\",\"zipCode\":\"07094\",\"city\":\"{savedCityPrimary}\",\"state\":\"{savedStatePrimary}\",\"country\":\"US\",\"email1\":\"{userEmail}\",\"phone1\":\"877752{randomFourDigits}\",\"xcont_addressField3\":\"{savedZipCodePrimary}\",\"phone1Publish\":false,\"xcont_addressField2\":\"2\",\"xcont_pageName\":\"myAccount\"}",
		"Body={\"firstName\":\"Joe\",\"lastName\":\"User\",\"addressLine\":[\"{addressIdPrimary}\",\"\",\"\"],\"attributes\":[{\"key\":\"addressField3\",\"value\":\"{savedZipCodePrimary}\"}],\"addressType\":\"ShippingAndBilling\",\"zipCode\":\"07094\",\"city\":\"{savedCityPrimary}\",\"state\":\"{savedStatePrimary}\",\"country\":\"US\",\"email1\":\"{userEmail}\",\"phone1\":\"201453{randomFourDigits}\",\"xcont_addressField3\":\"{savedZipCodePrimary}\",\"phone1Publish\":false,\"xcont_addressField2\":\"2\",\"xcont_pageName\":\"myAccount\"}",
		LAST);
	lr_end_sub_transaction("T08_ViewMyAccount_Profile_Edit_updateAddress", LR_AUTO);
*/	
	lr_end_transaction("T08_ViewMyAccount_Profile_Edit", LR_AUTO);

}

void myAccountOthers()
{
	int randomNumber = atoi(lr_eval_string("{RANDOM_PERCENT}"));

	if ( randomNumber <= RATIO_ACCOUNT_REWARDS )
		accountRewards();
	else if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY )
		viewOrderHistory();
	else if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY + RATIO_RESERVATION_HISTORY )
		reservationHistory();
	else if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY + RATIO_RESERVATION_HISTORY  + RATIO_ADDRESSBOOK )
		addressBook();
	else if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY + RATIO_RESERVATION_HISTORY  + RATIO_ADDRESSBOOK + RATIO_PAYMENT )
		payment();
	else if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY + RATIO_RESERVATION_HISTORY  + RATIO_ADDRESSBOOK + RATIO_PAYMENT + RATIO_PROFILE )
		profile();
//	else if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY + RATIO_RESERVATION_HISTORY  + RATIO_ADDRESSBOOK + RATIO_PAYMENT + RATIO_PROFILE + RATIO_PREFERENCES )
//		preferences();

	//lr_continue_on_error(0);

	return;
}



int viewMyAccount()
{
	int randomNumber = atoi(lr_eval_string("{RANDOM_PERCENT}"));
	int i = 0;
	lr_think_time ( LINK_TT ) ;
	
	lr_save_string("T08_ViewMyAccount", "mainTransaction");

	lr_start_transaction("T08_ViewMyAccount");

	if( atoi(lr_eval_string("{akamaiRatio}")) <= NO_BROWSE_RATIO ) {
		lr_start_sub_transaction("T08_ViewMyAccount ViewMyAccount", "T08_ViewMyAccount" );
	
			web_reg_find("TEXT/IC=window.__PRELOADED_STATE","SaveCount=MyACC_openCount", LAST);
	
			web_url("Account Overview",
				"URL=https://{host}/{country}/account/",
				"Resource=0",
				"RecContentType=text/html",
				"Referer=",
				"Mode=HTML",
				LAST);
		if (atoi(lr_eval_string("{MyACC_openCount}"))>0) {
			lr_end_sub_transaction("T08_ViewMyAccount ViewMyAccount",LR_AUTO);
		} else {
			lr_end_sub_transaction("T08_ViewMyAccount ViewMyAccount",LR_FAIL);
		}
	}
	
	lr_save_string("myAccount", "p_pageName");
	getRegisteredUserDetailsInfo();

	//bonusDay();

	getPointsAndOrderHistory();

	if (ESPOT_FLAG == 1)
	{
		lr_save_string("1", "getESpot");
		web_add_header("espotname", "GlobalHeaderBannerAboveHeader,DTGlobalSubNavBanner,GlobalHeaderBannerWithinHeader,GlobalHeaderLogo,topnav_girl_main,topnav_babygirl_main,topnav_boy_main,topnav_babyboy_main,topnav_newborn_main,topnav_shoes_main,topnav_accessories_main,topnav_girl_bottom,topnav_babygirl_bottom,topnav_boy_bottom,topnav_babyboy_bottom,topnav_newborn_bottom,topnav_shoes_bottom,topnav_accessories_bottom,GlobalHeaderPlccMoreInfoMdal,create_account_drawer,MinicartLoginBanner,MinicartRegisterBanner,drawer_myaccount_overview_banner,drawer_empty_bag,MinicartLoginFromFavoritesBanner,HeaderNavEspotLinks,DTMPRCCLanding,global_footer_signup_form_espot,global_above_footer,global_privacy_policy,DT_SiteWide_Above_Header,DT_SiteWide_Above_Header_nonStikcy,primary_global_nav_Girl_espot,secondary_global_nav_Girl_espot,primary_global_nav_Toddler_Girl_espot,secondary_global_nav_Toddler_Girl_espot,primary_global_nav_Boy_espot,secondary_global_nav_Boy_espot,primary_global_nav_Toddler_Boy_espot,secondary_global_nav_Toddler_Boy_espot,primary_global_nav_Baby_espot,secondary_global_nav_Baby_espot,primary_global_nav_Shoes_espot,secondary_global_nav_Shoes_espot,primary_global_nav_Accessories_espot,secondary_global_nav_Accessories_espot,primary_global_nav_Clearance_espot,secondary_global_nav_Clearance_espot,primary_global_nav_Trending_espot,secondary_global_nav_Trending_espot");
		getESpot();
		lr_save_string("2", "getESpot");
		web_add_header("espotname", "MyAcctHeaderBannerSignedIn_Espot1,MyAcctOverviewBottom_Espot1,MyAcctPaymentTop_Espot1,MyAccountMPRCCLanding,MyAccountMPRCCLandingMOB");
		getESpot();
		lr_save_string("3", "getESpot");
		web_add_header("espotname", "GlobalHeaderBannerAboveHeader,MobileNavHeaderLinks1");
		getESpot();
//		getESpot();
	}

	web_reg_save_param_json( "ParamName=expMonth", "QueryString=$.creditCardListJson..expMonth", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=expYear", "QueryString=$.creditCardListJson..expYear", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=ccType", "QueryString=$.creditCardListJson..ccType", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=accountNo", "QueryString=$.creditCardListJson..accountNo", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=ccBrand", "QueryString=$.creditCardListJson..ccBrand", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=creditCardId", "QueryString=$.creditCardListJson..creditCardId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);

	lr_start_sub_transaction ( "T08_ViewMyAccount_getCreditCardDetails", "T08_ViewMyAccount" );
	addHeader();
	web_add_header("isRest", "true" );
	web_custom_request("getCreditCardDetails",
		"URL=https://{api_host}/v2/account/getCreditCardDetails", 
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	lr_end_sub_transaction("T08_ViewMyAccount_getCreditCardDetails", LR_AUTO);
	
	getCC();
	

	socialNew();
//	socialNew();
	
	waysToEarn();
	getAllCoupons();
	
	lr_start_sub_transaction ( "T08_ViewMyAccount_getAddressFromBook", "T08_ViewMyAccount" );
	web_reg_save_param_json( "ParamName=nickName", "QueryString=$..nickName", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=addressId", "QueryString=$.contact..addressId", "SelectAll=NO", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=addressIdAll", "QueryString=$.contact..addressId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=savedAddressIdPrimary", "QueryString=$..contact..addressLine[*]", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=savedAddressPrimary", "QueryString=$..contact..primary", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=savedCityPrimary", "QueryString=$..contact..city", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=savedStatePrimary", "QueryString=$..contact..state", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=savedZipCodePrimary", "QueryString=$..contact..zipCode", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	web_reg_save_param_json( "ParamName=addressIdPrimary", "QueryString=$..contact..addressId", "SelectAll=YES", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
	addHeader();
	web_custom_request("getAddressFromBook",
		"URL=https://{api_host}/v2/account/getAddressFromBook", // 
		"Method=GET",
		"Resource=0",
		"RecContentType=application/json",
		LAST);
	lr_end_sub_transaction("T08_ViewMyAccount_getAddressFromBook", LR_AUTO);
	
	for (i=1; i <= atoi(lr_eval_string("{savedAddressPrimary_count}")); i++)
	{
		if (strcmp(lr_paramarr_idx("savedAddressPrimary", i), "true") == 0)
		{
		    lr_save_string(lr_paramarr_idx("addressIdPrimary", i), "addressIdPrimary");
		    lr_save_string(lr_paramarr_idx("addressIdPrimary", i), "addressId");
		    lr_save_string(lr_paramarr_idx("savedCityPrimary", i), "savedCityPrimary");
		    lr_save_string(lr_paramarr_idx("savedStatePrimary", i), "savedStatePrimary");
		    lr_save_string(lr_paramarr_idx("savedZipCodePrimary", i), "savedZipCodePrimary");
	    	lr_save_string(lr_paramarr_idx("savedAddressIdPrimary", i), "savedAddressIdPrimary"); 
	    	
		    if (i > 1) {
		    	lr_save_string(lr_paramarr_idx("savedAddressIdPrimary", i+1), "savedAddressIdPrimary");
		    	break;
		    }
		}
	}

//	userGroup();	
// bonusDay();
	preferences();
	
	lr_end_transaction("T08_ViewMyAccount", LR_AUTO);

	if (ONLINE_ORDER_SUBMIT==1){   //For the case when user places an order and wants to lookup order status directly
		if ( randomNumber <= RATIO_ACCOUNT_REWARDS + RATIO_ORDER_HISTORY )
//		viewOrderHistory();
	    return LR_PASS;							//-- not interested in doing anything else.- the rest cases are covered in DROP User.
	}

	if (randomNumber <= 15)
		myAccountOthers();

	 return LR_PASS;
}

void newsLetterSignup(){
	
}

void newsLetterSignup00()
{
	lr_think_time ( LINK_TT ) ;

	lr_save_string("T26_NewsLetterSignup", "mainTransaction");
	
	lr_start_transaction("T26_NewsLetterSignup");

		lr_start_sub_transaction("T26_NewsLetterSignup_addSignUpEmail", "T26_NewsLetterSignup");

		addHeader();
//		web_add_header("Accept", "application/json");
		web_add_header("Content-Type", "application/json");
		web_custom_request("addSignUpEmail", ///api/v2/store/addSignUpEmail - Phase 2
//		"URL=https://{api_host}/addSignUpEmail",
		"URL=https://{api_host}/v2/store/addSignUpEmail",
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
//			"Body={\"storeId\":\"{storeId}\",\"langId\":\"-1\",\"catalogId\":\"{catalogId}\",\"emailaddr\":\"{userEmail}\",\"URL\":\"email-confirmation\",\"response\":\"accept_all::true:false\"}",
//			"Body={\"storeId\":\"{storeId}\",\"catalogId\":\"{catalogId}\",\"langId\":\"-1\",\"emailaddr\":\"{userEmail}\",\"URL\":\"email-confirmation?icid=ct_na_f_na_na_emailsubscribe\",\"response\":\"accept_all::false:false\"}", //romano 01072019
//04162019	"Body={\"storeId\":\"10151\",\"catalogId\":\"10051\",\"langId\":\"-1\",\"emailaddr\":\"{userEmail}\",\"URL\":\"email-confirmation?icid=ct_na_f_na_na_emailsubscribe\",\"response\":\"accept_all::false:false\"}", //romano 01072019
			"Body={\"storeId\":\"10151\",\"catalogId\":\"10051\",\"langId\":\"-1\",\"emailaddr\":\"{userEmail}\",\"URL\":\"email-confirmation\",\"response\":\"valid::false:false\"}", //romano 01072019
		LAST);

		lr_end_sub_transaction("T26_NewsLetterSignup_addSignUpEmail", LR_AUTO);
		
		lr_save_string("content", "p_pageName");
		getRegisteredUserDetailsInfo();
		
		if (ESPOT_FLAG == 1)
		{	
			web_add_header("espotname", "DTPDPGlobalPromoHeader,DTPDPGlobalPromoPricing,DTPDPGlobalPromoAddtoBag,DTSizeChart,DTPDPBOPIS");
			getESpot();
		}
		

	lr_end_transaction("T26_NewsLetterSignup", LR_AUTO);
}


int webInstantCredit()  // need to decide where to insert this, most likely on drop scenario
{
	lr_think_time ( LINK_TT ) ;

//	web_set_sockets_option("SSL_VERSION", "TLS");
	web_url("place-card",
		"URL=https://{host}/us/place-card/",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);

	web_url("application",
		"URL=https://{host}/us/place-card/application",
		"Resource=0",
		"RecContentType=text/html",
		"Mode=HTML",
		LAST);

	lr_start_transaction ( "T18_WebInstantCredit_tcpstore/processWIC") ;
	web_reg_save_param("errorCode", "LB=\"errorCode\": \"", "RB=\"", "NotFound=Warning", LAST); //"errorCode": "",\n
	web_reg_save_param("errorMessage", "LB=\"errorMessage\": \"", "RB=\",\n", "NotFound=Warning", LAST); //"errorMessage": "",\n

	addHeader();
	web_custom_request("processWIC",
 		"URL=https://{api_host}/tcpstore/processWIC",
 		"Method=POST",
 		"Resource=0",
 		"RecContentType=application/json",
		"Mode=HTML",
		"Body=firstName=always&"
		"lastName=approve&"
		"middleInitial=&"
		"address1=500%20Plaza%20Dr&"
		"address2=&"
		"city=Secaucus&"
 		"state=NJ&"
 		"zipCode=07094&"
 		"country=US&"
		"ssn=9713&"
 		"alternatePhoneNumber=20145379531"
 		"emailAddress={userEmail}&"
// 		"emailAddress=bitrogue@mailinator.COM&"
 		"birthdayDate=1966{randomTwoDigits}25&"
 		"BF_ioBlackBox=0400mTR4Z7%2BBQcwNf94lis1ztosU8peJTCdUy7uGpRvabX7zVQjDWlYxYTXfueurujcaykJk9fR9lXIBdODzAYZvxp%2FyAGc0u%2F5UPzPPrHsRu27u2MJCSi12NBBoiZbfxP1Whlz5wlRFwWJi0FRulruXQQGCQaJkXU7GIIQMZuRs6N3qtP6CELnsH2G%2FcdkIMFbm6Yf0%2FpTJUUz1vNp0X2Zw8QydKgnOIDKXq4HnEqNOos0%2FB5zvrPJclqTOWG8pq1WO6yzJ1GNmMuMWZBamlGXoG%2FimnjwHY9HQtQzpGfcm0cR8X2Fd1ngNFGLDGZlWOX0jWbA6sSW4rBr%2FNrWzf5gYDruvNvEfRkJupBy3Z8hSEMEK7ZWd2T2HOoqcaV9FaOImRYT%2FgKodv2ev8lCwCfn1kaxBMac8RE9JQyxpl%2FFlRdEUxU0rlIm%2Bh0IdvEt%2BMbJEO8xzyZZyces4cd3X0jzFPr%2BN"
 		"op3TdJMFv9eOvsyvTBVVGK66EXEuaD0YLXuOWeFzcFTbpKHLiQrKf2hhwqstJ%2BrhYKIcxurY7kpCzwwdNW9e0%2F8IpHNM6NXh35GdB6Cc0rdKBra3m4ZITAN%2BPMPl1UvBE7jWZcVftxOczszofnt%2B%2Fu06EOo%2FUEYn9EFMRfvxVguGdCj5lheavfwPVmg%2BGk4OuHG49T1M%2BsnP9%2BSWhwxVxvHWf%2FuK445lNi%2BcovQlymqz1P3vzg9Oq87auE3RWwyPd429Hc3XdHevRMsQLV99Js2o8vmPAEOCbnRzpX9SwTbkqlmBR5N%2BYk9XXjom4y0AH2WgO40osBx6w1tx0mMZz7HtmSQrKuHt39bvxE86eVaYJEF00M2dmwpFJMf4g%2FnVivVfWl%2FGUEnvBpGzF9Rzh%2BjIcWSpp1SAFu7g0I1zyeSmTmPvDtmeZWysG5GNVi4ixCeMzU55NYBu%2FnSSujAF%2"
 		"FImMsuY70u6HOYdWG7EoZqeIeCHbDJ27mIZGZKEKh7xUN0sYF8r08f8uCcC0xOD%2BDPTu5VIg0CDMnwUlr%2F77l1ahRLPHCJ2nyP5hDtcTkIWM0MsrZYk6VspTTT4YfknEFHaD16ysmtjwcmmsgU0YREDCPutzcZTzmdVsVWccBDUhUs7HwJBLr5Keb%2BrR4RRGASpJ6ETmLbtOxKYkBFXNCg%2BO9YxO2R4GpUeravdrC26q6G5qP8J%2FK36i8%2FSM0OKZFYlvAevMmALkiiRCA6evWpL5xAYexDg4i8EgSau8uDTmXK3W95ikc26%2FuyCFY4R6KghlGeYLRb6hyFDOLPt%2B1kkJorDKbFLXB%2BZnOmejza7dnYdB3dXjB8n%2FNyT5AQKok7PtgJahH1UlSIs%2FQYMqtxvWZOLhAalWhbY%2B5yP8WdvLrD%2BP%2B1qaTpIvVu%2BuyQsaMh%2Bu9Xlp9HpZMEwvM9eWpJ"
		"75rIHe6%2FoPDSN%2Fxbk%2F43Sqb%2BGNnr6gpDIP0CxSx7kjSYP3C%2B3xVRrwyLRJ5XaZyrxo5J2FcY2%2BNO9P6IY2f%2BxktCOaw7AMCoqGG0O%2B3VHdw0mDejZrO1Umoq3MFE1OOL9Bn8XCTDfLXCixdWzqMvL5OMQ%2FiYXaPnuM20kjENkpO1N%2FdgoDKMciqJTUWwy1e%2FKq%2BVlO6QfGcU5hfv6m5A%3D%3D"
		"&prescreenId=",
 		LAST);


		if ( strcmp( lr_eval_string("{errorCode}") ,"") != 0 && strcmp(lr_eval_string("{errorMessage}") ,"") != 0 )
		{
			lr_fail_trans_with_error( lr_eval_string("T18_WebInstantCredit_tcpstore/processWIC - Failed with Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\"") ) ;
			lr_end_transaction( "T18_WebInstantCredit_tcpstore/processWIC", LR_FAIL);
			return LR_FAIL;
		}
		else
		{
			lr_end_transaction ( "T18_WebInstantCredit_tcpstore/processWIC" , LR_PASS ) ;
			return LR_PASS;
		}
}

int kidStyleSquad()
{
/*	lr_think_time ( FORM_TT ) ;

	lr_save_string("T18_Kids-Squad", "mainTransaction");
	
	lr_start_transaction("T18_Kids-Squad");

	lr_start_sub_transaction("T18_Kids-Squad-saveKidSquad","T18_Kids-Squad");
	web_add_header("storeId", "10151");
	web_add_header("Content-Type", "application/json");
	web_custom_request("saveKidSquad", ///api/v2/global-components/saveKidSquad - Phase 2
		"URL=https://{api_host}/v2/global-components/saveKidSquad",
		"Method=POST",
		"Resource=0",
		"RecContentType=application/json",
		"Referer=https://{host}/us/content/style-squad-form",
		"Snapshot=t21.inf",
		"Mode=HTML",
		"EncType=application/json",
		"Body={\"parentFirstName\":\"Joe\",\"parentLastName\":\"User\",\"email\":\"{userEmail}\",\"parentBirthYear\":\"1967\",\"city\":\"Metuchen\",\"state\":\"NJ\",\"zipCode\":\"08840\",\"parentFullName\":\"JoeUser\",\"childInfo\":[{\"childName\":\"Ashlee\",\"gender\":\"F\",\"birthMonth\":\"11\",\"birthYear\":\"2005\"}],\"socialMediaGuidelinesConfirmation\":\"1\",\"waiverAndReleaseConfirmation\":\"1\"}",
		LAST);
	lr_end_sub_transaction("T18_Kids-Squad-saveKidSquad", LR_AUTO);

	getRegisteredUserDetailsInfo();
	
	getFavouriteStoreLocation();

	lr_start_sub_transaction("T18_Kids-Squad-getOrderDetails","T18_Kids-Squad");
	web_add_header("calc", "false");
	web_add_header("pageName", "fullOrderInfo");
	web_add_header("recalculate", "true");
		getOrderDetails();

	lr_end_sub_transaction("T18_Kids-Squad-getOrderDetails", LR_AUTO);
	lr_end_transaction("T18_Kids-Squad", LR_AUTO);
*/
	return 0;
}

int smsSignUp()
{
	lr_think_time ( FORM_TT ) ;

	web_reg_find("TEXT/IC=participation_date", "SaveCount=apiCheck", LAST);

	lr_start_transaction("T12_SMS_SignUp");

	//addHeader();
	web_add_header( "Content-Type", "application/json" );
	web_custom_request("smsSignUp",
//		"URL=https://tcp-perf.childrensplace.com/api/tcpproduct/smsSignUp",
		"URL=https://{api_host}/v2/vibes/smsSignUp",
		"Method=POST",
		"Resource=0",
		"Body={\"mobile_phone\":{\"mdn\":\"299555{randomFourDigits}\"},\"custom_fields\":{\"zip_pos\":\"19702\",\"src_cd\":\"1\",\"sub_src_cd\":\"sms_landing_page\"}}",
		LAST);

	if (atoi(lr_eval_string("{apiCheck}")) == 0 )
		lr_end_sub_transaction("T12_SMS_SignUp", LR_FAIL);
	else
		lr_end_sub_transaction("T12_SMS_SignUp", LR_AUTO);

	return 0;
}
int navigateXHR()
{
	return 0;
}

int navigateXHROLD()
{
		lr_start_sub_transaction (lr_eval_string("{mainTransaction}_navigateXHR"), lr_eval_string("{mainTransaction}") ) ; 
			
		if(strcmp(lr_eval_string("{loginFirst}"), "true") == 0) {

			web_custom_request("navigateXHRLoginFirst",
				"URL=https://{myurlhost}/api/v2/appconfig/navigateXHR",
				"Method=POST",
				"Resource=0",
				"Mode=HTML",
				"Body={\"cookie\":[{\"name\":\"tcp_email_signup_modal_persistent\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"tcp_email_signup_modal_session\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"token\",\"path\":\"\/\",\"value\":\"{token}\"},{\"name\":\"iShippingCookie\",\"path\":\"\/\",\"value\":\"US|en_US|USD|1.0|1.0\"},{\"name\":\"tcpState\",\"path\":\"\/\",\"value\":\"NJ\"},{\"name\":\"tcpSegment\",\"path\":\"\/\",\"value\":\"5\"},{\"name\":\"tcpGeoLoc\",\"path\":\"\/\",\"value\":\"40.7808|-74.0651\"},{\"name\":\"tcpCountryDetail\",\"path\":\"\/\",\"value\":\"US\"},{\"name\":\"check\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"LandingBrand\",\"path\":\"\/\",\"value\":\"{p_Brand}\"},{\"name\":\"pv\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"cartItemsCount\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"WC_ACTIVEPOINTER\",\"path\":\"\/\",\"value\":\"{WC_ACTIVEPOINTER}\"},{\"name\":\"s_ecid\",\"path\":\"\/\",\"value\":\"MCMID%7C34000030970867548164616407686451354871\"},{\"name\":\"BVBRANDSID\",\"path\":\"\/\",\"value\":\"2788e75d-ee69-4446-862e-36af7d8950bb\"},{\"name\":\"BVBRANDID\",\"path\":\"\/\",\"value\":\"1b40b68f-ac30-4f84-9e66-86785a87d780\"},{\"name\":\"AMCVS_9A0A1C8B5329646E0A490D4D%40AdobeOrg\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"ls-brand\",\"path\":\"\/\",\"value\":\"{p_Brand}\"},{\"name\":\"AMCV_9A0A1C8B5329646E0A490D4D%40AdobeOrg\",\"path\":\"\/\",\"value\":\"-1712354808%7CMCIDTS%7C18325%7CMCMID%7C34000030970867548164616407686451354871%7CMCAID%7CNONE%7CMCOPTOUT-1583257021s%7CNONE%7CMCAAMLH-1583854621%7C7%7CMCAAMB-1583854621%7Cj8Odv6LonN4r3an7LhD3WZrU1bUpAkFkkiY1ncBR96t2PTI%7CvVersion%7C4.3.0\"},{\"name\":\"mbox\",\"path\":\"\/\",\"value\":\"session#e64fff93c950415a884884a06928e20e#1583251684|PC#e64fff93c950415a884884a06928e20e.17_0#1646494624\"},{\"name\":\"_caid\",\"path\":\"\/\",\"value\":\"82f71d1a-68f7-47b2-ae1a-27a5b8abcca9\"},{\"name\":\"_cavisit\",\"path\":\"\/\",\"value\":\"170a10b1bbc|\"},{\"name\":\"_fbp\",\"path\":\"\/\",\"value\":\"fb.1.1583249826069.385047578\"},{\"name\":\"unbxd.userId\",\"path\":\"\/\",\"value\":\"uid-1583249826115-98387\"},{\"name\":\"unbxd.visit\",\"path\":\"\/\",\"value\":\"first_time\"},{\"name\":\"unbxd.visitId\",\"path\":\"\/\",\"value\":\"visitId-1583249826163-2299\"},{\"name\":\"IR_gbd\",\"path\":\"\/\",\"value\":\"{domain}\"},{\"name\":\"IR_3971\",\"path\":\"\/\",\"value\":\"1583249826181%7C0%7C1583249826181%7C%7C\"},{\"name\":\"extole_access_token\",\"path\":\"\/\",\"value\":\"U34CMTMQ77QQF7KOH4KC9CQ6E7\"},{\"name\":\"QuantumMetricUserID\",\"path\":\"\/\",\"value\":\"e742f8e36551b98fc02ee9caa9336015\"},{\"name\":\"QuantumMetricSessionID\",\"path\":\"\/\",\"value\":\"749c3f315b1c401a3e3ad87616329db4\"},{\"name\":\"ipe_s\",\"path\":\"\/\",\"value\":\"33412062-9c8b-174a-b444-46689250ed91\"},{\"name\":\"ipe.30858.pageViewedCount\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"ipe.30858.pageViewedDay\",\"path\":\"\/\",\"value\":\"63\"},{\"name\":\"ipe_30858_fov\",\"path\":\"\/\",\"value\":\"{\\\"numberOfVisits\\\":1,\\\"sessionId\\\":\\\"33412062-9c8b-174a-b444-46689250ed91\\\",\\\"expiry\\\":\\\"2020-06-02T14:37:08.038Z\\\",\\\"lastVisit\\\":\\\"2020-03-03T15:37:08.038Z\\\"}\"},{\"name\":\"RT\",\"path\":\"\/\",\"value\":\"\\\"z\"},{\"name\":\"s_sess\",\"path\":\"\/\",\"value\":\"%20s_cm%3DTyped%252FBookmarkedTyped%252FBookmarkedundefined%3B%20s_cc%3Dtrue%3B%20v26%3Dnon-internal%2520search%3B%20s_cmdl%3D1%3B\"},{\"name\":\"s_pers\",\"path\":\"\/\",\"value\":\"%20v14%3D1583249854886-New%7C1585841854886%3B%20v28%3Dgl%253Ahome%2520page%7C1583251654894%3B\"},{\"name\":\"s_sq\",\"path\":\"\/\",\"value\":\"tcpglobalprod%3D%2526c.%2526a.%2526activitymap.%2526page%253Dgl%25253Ahome%252520page%2526link%253DLOG%252520IN%2526region%253Dtcp%2526pageIDType%253D1%2526.activitymap%2526.a%2526.c%2526pid%253Dgl%25253Ahome%252520page%2526pidt%253D1%2526oid%253DLOG%252520IN%2526oidt%253D3%2526ot%253DSUBMIT\"},{\"name\":\"hasPLCC\",\"path\":\"\/\",\"value\":\"false\"},{\"name\":\"WC_SESSION_ESTABLISHED\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"tcp_firstname\",\"path\":\"\/\",\"value\":\"{tcp_firstname}\"},{\"name\":\"acctId\",\"path\":\"\/\",\"value\":\"{acctId}\"},{\"name\":\"pntsAvail\",\"path\":\"\/\",\"value\":\"0\"},{\"name\":\"tcp_isregistered\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"acctStatus\",\"path\":\"\/\",\"value\":\"active\"},{\"name\":\"WC_AUTHENTICATION_{WC_AUTHENTICATION1}\",\"path\":\"\/\",\"value\":\"{WC_AUTHENTICATION2}\"},{\"name\":\"tier\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"showLoyaltyWebsiteInd\",\"path\":\"\/\",\"value\":\"{showLoyaltyWebsiteInd}\"},{\"name\":\"memberSince\",\"path\":\"\/\",\"value\":\"{memberSince}}\"},{\"name\":\"WC_PERSISTENT\",\"path\":\"\/\",\"value\":\"{WC_PERSISTENT}\"},{\"name\":\"tcp_email_signup_modal_persistent\",\"path\":\"\/\",\"value\":\"{tcp_email_signup_modal_persistent}\"},{\"name\":\"WC_USERACTIVITY_{WC_USERACTIVITY1}\",\"path\":\"\/\",\"value\":\"{WC_USERACTIVITY2}\"}]}",
				LAST);
		} else {
			
			web_custom_request("navigateXHRBroseLogin",
				"URL=https://{myurlhost}/api/v2/appconfig/navigateXHR",
				"Method=POST",
				"Resource=0",
				"Mode=HTML",
				"Body={\"cookie\":[{\"name\":\"tcp_email_signup_modal_persistent\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"tcp_email_signup_modal_session\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"token\",\"path\":\"\/\",\"value\":\"{token}\"},{\"name\":\"iShippingCookie\",\"path\":\"\/\",\"value\":\"US|en_US|USD|1.0|1.0\"},{\"name\":\"tcpState\",\"path\":\"\/\",\"value\":\"NJ\"},{\"name\":\"tcpSegment\",\"path\":\"\/\",\"value\":\"5\"},{\"name\":\"tcpGeoLoc\",\"path\":\"\/\",\"value\":\"40.7808|-74.0651\"},{\"name\":\"tcpCountryDetail\",\"path\":\"\/\",\"value\":\"US\"},{\"name\":\"check\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"LandingBrand\",\"path\":\"\/\",\"value\":\"{p_Brand}\"},{\"name\":\"pv\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"cartItemsCount\",\"path\":\"\/\",\"value\":\"{cartCount}\"},{\"name\":\"WC_ACTIVEPOINTER\",\"path\":\"\/\",\"value\":\"{WC_ACTIVEPOINTER}\"},{\"name\":\"s_ecid\",\"path\":\"\/\",\"value\":\"MCMID%7C34000030970867548164616407686451354871\"},{\"name\":\"BVBRANDSID\",\"path\":\"\/\",\"value\":\"2788e75d-ee69-4446-862e-36af7d8950bb\"},{\"name\":\"BVBRANDID\",\"path\":\"\/\",\"value\":\"1b40b68f-ac30-4f84-9e66-86785a87d780\"},{\"name\":\"AMCVS_9A0A1C8B5329646E0A490D4D%40AdobeOrg\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"ls-brand\",\"path\":\"\/\",\"value\":\"{p_Brand}\"},{\"name\":\"AMCV_9A0A1C8B5329646E0A490D4D%40AdobeOrg\",\"path\":\"\/\",\"value\":\"-1712354808%7CMCIDTS%7C18325%7CMCMID%7C34000030970867548164616407686451354871%7CMCAID%7CNONE%7CMCOPTOUT-1583257021s%7CNONE%7CMCAAMLH-1583854621%7C7%7CMCAAMB-1583854621%7Cj8Odv6LonN4r3an7LhD3WZrU1bUpAkFkkiY1ncBR96t2PTI%7CvVersion%7C4.3.0\"},{\"name\":\"mbox\",\"path\":\"\/\",\"value\":\"session#e64fff93c950415a884884a06928e20e#1583251684|PC#e64fff93c950415a884884a06928e20e.17_0#1646494624\"},{\"name\":\"_caid\",\"path\":\"\/\",\"value\":\"82f71d1a-68f7-47b2-ae1a-27a5b8abcca9\"},{\"name\":\"_cavisit\",\"path\":\"\/\",\"value\":\"170a10b1bbc|\"},{\"name\":\"_fbp\",\"path\":\"\/\",\"value\":\"fb.1.1583249826069.385047578\"},{\"name\":\"unbxd.userId\",\"path\":\"\/\",\"value\":\"uid-1583249826115-98387\"},{\"name\":\"unbxd.visit\",\"path\":\"\/\",\"value\":\"first_time\"},{\"name\":\"unbxd.visitId\",\"path\":\"\/\",\"value\":\"visitId-1583249826163-2299\"},{\"name\":\"IR_gbd\",\"path\":\"\/\",\"value\":\"{domain}\"},{\"name\":\"IR_3971\",\"path\":\"\/\",\"value\":\"1583249826181%7C0%7C1583249826181%7C%7C\"},{\"name\":\"extole_access_token\",\"path\":\"\/\",\"value\":\"U34CMTMQ77QQF7KOH4KC9CQ6E7\"},{\"name\":\"QuantumMetricUserID\",\"path\":\"\/\",\"value\":\"e742f8e36551b98fc02ee9caa9336015\"},{\"name\":\"QuantumMetricSessionID\",\"path\":\"\/\",\"value\":\"749c3f315b1c401a3e3ad87616329db4\"},{\"name\":\"ipe_s\",\"path\":\"\/\",\"value\":\"33412062-9c8b-174a-b444-46689250ed91\"},{\"name\":\"ipe.30858.pageViewedCount\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"ipe.30858.pageViewedDay\",\"path\":\"\/\",\"value\":\"63\"},{\"name\":\"ipe_30858_fov\",\"path\":\"\/\",\"value\":\"{\\\"numberOfVisits\\\":1,\\\"sessionId\\\":\\\"33412062-9c8b-174a-b444-46689250ed91\\\",\\\"expiry\\\":\\\"2020-06-02T14:37:08.038Z\\\",\\\"lastVisit\\\":\\\"2020-03-03T15:37:08.038Z\\\"}\"},{\"name\":\"RT\",\"path\":\"\/\",\"value\":\"\\\"z\"},{\"name\":\"s_sess\",\"path\":\"\/\",\"value\":\"%20s_cm%3DTyped%252FBookmarkedTyped%252FBookmarkedundefined%3B%20s_cc%3Dtrue%3B%20v26%3Dnon-internal%2520search%3B%20s_cmdl%3D1%3B\"},{\"name\":\"s_pers\",\"path\":\"\/\",\"value\":\"%20v14%3D1583249854886-New%7C1585841854886%3B%20v28%3Dgl%253Ahome%2520page%7C1583251654894%3B\"},{\"name\":\"s_sq\",\"path\":\"\/\",\"value\":\"tcpglobalprod%3D%2526c.%2526a.%2526activitymap.%2526page%253Dgl%25253Ahome%252520page%2526link%253DLOG%252520IN%2526region%253Dtcp%2526pageIDType%253D1%2526.activitymap%2526.a%2526.c%2526pid%253Dgl%25253Ahome%252520page%2526pidt%253D1%2526oid%253DLOG%252520IN%2526oidt%253D3%2526ot%253DSUBMIT\"},{\"name\":\"hasPLCC\",\"path\":\"\/\",\"value\":\"false\"},{\"name\":\"WC_SESSION_ESTABLISHED\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"tcp_firstname\",\"path\":\"\/\",\"value\":\"{tcp_firstname}\"},{\"name\":\"acctId\",\"path\":\"\/\",\"value\":\"{acctId}\"},{\"name\":\"pntsAvail\",\"path\":\"\/\",\"value\":\"0\"},{\"name\":\"tcp_isregistered\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"acctStatus\",\"path\":\"\/\",\"value\":\"active\"},{\"name\":\"WC_AUTHENTICATION_{WC_AUTHENTICATION1}\",\"path\":\"\/\",\"value\":\"{WC_AUTHENTICATION2}\"},{\"name\":\"tier\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"showLoyaltyWebsiteInd\",\"path\":\"\/\",\"value\":\"{showLoyaltyWebsiteInd}\"},{\"name\":\"memberSince\",\"path\":\"\/\",\"value\":\"{memberSince}}\"},{\"name\":\"WC_PERSISTENT\",\"path\":\"\/\",\"value\":\"{WC_PERSISTENT}\"},{\"name\":\"tcp_email_signup_modal_persistent\",\"path\":\"\/\",\"value\":\"{tcp_email_signup_modal_persistent}\"},{\"name\":\"WC_USERACTIVITY_{WC_USERACTIVITY1}\",\"path\":\"\/\",\"value\":\"{WC_USERACTIVITY2}\"}]}",
				LAST);
		}
		
	lr_start_sub_transaction (lr_eval_string("{mainTransaction}_navigateXHR"), lr_eval_string("{mainTransaction}") ) ; 
	
	return 0;
}

int parseHeaderInfo()
{
    extern char * strtok(char * string, const char * delimiters );
    char path[1000] = "";
    char separators[] = ";";
    char * token;
    int i;
    for (i= 1; i <= 3; i++) {
    	switch (i) {
    		case 1: strcpy(path, lr_eval_string("{WC_PERSISTENT_cookie}") ); break;
    		case 2: strcpy(path, lr_eval_string("{WC_ACTIVEPOINTER_cookie}") ); break;
    		case 3: strcpy(path, lr_eval_string("{WC_USERACTIVITY_cookie}") ); break;
    	}
    	
	    token = (char *)strtok(path, separators);
	
	    if (token != NULL ) {
	        
	    	switch (i) {
	    		case 1: lr_save_string(token, "WC_PERSISTENT"); break;
	    		case 2: lr_save_string(token, "WC_ACTIVEPOINTER"); break;
	    		case 3: lr_save_string(token, "WC_USERACTIVITY"); break;
	    	}
	    }
    }
	return 0;

}
int parseWC_AUTHENTICATION()
{
    extern char * strtok(char * string, const char * delimiters );
    char path[1000] = "";
    char separators[] = "=";
    char * token;
    int i;
	if (strlen(lr_eval_string("{WC_AUTHENTICATION}")) > 0) {
		
    	strcpy(path, lr_eval_string("{WC_AUTHENTICATION}") );
    	
	    token = (char *)strtok(path, separators);
		
		if (token != NULL ) {
	    	lr_save_string(token, "WC_AUTHENTICATION1");
	    	token = (char *)strtok(NULL, separators);	
	    	lr_save_string(token, "WC_AUTHENTICATION2");
	    }
    }
	return 0;
}
int parseUserActivity()
{
    extern char * strtok(char * string, const char * delimiters );
    char path[1000] = "";
    char separators[] = "=";
    char * token;
    int i;
	if (strlen(lr_eval_string("{WC_USERACTIVITY}")) > 0) {
		
    	strcpy(path, lr_eval_string("{WC_USERACTIVITY}") );
    	
	    token = (char *)strtok(path, separators);
		
		if (token != NULL ) {
	    	lr_save_string(token, "WC_USERACTIVITY1");
	    	token = (char *)strtok(NULL, separators);	
	    	lr_save_string(token, "WC_USERACTIVITY2");
	    }
    }
	return 0;
}
int CheckAndCallnavigateXHR(){
	return 0;
}
int CheckAndCallnavigateXHR01()
{
	lr_save_string("tcp-perf.childrensplace.com","myurlhost");
	if(strcmp(lr_eval_string("{navigateXHR}"), "false") == 0 ) {
		parseUserActivity();
		parseWC_AUTHENTICATION();
/*		
		if(strcmp(lr_eval_string("{navigatePath}"), "login") != 0) {
			if (parseHeaderInfo() == LR_FAIL) {
				lr_save_string("false", "navigateXHR");
				return 0;
			}
		}
*/		
		lr_start_sub_transaction (lr_eval_string("{mainTransaction}_navigateXHR"), lr_eval_string("{mainTransaction}") ) ; //added 06212018
			
		web_add_header("Content-Type", "application/json");
		//web_add_header("origin", lr_eval_string("https://{myurlhost}"));
		web_add_header("Accept", "application/json");
		web_reg_find("TEXT/IC=SUCCESS", "SaveCount=apiCheck", LAST);
		
		if(strcmp(lr_eval_string("{navigatePath}"), "login") == 0) {

			if(strcmp(lr_eval_string("{loginFirst}"), "true") == 0) {

				web_custom_request("navigateXHRLoginFirst",
					"URL=https://{myurlhost}/api/v2/appconfig/navigateXHR",
					"Method=POST",
					"Resource=0",
					"Mode=HTML",
					"Body={\"cookie\":[{\"name\":\"tcp_email_signup_modal_persistent\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"tcp_email_signup_modal_session\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"token\",\"path\":\"\/\",\"value\":\"{token}\"},{\"name\":\"iShippingCookie\",\"path\":\"\/\",\"value\":\"US|en_US|USD|1.0|1.0\"},{\"name\":\"tcpState\",\"path\":\"\/\",\"value\":\"NJ\"},{\"name\":\"tcpSegment\",\"path\":\"\/\",\"value\":\"5\"},{\"name\":\"tcpGeoLoc\",\"path\":\"\/\",\"value\":\"40.7808|-74.0651\"},{\"name\":\"tcpCountryDetail\",\"path\":\"\/\",\"value\":\"US\"},{\"name\":\"check\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"LandingBrand\",\"path\":\"\/\",\"value\":\"{p_Brand}\"},{\"name\":\"pv\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"cartItemsCount\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"WC_ACTIVEPOINTER\",\"path\":\"\/\",\"value\":\"{WC_ACTIVEPOINTER}\"},{\"name\":\"s_ecid\",\"path\":\"\/\",\"value\":\"MCMID%7C34000030970867548164616407686451354871\"},{\"name\":\"BVBRANDSID\",\"path\":\"\/\",\"value\":\"2788e75d-ee69-4446-862e-36af7d8950bb\"},{\"name\":\"BVBRANDID\",\"path\":\"\/\",\"value\":\"1b40b68f-ac30-4f84-9e66-86785a87d780\"},{\"name\":\"AMCVS_9A0A1C8B5329646E0A490D4D%40AdobeOrg\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"ls-brand\",\"path\":\"\/\",\"value\":\"{p_Brand}\"},{\"name\":\"AMCV_9A0A1C8B5329646E0A490D4D%40AdobeOrg\",\"path\":\"\/\",\"value\":\"-1712354808%7CMCIDTS%7C18325%7CMCMID%7C34000030970867548164616407686451354871%7CMCAID%7CNONE%7CMCOPTOUT-1583257021s%7CNONE%7CMCAAMLH-1583854621%7C7%7CMCAAMB-1583854621%7Cj8Odv6LonN4r3an7LhD3WZrU1bUpAkFkkiY1ncBR96t2PTI%7CvVersion%7C4.3.0\"},{\"name\":\"mbox\",\"path\":\"\/\",\"value\":\"session#e64fff93c950415a884884a06928e20e#1583251684|PC#e64fff93c950415a884884a06928e20e.17_0#1646494624\"},{\"name\":\"_caid\",\"path\":\"\/\",\"value\":\"82f71d1a-68f7-47b2-ae1a-27a5b8abcca9\"},{\"name\":\"_cavisit\",\"path\":\"\/\",\"value\":\"170a10b1bbc|\"},{\"name\":\"_fbp\",\"path\":\"\/\",\"value\":\"fb.1.1583249826069.385047578\"},{\"name\":\"unbxd.userId\",\"path\":\"\/\",\"value\":\"uid-1583249826115-98387\"},{\"name\":\"unbxd.visit\",\"path\":\"\/\",\"value\":\"first_time\"},{\"name\":\"unbxd.visitId\",\"path\":\"\/\",\"value\":\"visitId-1583249826163-2299\"},{\"name\":\"IR_gbd\",\"path\":\"\/\",\"value\":\"{domain}\"},{\"name\":\"IR_3971\",\"path\":\"\/\",\"value\":\"1583249826181%7C0%7C1583249826181%7C%7C\"},{\"name\":\"extole_access_token\",\"path\":\"\/\",\"value\":\"U34CMTMQ77QQF7KOH4KC9CQ6E7\"},{\"name\":\"QuantumMetricUserID\",\"path\":\"\/\",\"value\":\"e742f8e36551b98fc02ee9caa9336015\"},{\"name\":\"QuantumMetricSessionID\",\"path\":\"\/\",\"value\":\"749c3f315b1c401a3e3ad87616329db4\"},{\"name\":\"ipe_s\",\"path\":\"\/\",\"value\":\"33412062-9c8b-174a-b444-46689250ed91\"},{\"name\":\"ipe.30858.pageViewedCount\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"ipe.30858.pageViewedDay\",\"path\":\"\/\",\"value\":\"63\"},{\"name\":\"ipe_30858_fov\",\"path\":\"\/\",\"value\":\"{\\\"numberOfVisits\\\":1,\\\"sessionId\\\":\\\"33412062-9c8b-174a-b444-46689250ed91\\\",\\\"expiry\\\":\\\"2020-06-02T14:37:08.038Z\\\",\\\"lastVisit\\\":\\\"2020-03-03T15:37:08.038Z\\\"}\"},{\"name\":\"RT\",\"path\":\"\/\",\"value\":\"\\\"z\"},{\"name\":\"s_sess\",\"path\":\"\/\",\"value\":\"%20s_cm%3DTyped%252FBookmarkedTyped%252FBookmarkedundefined%3B%20s_cc%3Dtrue%3B%20v26%3Dnon-internal%2520search%3B%20s_cmdl%3D1%3B\"},{\"name\":\"s_pers\",\"path\":\"\/\",\"value\":\"%20v14%3D1583249854886-New%7C1585841854886%3B%20v28%3Dgl%253Ahome%2520page%7C1583251654894%3B\"},{\"name\":\"s_sq\",\"path\":\"\/\",\"value\":\"tcpglobalprod%3D%2526c.%2526a.%2526activitymap.%2526page%253Dgl%25253Ahome%252520page%2526link%253DLOG%252520IN%2526region%253Dtcp%2526pageIDType%253D1%2526.activitymap%2526.a%2526.c%2526pid%253Dgl%25253Ahome%252520page%2526pidt%253D1%2526oid%253DLOG%252520IN%2526oidt%253D3%2526ot%253DSUBMIT\"},{\"name\":\"hasPLCC\",\"path\":\"\/\",\"value\":\"false\"},{\"name\":\"WC_SESSION_ESTABLISHED\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"tcp_firstname\",\"path\":\"\/\",\"value\":\"{tcp_firstname}\"},{\"name\":\"acctId\",\"path\":\"\/\",\"value\":\"{acctId}\"},{\"name\":\"pntsAvail\",\"path\":\"\/\",\"value\":\"0\"},{\"name\":\"tcp_isregistered\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"acctStatus\",\"path\":\"\/\",\"value\":\"active\"},{\"name\":\"WC_AUTHENTICATION_{WC_AUTHENTICATION1}\",\"path\":\"\/\",\"value\":\"{WC_AUTHENTICATION2}\"},{\"name\":\"tier\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"showLoyaltyWebsiteInd\",\"path\":\"\/\",\"value\":\"{showLoyaltyWebsiteInd}\"},{\"name\":\"memberSince\",\"path\":\"\/\",\"value\":\"{memberSince}}\"},{\"name\":\"WC_PERSISTENT\",\"path\":\"\/\",\"value\":\"{WC_PERSISTENT}\"},{\"name\":\"tcp_email_signup_modal_persistent\",\"path\":\"\/\",\"value\":\"{tcp_email_signup_modal_persistent}\"},{\"name\":\"WC_USERACTIVITY_{WC_USERACTIVITY1}\",\"path\":\"\/\",\"value\":\"{WC_USERACTIVITY2}\"}]}",
					//"Body={\"cookie\":[{\"name\":\"iShippingCookie\",\"path\":\"\/\",\"value\":\"US|en_US|USD|1.0|1.0\"},{\"name\":\"LandingBrand\",\"path\":\"\/\",\"value\":\"tcp\"},{\"name\":\"cartItemsCount\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"WC_ACTIVEPOINTER\",\"path\":\"\/\",\"value\":\"{WC_ACTIVEPOINTER}\"},{\"name\":\"QuantumMetricUserID\",\"path\":\"\/\",\"value\":\"e742f8e36551b98fc02ee9caa9336015\"},{\"name\":\"WC_SESSION_ESTABLISHED\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"tcp_isregistered\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"WC_AUTHENTICATION_{WC_AUTHENTICATION1}\",\"path\":\"\/\",\"value\":\"{WC_AUTHENTICATION2}\"},{\"name\":\"WC_PERSISTENT\",\"path\":\"\/\",\"value\":\"{WC_PERSISTENT}\"},{\"name\":\"WC_USERACTIVITY_{WC_USERACTIVITY1}\",\"path\":\"\/\",\"value\":\"{WC_USERACTIVITY2}\"}]}",
					LAST);
			} else {
				
				web_custom_request("navigateXHRBroseLogin",
					"URL=https://{myurlhost}/api/v2/appconfig/navigateXHR",
					"Method=POST",
					"Resource=0",
					"Mode=HTML",
					"Body={\"cookie\":[{\"name\":\"tcp_email_signup_modal_persistent\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"tcp_email_signup_modal_session\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"token\",\"path\":\"\/\",\"value\":\"{token}\"},{\"name\":\"iShippingCookie\",\"path\":\"\/\",\"value\":\"US|en_US|USD|1.0|1.0\"},{\"name\":\"tcpState\",\"path\":\"\/\",\"value\":\"NJ\"},{\"name\":\"tcpSegment\",\"path\":\"\/\",\"value\":\"5\"},{\"name\":\"tcpGeoLoc\",\"path\":\"\/\",\"value\":\"40.7808|-74.0651\"},{\"name\":\"tcpCountryDetail\",\"path\":\"\/\",\"value\":\"US\"},{\"name\":\"check\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"LandingBrand\",\"path\":\"\/\",\"value\":\"{p_Brand}\"},{\"name\":\"pv\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"cartItemsCount\",\"path\":\"\/\",\"value\":\"{cartCount}\"},{\"name\":\"WC_ACTIVEPOINTER\",\"path\":\"\/\",\"value\":\"{WC_ACTIVEPOINTER}\"},{\"name\":\"s_ecid\",\"path\":\"\/\",\"value\":\"MCMID%7C34000030970867548164616407686451354871\"},{\"name\":\"BVBRANDSID\",\"path\":\"\/\",\"value\":\"2788e75d-ee69-4446-862e-36af7d8950bb\"},{\"name\":\"BVBRANDID\",\"path\":\"\/\",\"value\":\"1b40b68f-ac30-4f84-9e66-86785a87d780\"},{\"name\":\"AMCVS_9A0A1C8B5329646E0A490D4D%40AdobeOrg\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"ls-brand\",\"path\":\"\/\",\"value\":\"{p_Brand}\"},{\"name\":\"AMCV_9A0A1C8B5329646E0A490D4D%40AdobeOrg\",\"path\":\"\/\",\"value\":\"-1712354808%7CMCIDTS%7C18325%7CMCMID%7C34000030970867548164616407686451354871%7CMCAID%7CNONE%7CMCOPTOUT-1583257021s%7CNONE%7CMCAAMLH-1583854621%7C7%7CMCAAMB-1583854621%7Cj8Odv6LonN4r3an7LhD3WZrU1bUpAkFkkiY1ncBR96t2PTI%7CvVersion%7C4.3.0\"},{\"name\":\"mbox\",\"path\":\"\/\",\"value\":\"session#e64fff93c950415a884884a06928e20e#1583251684|PC#e64fff93c950415a884884a06928e20e.17_0#1646494624\"},{\"name\":\"_caid\",\"path\":\"\/\",\"value\":\"82f71d1a-68f7-47b2-ae1a-27a5b8abcca9\"},{\"name\":\"_cavisit\",\"path\":\"\/\",\"value\":\"170a10b1bbc|\"},{\"name\":\"_fbp\",\"path\":\"\/\",\"value\":\"fb.1.1583249826069.385047578\"},{\"name\":\"unbxd.userId\",\"path\":\"\/\",\"value\":\"uid-1583249826115-98387\"},{\"name\":\"unbxd.visit\",\"path\":\"\/\",\"value\":\"first_time\"},{\"name\":\"unbxd.visitId\",\"path\":\"\/\",\"value\":\"visitId-1583249826163-2299\"},{\"name\":\"IR_gbd\",\"path\":\"\/\",\"value\":\"{domain}\"},{\"name\":\"IR_3971\",\"path\":\"\/\",\"value\":\"1583249826181%7C0%7C1583249826181%7C%7C\"},{\"name\":\"extole_access_token\",\"path\":\"\/\",\"value\":\"U34CMTMQ77QQF7KOH4KC9CQ6E7\"},{\"name\":\"QuantumMetricUserID\",\"path\":\"\/\",\"value\":\"e742f8e36551b98fc02ee9caa9336015\"},{\"name\":\"QuantumMetricSessionID\",\"path\":\"\/\",\"value\":\"749c3f315b1c401a3e3ad87616329db4\"},{\"name\":\"ipe_s\",\"path\":\"\/\",\"value\":\"33412062-9c8b-174a-b444-46689250ed91\"},{\"name\":\"ipe.30858.pageViewedCount\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"ipe.30858.pageViewedDay\",\"path\":\"\/\",\"value\":\"63\"},{\"name\":\"ipe_30858_fov\",\"path\":\"\/\",\"value\":\"{\\\"numberOfVisits\\\":1,\\\"sessionId\\\":\\\"33412062-9c8b-174a-b444-46689250ed91\\\",\\\"expiry\\\":\\\"2020-06-02T14:37:08.038Z\\\",\\\"lastVisit\\\":\\\"2020-03-03T15:37:08.038Z\\\"}\"},{\"name\":\"RT\",\"path\":\"\/\",\"value\":\"\\\"z\"},{\"name\":\"s_sess\",\"path\":\"\/\",\"value\":\"%20s_cm%3DTyped%252FBookmarkedTyped%252FBookmarkedundefined%3B%20s_cc%3Dtrue%3B%20v26%3Dnon-internal%2520search%3B%20s_cmdl%3D1%3B\"},{\"name\":\"s_pers\",\"path\":\"\/\",\"value\":\"%20v14%3D1583249854886-New%7C1585841854886%3B%20v28%3Dgl%253Ahome%2520page%7C1583251654894%3B\"},{\"name\":\"s_sq\",\"path\":\"\/\",\"value\":\"tcpglobalprod%3D%2526c.%2526a.%2526activitymap.%2526page%253Dgl%25253Ahome%252520page%2526link%253DLOG%252520IN%2526region%253Dtcp%2526pageIDType%253D1%2526.activitymap%2526.a%2526.c%2526pid%253Dgl%25253Ahome%252520page%2526pidt%253D1%2526oid%253DLOG%252520IN%2526oidt%253D3%2526ot%253DSUBMIT\"},{\"name\":\"hasPLCC\",\"path\":\"\/\",\"value\":\"false\"},{\"name\":\"WC_SESSION_ESTABLISHED\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"tcp_firstname\",\"path\":\"\/\",\"value\":\"{tcp_firstname}\"},{\"name\":\"acctId\",\"path\":\"\/\",\"value\":\"{acctId}\"},{\"name\":\"pntsAvail\",\"path\":\"\/\",\"value\":\"0\"},{\"name\":\"tcp_isregistered\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"acctStatus\",\"path\":\"\/\",\"value\":\"active\"},{\"name\":\"WC_AUTHENTICATION_{WC_AUTHENTICATION1}\",\"path\":\"\/\",\"value\":\"{WC_AUTHENTICATION2}\"},{\"name\":\"tier\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"showLoyaltyWebsiteInd\",\"path\":\"\/\",\"value\":\"{showLoyaltyWebsiteInd}\"},{\"name\":\"memberSince\",\"path\":\"\/\",\"value\":\"{memberSince}}\"},{\"name\":\"WC_PERSISTENT\",\"path\":\"\/\",\"value\":\"{WC_PERSISTENT}\"},{\"name\":\"tcp_email_signup_modal_persistent\",\"path\":\"\/\",\"value\":\"{tcp_email_signup_modal_persistent}\"},{\"name\":\"WC_USERACTIVITY_{WC_USERACTIVITY1}\",\"path\":\"\/\",\"value\":\"{WC_USERACTIVITY2}\"}]}",
					//"Body={\"cookie\":[{\"name\":\"iShippingCookie\",\"path\":\"\/\",\"value\":\"US|en_US|USD|1.0|1.0\"},{\"name\":\"LandingBrand\",\"path\":\"\/\",\"value\":\"tcp\"},{\"name\":\"cartItemsCount\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"WC_ACTIVEPOINTER\",\"path\":\"\/\",\"value\":\"{WC_ACTIVEPOINTER}\"},{\"name\":\"QuantumMetricUserID\",\"path\":\"\/\",\"value\":\"e742f8e36551b98fc02ee9caa9336015\"},{\"name\":\"WC_SESSION_ESTABLISHED\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"tcp_isregistered\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"WC_AUTHENTICATION_{WC_AUTHENTICATION1}\",\"path\":\"\/\",\"value\":\"{WC_AUTHENTICATION2}\"},{\"name\":\"WC_PERSISTENT\",\"path\":\"\/\",\"value\":\"{WC_PERSISTENT}\"},{\"name\":\"WC_USERACTIVITY_{WC_USERACTIVITY1}\",\"path\":\"\/\",\"value\":\"{WC_USERACTIVITY2}\"}]}",
					LAST);
			}
		} else {
			web_custom_request("navigateXHRATC",
				"URL=https://{myurlhost}/api/v2/appconfig/navigateXHR",
				"Method=POST",
				"Resource=0",
				"Mode=HTML",
				//"Body={\"cookie\":[{\"name\":\"tcp_email_signup_modal_persistent\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"tcp_email_signup_modal_session\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"token\",\"path\":\"\/\",\"value\":\"{token}\"},{\"name\":\"iShippingCookie\",\"path\":\"\/\",\"value\":\"US|en_US|USD|1.0|1.0\"},{\"name\":\"tcpState\",\"path\":\"\/\",\"value\":\"NJ\"},{\"name\":\"tcpSegment\",\"path\":\"\/\",\"value\":\"5\"},{\"name\":\"tcpGeoLoc\",\"path\":\"\/\",\"value\":\"40.7808|-74.0651\"},{\"name\":\"tcpCountryDetail\",\"path\":\"\/\",\"value\":\"US\"},{\"name\":\"check\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"LandingBrand\",\"path\":\"\/\",\"value\":\"{p_Brand}\"},{\"name\":\"WC_ACTIVEPOINTER\",\"path\":\"\/\",\"value\":\"{WC_ACTIVEPOINTER}\"},{\"name\":\"s_ecid\",\"path\":\"\/\",\"value\":\"MCMID%7C34000030970867548164616407686451354871\"},{\"name\":\"BVBRANDID\",\"path\":\"\/\",\"value\":\"1b40b68f-ac30-4f84-9e66-86785a87d780\"},{\"name\":\"AMCVS_9A0A1C8B5329646E0A490D4D%40AdobeOrg\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"ls-brand\",\"path\":\"\/\",\"value\":\"{p_Brand}\"},{\"name\":\"_caid\",\"path\":\"\/\",\"value\":\"82f71d1a-68f7-47b2-ae1a-27a5b8abcca9\"},{\"name\":\"_fbp\",\"path\":\"\/\",\"value\":\"fb.1.1583249826069.385047578\"},{\"name\":\"unbxd.userId\",\"path\":\"\/\",\"value\":\"uid-1583249826115-98387\"},{\"name\":\"IR_gbd\",\"path\":\"\/\",\"value\":\"{domain}\"},{\"name\":\"QuantumMetricUserID\",\"path\":\"\/\",\"value\":\"e742f8e36551b98fc02ee9caa9336015\"},{\"name\":\"ipe_s\",\"path\":\"\/\",\"value\":\"33412062-9c8b-174a-b444-46689250ed91\"},{\"name\":\"ipe.30858.pageViewedCount\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"ipe.30858.pageViewedDay\",\"path\":\"\/\",\"value\":\"63\"},{\"name\":\"ipe_30858_fov\",\"path\":\"\/\",\"value\":\"{\\\"numberOfVisits\\\":1,\\\"sessionId\\\":\\\"33412062-9c8b-174a-b444-46689250ed91\\\",\\\"expiry\\\":\\\"2020-04-02T14:37:08.038Z\\\",\\\"lastVisit\\\":\\\"2020-03-03T15:37:08.038Z\\\"}\"},{\"name\":\"QuantumMetricSessionID\",\"path\":\"\/\",\"value\":\"4716617c454f1dc7e75302c99e885581\"},{\"name\":\"WC_GENERIC_ACTIVITYDATA\",\"path\":\"\/\",\"value\":\"[13629343091%3Atrue%3Afalse%3A0%3AbfErKi%2Bq9qy0nwsAq30qLt%2BfwR4%3D][com.ibm.commerce.context.audit.AuditContext|1583249855132-56799][com.ibm.commerce.store.facade.server.context.StoreGeoCodeContext|null%26null%26null%26null%26null%26null][CTXSETNAME|Default][com.ibm.commerce.context.globalization.GlobalizationContext|-1%26USD%26-1%26USD][com.ibm.commerce.catalog.businesscontext.CatalogContext|null%26null%26false%26false%26false][com.ibm.commerce.context.ExternalCartContext|null][com.ibm.commerce.context.base.BaseContext|10151%26-1002%26-1002%26-1][com.ibm.commerce.context.entitlement.EntitlementContext|null%26null%26null%26null%26null%26null%26null][com.ibm.commerce.giftcenter.context.GiftCenterContext|null%26null%26null]\"},{\"name\":\"sflItemsCount_CA\",\"path\":\"\/\",\"value\":\"0\"},{\"name\":\"sflItemsCount_US\",\"path\":\"\/\",\"value\":\"0\"},{\"name\":\"AMCV_9A0A1C8B5329646E0A490D4D%40AdobeOrg\",\"path\":\"\/\",\"value\":\"-1712354808%7CMCIDTS%7C18325%7CMCMID%7C34000030970867548164616407686451354871%7CMCAID%7CNONE%7CMCOPTOUT-1583277542s%7CNONE%7CMCAAMLH-1583875142%7C7%7CMCAAMB-1583875142%7Cj8Odv6LonN4r3an7LhD3WZrU1bUpAkFkkiY1ncBR96t2PTI%7CvVersion%7C4.3.0\"},{\"name\":\"BVBRANDSID\",\"path\":\"\/\",\"value\":\"93cce1f7-b63f-4ecd-bc62-688bdbd8f702\"},{\"name\":\"_cavisit\",\"path\":\"\/\",\"value\":\"170a2444111|\"},{\"name\":\"unbxd.visit\",\"path\":\"\/\",\"value\":\"repeat\"},{\"name\":\"unbxd.visitId\",\"path\":\"\/\",\"value\":\"visitId-1583270347380-50688\"},{\"name\":\"IR_3971\",\"path\":\"\/\",\"value\":\"1583270347398%7C0%7C1583270347398%7C%7C\"},{\"name\":\"pv\",\"path\":\"\/\",\"value\":\"3\"},{\"name\":\"extole_access_token\",\"path\":\"\/\",\"value\":\"ERR2VQUHOCD67KP5GDHNJNL85T\"},{\"name\":\"RT\",\"path\":\"\/\",\"value\":\"\\\"z\"},{\"name\":\"cartItemsCount\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"WC_SESSION_ESTABLISHED\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"mbox\",\"path\":\"\/\",\"value\":\"PC#e64fff93c950415a884884a06928e20e.17_0#1646494624|session#9db66d5abb6f4aedb430796970dc5af8#1583272284\"},{\"name\":\"s_sess\",\"path\":\"\/\",\"value\":\"%20s_cm%3DTyped%252FBookmarkedTyped%252FBookmarkedundefined%3B%20v26%3Dnon-internal%2520search%3B%20s_cc%3Dtrue%3B%20s_cmdl%3D1%3B\"},{\"name\":\"s_pers\",\"path\":\"\/\",\"value\":\"%20v14%3D1583270463807-Repeat%7C1585862463807%3B%20v28%3Dgl%253Abrowse%253Agirl%253Abottoms%7C1583272263813%3B\"},{\"name\":\"s_sq\",\"path\":\"\/\",\"value\":\"tcpglobalprod%3D%2526c.%2526a.%2526activitymap.%2526page%253Dgl%25253Abrowse%25253Agirl%25253Abottoms%2526link%253DLOG%252520IN%2526region%253Dtcp%2526pageIDType%253D1%2526.activitymap%2526.a%2526.c%2526pid%253Dgl%25253Abrowse%25253Agirl%25253Abottoms%2526pidt%253D1%2526oid%253DLOG%252520IN%2526oidt%253D3%2526ot%253DSUBMIT\"},{\"name\":\"WC_AUTHENTICATION_{WC_AUTHENTICATION1}\",\"path\":\"\/\",\"value\":\"{WC_AUTHENTICATION2}\"},{\"name\":\"orderItemsQtyList\",\"path\":\"\/\",\"value\":\"1,1,1,1,1,1,1,1\"},{\"name\":\"tcp_email_signup_modal_persistent\",\"path\":\"\/\",\"value\":\"{tcp_email_signup_modal_persistent}\"},{\"name\":\"tcp_isregistered\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"WC_PERSISTENT\",\"path\":\"\/\",\"value\":\"{WC_PERSISTENT}\"},{\"name\":\"tier\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"showLoyaltyWebsiteInd\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"tcp_firstname\",\"path\":\"\/\",\"value\":\"{tcp_firstname}\"},{\"name\":\"hasPLCC\",\"path\":\"\/\",\"value\":\"false\"},{\"name\":\"WC_USERACTIVITY_{WC_USERACTIVITY1}\",\"path\":\"\/\",\"value\":\"{WC_USERACTIVITY1}\"},{\"name\":\"pntsAvail\",\"path\":\"\/\",\"value\":\"0\"},{\"name\":\"memberSince\",\"path\":\"\/\",\"value\":\"{memberSince}\"},{\"name\":\"myPlaceAcctNbr\",\"path\":\"\/\",\"value\":\"{myPlaceAcctNbr}\"}]}",
				"Body={\"cookie\":[{\"name\":\"tcp_email_signup_modal_persistent\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"tcp_email_signup_modal_session\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"iShippingCookie\",\"path\":\"\/\",\"value\":\"US|en_US|USD|1.0|1.0\"},{\"name\":\"tcpState\",\"path\":\"\/\",\"value\":\"NJ\"},{\"name\":\"tcpSegment\",\"path\":\"\/\",\"value\":\"5\"},{\"name\":\"tcpGeoLoc\",\"path\":\"\/\",\"value\":\"40.7808|-74.0651\"},{\"name\":\"tcpCountryDetail\",\"path\":\"\/\",\"value\":\"US\"},{\"name\":\"check\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"LandingBrand\",\"path\":\"\/\",\"value\":\"tcp\"},{\"name\":\"WC_ACTIVEPOINTER\",\"path\":\"\/\",\"value\":\"{WC_ACTIVEPOINTER}\"},{\"name\":\"s_ecid\",\"path\":\"\/\",\"value\":\"MCMID%7C34000030970867548164616407686451354871\"},{\"name\":\"BVBRANDID\",\"path\":\"\/\",\"value\":\"1b40b68f-ac30-4f84-9e66-86785a87d780\"},{\"name\":\"AMCVS_9A0A1C8B5329646E0A490D4D%40AdobeOrg\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"ls-brand\",\"path\":\"\/\",\"value\":\"{p_Brand}\"},{\"name\":\"_caid\",\"path\":\"\/\",\"value\":\"82f71d1a-68f7-47b2-ae1a-27a5b8abcca9\"},{\"name\":\"_fbp\",\"path\":\"\/\",\"value\":\"fb.1.1583249826069.385047578\"},{\"name\":\"unbxd.userId\",\"path\":\"\/\",\"value\":\"uid-1583249826115-98387\"},{\"name\":\"IR_gbd\",\"path\":\"\/\",\"value\":\"{domain}\"},{\"name\":\"QuantumMetricUserID\",\"path\":\"\/\",\"value\":\"e742f8e36551b98fc02ee9caa9336015\"},{\"name\":\"ipe_s\",\"path\":\"\/\",\"value\":\"33412062-9c8b-174a-b444-46689250ed91\"},{\"name\":\"QuantumMetricSessionID\",\"path\":\"\/\",\"value\":\"4716617c454f1dc7e75302c99e885581\"},{\"name\":\"token\",\"path\":\"\/\",\"value\":\"{token}\"},{\"name\":\"BVBRANDSID\",\"path\":\"\/\",\"value\":\"de06dd83-e927-40a3-b052-733033e616ac\"},{\"name\":\"AMCV_9A0A1C8B5329646E0A490D4D%40AdobeOrg\",\"path\":\"\/\",\"value\":\"-1712354808%7CMCIDTS%7C18325%7CMCMID%7C34000030970867548164616407686451354871%7CMCAID%7CNONE%7CMCOPTOUT-1583343396s%7CNONE%7CMCAAMLH-1583940996%7C7%7CMCAAMB-1583940996%7Cj8Odv6LonN4r3an7LhD3WZrU1bUpAkFkkiY1ncBR96t2PTI%7CvVersion%7C4.3.0\"},{\"name\":\"_cavisit\",\"path\":\"\/\",\"value\":\"170a631187e|\"},{\"name\":\"unbxd.visit\",\"path\":\"\/\",\"value\":\"repeat\"},{\"name\":\"unbxd.visitId\",\"path\":\"\/\",\"value\":\"visitId-1583336200656-37676\"},{\"name\":\"ipe.30858.pageViewedDay\",\"path\":\"\/\",\"value\":\"64\"},{\"name\":\"tcp_email_signup_modal_persistent\",\"path\":\"\/\",\"value\":\"{tcp_email_signup_modal_persistent} GMT\"},{\"name\":\"acctId\",\"path\":\"\/\",\"value\":\"{acctId}\"},{\"name\":\"orderItemsQtyList\",\"path\":\"\/\",\"value\":\"1,1,1,1,1,1,1,1,1\"},{\"name\":\"acctStatus\",\"path\":\"\/\",\"value\":\"active\"},{\"name\":\"tier\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"showLoyaltyWebsiteInd\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"myPlaceAcctNbr\",\"path\":\"\/\",\"value\":\"\\\"\\\"\"},{\"name\":\"sflItemsCount_US\",\"path\":\"\/\",\"value\":\"0\"},{\"name\":\"sflItemsCount_CA\",\"path\":\"\/\",\"value\":\"0\"},{\"name\":\"WC_SESSION_ESTABLISHED\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"IR_3971\",\"path\":\"\/\",\"value\":\"1583337381710%7C0%7C1583336200789%7C%7C\"},{\"name\":\"ipe.30858.pageViewedCount\",\"path\":\"\/\",\"value\":\"2\"},{\"name\":\"ipe_30858_fov\",\"path\":\"\/\",\"value\":\"{\\\"numberOfVisits\\\":1,\\\"sessionId\\\":\\\"33412062-9c8b-174a-b444-46689250ed91\\\",\\\"expiry\\\":\\\"2020-06-02T14:37:08.038Z\\\",\\\"lastVisit\\\":\\\"2020-03-04T15:56:23.452Z\\\"}\"},{\"name\":\"pv\",\"path\":\"\/\",\"value\":\"15\"},{\"name\":\"mbox\",\"path\":\"\/\",\"value\":\"PC#e64fff93c950415a884884a06928e20e.17_0#1646494624|session#e66b66bc9b134196b219a245a5234f08#1583339255\"},{\"name\":\"extole_access_token\",\"path\":\"\/\",\"value\":\"E9SRDB8HJCUAHH9H225QQ7LGJQ\"},{\"name\":\"RT\",\"path\":\"\/\",\"value\":\"\\\"z\"},{\"name\":\"s_sess\",\"path\":\"\/\",\"value\":\"%20s_cm%3DTyped%252FBookmarkedTyped%252FBookmarkedundefined%3B%20v26%3Dnon-internal%2520search%3B%20s_cc%3Dtrue%3B%20s_cmdl%3D1%3B\"},{\"name\":\"s_pers\",\"path\":\"\/\",\"value\":\"%20v14%3D1583337401821-Repeat%7C1585929401821%3B%20v28%3Dgl%253Abrowse%253Agirl%253Ajeans%7C1583339201825%3B\"},{\"name\":\"s_sq\",\"path\":\"\/\",\"value\":\"tcpglobalprod%3D%2526c.%2526a.%2526activitymap.%2526page%253Dgl%25253Abrowse%25253Agirl%25253Ajeans%2526link%253DADD%252520TO%252520BAG%2526region%253Dtcp%2526pageIDType%253D1%2526.activitymap%2526.a%2526.c%2526pid%253Dgl%25253Abrowse%25253Agirl%25253Ajeans%2526pidt%253D1%2526oid%253Dfunctionbr%252528%252529%25257B%25257D%2526oidt%253D2%2526ot%253DSUBMIT\"},{\"name\":\"cartItemsCount\",\"path\":\"\/\",\"value\":\"{cartCount}\"},{\"name\":\"WC_PERSISTENT\",\"path\":\"\/\",\"value\":\"{WC_PERSISTENT}\"}]}",
				//"Body={\"cookie\":[{\"name\":\"iShippingCookie\",\"path\":\"\/\",\"value\":\"US|en_US|USD|1.0|1.0\"},{\"name\":\"LandingBrand\",\"path\":\"\/\",\"value\":\"tcp\"},{\"name\":\"cartItemsCount\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"WC_ACTIVEPOINTER\",\"path\":\"\/\",\"value\":\"{WC_ACTIVEPOINTER}\"},{\"name\":\"QuantumMetricUserID\",\"path\":\"\/\",\"value\":\"e742f8e36551b98fc02ee9caa9336015\"},{\"name\":\"WC_SESSION_ESTABLISHED\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"tcp_isregistered\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"WC_AUTHENTICATION_{WC_AUTHENTICATION1}\",\"path\":\"\/\",\"value\":\"{WC_AUTHENTICATION2}\"},{\"name\":\"WC_PERSISTENT\",\"path\":\"\/\",\"value\":\"{WC_PERSISTENT}\"},{\"name\":\"WC_USERACTIVITY_{WC_USERACTIVITY1}\",\"path\":\"\/\",\"value\":\"{WC_USERACTIVITY2}\"}]}",
				LAST);
		}
		
		if(atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction (lr_eval_string("{mainTransaction}_navigateXHR"), LR_FAIL) ;
		else
			lr_end_sub_transaction (lr_eval_string("{mainTransaction}_navigateXHR"), LR_AUTO) ;

//		lr_save_string("true", "navigateXHR");
	}
	
	return 0;
}
int CheckAndCallnavigateXHR1()
{
	lr_save_string("tcp-perf.childrensplace.com","myurlhost");
	if(strcmp(lr_eval_string("{navigateXHR}"), "false") == 0 ) {
		parseUserActivity();
		parseWC_AUTHENTICATION();
/*		
		if(strcmp(lr_eval_string("{navigatePath}"), "login") != 0) {
			if (parseHeaderInfo() == LR_FAIL) {
				lr_save_string("false", "navigateXHR");
				return 0;
			}
		}
*/		
		lr_start_sub_transaction (lr_eval_string("{mainTransaction}_navigateXHR"), lr_eval_string("{mainTransaction}") ) ; //added 06212018
			
		web_add_header("Content-Type", "application/json");
		//web_add_header("origin", lr_eval_string("https://{myurlhost}"));
		web_add_header("Accept", "application/json");
		web_reg_find("TEXT/IC=SUCCESS", "SaveCount=apiCheck", LAST);
		
		if(strcmp(lr_eval_string("{navigatePath}"), "login") == 0) {

			if(strcmp(lr_eval_string("{loginFirst}"), "true") == 0) {

				web_custom_request("navigateXHRLoginFirst",
					"URL=https://{myurlhost}/api/v2/appconfig/navigateXHR",
					"Method=POST",
					"Resource=0",
					"Mode=HTML",
					"Body={\"cookie\":[{\"name\":\"tcp_email_signup_modal_persistent\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"tcp_email_signup_modal_session\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"token\",\"path\":\"\/\",\"value\":\"{token}\"},{\"name\":\"iShippingCookie\",\"path\":\"\/\",\"value\":\"US|en_US|USD|1.0|1.0\"},{\"name\":\"tcpState\",\"path\":\"\/\",\"value\":\"NJ\"},{\"name\":\"tcpSegment\",\"path\":\"\/\",\"value\":\"5\"},{\"name\":\"tcpGeoLoc\",\"path\":\"\/\",\"value\":\"40.7808|-74.0651\"},{\"name\":\"tcpCountryDetail\",\"path\":\"\/\",\"value\":\"US\"},{\"name\":\"check\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"LandingBrand\",\"path\":\"\/\",\"value\":\"{p_Brand}\"},{\"name\":\"pv\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"cartItemsCount\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"WC_ACTIVEPOINTER\",\"path\":\"\/\",\"value\":\"{WC_ACTIVEPOINTER}\"},{\"name\":\"s_ecid\",\"path\":\"\/\",\"value\":\"MCMID%7C34000030970867548164616407686451354871\"},{\"name\":\"BVBRANDSID\",\"path\":\"\/\",\"value\":\"2788e75d-ee69-4446-862e-36af7d8950bb\"},{\"name\":\"BVBRANDID\",\"path\":\"\/\",\"value\":\"1b40b68f-ac30-4f84-9e66-86785a87d780\"},{\"name\":\"AMCVS_9A0A1C8B5329646E0A490D4D%40AdobeOrg\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"ls-brand\",\"path\":\"\/\",\"value\":\"{p_Brand}\"},{\"name\":\"AMCV_9A0A1C8B5329646E0A490D4D%40AdobeOrg\",\"path\":\"\/\",\"value\":\"-1712354808%7CMCIDTS%7C18325%7CMCMID%7C34000030970867548164616407686451354871%7CMCAID%7CNONE%7CMCOPTOUT-1583257021s%7CNONE%7CMCAAMLH-1583854621%7C7%7CMCAAMB-1583854621%7Cj8Odv6LonN4r3an7LhD3WZrU1bUpAkFkkiY1ncBR96t2PTI%7CvVersion%7C4.3.0\"},{\"name\":\"mbox\",\"path\":\"\/\",\"value\":\"session#e64fff93c950415a884884a06928e20e#1583251684|PC#e64fff93c950415a884884a06928e20e.17_0#1646494624\"},{\"name\":\"_caid\",\"path\":\"\/\",\"value\":\"82f71d1a-68f7-47b2-ae1a-27a5b8abcca9\"},{\"name\":\"_cavisit\",\"path\":\"\/\",\"value\":\"170a10b1bbc|\"},{\"name\":\"_fbp\",\"path\":\"\/\",\"value\":\"fb.1.1583249826069.385047578\"},{\"name\":\"unbxd.userId\",\"path\":\"\/\",\"value\":\"uid-1583249826115-98387\"},{\"name\":\"unbxd.visit\",\"path\":\"\/\",\"value\":\"first_time\"},{\"name\":\"unbxd.visitId\",\"path\":\"\/\",\"value\":\"visitId-1583249826163-2299\"},{\"name\":\"IR_gbd\",\"path\":\"\/\",\"value\":\"{domain}\"},{\"name\":\"IR_3971\",\"path\":\"\/\",\"value\":\"1583249826181%7C0%7C1583249826181%7C%7C\"},{\"name\":\"extole_access_token\",\"path\":\"\/\",\"value\":\"U34CMTMQ77QQF7KOH4KC9CQ6E7\"},{\"name\":\"QuantumMetricUserID\",\"path\":\"\/\",\"value\":\"e742f8e36551b98fc02ee9caa9336015\"},{\"name\":\"QuantumMetricSessionID\",\"path\":\"\/\",\"value\":\"749c3f315b1c401a3e3ad87616329db4\"},{\"name\":\"ipe_s\",\"path\":\"\/\",\"value\":\"33412062-9c8b-174a-b444-46689250ed91\"},{\"name\":\"ipe.30858.pageViewedCount\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"ipe.30858.pageViewedDay\",\"path\":\"\/\",\"value\":\"63\"},{\"name\":\"ipe_30858_fov\",\"path\":\"\/\",\"value\":\"{\\\"numberOfVisits\\\":1,\\\"sessionId\\\":\\\"33412062-9c8b-174a-b444-46689250ed91\\\",\\\"expiry\\\":\\\"2020-06-02T14:37:08.038Z\\\",\\\"lastVisit\\\":\\\"2020-03-03T15:37:08.038Z\\\"}\"},{\"name\":\"RT\",\"path\":\"\/\",\"value\":\"\\\"z\"},{\"name\":\"s_sess\",\"path\":\"\/\",\"value\":\"%20s_cm%3DTyped%252FBookmarkedTyped%252FBookmarkedundefined%3B%20s_cc%3Dtrue%3B%20v26%3Dnon-internal%2520search%3B%20s_cmdl%3D1%3B\"},{\"name\":\"s_pers\",\"path\":\"\/\",\"value\":\"%20v14%3D1583249854886-New%7C1585841854886%3B%20v28%3Dgl%253Ahome%2520page%7C1583251654894%3B\"},{\"name\":\"s_sq\",\"path\":\"\/\",\"value\":\"tcpglobalprod%3D%2526c.%2526a.%2526activitymap.%2526page%253Dgl%25253Ahome%252520page%2526link%253DLOG%252520IN%2526region%253Dtcp%2526pageIDType%253D1%2526.activitymap%2526.a%2526.c%2526pid%253Dgl%25253Ahome%252520page%2526pidt%253D1%2526oid%253DLOG%252520IN%2526oidt%253D3%2526ot%253DSUBMIT\"},{\"name\":\"hasPLCC\",\"path\":\"\/\",\"value\":\"false\"},{\"name\":\"WC_SESSION_ESTABLISHED\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"tcp_firstname\",\"path\":\"\/\",\"value\":\"{tcp_firstname}\"},{\"name\":\"acctId\",\"path\":\"\/\",\"value\":\"{acctId}\"},{\"name\":\"pntsAvail\",\"path\":\"\/\",\"value\":\"0\"},{\"name\":\"tcp_isregistered\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"acctStatus\",\"path\":\"\/\",\"value\":\"active\"},{\"name\":\"WC_AUTHENTICATION_{WC_AUTHENTICATION1}\",\"path\":\"\/\",\"value\":\"{WC_AUTHENTICATION2}\"},{\"name\":\"tier\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"showLoyaltyWebsiteInd\",\"path\":\"\/\",\"value\":\"{showLoyaltyWebsiteInd}\"},{\"name\":\"memberSince\",\"path\":\"\/\",\"value\":\"{memberSince}}\"},{\"name\":\"WC_PERSISTENT\",\"path\":\"\/\",\"value\":\"{WC_PERSISTENT}\"},{\"name\":\"tcp_email_signup_modal_persistent\",\"path\":\"\/\",\"value\":\"{tcp_email_signup_modal_persistent}\"},{\"name\":\"WC_USERACTIVITY_{WC_USERACTIVITY1}\",\"path\":\"\/\",\"value\":\"{WC_USERACTIVITY2}\"}]}",
					LAST);
			} else {
				
				web_custom_request("navigateXHRBroseLogin",
					"URL=https://{myurlhost}/api/v2/appconfig/navigateXHR",
					"Method=POST",
					"Resource=0",
					"Mode=HTML",
					"Body={\"cookie\":[{\"name\":\"tcp_email_signup_modal_persistent\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"tcp_email_signup_modal_session\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"token\",\"path\":\"\/\",\"value\":\"{token}\"},{\"name\":\"iShippingCookie\",\"path\":\"\/\",\"value\":\"US|en_US|USD|1.0|1.0\"},{\"name\":\"tcpState\",\"path\":\"\/\",\"value\":\"NJ\"},{\"name\":\"tcpSegment\",\"path\":\"\/\",\"value\":\"5\"},{\"name\":\"tcpGeoLoc\",\"path\":\"\/\",\"value\":\"40.7808|-74.0651\"},{\"name\":\"tcpCountryDetail\",\"path\":\"\/\",\"value\":\"US\"},{\"name\":\"check\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"LandingBrand\",\"path\":\"\/\",\"value\":\"{p_Brand}\"},{\"name\":\"pv\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"cartItemsCount\",\"path\":\"\/\",\"value\":\"{cartCount}\"},{\"name\":\"WC_ACTIVEPOINTER\",\"path\":\"\/\",\"value\":\"{WC_ACTIVEPOINTER}\"},{\"name\":\"s_ecid\",\"path\":\"\/\",\"value\":\"MCMID%7C34000030970867548164616407686451354871\"},{\"name\":\"BVBRANDSID\",\"path\":\"\/\",\"value\":\"2788e75d-ee69-4446-862e-36af7d8950bb\"},{\"name\":\"BVBRANDID\",\"path\":\"\/\",\"value\":\"1b40b68f-ac30-4f84-9e66-86785a87d780\"},{\"name\":\"AMCVS_9A0A1C8B5329646E0A490D4D%40AdobeOrg\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"ls-brand\",\"path\":\"\/\",\"value\":\"{p_Brand}\"},{\"name\":\"AMCV_9A0A1C8B5329646E0A490D4D%40AdobeOrg\",\"path\":\"\/\",\"value\":\"-1712354808%7CMCIDTS%7C18325%7CMCMID%7C34000030970867548164616407686451354871%7CMCAID%7CNONE%7CMCOPTOUT-1583257021s%7CNONE%7CMCAAMLH-1583854621%7C7%7CMCAAMB-1583854621%7Cj8Odv6LonN4r3an7LhD3WZrU1bUpAkFkkiY1ncBR96t2PTI%7CvVersion%7C4.3.0\"},{\"name\":\"mbox\",\"path\":\"\/\",\"value\":\"session#e64fff93c950415a884884a06928e20e#1583251684|PC#e64fff93c950415a884884a06928e20e.17_0#1646494624\"},{\"name\":\"_caid\",\"path\":\"\/\",\"value\":\"82f71d1a-68f7-47b2-ae1a-27a5b8abcca9\"},{\"name\":\"_cavisit\",\"path\":\"\/\",\"value\":\"170a10b1bbc|\"},{\"name\":\"_fbp\",\"path\":\"\/\",\"value\":\"fb.1.1583249826069.385047578\"},{\"name\":\"unbxd.userId\",\"path\":\"\/\",\"value\":\"uid-1583249826115-98387\"},{\"name\":\"unbxd.visit\",\"path\":\"\/\",\"value\":\"first_time\"},{\"name\":\"unbxd.visitId\",\"path\":\"\/\",\"value\":\"visitId-1583249826163-2299\"},{\"name\":\"IR_gbd\",\"path\":\"\/\",\"value\":\"{domain}\"},{\"name\":\"IR_3971\",\"path\":\"\/\",\"value\":\"1583249826181%7C0%7C1583249826181%7C%7C\"},{\"name\":\"extole_access_token\",\"path\":\"\/\",\"value\":\"U34CMTMQ77QQF7KOH4KC9CQ6E7\"},{\"name\":\"QuantumMetricUserID\",\"path\":\"\/\",\"value\":\"e742f8e36551b98fc02ee9caa9336015\"},{\"name\":\"QuantumMetricSessionID\",\"path\":\"\/\",\"value\":\"749c3f315b1c401a3e3ad87616329db4\"},{\"name\":\"ipe_s\",\"path\":\"\/\",\"value\":\"33412062-9c8b-174a-b444-46689250ed91\"},{\"name\":\"ipe.30858.pageViewedCount\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"ipe.30858.pageViewedDay\",\"path\":\"\/\",\"value\":\"63\"},{\"name\":\"ipe_30858_fov\",\"path\":\"\/\",\"value\":\"{\\\"numberOfVisits\\\":1,\\\"sessionId\\\":\\\"33412062-9c8b-174a-b444-46689250ed91\\\",\\\"expiry\\\":\\\"2020-06-02T14:37:08.038Z\\\",\\\"lastVisit\\\":\\\"2020-03-03T15:37:08.038Z\\\"}\"},{\"name\":\"RT\",\"path\":\"\/\",\"value\":\"\\\"z\"},{\"name\":\"s_sess\",\"path\":\"\/\",\"value\":\"%20s_cm%3DTyped%252FBookmarkedTyped%252FBookmarkedundefined%3B%20s_cc%3Dtrue%3B%20v26%3Dnon-internal%2520search%3B%20s_cmdl%3D1%3B\"},{\"name\":\"s_pers\",\"path\":\"\/\",\"value\":\"%20v14%3D1583249854886-New%7C1585841854886%3B%20v28%3Dgl%253Ahome%2520page%7C1583251654894%3B\"},{\"name\":\"s_sq\",\"path\":\"\/\",\"value\":\"tcpglobalprod%3D%2526c.%2526a.%2526activitymap.%2526page%253Dgl%25253Ahome%252520page%2526link%253DLOG%252520IN%2526region%253Dtcp%2526pageIDType%253D1%2526.activitymap%2526.a%2526.c%2526pid%253Dgl%25253Ahome%252520page%2526pidt%253D1%2526oid%253DLOG%252520IN%2526oidt%253D3%2526ot%253DSUBMIT\"},{\"name\":\"hasPLCC\",\"path\":\"\/\",\"value\":\"false\"},{\"name\":\"WC_SESSION_ESTABLISHED\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"tcp_firstname\",\"path\":\"\/\",\"value\":\"{tcp_firstname}\"},{\"name\":\"acctId\",\"path\":\"\/\",\"value\":\"{acctId}\"},{\"name\":\"pntsAvail\",\"path\":\"\/\",\"value\":\"0\"},{\"name\":\"tcp_isregistered\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"acctStatus\",\"path\":\"\/\",\"value\":\"active\"},{\"name\":\"WC_AUTHENTICATION_{WC_AUTHENTICATION1}\",\"path\":\"\/\",\"value\":\"{WC_AUTHENTICATION2}\"},{\"name\":\"tier\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"showLoyaltyWebsiteInd\",\"path\":\"\/\",\"value\":\"{showLoyaltyWebsiteInd}\"},{\"name\":\"memberSince\",\"path\":\"\/\",\"value\":\"{memberSince}}\"},{\"name\":\"WC_PERSISTENT\",\"path\":\"\/\",\"value\":\"{WC_PERSISTENT}\"},{\"name\":\"tcp_email_signup_modal_persistent\",\"path\":\"\/\",\"value\":\"{tcp_email_signup_modal_persistent}\"},{\"name\":\"WC_USERACTIVITY_{WC_USERACTIVITY1}\",\"path\":\"\/\",\"value\":\"{WC_USERACTIVITY2}\"}]}",
					LAST);
			}
		} else {
			web_custom_request("navigateXHRATC",
				"URL=https://{myurlhost}/api/v2/appconfig/navigateXHR",
				"Method=POST",
				"Resource=0",
				"Mode=HTML",
				//"Body={\"cookie\":[{\"name\":\"tcp_email_signup_modal_persistent\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"tcp_email_signup_modal_session\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"token\",\"path\":\"\/\",\"value\":\"{token}\"},{\"name\":\"iShippingCookie\",\"path\":\"\/\",\"value\":\"US|en_US|USD|1.0|1.0\"},{\"name\":\"tcpState\",\"path\":\"\/\",\"value\":\"NJ\"},{\"name\":\"tcpSegment\",\"path\":\"\/\",\"value\":\"5\"},{\"name\":\"tcpGeoLoc\",\"path\":\"\/\",\"value\":\"40.7808|-74.0651\"},{\"name\":\"tcpCountryDetail\",\"path\":\"\/\",\"value\":\"US\"},{\"name\":\"check\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"LandingBrand\",\"path\":\"\/\",\"value\":\"{p_Brand}\"},{\"name\":\"WC_ACTIVEPOINTER\",\"path\":\"\/\",\"value\":\"{WC_ACTIVEPOINTER}\"},{\"name\":\"s_ecid\",\"path\":\"\/\",\"value\":\"MCMID%7C34000030970867548164616407686451354871\"},{\"name\":\"BVBRANDID\",\"path\":\"\/\",\"value\":\"1b40b68f-ac30-4f84-9e66-86785a87d780\"},{\"name\":\"AMCVS_9A0A1C8B5329646E0A490D4D%40AdobeOrg\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"ls-brand\",\"path\":\"\/\",\"value\":\"{p_Brand}\"},{\"name\":\"_caid\",\"path\":\"\/\",\"value\":\"82f71d1a-68f7-47b2-ae1a-27a5b8abcca9\"},{\"name\":\"_fbp\",\"path\":\"\/\",\"value\":\"fb.1.1583249826069.385047578\"},{\"name\":\"unbxd.userId\",\"path\":\"\/\",\"value\":\"uid-1583249826115-98387\"},{\"name\":\"IR_gbd\",\"path\":\"\/\",\"value\":\"{domain}\"},{\"name\":\"QuantumMetricUserID\",\"path\":\"\/\",\"value\":\"e742f8e36551b98fc02ee9caa9336015\"},{\"name\":\"ipe_s\",\"path\":\"\/\",\"value\":\"33412062-9c8b-174a-b444-46689250ed91\"},{\"name\":\"ipe.30858.pageViewedCount\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"ipe.30858.pageViewedDay\",\"path\":\"\/\",\"value\":\"63\"},{\"name\":\"ipe_30858_fov\",\"path\":\"\/\",\"value\":\"{\\\"numberOfVisits\\\":1,\\\"sessionId\\\":\\\"33412062-9c8b-174a-b444-46689250ed91\\\",\\\"expiry\\\":\\\"2020-04-02T14:37:08.038Z\\\",\\\"lastVisit\\\":\\\"2020-03-03T15:37:08.038Z\\\"}\"},{\"name\":\"QuantumMetricSessionID\",\"path\":\"\/\",\"value\":\"4716617c454f1dc7e75302c99e885581\"},{\"name\":\"WC_GENERIC_ACTIVITYDATA\",\"path\":\"\/\",\"value\":\"[13629343091%3Atrue%3Afalse%3A0%3AbfErKi%2Bq9qy0nwsAq30qLt%2BfwR4%3D][com.ibm.commerce.context.audit.AuditContext|1583249855132-56799][com.ibm.commerce.store.facade.server.context.StoreGeoCodeContext|null%26null%26null%26null%26null%26null][CTXSETNAME|Default][com.ibm.commerce.context.globalization.GlobalizationContext|-1%26USD%26-1%26USD][com.ibm.commerce.catalog.businesscontext.CatalogContext|null%26null%26false%26false%26false][com.ibm.commerce.context.ExternalCartContext|null][com.ibm.commerce.context.base.BaseContext|10151%26-1002%26-1002%26-1][com.ibm.commerce.context.entitlement.EntitlementContext|null%26null%26null%26null%26null%26null%26null][com.ibm.commerce.giftcenter.context.GiftCenterContext|null%26null%26null]\"},{\"name\":\"sflItemsCount_CA\",\"path\":\"\/\",\"value\":\"0\"},{\"name\":\"sflItemsCount_US\",\"path\":\"\/\",\"value\":\"0\"},{\"name\":\"AMCV_9A0A1C8B5329646E0A490D4D%40AdobeOrg\",\"path\":\"\/\",\"value\":\"-1712354808%7CMCIDTS%7C18325%7CMCMID%7C34000030970867548164616407686451354871%7CMCAID%7CNONE%7CMCOPTOUT-1583277542s%7CNONE%7CMCAAMLH-1583875142%7C7%7CMCAAMB-1583875142%7Cj8Odv6LonN4r3an7LhD3WZrU1bUpAkFkkiY1ncBR96t2PTI%7CvVersion%7C4.3.0\"},{\"name\":\"BVBRANDSID\",\"path\":\"\/\",\"value\":\"93cce1f7-b63f-4ecd-bc62-688bdbd8f702\"},{\"name\":\"_cavisit\",\"path\":\"\/\",\"value\":\"170a2444111|\"},{\"name\":\"unbxd.visit\",\"path\":\"\/\",\"value\":\"repeat\"},{\"name\":\"unbxd.visitId\",\"path\":\"\/\",\"value\":\"visitId-1583270347380-50688\"},{\"name\":\"IR_3971\",\"path\":\"\/\",\"value\":\"1583270347398%7C0%7C1583270347398%7C%7C\"},{\"name\":\"pv\",\"path\":\"\/\",\"value\":\"3\"},{\"name\":\"extole_access_token\",\"path\":\"\/\",\"value\":\"ERR2VQUHOCD67KP5GDHNJNL85T\"},{\"name\":\"RT\",\"path\":\"\/\",\"value\":\"\\\"z\"},{\"name\":\"cartItemsCount\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"WC_SESSION_ESTABLISHED\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"mbox\",\"path\":\"\/\",\"value\":\"PC#e64fff93c950415a884884a06928e20e.17_0#1646494624|session#9db66d5abb6f4aedb430796970dc5af8#1583272284\"},{\"name\":\"s_sess\",\"path\":\"\/\",\"value\":\"%20s_cm%3DTyped%252FBookmarkedTyped%252FBookmarkedundefined%3B%20v26%3Dnon-internal%2520search%3B%20s_cc%3Dtrue%3B%20s_cmdl%3D1%3B\"},{\"name\":\"s_pers\",\"path\":\"\/\",\"value\":\"%20v14%3D1583270463807-Repeat%7C1585862463807%3B%20v28%3Dgl%253Abrowse%253Agirl%253Abottoms%7C1583272263813%3B\"},{\"name\":\"s_sq\",\"path\":\"\/\",\"value\":\"tcpglobalprod%3D%2526c.%2526a.%2526activitymap.%2526page%253Dgl%25253Abrowse%25253Agirl%25253Abottoms%2526link%253DLOG%252520IN%2526region%253Dtcp%2526pageIDType%253D1%2526.activitymap%2526.a%2526.c%2526pid%253Dgl%25253Abrowse%25253Agirl%25253Abottoms%2526pidt%253D1%2526oid%253DLOG%252520IN%2526oidt%253D3%2526ot%253DSUBMIT\"},{\"name\":\"WC_AUTHENTICATION_{WC_AUTHENTICATION1}\",\"path\":\"\/\",\"value\":\"{WC_AUTHENTICATION2}\"},{\"name\":\"orderItemsQtyList\",\"path\":\"\/\",\"value\":\"1,1,1,1,1,1,1,1\"},{\"name\":\"tcp_email_signup_modal_persistent\",\"path\":\"\/\",\"value\":\"{tcp_email_signup_modal_persistent}\"},{\"name\":\"tcp_isregistered\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"WC_PERSISTENT\",\"path\":\"\/\",\"value\":\"{WC_PERSISTENT}\"},{\"name\":\"tier\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"showLoyaltyWebsiteInd\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"tcp_firstname\",\"path\":\"\/\",\"value\":\"{tcp_firstname}\"},{\"name\":\"hasPLCC\",\"path\":\"\/\",\"value\":\"false\"},{\"name\":\"WC_USERACTIVITY_{WC_USERACTIVITY1}\",\"path\":\"\/\",\"value\":\"{WC_USERACTIVITY1}\"},{\"name\":\"pntsAvail\",\"path\":\"\/\",\"value\":\"0\"},{\"name\":\"memberSince\",\"path\":\"\/\",\"value\":\"{memberSince}\"},{\"name\":\"myPlaceAcctNbr\",\"path\":\"\/\",\"value\":\"{myPlaceAcctNbr}\"}]}",
				"Body={\"cookie\":[{\"name\":\"tcp_email_signup_modal_persistent\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"tcp_email_signup_modal_session\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"iShippingCookie\",\"path\":\"\/\",\"value\":\"US|en_US|USD|1.0|1.0\"},{\"name\":\"tcpState\",\"path\":\"\/\",\"value\":\"NJ\"},{\"name\":\"tcpSegment\",\"path\":\"\/\",\"value\":\"5\"},{\"name\":\"tcpGeoLoc\",\"path\":\"\/\",\"value\":\"40.7808|-74.0651\"},{\"name\":\"tcpCountryDetail\",\"path\":\"\/\",\"value\":\"US\"},{\"name\":\"check\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"LandingBrand\",\"path\":\"\/\",\"value\":\"tcp\"},{\"name\":\"WC_ACTIVEPOINTER\",\"path\":\"\/\",\"value\":\"{WC_ACTIVEPOINTER}\"},{\"name\":\"s_ecid\",\"path\":\"\/\",\"value\":\"MCMID%7C34000030970867548164616407686451354871\"},{\"name\":\"BVBRANDID\",\"path\":\"\/\",\"value\":\"1b40b68f-ac30-4f84-9e66-86785a87d780\"},{\"name\":\"AMCVS_9A0A1C8B5329646E0A490D4D%40AdobeOrg\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"ls-brand\",\"path\":\"\/\",\"value\":\"{p_Brand}\"},{\"name\":\"_caid\",\"path\":\"\/\",\"value\":\"82f71d1a-68f7-47b2-ae1a-27a5b8abcca9\"},{\"name\":\"_fbp\",\"path\":\"\/\",\"value\":\"fb.1.1583249826069.385047578\"},{\"name\":\"unbxd.userId\",\"path\":\"\/\",\"value\":\"uid-1583249826115-98387\"},{\"name\":\"IR_gbd\",\"path\":\"\/\",\"value\":\"{domain}\"},{\"name\":\"QuantumMetricUserID\",\"path\":\"\/\",\"value\":\"e742f8e36551b98fc02ee9caa9336015\"},{\"name\":\"ipe_s\",\"path\":\"\/\",\"value\":\"33412062-9c8b-174a-b444-46689250ed91\"},{\"name\":\"QuantumMetricSessionID\",\"path\":\"\/\",\"value\":\"4716617c454f1dc7e75302c99e885581\"},{\"name\":\"token\",\"path\":\"\/\",\"value\":\"{token}\"},{\"name\":\"BVBRANDSID\",\"path\":\"\/\",\"value\":\"de06dd83-e927-40a3-b052-733033e616ac\"},{\"name\":\"AMCV_9A0A1C8B5329646E0A490D4D%40AdobeOrg\",\"path\":\"\/\",\"value\":\"-1712354808%7CMCIDTS%7C18325%7CMCMID%7C34000030970867548164616407686451354871%7CMCAID%7CNONE%7CMCOPTOUT-1583343396s%7CNONE%7CMCAAMLH-1583940996%7C7%7CMCAAMB-1583940996%7Cj8Odv6LonN4r3an7LhD3WZrU1bUpAkFkkiY1ncBR96t2PTI%7CvVersion%7C4.3.0\"},{\"name\":\"_cavisit\",\"path\":\"\/\",\"value\":\"170a631187e|\"},{\"name\":\"unbxd.visit\",\"path\":\"\/\",\"value\":\"repeat\"},{\"name\":\"unbxd.visitId\",\"path\":\"\/\",\"value\":\"visitId-1583336200656-37676\"},{\"name\":\"ipe.30858.pageViewedDay\",\"path\":\"\/\",\"value\":\"64\"},{\"name\":\"tcp_email_signup_modal_persistent\",\"path\":\"\/\",\"value\":\"{tcp_email_signup_modal_persistent} GMT\"},{\"name\":\"acctId\",\"path\":\"\/\",\"value\":\"{acctId}\"},{\"name\":\"orderItemsQtyList\",\"path\":\"\/\",\"value\":\"1,1,1,1,1,1,1,1,1\"},{\"name\":\"acctStatus\",\"path\":\"\/\",\"value\":\"active\"},{\"name\":\"tier\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"showLoyaltyWebsiteInd\",\"path\":\"\/\",\"value\":\"1\"},{\"name\":\"myPlaceAcctNbr\",\"path\":\"\/\",\"value\":\"\\\"\\\"\"},{\"name\":\"sflItemsCount_US\",\"path\":\"\/\",\"value\":\"0\"},{\"name\":\"sflItemsCount_CA\",\"path\":\"\/\",\"value\":\"0\"},{\"name\":\"WC_SESSION_ESTABLISHED\",\"path\":\"\/\",\"value\":\"true\"},{\"name\":\"IR_3971\",\"path\":\"\/\",\"value\":\"1583337381710%7C0%7C1583336200789%7C%7C\"},{\"name\":\"ipe.30858.pageViewedCount\",\"path\":\"\/\",\"value\":\"2\"},{\"name\":\"ipe_30858_fov\",\"path\":\"\/\",\"value\":\"{\\\"numberOfVisits\\\":1,\\\"sessionId\\\":\\\"33412062-9c8b-174a-b444-46689250ed91\\\",\\\"expiry\\\":\\\"2020-06-02T14:37:08.038Z\\\",\\\"lastVisit\\\":\\\"2020-03-04T15:56:23.452Z\\\"}\"},{\"name\":\"pv\",\"path\":\"\/\",\"value\":\"15\"},{\"name\":\"mbox\",\"path\":\"\/\",\"value\":\"PC#e64fff93c950415a884884a06928e20e.17_0#1646494624|session#e66b66bc9b134196b219a245a5234f08#1583339255\"},{\"name\":\"extole_access_token\",\"path\":\"\/\",\"value\":\"E9SRDB8HJCUAHH9H225QQ7LGJQ\"},{\"name\":\"RT\",\"path\":\"\/\",\"value\":\"\\\"z\"},{\"name\":\"s_sess\",\"path\":\"\/\",\"value\":\"%20s_cm%3DTyped%252FBookmarkedTyped%252FBookmarkedundefined%3B%20v26%3Dnon-internal%2520search%3B%20s_cc%3Dtrue%3B%20s_cmdl%3D1%3B\"},{\"name\":\"s_pers\",\"path\":\"\/\",\"value\":\"%20v14%3D1583337401821-Repeat%7C1585929401821%3B%20v28%3Dgl%253Abrowse%253Agirl%253Ajeans%7C1583339201825%3B\"},{\"name\":\"s_sq\",\"path\":\"\/\",\"value\":\"tcpglobalprod%3D%2526c.%2526a.%2526activitymap.%2526page%253Dgl%25253Abrowse%25253Agirl%25253Ajeans%2526link%253DADD%252520TO%252520BAG%2526region%253Dtcp%2526pageIDType%253D1%2526.activitymap%2526.a%2526.c%2526pid%253Dgl%25253Abrowse%25253Agirl%25253Ajeans%2526pidt%253D1%2526oid%253Dfunctionbr%252528%252529%25257B%25257D%2526oidt%253D2%2526ot%253DSUBMIT\"},{\"name\":\"cartItemsCount\",\"path\":\"\/\",\"value\":\"{cartCount}\"},{\"name\":\"WC_PERSISTENT\",\"path\":\"\/\",\"value\":\"{WC_PERSISTENT}\"}]}",
				LAST);
		}
		
		if(atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction (lr_eval_string("{mainTransaction}_navigateXHR"), LR_FAIL) ;
		else
			lr_end_sub_transaction (lr_eval_string("{mainTransaction}_navigateXHR"), LR_AUTO) ;

//		lr_save_string("true", "navigateXHR");
	}
	
	return 0;
}


int getBOPISInvetoryDetails()
{
/*	char *strLocIdString ;	
	int firstPass = 0;

	if (strcmp(lr_eval_string("{mainTransaction}"), "T05_Add To Cart") == 0 )
		lr_start_transaction (lr_eval_string("{mainTransaction}_getBOPISInvetoryDetails") ) ; //added 06212018
	else
		lr_start_sub_transaction (lr_eval_string("{mainTransaction}_getBOPISInvetoryDetails"), lr_eval_string("{mainTransaction}") ) ; //added 06212018
	
	addHeader();
	web_add_header("Content-Type", "application/json");
	web_add_header("Accept", "application/json");
	web_reg_find("TEXT/IC=availabilityDetail", "SaveCount=apiCheck", LAST);
	
	web_custom_request("getBOPISInvetoryDetails",
		"URL=https://{api_host}/v2/vendor/getBOPISInvetoryDetails",
		"Method=POST",
		"Resource=0",
		"Mode=HTML",
//		"Body={body}",
		"Body={getBOPISInvetoryDetails}",
//						"Body={\"availabilityRequest\":{\"viewName\":\"US BOPIS\",\"availabilityCriteria\":{\"facilityNames\":{\"facilityName\":[\"0769\"]},\"itemNames\":{\"itemName\":[\"2113295003\"]},\"attributeMapping\":[{\"variantNo\":\"2113295003\",\"itemPartNumber\":\"00191755457895\"}]}}}",
		LAST);
	
	if (strcmp(lr_eval_string("{mainTransaction}"), "T05_Add To Cart") == 0 ) {
		if(atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_transaction (lr_eval_string("{mainTransaction}_getBOPISInvetoryDetails"), LR_FAIL) ;
		else
			lr_end_transaction (lr_eval_string("{mainTransaction}_getBOPISInvetoryDetails"), LR_AUTO) ;
	} else {
		if(atoi(lr_eval_string("{apiCheck}")) == 0 )
			lr_end_sub_transaction (lr_eval_string("{mainTransaction}_getBOPISInvetoryDetails"), LR_FAIL) ;
		else
			lr_end_sub_transaction (lr_eval_string("{mainTransaction}_getBOPISInvetoryDetails"), LR_AUTO) ;
	}
*/
	return 0;
}

void  callGetOffers()
{
	int i;
//	lr_param_sprintf("body", "%s", lr_eval_string("{\"personalizedOffersRequest\":{\"externalTransactionId\":{orderId},\"scenario\":\"1006\",\"order\":{\"store\":{\"location\":\"{p_locationId}\"},\"customer\":{\"customerLookupType\":\"1003\",\"customerLookupId\":\"{userEmail}\"},\"coupon\":[],\"lineItem\":["));
	lr_param_sprintf("body", "%s", lr_eval_string("{\"personalizedOffersRequest\":{\"externalTransactionId\":{orderId},\"scenario\":\"1006\",\"order\":{\"isElectiveBonus\":\"0\",\"currencyCode\":\"USD\",\"store\":{\"location\":\"{p_locationId}\"},\"customer\":{\"customerLookupType\":\"1003\",\"customerLookupId\":\"{userEmail}\"},\"coupon\":[],\"lineItem\":["));

	for (i=1; i<=atoi(lr_eval_string("{offersSku_count}")); i++)
	{
		if (strcmp(lr_paramarr_idx("offersSku",i), "") != 0) 
		{
			lr_save_string(lr_paramarr_idx("offersPrice",i), "p_offersPrice");
			lr_save_string(lr_paramarr_idx("offersQty",i), "p_offersQty");
			lr_save_string(lr_paramarr_idx("offersSku",i), "p_offersSku");
			
			lr_save_string(lr_paramarr_idx("orderItemId",i), "p_orderItemId");
			lr_save_string(lr_paramarr_idx("itemUnitPrice",i), "p_itemUnitPrice");

			if ( atoi(lr_eval_string("{offersSku_count}")) == 1) {
//				lr_param_sprintf("body", "%s%s", lr_eval_string("{body}"), lr_eval_string("{\"price\":{p_offersPrice},\"transactionType\":\"PURCHASE\",\"quantity\":{p_offersQty},\"product\":{\"skuNumber\":\"{p_offersSku}\"}}") );
				lr_param_sprintf("body", "%s%s", lr_eval_string("{body}"), lr_eval_string("{\"price\":{p_offersPrice},\"orderItemId\":{p_orderItemId},\"itemUnitPrice\":{p_itemUnitPrice},\"transactionType\":\"PURCHASE\",\"quantity\":{p_offersQty},\"product\":{\"skuNumber\":\"{p_offersSku}\"}}") ); 
			}
			else {
//				lr_param_sprintf("body", "%s%s", lr_eval_string("{body}"), lr_eval_string("{\"price\":{p_offersPrice},\"transactionType\":\"PURCHASE\",\"quantity\":{p_offersQty},\"product\":{\"skuNumber\":\"{p_offersSku}\"}}") );
				lr_param_sprintf("body", "%s%s", lr_eval_string("{body}"), lr_eval_string("{\"price\":{p_offersPrice},\"orderItemId\":{p_orderItemId},\"itemUnitPrice\":{p_itemUnitPrice},\"transactionType\":\"PURCHASE\",\"quantity\":{p_offersQty},\"product\":{\"skuNumber\":\"{p_offersSku}\"}}") ); 
				
				if ( i < atoi(lr_eval_string("{offersSku_count}"))) { 
					lr_param_sprintf("body", "%s%s", lr_eval_string("{body}"), ","); 
				}
			}
		}
	}
	
//	lr_param_sprintf("body", "%s%s", lr_eval_string("{body}"), "]}}}"); 
   	if ( strcmp(lr_eval_string("{userType}"), "VEN") == 0 )
   		lr_param_sprintf("body", "%s%s", lr_eval_string("{body}"), lr_eval_string("]},\"paymentsList\":[{\"authorizedAmount\":{authorizedAmount},\"paymentMethod\":\"VENMO\"}]}}") );
   	else
   		lr_param_sprintf("body", "%s%s", lr_eval_string("{body}"), lr_eval_string("]},\"paymentsList\":[{\"authorizedAmount\":{authorizedAmount},\"paymentMethod\":\"VISA\"}]}}") );
//	return 0;
}

/*
{"personalizedOffersRequest":{"externalTransactionId":251625284,"scenario":"1006","order":{"store":{"location":"0180"},"customer":{"customerLookupType":"1003","customerLookupId":"manny@gmail.com"},"coupon":[],"lineItem":[{"price":12.71,"transactionType":"PURCHASE","quantity":1,"product":{"skuNumber":"2113298010"}}]}}}

{"personalizedOffersRequest":{"externalTransactionId":251629913,"scenario":"1006","order":{"store":{"location":"0180"},"customer":{"customerLookupType":"1003","customerLookupId":"bit442@mair.com"},"coupon":[],"lineItem":[{"price":14.96,"transactionType":"PURCHASE","quantity":1,"product":{"skuNumber":"1125063031"}}]}}}
*/


writeOrderIdToFile()
{
/*
	char *ip;
    char fullpath[1024], * filename1 = "\\c$\\Temp\\OrderId.dat";
	long file1;
//	char * filename1 = "\\\\10.56.29.36\\E$\\Performance\\Scripts\\2016_R1\\03_PERF\\Datafiles\\TestData\\searchStrNoResult.dat";
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath, ip);
	strcat(fullpath, filename1);


    if ((file1 = fopen(fullpath, "a+")) == NULL) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }

	lr_start_transaction("Writing Log");
	fprintf(file1, "%s\n", lr_eval_string("{orderId}, {userEmail}"));
	lr_end_transaction("Writing Log", LR_AUTO);

    fclose(file1);
*/	return 0;
}


writeToFile()
{
/*	char *ip;
    char fullpath[1024];
//    char fullpath[1024], * filename1; // = "\\e$\\Performance\\LogOutput\\ErrorLog.dat";
	long file1;
	char * filename1 = "\\\\10.56.29.36\\e$\\Performance\\LogOutput\\ErrorLog.dat";
//    strcpy(fullpath, "\\\\" );
    strcpy(fullpath, filename1 );
	ip = lr_get_host_name( );
    strcat(fullpath, ip);
	strcat(fullpath, filename1);
*/

	char *ip;
//    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\ErrorLog.dat"; //1000vu
//    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LoyaltyDeleteCoupon1_5000vu.dat";
//    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LoyaltyScenario1_ErrorLog_300vu.dat";
//    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LoyaltyDeleteCoupon2_5000vu.dat";
//    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LoyaltyScenario2_ErrorLog_300vu.dat";
//    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LoyaltyDeleteCoupon3_5000vu.dat";
//    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LoyaltyScenario3_ErrorLog_300vu.dat";
//    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LoyaltyScenario1_v1_ErrorLog_300vu.dat";
//    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LoyaltyScenario1_v3_ErrorLog_300vu.dat";
//    char fullpath[1024], * filename1 = "\\\\10.56.29.36\\e$\\Performance\\LogOutput\\CustomErrorMessages.dat";
    char fullpath[1024], * filename1 = "\\c$\\Temp\\CustomErrorMessages.dat";
	long file1;
//	char * filename1 = "\\\\10.56.29.36\\E$\\Performance\\Scripts\\2016_R1\\03_PERF\\Datafiles\\TestData\\searchStrNoResult.dat";
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath, ip);
	strcat(fullpath, filename1);


    if ((file1 = fopen(fullpath, "a+")) == NULL) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }

	lr_start_transaction("Writing Log");
//	fprintf(file1, "%s\n", lr_eval_string("{userEmail},{errorCode},{errorMessage},{orderId}"));
	fprintf(file1, "%s\n", lr_eval_string("{TIMESTAMP}{errorMessageToFile}"));
	lr_end_transaction("Writing Log", LR_AUTO);

    fclose(file1);
	return 0;
}


writeToFileMelvin()
{
/*	char *ip;
    char fullpath[1024];
//    char fullpath[1024], * filename1; // = "\\e$\\Performance\\LogOutput\\ErrorLog.dat";
	long file1;
	char * filename1 = "\\\\10.56.29.36\\e$\\Performance\\LogOutput\\ErrorLog.dat";
//    strcpy(fullpath, "\\\\" );
    strcpy(fullpath, filename1 );
	ip = lr_get_host_name( );
    strcat(fullpath, ip);
	strcat(fullpath, filename1);
*/

	char *ip;
//    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\ErrorLog.dat"; //1000vu
//    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LoyaltyDeleteCoupon1_5000vu.dat";
//    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LoyaltyScenario1_ErrorLog_300vu.dat";
//    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LoyaltyDeleteCoupon2_5000vu.dat";
//    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LoyaltyScenario2_ErrorLog_300vu.dat";
//    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LoyaltyDeleteCoupon3_5000vu.dat";
//    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LoyaltyScenario3_ErrorLog_300vu.dat";
//    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\LoyaltyScenario1_v1_ErrorLog_300vu.dat";
    char fullpath[1024], * filename1 = "\\e$\\Performance\\LogOutput\\Melvin_v3_ErrorLog_couponerror_upd.dat";
	long file1;
//	char * filename1 = "\\\\10.56.29.36\\e$\\Performance\\Scripts\\2016_R1\\03_PERF\\Datafiles\\TestData\\searchStrNoResult.dat";
    strcpy(fullpath, "\\\\" );
	ip = lr_get_host_name( );
    strcat(fullpath, ip);
	strcat(fullpath, filename1);


    if ((file1 = fopen(fullpath, "a+")) == NULL) {
        lr_output_message ("Unable to open %s", fullpath);
        return -1;
    }

	lr_start_transaction("Writing Log Melvin");
//	fprintf(file1, "%s\n", lr_eval_string("{userEmail},{errorCode},{errorMessage},{orderId}"));
	fprintf(file1, "%s\n", lr_eval_string("{TIMESTAMP},{userEmail},{orderId},{errorMessageToFile}"));
	lr_end_transaction("Writing Log Melvin", LR_AUTO);

    fclose(file1);
	return 0;
}

void buildEddPersistNodeString(){
	//To Build sample String
	//'{"orderId":335625906,"orderItems":[{"edd":"2020-09-21","orderItemId":"1476483826","quantity":1,"node":"SEDC-ECOM","nodeProcessingTime":"2020-09-13","transitTime":4},{"edd":"2020-09-21","orderItemId":"1476512496","quantity":1,"node":"SEDC-ECOM","nodeProcessingTime":"2020-09-13","transitTime":4},{"edd":"2020-09-21","orderItemId":"1476512495","quantity":4,"node":"SEDC-ECOM","nodeProcessingTime":"2020-09-13","transitTime":4},{"edd":"2020-09-21","orderItemId":"1476512493","quantity":2,"node":"SEDC-ECOM","nodeProcessingTime":"2020-09-13","transitTime":4},{"edd":"2020-09-21","orderItemId":"1476512492","quantity":2,"node":"SEDC-ECOM","nodeProcessingTime":"2020-09-13","transitTime":4},{"edd":"2020-09-21","orderItemId":"1476532048","quantity":3,"node":"SEDC-ECOM","nodeProcessingTime":"2020-09-13","transitTime":4}],"orderEdd":"2020-09-21","showEDD":"true"}'
	
	int view_cartItemsCount=0, rc=1;
	view_cartItemsCount=atoi(lr_eval_string("{orderItemIds_count}"));
	lr_save_string("", "FinalbuildEddPersistString" );
	if (view_cartItemsCount > 0) {
		
		for (rc=1;rc<=view_cartItemsCount;rc++) {
			//\{\"edd\":\"2020-09-21\",\"orderItemId\":\"1476483826\",\"quantity\":1,\"node\":\"SEDC-ECOM\",\"nodeProcessingTime\":\"2020-09-13\",\"transitTime\":4\}			
			//\{\"edd\":\"%s\",\"orderItemId\":\"%s\",\"quantity\":%s,\"node\":\"%s\",\"nodeProcessingTime\":\"%s\",\"transitTime\":%s\}	

//			lr_param_sprintf("buildEddNodePersistString","\{\"edd\":\"%s\",\"orderItemId\":\"%s\",\"quantity\":%s,\"node\":\"%s\",\"nodeProcessingTime\":\"%s\",\"transitTime\":%s\}",lr_paramarr_idx("eddedd", rc),lr_paramarr_idx("eddorderItemId", rc),lr_paramarr_idx("eddskuQty", rc),lr_paramarr_idx("eddNodeId", rc),lr_paramarr_idx("eddNodeProcessingTime", rc),lr_paramarr_idx("eddtransitTime", rc));

			lr_param_sprintf("buildEddNodePersistString","\{\"edd\":\"%s\",\"orderItemId\":\"%s\",\"quantity\":%s,\"node\":\"%s\",\"nodeProcessingTime\":\"%s\",\"transitTime\":%s\}",
				                 "2020-10-02",
				                 lr_paramarr_idx("orderItemIds", rc), 
				                 lr_paramarr_idx("orderItemIdsQty", rc),
				                 "SEDC-ECOM",
				                 "2020-09-22",
				                 "4");
			if (rc < view_cartItemsCount)	//If additional json elements, add a comma
				lr_save_string(lr_eval_string("{FinalbuildEddPersistString}{buildEddNodePersistString},"), "FinalbuildEddPersistString");
			else
				lr_save_string(lr_eval_string("{FinalbuildEddPersistString}{buildEddNodePersistString}"), "FinalbuildEddPersistString");
			
		}
		
		lr_param_sprintf("FinalEddPersistString","\{\"orderId\":%s,\"orderItems\":[%s],\"orderEdd\":\"%s\",\"showEDD\":\"true\"\}",lr_eval_string("{orderId}"),lr_eval_string("{FinalbuildEddPersistString}"),lr_eval_string("{eddfinalEdd}"));
	
		lr_save_string("TRUE","EDDPERSISTSTRING");
		
	} else {
		lr_save_string("FALSE","EDDPERSISTSTRING");
	}

}


void sendEddPersistNodeString()
{
	if( atoi(lr_eval_string("{RANDOM_PERCENT}")) <= RATIO_EDD_NODE ) {
	
//{"orderId":{orderId},"orderItems":[{"edd":"2020-09-21","orderItemId":"1476483826","quantity":1,"node":"SEDC-ECOM","nodeProcessingTime":"2020-09-13","transitTime":4},{"edd":"2020-09-21","orderItemId":"1476512496","quantity":1,"node":"SEDC-ECOM","nodeProcessingTime":"2020-09-13","transitTime":4},{"edd":"2020-09-21","orderItemId":"1476512495","quantity":4,"node":"SEDC-ECOM","nodeProcessingTime":"2020-09-13","transitTime":4},{"edd":"2020-09-21","orderItemId":"1476512493","quantity":2,"node":"SEDC-ECOM","nodeProcessingTime":"2020-09-13","transitTime":4},{"edd":"2020-09-21","orderItemId":"1476512492","quantity":2,"node":"SEDC-ECOM","nodeProcessingTime":"2020-09-13","transitTime":4},{"edd":"2020-09-21","orderItemId":"1476532048","quantity":3,"node":"SEDC-ECOM","nodeProcessingTime":"2020-09-13","transitTime":4}],"orderEdd":"2020-09-21","showEDD":"true"}
	buildEddPersistNodeString();
	if (strcmp(lr_eval_string("{EDDPERSISTSTRING}"),"TRUE")==0){
		addHeader();
		registerErrorCodeCheck();
		
		lr_start_sub_transaction(lr_eval_string("{mainTransaction}_persistEddNode"), lr_eval_string("{mainTransaction}") );
		web_reg_save_param_json("ParamName=persistEddNodeStatus", "QueryString=$.status", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);	
	
		web_custom_request("CallToEDD",
			"URL=https://{api_host}/v2/checkout/persistEddNode", 
			"Method=POST",
			"Resource=0",
			"RecContentType=application/json",
			"Mode=HTTP",
			"EncType=application/json",
//			"Body=\{\"orderId\":{orderId},\"orderItems\":[\{\"edd\":\"2020-09-21\",\"orderItemId\":\"1476483826\",\"quantity\":1,\"node\":\"SEDC-ECOM\",\"nodeProcessingTime\":\"2020-09-13\",\"transitTime\":4\},\{\"edd\":\"2020-09-21\",\"orderItemId\":\"1476512496\",\"quantity\":1,\"node\":\"SEDC-ECOM\",\"nodeProcessingTime\":\"2020-09-13\",\"transitTime\":4\},\{\"edd\":\"2020-09-21\",\"orderItemId\":\"1476512495\",\"quantity\":4,\"node\":\"SEDC-ECOM\",\"nodeProcessingTime\":\"2020-09-13\",\"transitTime\":4\},\{\"edd\":\"2020-09-21\",\"orderItemId\":\"1476512493\",\"quantity\":2,\"node\":\"SEDC-ECOM\",\"nodeProcessingTime\":\"2020-09-13\",\"transitTime\":4\},\{\"edd\":\"2020-09-21\",\"orderItemId\":\"1476512492\",\"quantity\":2,\"node\":\"SEDC-ECOM\",\"nodeProcessingTime\":\"2020-09-13\",\"transitTime\":4\},\{\"edd\":\"2020-09-21\",\"orderItemId\":\"1476532048\",\"quantity\":3,\"node\":\"SEDC-ECOM\",\"nodeProcessingTime\":\"2020-09-13\",\"transitTime\":4\}],\"orderEdd\":\"2020-09-21\",\"showEDD\":\"true\"\}",
			"Body={FinalEddPersistString}",
			LAST);
		
		if (strcmp(lr_eval_string("{persistEddNodeStatus}"), "success")==0) {
			lr_end_transaction(lr_eval_string("{mainTransaction}_persistEddNode"), LR_AUTO);
		
		}
		else{
			lr_end_transaction(lr_eval_string("{mainTransaction}_persistEddNode"), LR_FAIL);
		}

		//Reset EDDPERSISTSTRING = FALSE;
		lr_save_string("FALSE","EDDPERSISTSTRING");
	}
	}
}

int resendEmailConfirmation()
{
	if (atoi(lr_eval_string("{orderId}")) > 0) {
		lr_think_time ( FORM_TT ) ;
		
		lr_save_string("T17_resendEmailConfirmation", "mainTransaction");
	
		lr_start_transaction ( "T17_resendEmailConfirmation" ) ;
	
		web_reg_save_param_json("ParamName=response", "QueryString=$..response", "SelectAll=No", "SEARCH_FILTERS", "Scope=Body", "Notfound=warning", LAST);
		web_add_header( "Accept", "application/json" );
		web_add_header( "Content-Type", "application/json" );
		
		addHeader();
		web_custom_request("resendEmailConfirmation",
		"URL=https://{api_host}/v2/wallet/resendEmailConfirmation", 
		"Method=POST",
		"Resource=0",
		"Body={\"emailld\":\"{userEmail}\",\"orderId\":\"{orderId}\",\"fromPage\":\"orderHistory\"}",
		LAST);
	
		if ( strcmp( lr_eval_string("{response}") ,"success") == 0 ) // && strcmp(lr_eval_string("{x_parentOrderId}"), lr_eval_string("{orderId}"))==0 &&  strcmp(lr_eval_string("{x_parentOrderStatus}"),"M")==0) //if no error code and message
		{
			lr_end_sub_transaction("T17_resendEmailConfirmation", LR_AUTO);
		}
		else {
			lr_error_message( lr_eval_string("T17_resendEmailConfirmation - Failed with API Error Code: \"{errorCode}\", Error Message: \"{errorMessage}\", OrderId:{orderId}") ) ;
			lr_end_transaction ("T17_resendEmailConfirmation", LR_FAIL ) ;
			return LR_FAIL;
		}
	}
}